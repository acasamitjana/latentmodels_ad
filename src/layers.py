import torch
import torch.nn as nn
import torch.nn.functional as F



class Mean_layer(nn.Module):


    def forward(self, x_list, x_list_label = None):

        out_mean = torch.zeros_like(x_list[0])
        if x_list_label is None:
            for x in x_list:
                out_mean += x
            num_mod = len(x_list)

        else:
            num_mod = torch.zeros_like(x_list_label[0])
            for it_x_label, x_label in enumerate(x_list_label):
                out_mean += x_label * x_list[it_x_label]
                num_mod += x_label

        return out_mean/num_mod


class Var_layer(nn.Module):

    def forward(self, x_list, x_list_label = None):

        out_mean = torch.zeros_like(x_list[0])
        out_power = torch.zeros_like(x_list[0])

        if x_list_label is None:
            for x in x_list:
                out_mean += x
                out_power += x ** 2
            num_mod = len(x_list)

        else:
            num_mod = torch.zeros_like(x_list_label[0])
            for it_x_label, x_label in enumerate(x_list_label):
                out_mean += x_label * x_list[it_x_label]
                out_power += x_label * x_list[it_x_label]**2
                num_mod += x_label

        out_var =  out_power/num_mod - (out_mean/num_mod)**2
        return out_var