import torch
import torch.nn as nn
import torch.nn.functional as F


class AutoencoderCNN(nn.Module):
    def __init__(self, input_size):

        self.input_size = input_size
        self.fc1_input_size = int((((input_size[0]-4)/2-4)/2) *  (((input_size[1]-4)/2-4)/2) * 20)

        super(AutoencoderCNN, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(1, 10, kernel_size=5),
            nn.MaxPool2d(2, 2),
            nn.ReLU(),
            nn.Conv2d(10, 20, kernel_size=5),
            nn.MaxPool2d(2, 2),
            nn.ReLU()
        )

        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(20, 10, kernel_size = 5, stride = 2,  padding=0, output_padding = 1),
            nn.ReLU(),
            nn.ConvTranspose2d(10, 1, kernel_size=5, stride=2, padding=0, output_padding = 1),
        )

        self.classification = nn.Sequential(
            nn.Linear(self.fc1_input_size, 50),
            nn.Linear(50, 10)
        )


    def forward(self, x):

        hidden_layer = self.encoder(x)
        output_reconstruction = self.decoder(hidden_layer)
        hidden_layer = hidden_layer.view(-1, self.fc1_input_size)
        output_prediction = self.classification(hidden_layer)

        return output_reconstruction, F.log_softmax(output_prediction,dim=1)


class Autoencoder_supervised(nn.Module):

    def __init__(self, input_size, hidden_layer_dims):

        self.input_size = input_size
        self.hidden_dims = hidden_layer_dims

        super().__init__()

        layer_sizes_encoder = [input_size] + hidden_layer_dims
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_encoder[:-1], layer_sizes_encoder[1:])]
        self.encoder_layers = ListModule(*lin_layers)

        layer_sizes_decoder = layer_sizes_encoder[::-1]
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_decoder[:-1], layer_sizes_decoder[1:])]
        self.decoder_layers = ListModule(*lin_layers)


    def forward(self, x):

        x_latent = x
        for it_layer in range(len(self.encoder_layers)-1):
            x_latent = self.lin_layers[it_layer](x_latent)
            x_latent = F.relu(x_latent)
        x_latent = self.encoder_layers[len(self.encoder_layers)-1](x_latent)

        x_rec = x_latent
        for it_layer in range(len(self.decoder_layers) - 1):
            x_rec = self.decoder_layers[it_layer](x_rec)
            x_rec = F.relu(x_rec)
        x_rec = self.decoder_layers[len(self.decoder_layers)-1](x_rec)

        hidden_layer = self.encoder(x_rec)
        output_reconstruction = self.decoder(hidden_layer)
        hidden_layer = hidden_layer.view(-1, self.fc1_input_size)
        output_prediction = self.classification(hidden_layer)

        return output_reconstruction, F.log_softmax(output_prediction,dim=1)


class VAE(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims, activation = None):
        super(VAE, self).__init__()

        self.layer_sizes = [input_dim] + hidden_layer_dims
        self.activation = activation

        layer_sizes_encoder = self.layer_sizes
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_encoder[:-2], layer_sizes_encoder[1:-1])]
        self.encoder_layers = ListModule(*lin_layers)
        self.mean_layer = nn.Linear(self.layer_sizes[-2], self.layer_sizes[-1])
        self.std_layer = nn.Linear(self.layer_sizes[-2], self.layer_sizes[-1])


        layer_sizes_decoder = layer_sizes_encoder[::-1]
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_decoder[:-1], layer_sizes_decoder[1:])]
        self.decoder_layers = ListModule(*lin_layers)


    def encode(self, x):
        h1 = x
        for layer in self.encoder_layers:
            h1 = F.relu(layer(h1))

        return self.mean_layer(h1), self.std_layer(h1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(mu)
        return mu + eps*std

    def decode(self, z):
        x_rec = z
        for it_layer, layer in enumerate(self.decoder_layers):
            x_rec = layer(x_rec)
            if it_layer < len(self.decoder_layers) -1:
                x_rec = F.relu(x_rec)
            elif self.activation == 'sigmoid':
                x_rec = torch.sigmoid(x_rec)

        return x_rec

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar


class VAE_Linear(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims):
        super().__init__()

        self.layer_sizes = [input_dim] + hidden_layer_dims

        layer_sizes_encoder = self.layer_sizes
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_encoder[:-2], layer_sizes_encoder[1:-1])]
        self.encoder_layers = ListModule(*lin_layers)
        self.mean_layer = nn.Linear(self.layer_sizes[-2], self.layer_sizes[-1])
        self.std_layer = nn.Linear(self.layer_sizes[-2], self.layer_sizes[-1])


        layer_sizes_decoder = layer_sizes_encoder[::-1]
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_decoder[:-1], layer_sizes_decoder[1:])]
        self.decoder_layers = ListModule(*lin_layers)


    def encode(self, x):
        h1 = x
        for layer in self.encoder_layers:
            print('a')
            h1 = layer(h1)

        return self.mean_layer(h1), self.std_layer(h1)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5*logvar)
        eps = torch.randn_like(std)
        return mu + eps*std

    def decode(self, z):
        x_rec = z
        for it_layer, layer in enumerate(self.decoder_layers):
            x_rec = layer(x_rec)

        return x_rec

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar


class Autoencoder(nn.Module):

    def __init__(self, encoder, decoder):
        super().__init__()

        self.encoder = encoder
        self.decoder = decoder

    def encode(self, x):
        return self.encoder(x)

    def decode(self, x):
        return self.decoder(x)

    def forward(self, x):
        z = self.encoder(x)
        return self.decoder(z), z


class Autoencoder_disentangle(nn.Module):

    def __init__(self, encoder, decoder):
        super().__init__()

        self.encoder = encoder
        self.decoder = decoder

    def encode(self, x):
        return self.encoder(x)

    def decode(self, x, y):
        t = torch.cat((x, y), 1)
        return self.decoder(t)

    def forward(self, x, y):
        z = self.encoder(x)
        t = torch.cat((z, y), 1)
        return self.decoder(t), z

class AAE(nn.Module):


    def __init__(self, input_dim, hidden_layer_dims):
        super().__init__()

        self.input_size = input_dim
        self.hidden_dims = hidden_layer_dims

        layer_sizes_encoder = [input_dim] + hidden_layer_dims
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_encoder[:-1], layer_sizes_encoder[1:])]
        self.encoder_layers = ListModule(*lin_layers)

        layer_sizes_decoder = layer_sizes_encoder[::-1]
        lin_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes_decoder[:-1], layer_sizes_decoder[1:])]
        self.decoder_layers = ListModule(*lin_layers)

    def encode(self, x):
        h1 = x
        for it_layer, layer in enumerate(self.encoder_layers):
            h1 = layer(h1)
            if it_layer < len(self.encoder_layers) -1:
                h1 = F.relu(h1)

        return h1

    def decode(self, z):
        x_rec = z
        for it_layer, layer in enumerate(self.decoder_layers):
            x_rec = layer(x_rec)
            if it_layer < len(self.decoder_layers) -1:
                x_rec = F.relu(x_rec)
            else:
                x_rec = torch.sigmoid(x_rec)

        return x_rec

    def forward(self, x):
        z = self.encode(x)
        return self.decode(z), z




class Encoder(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims, bias=True):
        super().__init__()

        self.input_size = input_dim
        self.hidden_dims = hidden_layer_dims

        layer_sizes_encoder = [input_dim] + hidden_layer_dims
        lin_layers = [nn.Linear(dim0, dim1, bias=bias) for dim0, dim1 in zip(layer_sizes_encoder[:-1], layer_sizes_encoder[1:])]
        self.encoder_layers = ListModule(*lin_layers)


    def forward(self, x):
        h1 = x
        for it_layer, layer in enumerate(self.encoder_layers):
            h1 = layer(h1)
            if it_layer < len(self.encoder_layers) - 1:
                h1 = F.relu(h1)

        return h1

class Encoder_lineal(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims, bias=True):
        super().__init__()

        self.input_size = input_dim
        self.hidden_dims = hidden_layer_dims

        layer_sizes_encoder = [input_dim] + hidden_layer_dims
        lin_layers = [nn.Linear(dim0, dim1, bias=bias) for dim0, dim1 in zip(layer_sizes_encoder[:-1], layer_sizes_encoder[1:])]
        self.encoder_layers = ListModule(*lin_layers)


    def forward(self, x):
        h1 = x
        for it_layer, layer in enumerate(self.encoder_layers):
            h1 = layer(h1)

        return h1

class Decoder(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims, activation = None, bias=True):
        self.input_size = input_dim
        self.hidden_dims = hidden_layer_dims
        self.activation = activation
        super().__init__()

        layer_sizes_decoder = hidden_layer_dims[::-1] + [input_dim]
        lin_layers = [nn.Linear(dim0, dim1, bias=bias) for dim0, dim1 in zip(layer_sizes_decoder[:-1], layer_sizes_decoder[1:])]
        self.decoder_layers = ListModule(*lin_layers)


    def forward(self, z):
        x_rec = z
        for it_layer, layer in enumerate(self.decoder_layers):
            x_rec = layer(x_rec)
            if it_layer < len(self.decoder_layers) -1:
                x_rec = F.relu(x_rec)
            else:
                if self.activation is None:
                    pass
                elif self.activation == 'sigmoid':
                    x_rec = torch.sigmoid(x_rec)


        return x_rec


class Decoder_lineal(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims, activation = None, bias=True):
        self.input_size = input_dim
        self.hidden_dims = hidden_layer_dims
        self.activation = activation
        super().__init__()

        layer_sizes_decoder = hidden_layer_dims[::-1] + [input_dim]
        lin_layers = [nn.Linear(dim0, dim1, bias=bias) for dim0, dim1 in zip(layer_sizes_decoder[:-1], layer_sizes_decoder[1:])]
        self.decoder_layers = ListModule(*lin_layers)


    def forward(self, z):
        x_rec = z
        for it_layer, layer in enumerate(self.decoder_layers):
            x_rec = layer(x_rec)

        return x_rec

# Discriminator
class Discriminator(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims):
        self.hidden_dims = hidden_layer_dims

        super().__init__()

        layer_sizes = [input_dim] + hidden_layer_dims
        linear_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes[:-1], layer_sizes[1:])]
        self.linear_layers = ListModule(*linear_layers)


    def forward(self, x):
        for it_layer, layer in enumerate(self.linear_layers):
            x = layer(x)#F.dropout(layer(x), p=0.25, training=self.training)
            if it_layer < len(self.linear_layers) -1:
                x = F.relu(x)
            else:
                if self.hidden_dims[-1]>1:
                    x = nn.Softmax(dim=-1)(x)
                else:
                    x = torch.sigmoid(x)

        return x


# Predictor
class Regression_predictor(nn.Module):
    def __init__(self, input_dim, hidden_layer_dims):
        self.hidden_dims = hidden_layer_dims

        super().__init__()

        layer_sizes = [input_dim] + hidden_layer_dims
        linear_layers = [nn.Linear(dim0, dim1) for dim0, dim1 in zip(layer_sizes[:-1], layer_sizes[1:])]
        self.linear_layers = ListModule(*linear_layers)


    def forward(self, x):
        for it_layer, layer in enumerate(self.linear_layers):
            x = layer(x)#F.dropout(layer(x), p=0.25, training=self.training)
            if it_layer < len(self.linear_layers) -1:
                x = F.relu(x)

        return x


class ListModule(nn.Module):
    def __init__(self, *args):
        super(ListModule, self).__init__()
        idx = 0
        for module in args:
            self.add_module(str(idx), module)
            idx += 1

    def __getitem__(self, idx):
        if idx < 0 or idx >= len(self._modules):
            raise IndexError('index {} is out of range'.format(idx))
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __iter__(self):
        return iter(self._modules.values())

    def __len__(self):
        return len(self._modules)

