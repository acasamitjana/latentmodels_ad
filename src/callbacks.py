


class Callbacks(object):

    def on_train_init(self):
        pass

    def on_train_fi(self):
        pass

    def on_epoch_init(self):
        pass

    def on_epoch_fi(self):
        pass

    def on_step_init(self):
        pass

    def on_step_fi(self):
        pass


class History(object):

    def __init__(self):
        self.logs = {}

    def on_train_init(self, keys):
        for k in keys:
            self.logs[k] = []

    def on_step_fi(self,logs_dict):
        for k,v in logs_dict.items():
            self.logs[k].append(v)