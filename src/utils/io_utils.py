import yaml
import csv

ROW_DICT = {
    1:2,2:2,3:2,4:2,5:2,6:2,7:3,8:3,9:3,10:3,11:3,12:3,13:4,14:4,15:4,16:4,17:5,18:5,19:5,20:5,21:5,22:5,23:5,24:5,25:5,
    26:6,27:6,28:6,29:6,30:6,31:6,32:6,33:6,34:6,35:6,36:6,37:7,38:7,39:7,40:7,41:7,42:7
}

class ResultsWriter():
    def __init__(self, filepath = None, attach = True):
        self.filepath = filepath
        if not attach:
            with open(self.filepath, 'w') as txtfile:
                pass

    def write(self, string):
        if self.filepath is None:
            print(string)
        else:
            with open(self.filepath, 'a') as txtfile:
                txtfile.write(string)


class LogWriter():
    def __init__(self, filepath = None, attach = True):
        self.filepath = filepath
        if not attach:
            with open(self.filepath, 'w') as txtfile:
                pass

    def write(self, string):
        if self.filepath is None:
            print(string)
        else:
            with open(self.filepath, 'a') as txtfile:
                txtfile.write(string)


    def write_log(self, dict):
        string = []
        for k,v in dict.items():
            if k == 'Epoch':
                string.append(k + ': {:04d}'.format(v))
            elif k == 'time':
                string.append('time: {:.4f}s'.format(v))
            else:
                string.append(k + ': {:.4f}'.format(v))


        if self.filepath is None:
            print(' '.join(string))
        else:
            with open(self.filepath, 'a') as txtfile:
                txtfile.write(' '.join(string))
                txtfile.write('\n')


class CSVWriter():
    def __init__(self, filepath, header, attach = True):
        self.filepath = filepath
        self.header = header
        if not attach:
            with open(self.filepath, 'w') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=self.header)
                writer.writeheader()

    def write(self, csvrow):
        with open(self.filepath, 'a') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames = self.header)
            writer.writerow(csvrow)

    def writerows(self, csvrow):
        with open(self.filepath, 'a') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames = self.header)
            writer.writerows(csvrow)



class ConfigFile(object):
    """
    Loads the subjects and the configuration of a study given the path to the configuration file for this study
    """

    def __init__(self, configuration_path):
        """
        Initializes a DataLoader with the given configuration file

        Parameters
        ----------
        configuration_path : String
            Path to the YAMP configuration file with the configuration parameters expected for a study.
            See config/exampleConfig.yaml for more information about the format of configuration files.
        """
        # Load the configuration
        with open(configuration_path, 'r') as conf_file:
            conf = yaml.load(conf_file, yaml.FullLoader)
        self._conf = conf

        self.output_dir = conf['data_management']['output_dir']
        self.use_GPU = conf['general_options']['use_GPU']

        self.learning_rate = conf['training']['learning_rate']
        self.batch_size = conf['training']['batch_size']
        self.n_epochs = conf['training']['n_epochs']
        self.log_interval = conf['training']['log_interval']

        self.momentum = conf['optimizer']['momentum']


