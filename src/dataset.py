import torch
from torch.utils.data import Dataset, DataLoader

import numpy as np


class MyDataset(Dataset):
    def __init__(self, data, target, indices = False, transform=None):
        if isinstance(data,list):
            self.data = [torch.from_numpy(d).float() for d in data ]
            self.N = len(self.data[0])
        else:
            self.data = torch.from_numpy(data).float()
            self.N = len(self.data)

        if isinstance(target,list):
            self.target = [torch.from_numpy(t).float() for t in target]
        else:
            self.target = torch.from_numpy(target).float()

        self.transform = transform
        self.indices = indices

    def __getitem__(self, index):
        if isinstance(self.data,list):
            x = [d[index] for d in self.data]
        else:
            x = self.data[index]

        if isinstance(self.target,list):
            y = [t[index] for t in self.target]
        else:
            y = self.target[index]

        if self.transform:
            x = self.transform(x)

        if self.indices:
            return x, y, index
        else:
            return x, y

    def __len__(self):
        return self.N


class MyDataset_disentangle(Dataset):
    def __init__(self, data, target, factor, indices = False, transform=None):
        if isinstance(data,list):
            self.data = [torch.from_numpy(d).float() for d in data ]
            self.N = len(self.data[0])
        else:
            self.data = torch.from_numpy(data).float()
            self.N = len(self.data)

        if isinstance(target,list):
            self.target = [torch.from_numpy(t).float() for t in target]
        else:
            self.target = torch.from_numpy(target).float()

        self.factor = torch.from_numpy(factor).float()
        self.transform = transform
        self.indices = indices

    def __getitem__(self, index):
        if isinstance(self.data,list):
            x = [d[index] for d in self.data]
        else:
            x = self.data[index]

        if isinstance(self.target,list):
            y = [t[index] for t in self.target]
        else:
            y = self.target[index]

        if self.transform:
            x = self.transform(x)

        factor = self.factor[index]
        if self.indices:
            return x, y, factor, index
        else:
            return x, y, factor

    def __len__(self):
        return self.N


class MyDataset_clf(Dataset):
    def __init__(self, data, flag, target, rid_events_list = None, transform=None):
        if isinstance(data,list):
            self.data = [torch.from_numpy(d).float() for d in data ]
            self.N = len(self.data[0])
        else:
            self.data = torch.from_numpy(data).float()
            self.N = len(self.data)

        if isinstance(flag,list):
            self.flag = [torch.from_numpy(t).float() for t in flag]
        else:
            self.flag = torch.from_numpy(flag).float()

        unique_labels = [ul for ul in np.unique(target[0]) if ul >= 0]
        self.target = np.zeros((target[0].shape[0], len(unique_labels)))
        for it_t, t in enumerate(unique_labels):
            self.target[np.where(target[0] == t)[0],it_t] = 1

        self.target = torch.from_numpy(self.target).float()
        self.target_labels = torch.from_numpy(target[1]).float()
        self.transform = transform

    def __getitem__(self, index):
        if isinstance(self.data,list):
            x = [d[index] for d in self.data]
        else:
            x = self.data[index]

        if isinstance(self.flag,list):
            y = [t[index] for t in self.flag]
        else:
            y = self.flag[index]

        if self.transform:
            x = self.transform(x)

        z = [self.target[index], self.target_labels[index]]


        return x, y, z

    def __len__(self):
        return self.N


class MyDataset_structure(Dataset):
    def __init__(self, data, target, sim_matrix, rid_events_list = None, transform=None):
        if isinstance(data,list):
            self.data = [torch.from_numpy(d).float() for d in data ]
            self.N = len(self.data[0])
        else:
            self.data = torch.from_numpy(data).float()
            self.N = len(self.data)

        if isinstance(target,list):
            self.target = [torch.from_numpy(t).float() for t in target]
        else:
            self.target = torch.from_numpy(target).float()

        self.transform = transform
        self.sim_matrix = sim_matrix

    def __getitem__(self, index):

        if isinstance(self.data,list):
            x = [d[index] for d in self.data]
        else:
            x = self.data[index]

        if isinstance(self.target,list):
            y = [t[index] for t in self.target]
        else:
            y = self.target[index]

        if self.transform:
            x = self.transform(x)

        # if not isinstance(index, list) or not isinstance(index, np.ndarray):
        #     index = [index]

        return x, y, index

    def __len__(self):
        return self.N


class MyDataset_mixed_effects(Dataset):
    def __init__(self, data, target, time_covariate, rid = None, indices = False, transform=None):

        if isinstance(data,list):
            self.data = [torch.from_numpy(d).float() for d in data ]
            self.N = len(self.data[0])
        else:
            self.data = torch.from_numpy(data).float()
            self.N = len(self.data)

        if isinstance(target,list):
            self.target = [torch.from_numpy(t).float() for t in target]
        else:
            self.target = torch.from_numpy(target).float()

        self.transform = transform
        self.time_covariate = time_covariate
        self.rid = rid
        self.indices = indices

    def __getitem__(self, index):

        if isinstance(self.data,list):
            x = [d[index] for d in self.data]
        else:
            x = self.data[index]

        if isinstance(self.target,list):
            y = [t[index] for t in self.target]
        else:
            y = self.target[index]

        if self.transform:
            x = self.transform(x)

        rid = [self.rid[i] for i in index]
        time_covariate = self.time_covariate[index]
        if self.indices:
            return x, y, time_covariate, rid, index
        else:
            return x, y, time_covariate, rid

    def __len__(self):
        return self.N

