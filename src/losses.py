import torch
import torch.utils.data
from torch.nn import functional as F


class CustomTrace(torch.autograd.Function):

    def forward(self, input):
        self.isize = input.size()
        return input.new([torch.trace(input)])

    def backward(self, grad_output):
        isize = self.isize
        grad_input = grad_output.new(isize).copy_(torch.eye(*isize))
        grad_input.mul_(grad_output[0])
        return grad_input

def VAE_loss_function(recon_x, x, mu, logvar):
    BCE = F.binary_cross_entropy(recon_x, x.view(-1, 784), reduction='sum')

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return BCE + KLD


def graph_smoothness_loss(x, sim_matrix):
    x1 = x.repeat(x.size()[0], 1, 1)
    x2 = torch.transpose(x1, 0, 1)
    diff2 = ((x1-x2)**2).sum(-1)

    return (0.5 * diff2 * sim_matrix).sum()





