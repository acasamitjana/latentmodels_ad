import numpy as np
import csv
from os.path import join, split
from Data.ukbiobank import get_phenotype_dict

header = ['VARIANT', 'SNP', 'BP', 'CHR', 'EFFECT_ALLELE', 'OTHER_ALLELE', 'BETA', 'PVAL', 'TSTAT', 'SE', 'INFO']

FAILED_PRS = ['5540_0_both_sexes', '20003_1140867490_both_sexes', '24008_raw_both_sexes', '4653_both_sexes']

def read_multiple_trait_file(filepath):
    phenotype_dict = get_phenotype_dict()

    prs_dict = {}
    with open(filepath, 'r') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=',')
        header = csvreader.fieldnames
        for row in csvreader:
            prs_dict[row['Subject']] = [get_float(value) for key, value in row.items() if key not in ['Subject'] + FAILED_PRS]

    prs_description_list = []
    for h in header:
        if h not in ['Subject'] + FAILED_PRS:
            if phenotype_dict[h]['phenotype_description'] == 'NA':
                prs_description_list.append(h)
            else:
                prs_description_list.append(phenotype_dict[h]['phenotype_description'])

    return prs_dict, prs_description_list, [h for h in header[1:] if h not in FAILED_PRS]

def read_prsice(filepath, pvalue):
    prs_dict = {}
    with open(filepath, 'r') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=',')
        if str(pvalue) in csvreader.fieldnames:
            for row in csvreader:
                prs_dict[row['FID']] = get_float(row[str(pvalue)])
        else:
            raise ValueError('Please, specify a valid p-value')

    return prs_dict

def arrange_prsice_scores(filepath, out_filepath = None):
    if out_filepath is None:
        path, filename = split(filepath)
        filename = 'arranged_' + filename
        out_filepath = join(path, filename)

    to_write = []
    with open(filepath, 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=' ')
        for it_row, row in enumerate(csvreader):
            to_write.append([r for r in row if r != ''])

    with open(out_filepath, 'w') as csvfile:
        csvreader = csv.writer(csvfile, delimiter=',')
        csvreader.writerows(to_write)


def get_float(a):
    try:
        return float(a)
    except:
        print(a)
        exit()
        print('[WARNING] While reading floats: None is used for non-float values.')
        return None
        # raise ValueError('[ERROR] While reading floats: No float value')