import csv
import numpy as np
from os.path import join
import pickle
from Data.TADPOLE.config_db import *
from sys import stdout

DX_DICT = {
    'NL': 0,
    'MCI': 1,
    'Dementia': 2,
    'MCI to Dementia': 2,
    'Dementia to MCI': 1,
    'MCI to NL': 1,
    'NL to MCI': 2,
    'NL to Dementia': 2,
    '': None
}

EXCLUDED_FDG_PET_HEADER = ['HIPPR01_BAIPETNMRC_09_12_16', 'CINGPSTR01_BAIPETNMRC_09_12_16', 'LINGUALL01_BAIPETNMRC_09_12_16',
          'LINGUALR01_BAIPETNMRC_09_12_16', 'FRTINFL01_BAIPETNMRC_09_12_16', 'FRTINFR01_BAIPETNMRC_09_12_16',
          'PARISUPL01_BAIPETNMRC_09_12_16', 'PARISUPR01_BAIPETNMRC_09_12_16', 'INSULAL01_BAIPETNMRC_09_12_16',
          'INSULAR01_BAIPETNMRC_09_12_16', 'CINGANTL01_BAIPETNMRC_09_12_16', 'CINGANTR01_BAIPETNMRC_09_12_16',
          'CINGMIDL01_BAIPETNMRC_09_12_16', 'CINGMIDR01_BAIPETNMRC_09_12_16', 'TMPSUPL01_BAIPETNMRC_09_12_16',
          'TMPSUPR01_BAIPETNMRC_09_12_16', 'TMPINFL01_BAIPETNMRC_09_12_16', 'CINGPSTR02_BAIPETNMRC_09_12_16',
          'PARIINFL02_BAIPETNMRC_09_12_16', 'INSULAL02_BAIPETNMRC_09_12_16', 'FRTINFL02_BAIPETNMRC_09_12_16',
          'FRTINFR02_BAIPETNMRC_09_12_16', 'OCCMIDR02_BAIPETNMRC_09_12_16', 'FRTMIDOL02_BAIPETNMRC_09_12_16',
          'FRTSMEDR02_BAIPETNMRC_09_12_16', 'FUSFMR02_BAIPETNMRC_09_12_16', 'RECTUSL02_BAIPETNMRC_09_12_16',
          'TMPPOSL02_BAIPETNMRC_09_12_16', 'PARISUPL02_BAIPETNMRC_09_12_16', 'PARISUPR02_BAIPETNMRC_09_12_16',
          'SUPMRGL02_BAIPETNMRC_09_12_16', 'SUPMRGR02_BAIPETNMRC_09_12_16', 'TMPINFR02_BAIPETNMRC_09_12_16']


RACE_DICT = {
    'White': 0,
    'Asian' : 1,
    'Black': 2,
    'Am Indian/Alaskan': 3,
    'More than one': 4,
    'Hawaiian/Other PI': 5,
    'Unknown': 6
}


class Clinical(object):
    def __init__(self,row):
        self.cdrsb = get_float(row['CDRSB'])
        self.adas11 = get_float(row['ADAS11'])
        self.adas13 = get_float(row['ADAS13'])
        self.mmse = get_float(row['MMSE'])
        self.ravlt_immediate = get_float(row['RAVLT_immediate'])
        self.ravlt_forgetting = get_float(row['RAVLT_forgetting'])
        self.ravlt_per_forgetting = get_float(row['RAVLT_perc_forgetting'])
        self.ravlt_learning = get_float(row['RAVLT_learning'])
        self.faq = get_float(row['FAQ'])
        self.moca = get_float(row['MOCA'])

    def get_data(self):

        return [self.cdrsb, self.mmse, self.adas11, self.adas13, self.ravlt_immediate, self.ravlt_forgetting,
                self.ravlt_per_forgetting, self.ravlt_learning, self.faq, self.moca]

    @staticmethod
    def get_names():
        return ['cdrsb', 'mmse', 'adas11', 'adas13','ravlt_immediate', 'ravlt_forgetting', 'ravlt_per_forgetting',
                'ravlt_learning', 'faq', 'moca']


class Demographic(object):
    def __init__(self,row):
        self.apoe4 = get_int(row['APOE4'])
        self.age_bl = get_float(row['AGE'])
        self.age = None
        self.gender = get_gender(row['PTGENDER'])
        self.education = get_float(row['PTEDUCAT'])
        self.ethnicity = row['PTETHCAT']
        self.race = row['PTRACCAT']

    @staticmethod
    def get_names():
        return ['apoe4', 'age_bl', 'age', 'gender', 'education', 'ethnicity', 'race']

class Diagnosis(object):
    def __init__(self,row):
        self._dx = row['DX']
        self.dx_change = row['DXCHANGE']

    @property
    def dx(self):
        return DX_DICT[self._dx]


class Imaging_Phenotypes(object):
    def __init__(self, row):
        self.fdg = row['FDG']
        self.pib = row['PIB']
        self.av45 = row['AV45']
        self.ventricles = row['Ventricles']
        self.hippocampus = row['Hippocampus']
        self.whole_brain = row['WholeBrain']
        self.entorhinal = row['Entorhinal']
        self.fusiform = row['Fusiform']
        self.middle_temporal = row['MidTemp']
        self.icv = row['ICV']

    def get_data(self):
        try:
            # return  [self.pib]
            return [float(self.fdg), float(self.av45), float(self.ventricles), float(self.hippocampus),
                    float(self.whole_brain), float(self.entorhinal), float(self.fusiform), float(self.middle_temporal),
                    float(self.icv)]
        except:
            return None

    @staticmethod
    def get_names():
        return ['fdg', 'av45', 'ventricles', 'hippocampus', 'whole_brain', 'entorhinal',
                'fusiform', 'middle_temporal', 'icv' ]



class CSF(object):
    def __init__(self,row):
        self.ab = get_float(row['ABETA_UPENNBIOMK9_04_19_17'])
        self.ptau = get_float(row['PTAU_UPENNBIOMK9_04_19_17'])
        self.ttau = get_float(row['TAU_UPENNBIOMK9_04_19_17'])

    def get_data(self):
        try:
            return [float(self.ab), float(self.ptau), float(self.ttau)]
        except:
            return None

    @staticmethod
    def get_names():
        return ['ab', 'ptau', 'ttau']


class Volumetric_MRI(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_volumetric), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE']+'_UCSFFSL_02_01_16_UCSFFSL51ALL_08_01_16')

        return header

    def get_data(self):
        try:
            return [float(d)  for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_volumetric), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])

        return header


class CorticalThickness_MRI(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_corticalthickness), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE']+'_UCSFFSL_02_01_16_UCSFFSL51ALL_08_01_16')

        return header

    def get_data(self):
        try:
            return [float(d)  for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_corticalthickness), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])
        return header



class SurfaceArea_MRI(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_surface_area), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE']+'_UCSFFSL_02_01_16_UCSFFSL51ALL_08_01_16')

        return header

    def get_data(self):
        try:
            return [float(d)  for d in self._data]
        except:
            return None


    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_surface_area), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])

        return header



class FDG_PET(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_fdg_pet), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header

    def get_data(self):
        try:
            data = []
            for d,h in zip(self._data, self._header()):
                if h not in EXCLUDED_FDG_PET_HEADER:
                    if float(d) == -4:
                        return None
                data.append(float(d))
            return data
        except:
            return None

    @staticmethod
    def get_names():
        stdout.write('\r#### Please, check the header file for FDG_PET')
        with open(join(dir_TADPOLE, legend_fdg_pet), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                if row['FEATURE_CODE'] not in EXCLUDED_FDG_PET_HEADER:
                    header.append(row['FEATURE_CODE'])

        return header



class AV45_PET(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())


    def _header(self):
        with open(join(dir_TADPOLE, legend_av45), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header

    def get_data(self):
        try:
            return [float(d) for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_av45), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])

        return header



class AV1451_PET(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())


    def _header(self):
        with open(join(dir_TADPOLE, legend_av1451), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header

    def get_data(self):
        try:
            return [float(d) for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_av1451), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])
        return header


class FA_DTI(object):
    def __init__(self,row):
        self._data = read_row_columns(row,self._header())


    def _header(self):
        with open(join(dir_TADPOLE, legend_dti_FA), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header



    def get_data(self):
        try:
            return [float(d) for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_dti_FA), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])
        return header



class MD_DTI(object):

    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_dti_MD), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header

    def get_data(self):
        try:
            return [float(d) for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_dti_MD), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])
        return header


class RD_DTI(object):

    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_dti_RD), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header

    def get_data(self):
        try:
            return [float(d) for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_dti_RD), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])
        return header


class AD_DTI(object):

    def __init__(self,row):
        self._data = read_row_columns(row,self._header())

    def _header(self):
        with open(join(dir_TADPOLE, legend_dti_AD), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['FEATURE_CODE'])

        return header

    def get_data(self):
        try:
            return [float(d) for d in self._data]
        except:
            return None

    @staticmethod
    def get_names():
        with open(join(dir_TADPOLE, legend_dti_AD), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            header = []
            for row in csv_reader:
                header.append(row['NAME'])
        return header


class Event(object):
    def __init__(self, rid, ptid, viscode, release, examdate, imaging_phenotypes, dx, clinical, volumetric_mri, cortical_thickness_mri, surface_area_mri,
                 fdg_pet, av45_pet, av1451_pet, fa_dti, md_dti, rd_dti, ad_dti, csf, demographics):

        self.rid = rid
        self.ptid = ptid
        self.viscode = viscode
        self.examdate = examdate.split('-') #['year','month','day']
        self.release = release

        self.imaging_phenotypes = imaging_phenotypes

        self.dx = dx
        self.clinical = clinical

        self.volumetric_mri = volumetric_mri
        self.cortical_thickness_mri = cortical_thickness_mri
        self.surface_area_mri = surface_area_mri

        self.fdg_pet = fdg_pet
        self.av45_pet = av45_pet
        self.av1451_pet = av1451_pet

        self.fa_dti = fa_dti
        self.md_dti = md_dti
        self.rd_dti = rd_dti
        self.ad_dti = ad_dti

        self.csf = csf
        self.demographics = demographics

        self.fa_dti_flag = False if fa_dti.get_data() is None else True
        self.md_dti_flag = False if md_dti.get_data() is None else True
        self.rd_dti_flag = False if rd_dti.get_data() is None else True
        self.ad_dti_flag = False if ad_dti.get_data() is None else True
        self.av45_pet_flag = False if av45_pet.get_data() is None else True
        self.av1451_pet_flag = False if av1451_pet.get_data() is None else True
        self.fdg_pet_flag = False if fdg_pet.get_data() is None else True
        self.volumetric_mri_flag = False if volumetric_mri.get_data() is None else True
        self.cortical_thickness_mri_flag = False if cortical_thickness_mri.get_data() is None else True
        self.surface_area_mri_flag = False if surface_area_mri.get_data() is None else True
        self.csf_flag = False if csf.get_data() is None else True

        self._step = None
        self._dt = None


    def get_modality_data(self, modality, **kwargs):
        """
        Returns the compiled model specified by its name.
        If no name is given, the default model is returned, which corresponds to the
        hand-picked model that performs the best.

        Parameters
        ----------
        modality : str
            Name of the modlity to read

        Returns
        -------
        np.array
            Return modality data as a numpy array.

        Raises
        ------
        TypeError
            If modality is not a String or None
        ValueError
            If the model specified by model_name does not exist
        """
        if isinstance(modality, str):
            try:
                return getattr(self,modality).get_data()
            except KeyError:
                raise ValueError('Modality {} does not exist.' .format(modality))
        else:
            raise TypeError('Modality must be a String/Unicode sequence or None')

    def get_modality_flag(self, modality, **kwargs):
        """
        Returns the compiled model specified by its name.
        If no name is given, the default model is returned, which corresponds to the
        hand-picked model that performs the best.

        Parameters
        ----------
        modality : str
            Name of the modlity to read

        Returns
        -------
        np.array
            Return modality data as a numpy array.

        Raises
        ------
        TypeError
            If modality is not a String or None
        ValueError
            If the model specified by model_name does not exist
        """
        if isinstance(modality, str):
            try:
                return getattr(self,modality+'_flag')
            except KeyError:
                raise ValueError('Modality {} does not exist.' .format(modality))
            except AttributeError:
                return False if self.get_modality_data(modality) is None else True
        else:
            raise TypeError('Modality must be a String/Unicode sequence or None')


    @property
    def dt(self):
        return self._dt

    @property
    def step(self):
        return self._step


    def set_step(self, step):
        self._step = step

    def set_dt(self, dt):
        self._dt = dt



class Subject(object):
    def __init__(self, rid, examdate_bl = None):
        self.rid = rid
        self._events = []
        self.examdate_bl = examdate_bl


        #sort events + _set_step()

    def add_event(self,event):
        if isinstance(event, list):
            for e in event:
                self._events.append(e)
        else:
            self._events.append(event)

    def filter_events(self, function):
        self._events = list(filter(function, self._events))
        # return list(filter(function, self._events))

    @property
    def events(self):
        return self._events



class Loader(object):

    def load_subjects(self):
        events_list = []
        subject_dict = {}
        rid_list = []
        with open(join(dir_TADPOLE,file_TADPOLE), 'r') as infile:
            csv_reader = csv.DictReader(infile)
            n_rows=0
            for row in csv_reader:

                e = Event(rid=get_int(row['RID']),
                          ptid=row['PTID'],
                          viscode=row['VISCODE'],
                          examdate=row['EXAMDATE'],
                          release=10*int(row['D1'])+int(row['D2']),
                          imaging_phenotypes=Imaging_Phenotypes(row),
                          dx=Diagnosis(row),
                          clinical=Clinical(row),
                          volumetric_mri=Volumetric_MRI(row),
                          cortical_thickness_mri=CorticalThickness_MRI(row),
                          surface_area_mri=SurfaceArea_MRI(row),
                          fdg_pet=FDG_PET(row),
                          av45_pet=AV45_PET(row),
                          av1451_pet=AV1451_PET(row),
                          fa_dti=FA_DTI(row),
                          md_dti=MD_DTI(row),
                          rd_dti=RD_DTI(row),
                          ad_dti=AD_DTI(row),
                          csf=CSF(row),
                          demographics=Demographic(row)
                          )


                events_list.append(e)
                if row['RID'] not in rid_list:
                    s = Subject(row['RID'])
                    s.add_event(e)
                    rid_list.append(row['RID'])
                else:
                    s = subject_dict[row['RID']]
                    s.add_event(e)

                subject_dict[row['RID']] = s

                n_rows += 1
                if np.mod(n_rows,500) == 0:
                    print(n_rows)


        for rid,subject in subject_dict.items():
            set_events_step(subject)
            set_subject_examdate_bl(subject)
            set_events_dt(subject)
            set_events_age(subject)


        return subject_dict


class Legend(object):
    pass


def get_int(var):
    try:
        return int(var)
    except:
        return -1


def get_float(var):

    try:
        if float(var) == -4:
            return None
        elif var == 'NaN':
            return None
        else:
            return float(var)
    except:
        return None


def get_gender(var):
    try:
        if var == 'Male':
            return 1
        elif var == 'Female':
            return 0
    except:
        return None


def set_events_dt(subject):
    if subject.examdate_bl is None:
        print(subject.rid)
        return
    y0,m0,d0 = [int(ed) for ed in subject.examdate_bl]
    for e in subject.events:
        if e.viscode == 'bl':
            e.set_dt(0)
        else:
            y1,m1,d1 = [int(ed) for ed in e.examdate]
            if y1 >= y0:
                e.set_dt(12*(y1-y0)*30 + (m1 - m0) * 30 + d1 - d0)
            else:
                raise ValueError('[ERROR] While reading TADPOLE dataset, your baseline visit is not set up properly. Check it up again')


def set_events_age(subject):
    for e in subject.events:
        e.age = e.demographics.age_bl + e.dt/(12*30)


    # y0,m0,d0 = [int(ed) for ed in subject.examdate_bl]
    # for e in subject.events:
    #     if e.viscode == 'bl':
    #         e.age = e.demographics.age_bl
    #     else:
    #         y1,m1,d1 = [int(ed) for ed in e.examdate]
    #         e.age = e.demographics.age_bl + ( (y1-y0)*(12)*30.5 + (m1 - m0) * 30.5 + d1 - d0 )/366


def set_events_step(subject):
    viscode = []
    for e in subject.events:
        if e.viscode == 'bl':
            viscode.append(0)
        else:
            viscode.append(int((e.viscode.split('m')[-1])))

    index_viscode_sorted = np.argsort(viscode)
    n_step = 0
    for idx in index_viscode_sorted:
        subject.events[idx].set_step(n_step)
        n_step += 1


def read_row_columns(row,header):

    data = []
    for h in header:
        data.append(row[h])
    return data


def set_subject_examdate_bl(subject):

    try:
        for e in subject.events:
            if e.step == 0:
                subject.examdate_bl = e.examdate
                break
    except:
        viscode = []
        for e in subject.events:
            if e.viscode == 'bl':
                viscode.append(0)
            else:
                viscode.append(e.viscode.split('m')[-1])

        index_viscode_sorted = np.argsort(viscode)
        subject.examdate_bl = subject[index_viscode_sorted[0]].examdate



MODALITY_DICT = {
    'csf': CSF,
    'imaging_phenotypes': Imaging_Phenotypes,
    'volumetric_mri': Volumetric_MRI,
    'cortical_thickness_mri': CorticalThickness_MRI,
    'surface_area_mri': SurfaceArea_MRI,
    'fdg_pet': FDG_PET,
    'av45_pet': AV45_PET,
    'av1451_pet': AV1451_PET,
    'fa_dti': FA_DTI,
    'md_dti': MD_DTI,
    'ad_dti': AD_DTI,
    'rd_dti': RD_DTI,
}

def get_modality_labels(modality):
    return MODALITY_DICT[modality].get_names()

if __name__ == '__main__':

    loader = Loader()
    subject_list = loader.load_subjects()
    pickle.dump(subject_list,open('tadpole_pickle.p','wb'))
