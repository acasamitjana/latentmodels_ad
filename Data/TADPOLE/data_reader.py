from Data.TADPOLE.tadpole_reader import *
import pickle
from Data.ukbiobank import get_phenotype_dict
from Data import prsice
DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
DATA_PATH_D3 = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle_D3.p'

GENETICS_PATH = '/work/acasamitjana/Imaging-genetics/'
CLINICAL_NAMES = ['cdrsb','adas11', 'adas13', 'mmse', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning',
                  'ravlt_per_forgetting', 'faq', 'moca']
RELEASE_DICT = {
    'D1': 10,
    'D2': 11
}
class DataLoader(object):
    def __init__(self, data_path = DATA_PATH):

        handle = open(data_path, 'rb')
        self.subjects_dict = pickle.load(handle)
        self.subject_list = list(self.subjects_dict.values())

    def filter_stable_label_subject_dict(self, *args, **kwargs):
        # Load subject list with a single label according to their longitudinal evolution

        subject_dict = self.subjects_dict
        auxiliary_dict = find_longitudinal_label(subject_dict)

        new_subject_dict = {}
        for rid, auxiliar in auxiliary_dict.items():
            subject = subject_dict[rid]
            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)

            if auxiliar['dx'] == 0:
                for idx in idx_ordered_events[::-1]:
                    if subject.events[idx].dx.dx == 0:
                        new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                        new_subject_dict[rid].add_event(subject.events[idx])
                        break

            if auxiliar['dx'] in [22]:
                for idx in idx_ordered_events:
                    if subject.events[idx].dx.dx == 2:
                        new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                        new_subject_dict[rid].add_event(subject.events[idx])
                        break

            if auxiliar['dx'] in [11]:
                for idx in idx_ordered_events[::-1]:
                    if subject.events[idx].dx.dx == 1:
                        new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                        new_subject_dict[rid].add_event(subject.events[idx])
                        break


        self.subjects_dict = new_subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del new_subject_dict
        del auxiliary_dict
        del subject_dict

        print(len(self.subject_list))

    def filter_single_label_subject_dict(self, *args, **kwargs):
        # Load subject list with a single label according to their longitudinal evolution

        subject_dict = self.subjects_dict
        auxiliary_dict = find_longitudinal_label(subject_dict)

        new_subject_dict = {}
        for rid, auxiliar in auxiliary_dict.items():
            subject = subject_dict[rid]
            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)

            if auxiliar['dx'] == 0:
                for idx in idx_ordered_events[::-1]:
                    if subject.events[idx].dx.dx == 0:
                        new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                        new_subject_dict[rid].add_event(subject.events[idx])
                        break

            if auxiliar['dx'] in [2,12,22]:
                for idx in idx_ordered_events:
                    if subject.events[idx].dx.dx == 2:
                        new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                        new_subject_dict[rid].add_event(subject.events[idx])
                        break

            if auxiliar['dx'] in [1,11]:
                for idx in idx_ordered_events[::-1]:
                    if subject.events[idx].dx.dx == 1:
                        new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                        new_subject_dict[rid].add_event(subject.events[idx])
                        break


        self.subjects_dict = new_subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del new_subject_dict
        del auxiliary_dict
        del subject_dict

        print(len(self.subject_list))

    def filter_baseline_subject_dict(self, *args, **kwargs):
        # Load subject list with a single label according to their longitudinal evolution


        subject_dict = self.subjects_dict

        new_subject_dict = {}
        for rid, subject in subject_dict.items():

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
            new_subject_dict[rid].add_event(subject.events[idx_ordered_events[0]])


        self.subjects_dict = new_subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del new_subject_dict
        del subject_dict

    def filter_rid_subject_dict(self, rid_excluded = None, rid_included = None, *args, **kwargs):
        # Load subject list with a single label according to their longitudinal evolution

        if rid_excluded is None and rid_included is None:
            return

        subject_dict = self.subjects_dict
        if rid_excluded is not None:
            subject_dict = dict(filter(lambda x: x[0] not in rid_excluded, subject_dict.items()))

        if rid_included is not None:
            subject_dict = dict(filter(lambda x: x[0] in rid_included, subject_dict.items()))

        self.subjects_dict = subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del subject_dict

    def filter_subjects_release(self, release = 'D1', *args, **kwargs):
        # Load subject list with a single label according to their longitudinal evolution

        subject_dict = self.subjects_dict
        subject_dict = dict(filter(lambda x: x[1].events[0].release == RELEASE_DICT[release], subject_dict.items()))

        self.subjects_dict = subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del subject_dict

    def load_imaging_phenotypes_baseline(self, imaging_phenotye = None):

        ip_dict = {}
        if imaging_phenotye is None:
            for it_subject, subject in enumerate(self.subject_list):
                baseline_event = [e for e in subject.events if e.dt == 0][0]
                baseline_ip = getattr(baseline_event, 'imaging_phenotypes').get_data()
                try:
                    ip_dict[subject.rid] = [float(b_ip) for b_ip in baseline_ip]
                except:
                    pass
        else:
            for it_subject, subject in enumerate(self.subject_list):
                baseline_event = [e for e in subject.events if e.dt == 0][0]
                baseline_ip = getattr(baseline_event, 'imaging_phenotypes')

                if isinstance(imaging_phenotye, list):
                    baseline_feature = [getattr(baseline_ip, ip) for ip in imaging_phenotye]
                else:
                    baseline_feature = [getattr(baseline_ip, imaging_phenotye)]

                try:
                    ip_dict[subject.rid] = [float(bf) for bf in baseline_feature]
                except:
                    pass


        return ip_dict

    def load_modality_baseline(self, modality_name = None, *args, **kwargs):
        if modality_name is None:
            return None

        if modality_name == 'clinical':
            return self.load_clinical_baseline(clinical_names=kwargs['clinical_names'])

        modality_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            baseline_event = [e for e in subject.events if e.dt == 0][0]
            baseline_modality = getattr(baseline_event, modality_name).get_data()
            if baseline_modality is not None:
                modality_dict[subject.rid] = baseline_modality

        return modality_dict

    def load_dx_baseline(self):
        dx_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            baseline_event = [e for e in subject.events if e.dt == 0][0]
            baseline_dx = baseline_event.dx.dx
            if baseline_dx is not None:
                dx_dict[subject.rid] = baseline_dx

        return dx_dict

    def load_clinical_baseline(self, clinical_names = None):

        if not isinstance(clinical_names, list):
            clinical_names = [clinical_names]

        clinical_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            baseline_event = [e for e in subject.events if e.dt == 0][0]
            baseline_clinical = [getattr(baseline_event.clinical, cl_name) for cl_name in clinical_names]

            if None not in baseline_clinical:
                clinical_dict[subject.rid] = baseline_clinical

        return clinical_dict

    def load_demographic_baseline(self, demo_names):
        demo_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            baseline_event = [e for e in subject.events if e.dt == 0][0]
            baseline_demo = [getattr(baseline_event.demographics, d_name) for d_name in demo_names]
            if None not in baseline_demo:
                demo_dict[subject.rid] = baseline_demo

        return demo_dict

    def load_age_baseline(self):
        age_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            baseline_event = [e for e in subject.events if e.dt == 0][0]
            baseline_age = baseline_event.age
            if baseline_age is not None:
                age_dict[subject.rid] = baseline_age

        return age_dict


    def load_imaging_phenotypes_single(self, imaging_phenotye = None):

        ip_dict = {}
        if imaging_phenotye is None:
            for it_subject, subject in enumerate(self.subject_list):
                single_event = [e for e in subject.events][0]
                single_ip = getattr(single_event, 'imaging_phenotypes').get_data()
                try:
                    ip_dict[subject.rid] = [float(s_ip) for s_ip in single_ip]
                except:
                    pass
        else:
            for it_subject, subject in enumerate(self.subject_list):
                single_event = [e for e in subject.events][0]
                single_ip = getattr(single_event, 'imaging_phenotypes')

                if isinstance(imaging_phenotye, list):
                    single_feature = [getattr(single_ip, ip) for ip in imaging_phenotye]
                else:
                    single_feature = [getattr(single_ip, imaging_phenotye)]

                try:
                    ip_dict[subject.rid] = [float(sf) for sf in single_feature]
                except:
                    pass


        return ip_dict

    def load_modality_single(self, modality_name = None, *args, **kwargs):
        if modality_name is None:
            return None

        if modality_name == 'clinical':
            return self.load_clinical_single(clinical_names=kwargs['clinical_names'])

        modality_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_modality = getattr(single_event, modality_name).get_data()
            if single_modality is not None:
                modality_dict[subject.rid] = single_modality

        return modality_dict

    def load_dx_single(self):

        dx_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_dx = single_event.dx.dx
            if single_dx is not None:
                dx_dict[subject.rid] = single_dx

        return dx_dict

    def load_clinical_single(self, clinical_names = None):

        if not isinstance(clinical_names, list):
            clinical_names = [clinical_names]

        clinical_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_clinical = [getattr(single_event.clinical, cl_name) for cl_name in clinical_names]

            if None not in single_clinical:
                clinical_dict[subject.rid] = single_clinical if len(single_clinical)>1 else single_clinical[0]

        return clinical_dict

    def load_demographic_single(self, demo_names):

        if not isinstance(demo_names, list):
            demo_names = [demo_names]

        demo_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_demo = [getattr(single_event.demographics, d_name) for d_name in demo_names]
            if None not in single_demo:
                demo_dict[subject.rid] = single_demo if len(single_demo)>1 else single_demo[0]

        return demo_dict

    def load_amyloid_status_single(self):
        dx_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_ab = single_event.csf.ab
            if single_ab is not None:
                if single_ab > 500:
                    dx_dict[subject.rid] = 0
                else:
                    dx_dict[subject.rid] = 1
        return dx_dict

    def load_age_single(self):
        age_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_age = single_event.age
            if single_age is not None:
                age_dict[subject.rid] = single_age

        return age_dict

    def load_modality_codenames(self, modality_name = None, *args, **kwargs):
        if modality_name is None:
            return None

        if modality_name == 'clinical':
            if 'clinical_names' in kwargs:
                return kwargs['clinical_names']
            else:
                raise ValueError('Specify clinical_names kwargs')

        baseline_event = [e for e in self.subject_list[0].events][0]
        names = getattr(baseline_event, modality_name).get_names()

        return names

    def load_modality_longitudinal(self, modality_name):
        modality_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            modality_dict[subject.rid] = {}
            for e in subject.events[:]:
                modality_dict[subject.rid][e.dt] = getattr(e, modality_name).get_data()

        return modality_dict

    def get_phenotype_description(self):
        phenotype_dict = get_phenotype_dict()
        prs_descriptive_list = []
        for prs_id in self.prs_id_list:
            prs_descriptive_list.append(phenotype_dict[prs_id]['phenotype_description'])

    def load_from_other_dict_baseline(self, data_dict, attribute_name = None):
        n_e = 0
        n_s = 0
        output_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            if subject.rid in data_dict.keys():
                baseline_event = [e for e in subject.events if e.dt == 0][0]
                if 'bl' in data_dict[subject.rid]:
                    baseline_data = data_dict[subject.rid]['bl']
                    if baseline_data is not None:
                        output_dict[subject.rid] = baseline_data
        return output_dict

    def load_from_other_dict(self, data_dict, attribute_name = None):
        output_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            if subject.rid in data_dict.keys():
                for e in subject.events:
                    if e.viscode in data_dict[subject.rid]:
                        data = data_dict[subject.rid][e.viscode]
                        if data is not None:
                            output_dict[subject.rid + '_' + e.viscode] = data

        return output_dict


class DataLoader_semisupervised(DataLoader):

    def load_imaging_phenotypes_single(self, imaging_phenotye = None):
        ip_dict = {}
        labels_dict = {}
        if imaging_phenotye is None:
            for it_subject, subject in enumerate(self.subject_list):
                single_event = [e for e in subject.events][0]
                single_ip = getattr(single_event, 'imaging_phenotypes').get_data()
                try:
                    ip_dict[subject.rid] = [float(s_ip) for s_ip in single_ip]
                    labels_dict[subject.rid] = 1
                except:
                    ip_dict[subject.rid] = [0 for b_ip in single_ip]
                    labels_dict[subject.rid] = 0
        else:
            for it_subject, subject in enumerate(self.subject_list):
                single_event = [e for e in subject.events][0]
                single_ip = getattr(single_event, 'imaging_phenotypes')

                if isinstance(imaging_phenotye, list):
                    single_feature = [getattr(single_ip, ip) for ip in imaging_phenotye]
                else:
                    single_feature = [getattr(single_ip, imaging_phenotye)]

                try:
                    ip_dict[subject.rid] = [float(sf) for sf in single_feature]
                    labels_dict[subject.rid] = 1
                except:
                    ip_dict[subject.rid] = 0
                    labels_dict[subject.rid] = 0


        return ip_dict, labels_dict

    def load_modality_single(self, modality_name = None, *args, **kwargs):
        if modality_name is None:
            return None

        if modality_name == 'clinical':
            return self.load_clinical_single(clinical_names=kwargs['clinical_names'])

        modality_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_modality = getattr(single_event, modality_name).get_data()
            if single_modality is not None:
                modality_dict[subject.rid] = single_modality
                labels_dict[subject.rid] = 1

            else:
                modality_dict[subject.rid] = [0]*len(getattr(single_event, modality_name).get_names())
                labels_dict[subject.rid] = 0

        return modality_dict, labels_dict

    def load_modality_codenames(self, modality_name = None, *args, **kwargs):
        if modality_name is None:
            return None

        if modality_name == 'clinical':
            if 'clinical_names' in kwargs:
                return kwargs['clinical_names']
            else:
                raise ValueError('Specify clinical_names kwargs')

        single_event = [e for e in self.subject_list[0].events][0]
        names = getattr(single_event, modality_name).get_names()

        return names

    def load_clinical_single(self, clinical_names = None):

        if not isinstance(clinical_names, list):
            clinical_names = [clinical_names]

        clinical_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            baseline_event = [e for e in subject.events if e.dt == 0][0]
            baseline_clinical = [getattr(baseline_event.clinical, cl_name) for cl_name in clinical_names]

            if None not in baseline_clinical:
                clinical_dict[subject.rid] = baseline_clinical
                labels_dict[subject.rid] = 1
            else:
                clinical_dict[subject.rid] = [0]*len(clinical_names)
                labels_dict[subject.rid] = 0

        return clinical_dict, labels_dict

    def load_demographic_single(self, demo_names):
        demo_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_demo = [getattr(single_event.demographics, d_name) for d_name in demo_names]
            if None not in single_demo:
                demo_dict[subject.rid] = single_demo
                labels_dict[subject.rid] = 1

            else:
                demo_dict[subject.rid] = [0]*len(demo_names)
                labels_dict[subject.rid] = 0

        return demo_dict, labels_dict

    def load_age_single(self):
        age_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_age = single_event.age
            if single_age is not None:
                age_dict[subject.rid] = single_age
                labels_dict[subject.rid] = 1

            else:
                age_dict[subject.rid] = 0
                labels_dict[subject.rid] = 0



        return age_dict, labels_dict

    def load_dx_single(self):
        dx_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_dx = single_event.dx.dx
            if single_dx is not None:
                dx_dict[subject.rid] = single_dx
                labels_dict[subject.rid] = 1

            else:
                dx_dict[subject.rid] = -4
                labels_dict[subject.rid] = 0

        return dx_dict, labels_dict

    def load_amyloid_status_single(self):
        dx_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events if e.dt == 0][0]
            single_ab = single_event.csf.ab
            if single_ab is not None:
                labels_dict[subject.rid] = 1
                if single_ab > 500:
                    dx_dict[subject.rid] = 0
                else:
                    dx_dict[subject.rid] = 1

            else:
                labels_dict[subject.rid] = 0
                dx_dict[subject.rid] = -4

        return dx_dict, labels_dict

class DataLoader_Longitudinal(DataLoader):

    def __init__(self):

        handle = open(DATA_PATH, 'rb')
        self.subjects_dict = pickle.load(handle)
        self.subject_list = list(self.subjects_dict.values())

    def filter_stable_label_subject_dict(self, *args, **kwargs):
        # Load subject list with a single label according to their longitudinal evolution

        subject_dict = self.subjects_dict
        auxiliary_dict = find_longitudinal_label(subject_dict)

        new_subject_dict = {}
        for rid, auxiliar in auxiliary_dict.items():
            subject = subject_dict[rid]
            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)

            if auxiliar['dx'] == 0:
                new_subject_dict[rid] = subject

            if auxiliar['dx'] in [22]:
                new_subject_dict[rid] = subject

            if auxiliar['dx'] in [11]:
                new_subject_dict[rid] = subject

        self.subjects_dict = new_subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del new_subject_dict
        del auxiliary_dict
        del subject_dict

    def filter_subject_events_from_dict(self, events_dict):

        subject_dict = self.subjects_dict
        new_subject_dict = {}
        for rid, subject in subject_dict.items():
            if rid in list(events_dict.keys()):
                new_subject_dict[rid] = Subject(rid, examdate_bl=subject.examdate_bl)
                for event in subject.events:
                    if event.step in events_dict[rid]:
                        new_subject_dict[rid].add_event(event)

        self.subjects_dict = new_subject_dict
        self.subject_list = list(self.subjects_dict.values())

        del new_subject_dict
        del events_dict
        del subject_dict

    def load_imaging_phenotypes(self, imaging_phenotye = None):

        ip_dict = {}
        if imaging_phenotye is None:
            for it_subject, subject in enumerate(self.subject_list):
                ip_dict[subject.rid] = {}

                step_list = [e.step for e in subject.events]
                idx_ordered_events = np.argsort(step_list)
                for idx in idx_ordered_events:
                    single_ip = getattr(subject.events[idx], 'imaging_phenotypes').get_data()
                    try:
                        ip_dict[subject.rid][step_list[idx]] = [float(s_ip) for s_ip in single_ip]
                    except:
                        pass

        else:
            for it_subject, subject in enumerate(self.subject_list):
                ip_dict[subject.rid] = {}

                step_list = [e.step for e in subject.events]
                idx_ordered_events = np.argsort(step_list)
                for idx in idx_ordered_events:
                    single_ip = getattr(subject.events[idx], 'imaging_phenotypes')

                    if isinstance(imaging_phenotye, list):
                        single_feature = [getattr(single_ip, ip) for ip in imaging_phenotye]
                    else:
                        single_feature = [getattr(single_ip, imaging_phenotye)]

                    try:
                        ip_dict[subject.rid][step_list[idx]] = [float(sf) for sf in single_feature]

                    except:
                        pass

        return ip_dict

    def load_modality(self, modality_name = None, *args, **kwargs):
        if modality_name is None:
            return None

        if modality_name == 'clinical':
            return self.load_clinical(clinical_names=kwargs['clinical_names'])

        modality_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            modality_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                single_modality = getattr(subject.events[idx], modality_name).get_data()

                if single_modality is not None:
                    modality_dict[subject.rid][step_list[idx]] = single_modality

        return modality_dict

    def load_clinical(self, clinical_names = None):

        if not isinstance(clinical_names, list):
            clinical_names = [clinical_names]

        clinical_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            clinical_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                single_clinical = [getattr(subject.events[idx].clinical, cl_name) for cl_name in clinical_names]
                if None not in single_clinical:
                    clinical_dict[subject.rid][step_list[idx]] = single_clinical if len(single_clinical)>1 else single_clinical[0]

        return clinical_dict

    def load_demographic(self, demo_names):

        if not isinstance(demo_names, list):
            demo_names = [demo_names]

        demo_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            demo_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                single_demo = [getattr(subject.events[idx].demographics, d_name) for d_name in demo_names]

                if None not in single_demo:
                    demo_dict[subject.rid][step_list[idx]] = single_demo if len(single_demo)>1 else single_demo[0]

        return demo_dict

    def load_amyloid_status(self):
        dx_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            dx_dict[subject.rid] = {}
            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                single_ab = subject.events[idx].csf.ab
                if single_ab is not None:
                    if single_ab > 750:
                        dx_dict[subject.rid][step_list[idx]] = 0
                    else:
                        dx_dict[subject.rid][step_list[idx]] = 1
        return dx_dict

    def load_age(self):
        age_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            age_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                single_age = subject.events[idx].age
                if single_age is not None:
                    age_dict[subject.rid][step_list[idx]]  = single_age

        return age_dict

    def load_dx(self):

        dx_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            dx_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                single_dx = subject.events[idx].dx.dx
                if single_dx is not None:
                    dx_dict[subject.rid][step_list[idx]] = single_dx

        return dx_dict

class DataLoader_Longitudinal_semisupervised(DataLoader_semisupervised):

    def load_imaging_phenotypes(self, imaging_phenotye = None):

        ip_dict = {}
        labels_dict = {}
        if imaging_phenotye is None:
            for it_subject, subject in enumerate(self.subject_list):
                ip_dict[subject.rid] = {}
                labels_dict[subject.rid] = {}

                step_list = [e.step for e in subject.events]
                idx_ordered_events = np.argsort(step_list)
                for idx in idx_ordered_events:
                    try:
                        single_ip = getattr(subject.events[idx], 'imaging_phenotypes').get_data()
                    except:
                        single_ip = [None]

                    if None not in single_ip:
                        ip_dict[subject.rid][step_list[idx]] = [get_float(s_ip) for s_ip in single_ip]
                        labels_dict[subject.rid][step_list[idx]] = 1
                    else:
                        try:
                            ip_dict[subject.rid][step_list[idx]] = [0] * len(getattr(subject.events[idx], 'imaging_phenotypes').get_names())
                        except:
                            ip_dict[subject.rid][step_list[idx]] = 0
                        labels_dict[subject.rid][step_list[idx]] = 0


        else:
            if not isinstance(imaging_phenotye,list):
                imaging_phenotye = [imaging_phenotye]

            for it_subject, subject in enumerate(self.subject_list):
                ip_dict[subject.rid] = {}
                labels_dict[subject.rid] = {}

                step_list = [e.step for e in subject.events]
                idx_ordered_events = np.argsort(step_list)
                for idx in idx_ordered_events:
                    try:
                        single_ip = getattr(subject.events[idx], 'imaging_phenotypes')
                        single_feature = [get_float(getattr(single_ip, ip)) for ip in imaging_phenotye]
                    except:
                        single_feature = [None]

                    if None not in single_feature:
                        ip_dict[subject.rid][step_list[idx]] = single_feature
                        labels_dict[subject.rid][step_list[idx]] = 1
                    else:
                        try:
                            ip_dict[subject.rid][step_list[idx]] = [0] * len(imaging_phenotye)
                        except:
                            ip_dict[subject.rid][step_list[idx]] = 0
                        labels_dict[subject.rid][step_list[idx]] = 0

        return ip_dict, labels_dict

    def load_modality(self, modality_name = None, *args, **kwargs):

        if modality_name is None:
            return None

        if modality_name == 'clinical':
            return self.load_clinical(clinical_names=kwargs['clinical_names'])

        modality_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            modality_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_modality = getattr(subject.events[idx], modality_name).get_data()
                except:
                    single_modality = None

                if single_modality is not None:
                    modality_dict[subject.rid][step_list[idx]] = single_modality
                    labels_dict[subject.rid][step_list[idx]] = 1
                else:
                    try:
                        modality_dict[subject.rid][step_list[idx]] = [0] * len(getattr(subject.events[idx], modality_name).get_names())
                    except:
                        modality_dict[subject.rid][step_list[idx]] = 0
                    labels_dict[subject.rid][step_list[idx]] = 0

        return modality_dict, labels_dict

    def load_clinical(self, clinical_names):

        if not isinstance(clinical_names, list):
            clinical_names = [clinical_names]

        clinical_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            clinical_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_clinical = [getattr(subject.events[idx].clinical, cl_name) for cl_name in clinical_names]
                except:
                    single_clinical = [None]

                if None not in single_clinical:
                    clinical_dict[subject.rid][step_list[idx]] = single_clinical 
                    labels_dict[subject.rid][step_list[idx]] = 1
                else:
                    try:
                        clinical_dict[subject.rid][step_list[idx]] = [0] * len(clinical_names)
                    except:
                        clinical_dict[subject.rid][step_list[idx]] = 0
                    labels_dict[subject.rid][step_list[idx]] = 0
        return clinical_dict, labels_dict

    def load_csf(self, csf_names = None):

        if csf_names is None:
            csf_names = ['ab', 'ptau', 'ttau']
        if not isinstance(csf_names, list):
            csf_names = [csf_names]

        csf_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            csf_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}
            step_list = [e.step for e in subject.events]

            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_csf = [getattr(subject.events[idx].csf, c_name) for c_name in csf_names]
                except:
                    single_csf = [None]

                if None not in single_csf:
                    csf_dict[subject.rid][step_list[idx]] = single_csf if len(single_csf)>1 else single_csf[0]
                    labels_dict[subject.rid][step_list[idx]] = 1
                else:
                    try:
                        csf_dict[subject.rid][step_list[idx]] = [0] * len(csf_names)
                    except:
                        csf_dict[subject.rid][step_list[idx]] = 0

                    labels_dict[subject.rid][step_list[idx]] = 0

        return csf_dict, labels_dict

    def load_demographic(self, demo_names):

        if not isinstance(demo_names, list):
            demo_names = [demo_names]

        demo_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            demo_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_demo = [getattr(subject.events[idx].demographics, d_name) for d_name in demo_names]
                except:
                    single_demo = [None]

                if None not in single_demo:
                    demo_dict[subject.rid][step_list[idx]] = single_demo
                    labels_dict[subject.rid][step_list[idx]] = 1
                else:
                    try:
                        demo_dict[subject.rid][step_list[idx]] = [0] * len(demo_names)
                    except:
                        demo_dict[subject.rid][step_list[idx]] = 0


                    labels_dict[subject.rid][step_list[idx]] = 0

        return demo_dict, labels_dict

    def load_amyloid_status(self):
        dx_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            dx_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_ab = subject.events[idx].csf.ab
                except:
                    single_ab = None

                if single_ab is not None:
                    labels_dict[subject.rid][step_list[idx]] = 1
                    if single_ab > 1000:
                        dx_dict[subject.rid][step_list[idx]] = 0
                    else:
                        dx_dict[subject.rid][step_list[idx]] = 1
                else:
                    labels_dict[subject.rid][step_list[idx]] = 0
                    dx_dict[subject.rid][step_list[idx]] = -4

        return dx_dict, labels_dict

    def load_age(self):
        age_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            age_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_age = subject.events[idx].age
                except:
                    single_age = None

                if single_age is not None:
                    age_dict[subject.rid][step_list[idx]]  = single_age
                    labels_dict[subject.rid][step_list[idx]] = 1
                else:
                    age_dict[subject.rid][step_list[idx]] = 0
                    labels_dict[subject.rid][step_list[idx]] = 0

        return age_dict, labels_dict

    def load_dx(self, dx_transformation_dict = None):

        dx_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            dx_dict[subject.rid] = {}
            labels_dict[subject.rid] = {}

            step_list = [e.step for e in subject.events]
            idx_ordered_events = np.argsort(step_list)
            for idx in idx_ordered_events:
                try:
                    single_dx = subject.events[idx].dx.dx
                except:
                    single_dx = None
                if single_dx is not None:
                    if dx_transformation_dict is None:
                        dx_dict[subject.rid][step_list[idx]] = single_dx
                    else:
                        dx_dict[subject.rid][step_list[idx]] = dx_transformation_dict[single_dx]

                    labels_dict[subject.rid][step_list[idx]] = 1
                else:
                    dx_dict[subject.rid][step_list[idx]] = -4
                    labels_dict[subject.rid][step_list[idx]] = 0


        return dx_dict, labels_dict


class DataLoader_Genetics(DataLoader):

    def __init__(self, genetic_gwas, pvalue, corrected=True):

        super().__init__()

        if corrected:
            prs_filename = 'resume_file_pvalue' + str(pvalue) + '_corrected.csv'
        else:
            prs_filename = 'resume_file_pvalue' + str(pvalue) + '.csv'

        PRS_PATH = join(GENETICS_PATH, genetic_gwas, 'PRS', prs_filename)
        self.prs_dict, self.prs_id_list, self.prs_code_list = prsice.read_multiple_trait_file(PRS_PATH)

        subjects_dict = {}
        for subject_rid, subject in self.subjects_dict.items():
            if subject.events[0].ptid in self.prs_dict.keys() and subject.events[0].demographics.ethnicity == 'Not Hisp/Latino':
                subjects_dict[subject_rid] = subject

        self.subjects_dict = subjects_dict
        self.subject_list = list(self.subjects_dict.values())

        del subjects_dict


    def load_prs(self):
        prs_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events][0]
            single_prs = self.prs_dict[single_event.ptid]
            if single_prs is not None:
                prs_dict[subject.rid] = single_prs

        return prs_dict


class DataLoader_Genetics_semisupervised(DataLoader_semisupervised):

    def __init__(self, genetic_gwas, pvalue, corrected=True):

        super().__init__()

        if corrected:
            prs_filename = 'resume_file_pvalue' + str(pvalue) + '_corrected.csv'
        else:
            prs_filename = 'resume_file_pvalue' + str(pvalue) + '.csv'

        PRS_PATH = join(GENETICS_PATH, genetic_gwas, 'PRS', prs_filename)
        self.prs_dict, self.prs_id_list, self.prs_code_list = prsice.read_multiple_trait_file(PRS_PATH)

        subjects_dict = {}
        for subject_rid, subject in self.subjects_dict.items():
            if subject.events[0].demographics.ethnicity == 'Not Hisp/Latino':
                subjects_dict[subject_rid] = subject

        self.subjects_dict = subjects_dict
        self.subject_list = list(self.subjects_dict.values())

        del subjects_dict


    def load_prs(self):
        prs_dict = {}
        labels_dict = {}
        for it_subject, subject in enumerate(self.subject_list):
            single_event = [e for e in subject.events if e.dt == 0][0]
            if single_event.ptid not in self.prs_dict:
                prs_dict[subject.rid] = [0] * len(self.prs_id_list)
                labels_dict[subject.rid] = 0
                continue

            single_prs = self.prs_dict[single_event.ptid]
            if single_prs is not None:
                prs_dict[subject.rid] = single_prs
                labels_dict[subject.rid] = 1
            else:
                prs_dict[subject.rid] = [0]*len(self.prs_id_list)
                labels_dict[subject.rid] = 0

        return prs_dict, labels_dict


def find_list_intersection(rid_lists):

    final_list = rid_lists[0]
    for rid_list in rid_lists[1:]:
        final_list = [rid for rid in final_list if rid in rid_list]

    return final_list

def find_list_tuple_intersection(rid_lists):
    final_list = rid_lists[0]
    for rid_list in rid_lists[1:]:
        final_list = [(a,b) for (a,b) in rid_list if (a,b) in final_list]

    return final_list

def find_list_intersection_two_levels(rid_dicts):
    #rid_dicts = list of dictionaries with rid:event_steps


    subject_list = list(rid_dicts[0].keys())
    for rid_d in rid_dicts[1:]:
        subject_list = [rid for rid in list(rid_d.keys()) if rid in subject_list]

    s_e_dict = {}
    for s in subject_list:
        events_list = rid_dicts[0][s]
        for rid_d in rid_dicts[1:]:
            events_list = [step for step in rid_d[s] if step in events_list]

        if events_list:
            s_e_dict[s] = events_list

    return s_e_dict

def find_longitudinal_label(subject_dict):

    auxiliary_dict = {}
    for rid, subject in subject_dict.items():
        dx_sbj = [e.dx.dx for e in subject.events]
        step_list = [e.step for e in subject.events]
        index_list = np.argsort(step_list)
        for idx in index_list[::-1]:
            if dx_sbj[idx] is not None:
                dx_last = dx_sbj[idx]
                step_last = idx
                break
        for idx in index_list:
            if dx_sbj[idx] is not None:
                dx_init = dx_sbj[idx]
                step_init = idx
                break
        auxiliary_dict[rid] = {}
        auxiliary_dict[rid]['dx'] = int(str(dx_init) + str(dx_last))

    return auxiliary_dict
