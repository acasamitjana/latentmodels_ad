import numpy as np
from os.path import join
import csv
from src.utils.preprocessing import get_float


# ADNI dictionary structure:
# Subject_dict = {keys: subject rid, values: events_dict}
# Events_dicts = {keys: event.ptid, values, features}
# Feature list = ordered list referencing feature names in events_dict.

ADNI_PATH = '/projects/neuro/ADNI'
WMH_FILES = ['UCD_ADNI1_WMH.csv', 'UCD_ADNI2_WMH_10_26_15.csv']
def load_WMH():
    subjects_dict = {}
    for file in WMH_FILES:
        with open(join(ADNI_PATH, file), 'r') as csvfile:
            csvreader = csv.DictReader(csvfile)
            header = csvreader.fieldnames
            for row in csvreader:
                if row['RID'] not in subjects_dict.keys():
                    subjects_dict[row['RID']] = {}
                if 'VISCODE2' in header:
                    key = row['VISCODE2']
                    if key == 'sc':
                        key = 'bl'
                    subjects_dict[row['RID']][key] = get_float(row['WHITMATHYP'])
                else:
                    key = row['VISCODE']
                    if key == 'sc':
                        key = 'bl'
                    subjects_dict[row['RID']][key] = get_float(row['WHITMATHYP'])


    return subjects_dict


PC_FILE = 'ADNI_CEU08_ldpr_rel_pca.eigenvec'
def load_PC(n_components=5):

    if n_components > 20:
        raise ValueError("Only 20 components are found for ADNI dataset")

    pc_dict = {}
    with open(join(ADNI_PATH, PC_FILE), 'r') as csvfile:
        csvreader = csv.reader(csvfile, delimiter = ' ')
        for row in csvreader:
            pc_dict[str(int(row[0].split('_')[2]))] = [float(r) for r in row[2:2+n_components]]

    return pc_dict


