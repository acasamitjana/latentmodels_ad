import csv
from os.path import join, exists, basename
from utils.preprocessing import get_float
import gzip
import os
import shutil
import numpy as np
from collections import OrderedDict


UKBB_DATA_DIR = '/projects/neuro/UKBiobank_GWAS'
UKBB_GWAS_file = 'UKBB_GWAS_Imputed_v3-File_Manifest_Release_20180731-Manifest_201807.csv'
UKBB_h2_file = 'ukbb_all_h2univar_results.txt'
UKBB_phenotyes_file = 'phenotypes.both_sexes.tsv'
UKBB_variants_file = 'variants.tsv.bgz'
UKBB_snp_chromosome_fiile = 'snp_chromosome.csv'


def print_chromosomes(ch_list):
    chromosome_array = np.asarray(ch_list)
    num_chromosomes = len(np.unique(chromosome_array))
    for it_ch in range(1,num_chromosomes):
        index_ch = np.where(chromosome_array == it_ch)[0]
        print(str(np.min(index_ch)) + '_' + str(np.max(index_ch)))

def create_base_file(variants_filepath, snp_filepath, output_filepath, header):
    tsvfile1 = gzip.open(variants_filepath, 'rt')
    tsvfile2 = gzip.open(snp_filepath, 'rt')
    outfile = open(output_filepath, 'wt', newline='', encoding='utf-8')
    tsvreader1 = csv.DictReader(tsvfile1, delimiter = '\t')
    tsvreader2 = csv.DictReader(tsvfile2, delimiter = '\t')
    outwriter = csv.writer(outfile, delimiter = ' ')

    outwriter.writerow(header)

    for row1, row2 in zip(tsvreader1, tsvreader2):
        if row1['chr'] == 'X':
            break
        outwriter.writerow([row1['variant'],  row1['rsid'],  int(row1['pos']),
                          int(row1['chr']),  row1['ref'],
                          row1['alt'],  get_float(row2['beta']),
                          get_float(row2['pval']),  get_float(row2['tstat']),
                          get_float(row2['se']), get_float(row1['info'])])


def read_SNP(filepath, read_only_rows = 99999999999):

    with gzip.open(filepath, 'rt', encoding='utf-8') as tsvfile:
        tsvreader = csv.DictReader(tsvfile, delimiter='\t') #['variant', 'minor_allele', 'minor_AF', 'expected_case_minor_AC', 'low_confidence_variant', 'n_complete_samples', 'AC', 'ytx', 'beta', 'se', 'tstat', 'pval']
        results = OrderedDict({'variant': [], 'pval': [], 'beta': [], 'tstat': [], 'se': []})
        it_rows = 0
        for row in tsvreader:
            results['variant'].append(row['variant'])
            results['pval'].append(get_float(row['pval']))
            results['beta'].append(get_float(row['beta']))
            results['tstat'].append(get_float(row['tstat']))
            results['se'].append(get_float(row['se']))
            if it_rows > read_only_rows:
                break
            else:
                it_rows+=1

    return results

def read_snp_chromosome_correspondence():
    chromosome_range_dict = {}
    with open(join(UKBB_DATA_DIR, UKBB_snp_chromosome_fiile), 'r') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter=',')
        for row in csvreader:
            chromosome_range_dict[int(row['Chromosome'])] = (int(row['SNP_init']),int(row['SNP_fi']))

    return chromosome_range_dict


def read_genetic_variants_chromosome_summary():

    variants_dict = {}
    with gzip.open(join(UKBB_DATA_DIR,UKBB_variants_file), 'rt', encoding='utf-8') as tsvfile:
        tsvreader = csv.DictReader(tsvfile, delimiter='\t')
        for row in tsvreader:
            try:
                variants_dict[row['variant']] = int(row['chr'])
            except:
                variants_dict[row['variant']] = 23


    return variants_dict

def read_genetic_variants_summary():

    variants_dict = {}
    with gzip.open(join(UKBB_DATA_DIR,UKBB_variants_file), 'rt', encoding='utf-8') as tsvfile:
        tsvreader = csv.DictReader(tsvfile, delimiter='\t')
        for row in tsvreader:
            variants_dict[row['variant']] = {'chromosome':row['chr'], 'rsid': row['rsid'], 'varid': row['varid'],
                                             'ref': row['ref'], 'alt': row['alt'], 'pos': row['pos'],
                                             'info': row['info']}

    return variants_dict

def read_phenotype_summary():

    with open(join(UKBB_DATA_DIR,UKBB_phenotyes_file), 'rt') as tsvfile:
        tsvreader = csv.DictReader(tsvfile, delimiter = '\t')
        variable_type_dict = {'categorical': 0, 'ordinal': 0, 'binary': 0, 'continuous_irnt': 0, 'continuous_raw': 0}
        source_tf_dict = {'icd10': 0, 'finngen': 0, 'phesant': 0, 'covariate': 0}

        headers = tsvreader.fieldnames #['phenotype', 'description', 'variable_type', 'source', 'n_non_missing', 'n_missing', 'n_controls', 'n_cases', 'PHESANT_transformation', 'notes']
        for line in tsvreader:
            variable_type_dict[line['variable_type']] += 1
            source_tf_dict[line['source']] += 1
            if line['source'] == 'covariate':
                print(line['description'])
                print(line['PHESANT_transformation'])
                print(line['variable_type'])
                print('\n')



def get_phenotype_dict():

    filepath = join(UKBB_DATA_DIR,UKBB_GWAS_file)
    output_dict = {}

    with open(filepath, 'rt') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            output_dict[row['Phenotype Code'] + '_' + row['Sex']] =  {'wget': row['wget command'],
                                                                      'phenotype_description': row['Phenotype Description'],
                                                                      'file': row['File'],
                                                                      'sex': row['Sex']}

    return output_dict

def get_phenotype(phenotype_dict, phenotype_key, output_directory = UKBB_DATA_DIR):

    filename = basename(phenotype_dict[phenotype_key]['file'])
    if exists(join(output_directory, filename)):
        print("##### FILE " + filename + ' already exists in ' + output_directory)
    else:
        os.system(phenotype_dict[phenotype_key]['wget'])
        shutil.move(filename, join(output_directory,filename))

    return filename

def get_phenotype_heritability():
    filepath = join(UKBB_DATA_DIR, UKBB_h2_file)
    output_dict = {}

    with open(filepath, 'rt') as csvfile:
        csvreader = csv.DictReader(csvfile, delimiter = '\t')
        for row in csvreader:
            output_dict[row['phenotype']] = get_float(row['h2_p'])

    return output_dict








