from os.path import join
import csv



dir_GENERATED_TABLES = '/work/acasamitjana/Datasets/ADNI/Tables/Generated'
dir_TADPOLE = '/projects/neuro/TADPOLE'


file_FSLABELS = 'FS_labels.csv'
file_TADPOLEDICT = 'TADPOLE_D1_D2_Dict.csv'
file_CT = 'legend_cortical_thickness' + '.csv'


def generate_legend0(outputfile, pattern_str):

    to_write = []
    with open(join(dir_GENERATED_TABLES, file_FSLABELS), 'r') as infile:
        csv_reader = csv.DictReader(infile)
        for row in csv_reader:
            if any([p_str in row['FEATURE_CODE'] for p_str in pattern_str]):
                to_write.append({'FEATURE_CODE': row['FEATURE_CODE'], 'NAME': row['NAME']})

    with open(join(dir_TADPOLE,outputfile),'w') as outfile:
        csv_writer = csv.DictWriter(outfile, fieldnames=['FEATURE_CODE', 'NAME'])
        csv_writer.writeheader()
        for row in to_write:
            csv_writer.writerow(row)



def generate_legend1(outputfile, pattern_str):

    to_write = []
    with open(join(dir_TADPOLE, file_TADPOLEDICT), 'r') as infile:
        csv_reader = csv.DictReader(infile)
        print(csv_reader.fieldnames)
        n_rows = 0
        for row in csv_reader:
            n_rows += 1
            if any([p_str in row['FLDNAME'] for p_str in pattern_str]):
                print(row['FLDNAME'])
                to_write.append({'FEATURE_CODE': row['FLDNAME'], 'NAME': row['TEXT'].split(' ')[-1]})

    with open(join(dir_TADPOLE,outputfile),'w') as outfile:
        csv_writer = csv.DictWriter(outfile, fieldnames=['FEATURE_CODE', 'NAME'])
        csv_writer.writeheader()
        for row in to_write:
            csv_writer.writerow(row)