import csv
import warnings
from os.path import join
from src.utils.io_utils import ResultsWriter
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, roc_auc_score, \
    precision_recall_curve, roc_curve, confusion_matrix
from sklearn.exceptions import UndefinedMetricWarning

warnings.simplefilter(action='ignore', category=UndefinedMetricWarning)


def get_float(value):
    try:
        return float(value)
    except:
        return None

INPUT_PATH = '/work/acasamitjana/Multimodal_autoencoders/AAE_multiview_semisupervised/nc_16/lineal/av45_vMRI_cMRI_clinical/Y_SCORES'
## Read Y-scores
DataLoader = data_reader.DataLoader_semisupervised()
writer = ResultsWriter(filepath=join(INPUT_PATH, 'classification.txt'), attach=False)

dx_list = [{0:1,1:0,2:0}, {0:0,1:1,2:0}, {0:0,1:0,2:1}]
for DX_DICT in dx_list:
    DIAGNOSIS_1_AB_0 = 1
    if DIAGNOSIS_1_AB_0 == 1:
        dx_dict, dx_labels_dict = DataLoader.load_dx_baseline()

        valid_dx = [0,1, 2]
        dx_dict = {rid: DX_DICT[dx] for rid, dx in dx_dict.items() if dx in valid_dx}
        dx_labels_dict = {rid: dx_labels_dict[rid] for rid, dx in dx_dict.items() if dx in valid_dx}
        rid_list = [rid for rid in dx_dict.keys() if dx_labels_dict[rid] == 1]

    else:
        dx_dict, dx_labels_dict = DataLoader.load_dx_baseline()
        abstatus_dict, abstatus_labels_dict = DataLoader.load_amyloid_status_baseline()

        abstatus_dict = {rid: ab for rid, ab in abstatus_dict.items() if dx_dict[rid] == 0}
        abstatus_labels_dict = {rid: abstatus_labels_dict[rid] for rid, ab in abstatus_dict.items() if dx_dict[rid] == 0}
        rid_list = [rid for rid in abstatus_dict.keys() if abstatus_labels_dict[rid] == 1]

    modality_list = ['av45', 'vMRI', 'clinical']
    modality_dict_list = []
    for modality in modality_list:
        modality_dict = {}
        with open(join(INPUT_PATH,modality + '.csv'), 'r') as csvfile:
            csvreader = csv.DictReader(csvfile)
            header = csvreader.fieldnames
            M = len(header) - 1
            for row in csvreader:
                modality_dict[row['RID']] = [get_float(row[h]) for h in header[1:]]

        modality_dict_list.append(modality_dict)


    modality_array_list = [np.zeros((len(mdl.keys()),M)) for mdl in modality_dict_list]


    ## Compute mean and variance layers
    rid_exclude = []
    mean_array = np.zeros((len(rid_list), M))
    var_array = np.zeros((len(rid_list), M))
    for it_rid, rid in enumerate(rid_list):
        n_sbj = [np.sum([1 for mdl in modality_dict_list if mdl[rid][m] is not None]) for m in range(M)]
        if 0 in n_sbj:
            rid_exclude.append(rid)
        else:
            mean_sbj = [np.sum([mdl[rid][m] for mdl in modality_dict_list if mdl[rid][m] is not None])/n_sbj[m] for m in range(M)]
            var_sbj = [np.sum([(mdl[rid][m] - mean_sbj[m])**2 for mdl in modality_dict_list if mdl[rid][m] is not None])/n_sbj[m] for m in range(M)]

            mean_array[it_rid - len(rid_exclude)] = mean_sbj
            var_array[it_rid - len(rid_exclude)] = var_sbj

    rid_list = [rid for rid in rid_list if rid not in rid_exclude]
    mean_array = mean_array[:mean_array.shape[0]-len(rid_exclude)]
    var_array = var_array[:var_array.shape[0]-len(rid_exclude)]


    if DIAGNOSIS_1_AB_0 == 1:
        Y_array = np.asarray([dx for rid, dx in dx_dict.items() if rid in rid_list])

    else:
        Y_array = np.asarray([dx for rid, dx in abstatus_dict.items() if rid in rid_list])

    print('N: ' + str(mean_array.shape[0]))
    print('M: ' + str(mean_array.shape[1]))

    ## Logistic regression
    X_array = mean_array

    if len(np.unique(Y_array)) == 2:
        train_results = {'accuracy': [], 'recall': [], 'precision': [], 'specificity': [],  'f1': [], 'auc': []}
        test_results = {'accuracy': [], 'recall': [], 'precision': [], 'specificity': [],  'f1': [], 'auc': []}

    else:
        train_results = {'confusion_matrix': np.zeros((len(np.unique(Y_array)),len(np.unique(Y_array))))}
        test_results = {'confusion_matrix': np.zeros((len(np.unique(Y_array)),len(np.unique(Y_array))))}

    n_splits = 20
    train_size = 0.8
    sss = StratifiedShuffleSplit(n_splits = n_splits, train_size=train_size)
    it_split = 0


    for train_index, test_index in sss.split(X_array,Y_array):
        # print('Split ' + str(it_split) + '/' + str(n_splits))
        it_split +=1
        X_train = X_array[train_index]
        X_test = X_array[test_index]

        y_train = Y_array[train_index]
        y_test = Y_array[test_index]

        lr_model = LogisticRegression(solver='liblinear', multi_class='auto')

        lr_model.fit(X_train, y_train)
        y_train_predicted = lr_model.predict(X_train)
        y_test_predicted = lr_model.predict(X_test)

        if len(np.unique(Y_array)) == 2:

            train_results['accuracy'].append(accuracy_score(y_train, y_train_predicted))
            train_results['recall'].append(recall_score(y_train, y_train_predicted, pos_label=np.max(np.unique(Y_array))))
            train_results['precision'].append(precision_score(y_train, y_train_predicted, pos_label=np.max(np.unique(Y_array))))
            train_results['specificity'].append(recall_score(y_train, y_train_predicted, pos_label=np.min(np.unique(Y_array))))
            train_results['f1'].append(f1_score(y_train, y_train_predicted, pos_label=np.max(np.unique(Y_array))))
            train_results['auc'].append(roc_auc_score(y_train, y_train_predicted))

            test_results['accuracy'].append(accuracy_score(y_test, y_test_predicted))
            test_results['recall'].append(recall_score(y_test, y_test_predicted, pos_label=np.max(np.unique(Y_array))))
            test_results['precision'].append(precision_score(y_test, y_test_predicted, pos_label=np.max(np.unique(Y_array))))
            test_results['specificity'].append(recall_score(y_test, y_test_predicted, pos_label=np.min(np.unique(Y_array))))
            test_results['f1'].append(f1_score(y_test, y_test_predicted, pos_label=np.max(np.unique(Y_array))))
            test_results['auc'].append(roc_auc_score(y_test, y_test_predicted))

        else:
            train_results['confusion_matrix'] += confusion_matrix(y_train, y_train_predicted, labels=np.sort(np.unique(Y_array)))
            test_results['confusion_matrix'] += confusion_matrix(y_test, y_test_predicted, labels=np.sort(np.unique(Y_array)))


    #Results

    if len(np.unique(Y_array)) == 2:
        print('TRAINING RESULTS')
        print('==> Global')
        print('     - Accuracy: ' + str(np.mean(train_results['accuracy'])))
        print('     - Recall: ' + str(np.mean(train_results['recall'])))
        print('     - Precision: ' + str(np.mean(train_results['precision'])))
        print('     - F1: ' + str(np.mean(train_results['f1'])))
        print('     - Specificity: ' + str(np.mean(train_results['specificity'])))
        print('     - AUC: ' + str(np.mean(train_results['auc'])))

        print('')
        print('TESTING RESULTS')
        print('==> Global')
        print('     - Accuracy: ' + str(np.mean(test_results['accuracy'])))
        print('     - Recall: ' + str(np.mean(test_results['recall'])))
        print('     - Precision: ' + str(np.mean(test_results['precision'])))
        print('     - F1: ' + str(np.mean(test_results['f1'])))
        print('     - Specificity: ' + str(np.mean(test_results['specificity'])))
        print('     - AUC: ' + str(np.mean(test_results['auc'])))

        if DIAGNOSIS_1_AB_0 == 1:
            writer.write('DIAGNOSIS CLASSIFICATION: ')
            writer.write(str(DX_DICT))
            writer.write('\n')
        else:
            writer.write('AMYLOID POSITIVITY CLASSIFICATION: ')


        writer.write('TRAINING RESULTS')
        writer.write('\n')
        writer.write('==> Global')
        writer.write('\n')
        writer.write('     - Accuracy: ' + str(np.mean(train_results['accuracy'])))
        writer.write('\n')
        writer.write('     - Recall: ' + str(np.mean(train_results['recall'])))
        writer.write('\n')
        writer.write('     - Precision: ' + str(np.mean(train_results['precision'])))
        writer.write('\n')
        writer.write('     - F1: ' + str(np.mean(train_results['f1'])))
        writer.write('\n')
        writer.write('     - Specificity: ' + str(np.mean(train_results['specificity'])))
        writer.write('\n')
        writer.write('     - AUC: ' + str(np.mean(train_results['auc'])))

        writer.write('\n\n')
        writer.write('TESTING RESULTS')
        writer.write('\n')
        writer.write('==> Global')
        writer.write('\n')
        writer.write('     - Accuracy: ' + str(np.mean(test_results['accuracy'])))
        writer.write('\n')
        writer.write('     - Recall: ' + str(np.mean(test_results['recall'])))
        writer.write('\n')
        writer.write('     - Precision: ' + str(np.mean(test_results['precision'])))
        writer.write('\n')
        writer.write('     - F1: ' + str(np.mean(test_results['f1'])))
        writer.write('\n')
        writer.write('     - Specificity: ' + str(np.mean(test_results['specificity'])))
        writer.write('\n')
        writer.write('     - AUC: ' + str(np.mean(test_results['auc'])))
        writer.write('\n\n')


    else:
        print('TRAINING RESULTS')
        print(' - Confusion matrix: ')
        print(train_results['confusion_matrix'])

        print()
        print('TESTING RESULTS')
        print(' - Confusion matrix: ')
        print(test_results['confusion_matrix'])

        if DIAGNOSIS_1_AB_0 == 1:
            writer.write('DIAGNOSIS CLASSIFICATION: ')
            writer.write(str(DX_DICT))
            writer.write('\n')
        else:
            writer.write('AMYLOID POSITIVITY CLASSIFICATION: ')

        writer.write('TRAINING RESULTS')
        writer.write('\n')
        writer.write(' - Confusion matrix: ')
        writer.write('\n')
        writer.write(train_results['confusion_matrix'])
        writer.write('\n')

        writer.write('\n')
        writer.write('TESTING RESULTS')
        writer.write('\n')
        writer.write(' - Confusion matrix: ')
        writer.write('\n')
        writer.write(test_results['confusion_matrix'])
        writer.write('\n\n')


