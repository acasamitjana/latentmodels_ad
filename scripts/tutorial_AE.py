import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from torchvision import datasets, transforms

from src.utils.io_utils import ConfigFile
from src.models import AutoencoderCNN

# Training scheme
def train(model, device, generator_train, optimizer, epoch, **kwargs):
    model.train() #Initialize self.training = True
    for batch_idx, (data,target) in enumerate(generator_train):
        data, target = data.to(device), target.to(device) #sento to either gpu or cpu
        optimizer.zero_grad() #Initialize gradients=0 in all layers, otherwise it accumulates th gradient from previous optimitzation steps
        output_rec, output_pred = model(data) #Feed-forward pass
        loss_pred = F.nll_loss(output_pred,target) #negative log-likelihood: it is used in conjuntion with log_softmax in the end of the newtork. Use cross_entropy if only softmax() is used..
        loss_rec = F.mse_loss(output_rec,data)

        #Multitask loss: composite loss
        loss_pred.backward(retain_graph=True) #compute the gradients at each layer, which are stored at each class (not yet applied)
        loss_rec.backward() #compute the gradients at each layer, which are stored at each class (not yet applied)
        optimizer.step() #uses the gradients computed in the previous step to update weights

        if batch_idx % kwargs['log_interval'] == 0: #Logging
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss_pred: {:.6f} Loss_rec: {:.6f}'.format(
                epoch, batch_idx * len(data), len(generator_train.dataset),
                       100. * batch_idx / len(generator_train), loss_pred.item(), loss_rec.item()))


# Testing scheme
def test(model, device, generator_test, **kwargs):
    model.eval() #Initialize self.training = False
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data,target in generator_test:
            data, target = data.to(device), target.to(device)
            _, output_pred = model(data)
            test_loss += F.nll_loss(output_pred, target, reduction='sum').item() #sum up all batch loss
            pred = output_pred.max(1, keepdim=True)[1] # get the index of the max log-probability for the accuracy computation
            correct += pred.eq(target.view_as(pred)).sum().item() #number of correct predictions


    test_loss /= len(generator_test.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(generator_test.dataset),
        100. * correct / len(generator_test.dataset)))




###########################################################################
############################### Main ######################################
###########################################################################

#Load parameters from YAML
config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/tutorial_AE.yaml')

use_GPU = config_file.use_GPU
batch_size = config_file.batch_size
use_cuda =  use_GPU and torch.cuda.is_available()
learning_rate = config_file.learning_rate
momentum = config_file.momentum
n_epochs = config_file.n_epochs

kwargs_training={'log_interval' : config_file.log_interval } #Nmber of steps
kwargs_testing={}
kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
device = torch.device("cuda:0" if use_cuda else "cpu")

# Load data + build generators
generator_train = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=True, download=True, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
    batch_size=batch_size, shuffle=True, **kwargs_generator
)

generator_test = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=False, download=True, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,), (0.3081,))
                       ])),
    batch_size=batch_size, shuffle=True, **kwargs_generator
)


# Compile: initialize model + set the optimizer

model = AutoencoderCNN(input_size=(28,28)).to(device) # Initialize network + send to GPU/CPU
optimizer = optim.SGD(model.parameters(), lr=learning_rate, momentum=momentum)


# Train: epochs
for epoch in range(1, n_epochs + 1):
    train(model, device, generator_train, optimizer, epoch, **kwargs_training)
    test(model, device, generator_test, **kwargs_testing)
