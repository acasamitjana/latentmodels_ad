import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image
from torch.autograd import Variable


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
import numpy as np

from Data.TADPOLE.tadpole_reader import *
from Data.TADPOLE import data_reader
from src.utils.io_utils import ResultsWriter, ROW_DICT, ConfigFile, CSVWriter
from src.models import VAE, VAE_Linear
from src.dataset import MyDataset
from src.callbacks import History

from matplotlib import pyplot as plt
import os
from os.path import exists, join
import itertools

plt.switch_backend('Agg')

torch.manual_seed(42)
torch.cuda.manual_seed(42)
np.random.seed(42)
torch.backends.cudnn.deterministic=True

N_COMPONENTS = 16

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'Multiple_events','VAE_multichannel', 'nc_' + str(N_COMPONENTS))

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)


clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']


def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_dim, logger, *args, **kwargs):
    model_dict['vae_1'].train(mode=True)
    model_dict['vae_2'].train(mode=True)
    model_dict['vae_3'].train(mode=True)
    model_dict['vae_4'].train(mode=True)

    for batch_idx, (data, target) in enumerate(generator_train):
        data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)  # sento to either gpu or cpu

        for model_name, model in model_dict.items():
            model.zero_grad()

        #### LATENT LOSS
        z_mean_1, z_log_var_1 = model_dict['vae_1'].encode(data_1)
        z_mean_2, z_log_var_2 = model_dict['vae_2'].encode(data_2)
        z_mean_3, z_log_var_3 = model_dict['vae_3'].encode(data_3)
        z_mean_4, z_log_var_4 = model_dict['vae_4'].encode(data_4)

        z_1 = model_dict['vae_1'].reparameterize(z_mean_1, z_log_var_1)
        z_2 = model_dict['vae_2'].reparameterize(z_mean_2, z_log_var_2)
        z_3 = model_dict['vae_3'].reparameterize(z_mean_3, z_log_var_3)
        z_4 = model_dict['vae_4'].reparameterize(z_mean_4, z_log_var_4)

        x_reconstructed_1_1 = model_dict['vae_1'].decode(z_1)  # decode to X reconstruction
        x_reconstructed_1_2 = model_dict['vae_1'].decode(z_2)  # decode to X reconstruction
        x_reconstructed_1_3 = model_dict['vae_1'].decode(z_3)  # decode to X reconstruction
        x_reconstructed_1_4 = model_dict['vae_1'].decode(z_4)  # decode to X reconstruction

        x_reconstructed_2_1 = model_dict['vae_2'].decode(z_1)  # decode to X reconstruction
        x_reconstructed_2_2 = model_dict['vae_2'].decode(z_2)  # decode to X reconstruction
        x_reconstructed_2_3 = model_dict['vae_2'].decode(z_3)  # decode to X reconstruction
        x_reconstructed_2_4 = model_dict['vae_2'].decode(z_4)  # decode to X reconstruction

        x_reconstructed_3_1 = model_dict['vae_3'].decode(z_1)  # decode to X reconstruction
        x_reconstructed_3_2 = model_dict['vae_3'].decode(z_2)  # decode to X reconstruction
        x_reconstructed_3_3 = model_dict['vae_3'].decode(z_3)  # decode to X reconstruction
        x_reconstructed_3_4 = model_dict['vae_3'].decode(z_4)  # decode to X reconstruction

        x_reconstructed_4_1 = model_dict['vae_4'].decode(z_1)  # decode to X reconstruction
        x_reconstructed_4_2 = model_dict['vae_4'].decode(z_2)  # decode to X reconstruction
        x_reconstructed_4_3 = model_dict['vae_4'].decode(z_3)  # decode to X reconstruction
        x_reconstructed_4_4 = model_dict['vae_4'].decode(z_4)  # decode to X reconstruction

        ####### LOSSES
        kl_loss_1 = -0.5 * torch.sum(1 + z_log_var_1 - z_mean_1.pow(2) - z_log_var_1.exp(), dim=-1)
        kl_loss_2 = -0.5 * torch.sum(1 + z_log_var_2 - z_mean_2.pow(2) - z_log_var_2.exp(), dim=-1)
        kl_loss_3 = -0.5 * torch.sum(1 + z_log_var_3 - z_mean_3.pow(2) - z_log_var_3.exp(), dim=-1)
        kl_loss_4 = -0.5 * torch.sum(1 + z_log_var_4 - z_mean_4.pow(2) - z_log_var_4.exp(), dim=-1)

        kl_loss = kl_loss_1 + kl_loss_2 + kl_loss_3 + kl_loss_4


        reconstruction_loss_1 = ((x_reconstructed_1_1 - data_1)**2).sum(dim=-1) +\
                                ((x_reconstructed_1_2 - data_1)**2).sum(dim=-1) +\
                                ((x_reconstructed_1_3 - data_1)**2).sum(dim=-1) +\
                                ((x_reconstructed_1_4 - data_1)**2).sum(dim=-1)

        reconstruction_loss_2 = ((x_reconstructed_2_1 - data_2)**2).sum(dim=-1) +\
                                ((x_reconstructed_2_2 - data_2)**2).sum(dim=-1) +\
                                ((x_reconstructed_2_3 - data_2)**2).sum(dim=-1) +\
                                ((x_reconstructed_2_4 - data_2)**2).sum(dim=-1)

        reconstruction_loss_3 = ((x_reconstructed_3_1 - data_3)**2).sum(dim=-1) +\
                                ((x_reconstructed_3_2 - data_3)**2).sum(dim=-1) +\
                                ((x_reconstructed_3_3 - data_3)**2).sum(dim=-1) +\
                                ((x_reconstructed_3_4 - data_3)**2).sum(dim=-1)

        reconstruction_loss_4 = ((x_reconstructed_4_1 - data_4)**2).sum(dim=-1) +\
                                ((x_reconstructed_4_2 - data_4)**2).sum(dim=-1) +\
                                ((x_reconstructed_4_3 - data_4)**2).sum(dim=-1) +\
                                ((x_reconstructed_4_4 - data_4)**2).sum(dim=-1)

        reconstruction_loss = reconstruction_loss_1 + reconstruction_loss_2 + reconstruction_loss_3 + reconstruction_loss_4

        total_loss = torch.mean(reconstruction_loss + kl_loss)


        total_loss.backward()
        torch.nn.utils.clip_grad_norm_(model_dict['vae_1'].parameters(), 1.0)
        torch.nn.utils.clip_grad_norm_(model_dict['vae_2'].parameters(), 1.0)
        torch.nn.utils.clip_grad_norm_(model_dict['vae_3'].parameters(), 1.0)
        torch.nn.utils.clip_grad_norm_(model_dict['vae_4'].parameters(), 1.0)

        optimizer_dict['vae'].step()
        # optimizer_dict['vae_1'].step()
        # optimizer_dict['vae_2'].step()
        # optimizer_dict['vae_3'].step()
        # optimizer_dict['vae_4'].step()

        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': torch.mean(reconstruction_loss).item(),
                        'kl_loss': torch.mean(kl_loss).item(),
                        'loss': total_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)
    return logger


def predict_latent(model, generator_test, prediction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(prediction_size)
    labels = torch.zeros(num_elements, 1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            mu, logvar = model.encode(data)
            torch.manual_seed(42)
            torch.cuda.manual_seed(42)
            pred = model.reparameterize(mu, logvar)

            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()

    return predictions.detach().numpy(), labels.detach().numpy()


def predict_decoder(model, generator_test, prediction_size):
    model.train(mode=False)

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(prediction_size)
    labels = torch.zeros(num_elements, 1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            pred = model.decode(data)

            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()

    return predictions.detach().numpy(), labels.detach().numpy()


if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    DataLoader = data_reader.DataLoader_Longitudinal_semisupervised()

    ## Dictionaries
    av45_dict, av45_labels_dict = DataLoader.load_modality(modality_name='av45_pet')
    vMRI_dict, vMRI_labels_dict = DataLoader.load_modality(modality_name='volumetric_mri')
    cMRI_dict, cMRI_labels_dict = DataLoader.load_modality(modality_name='cortical_thickness_mri')
    FDG_dict, FDG_labels_dict = DataLoader.load_modality(modality_name='fdg_pet')
    csf_dict, csf_labels_dict = DataLoader.load_modality(modality_name='csf')
    imaging_phenotypes_dict, imaging_phenotypes_labels_dict = DataLoader.load_modality( modality_name='imaging_phenotypes')
    education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
    age_dict, age_labels_dict = DataLoader.load_age()
    clinical_dict, clinical_labels_dict = DataLoader.load_clinical(clinical_names=clinical_names)
    dx_dict, dx_labels_dict = DataLoader.load_dx()

    rid_events_dict = {rid: [e.step for e in subject.events] for rid, subject in DataLoader.subjects_dict.items()}
    rid_events_list = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list]

    test_dict = {rid: {e_step: [age_dict[rid][e_step]] + clinical_dict[rid][e_step] + education_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}
    test_labels_dict = {{e_step: age_labels_dict[rid][e_step] * clinical_labels_dict[rid][e_step] * education_labels_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}

    ## Codenames
    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    clinical_id_list = clinical_names
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    test_id_list = ['age'] + clinical_names + ['education']

    ## Modalities used
    modality_list = ['av45', 'vMRI', 'FDG', 'test']
    modality_dict_list = [av45_dict, vMRI_dict, FDG_dict, test_dict]
    modality_labels_dict_list = [av45_labels_dict, vMRI_labels_dict, FDG_labels_dict, test_labels_dict]
    modality_id_list = [av45_id_list, vMRI_id_list, FDG_id_list, test_id_list]

    rid_events_list_all_modalities = data_reader.find_list_tuple_intersection([
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[0][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[1][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[2][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[3][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         dx_dict[rid][e_step] == 1]
    ])

    m1_array = np.asarray([modality_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m1_array.shape) < 2:
        m1_array = m1_array.reshape(-1, 1)
    m1_labels_array = np.asarray([modality_labels_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m2_array = np.asarray([modality_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m2_array.shape) < 2:
        m2_array = m2_array.reshape(-1, 1)
    m2_labels_array = np.asarray([modality_labels_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m3_array = np.asarray([modality_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m3_array.shape) < 2:
        m3_array = m3_array.reshape(-1, 1)
    m3_labels_array = np.asarray([modality_labels_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m4_array = np.asarray([modality_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m4_array.shape) < 2:
        m4_array = m4_array.reshape(-1, 1)
    m4_labels_array = np.asarray([modality_labels_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    dx_array = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(dx_array.shape) < 2:
        dx_array = dx_array.reshape(-1, 1)
    dx_labels_array = np.asarray([dx_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    ## PRE-PROCESSING

    std_scaler = StandardScaler()
    m1_array[np.where(m1_labels_array == 1)] = std_scaler.fit_transform(m1_array[np.where(m1_labels_array == 1)])
    m2_array[np.where(m2_labels_array == 1)] = std_scaler.fit_transform(m2_array[np.where(m2_labels_array == 1)])
    m3_array[np.where(m3_labels_array == 1)] = std_scaler.fit_transform(m3_array[np.where(m3_labels_array == 1)])
    m4_array[np.where(m4_labels_array == 1)] = std_scaler.fit_transform(m4_array[np.where(m4_labels_array == 1)])


    N1, M1 = m1_array.shape
    N2, M2 = m2_array.shape
    N3, M3 = m3_array.shape
    N4, M4 = m4_array.shape
    N = [N1, N2, N3, N4]
    M = [M1, M2, M3, M4]

    print('N1: ' + str(N1) + ', M1: ' + str(M1))
    print('N2: ' + str(N1) + ', M2: ' + str(M2))
    print('N3: ' + str(N1) + ', M3: ' + str(M3))
    print('N4: ' + str(N1) + ', M4: ' + str(M4))



    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/VAE_multichannel.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    hidden_layers_dim_1 = [N_COMPONENTS]
    hidden_layers_dim_2 = [N_COMPONENTS]
    hidden_layers_dim_3 = [N_COMPONENTS]
    hidden_layers_dim_4 = [N_COMPONENTS]

    latent_dim = N_COMPONENTS

    VAE_1 = VAE(M1, hidden_layers_dim_1)
    VAE_2 = VAE(M2, hidden_layers_dim_2)
    VAE_3 = VAE(M3, hidden_layers_dim_3)
    VAE_4 = VAE(M4, hidden_layers_dim_4)


    # Set learning rates
    vae_lr = learning_rate

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    # encode/decode optimizers
    optim_VAE = torch.optim.Adam(list(VAE_1.parameters()) + list(VAE_2.parameters()) +
                                 list(VAE_3.parameters()) + list(VAE_4.parameters()), lr=vae_lr)
    optim_VAE_1 = torch.optim.Adam(VAE_1.parameters(), lr=vae_lr)
    optim_VAE_2 = torch.optim.Adam(VAE_2.parameters(), lr=vae_lr)
    optim_VAE_3 = torch.optim.Adam(VAE_3.parameters(), lr=vae_lr)
    optim_VAE_4 = torch.optim.Adam(VAE_4.parameters(), lr=vae_lr)


    model_dict = {'vae_1': VAE_1, 'vae_2': VAE_2, 'vae_3': VAE_3, 'vae_4': VAE_4}
    optimizer_dict = {'vae': optim_VAE}#{'vae_1': optim_VAE_1, 'vae_2': optim_VAE_2, 'vae_3': optim_VAE_3, 'vae_4': optim_VAE_4}

    #################################
    ##########  READ DATA  ##########
    #################################

    dataset = MyDataset([m1_array, m2_array, m3_array, m4_array], dx_array)
    generator_train = torch.utils.data.DataLoader(
        dataset,
        batch_size=N1,
        shuffle=True,
        num_workers=2,
        pin_memory=torch.cuda.is_available()

    )

    dataset_1 = MyDataset(m1_array, dx_array)
    dataset_2 = MyDataset(m2_array, dx_array)
    dataset_3 = MyDataset(m3_array, dx_array)
    dataset_4 = MyDataset(m4_array, dx_array)

    generator_test_1 = torch.utils.data.DataLoader(
        dataset_1,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_2 = torch.utils.data.DataLoader(
        dataset_2,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_3 = torch.utils.data.DataLoader(
        dataset_3,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_4 = torch.utils.data.DataLoader(
        dataset_4,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    ################################
    ##########  TRAINING  ##########
    ################################
    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    log_keys = ['reconstruction_loss', 'kl_loss', 'loss']
    logger = History()
    logger.on_train_init(log_keys)
    for epoch in range(1, n_epochs + 1):
        logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)


    writer = ResultsWriter(filepath=join(MODALITY_PATH, 'results_file.txt'), attach=False)

    plt.figure()
    plt.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
    plt.plot(logger.logs['kl_loss'], 'r', label='KL_loss')
    plt.plot(logger.logs['loss'], 'k', label='Total loss')

    plt.legend()
    plt.savefig(join(MODALITY_PATH, 'training.png'))
    plt.close()

    ###############################
    ##########  RESULTS  ##########
    ###############################
    writer_list = []
    for it_component in range(N_COMPONENTS):
        COMPONENTS_PATH = join(MODALITY_PATH, str(it_component))
        if not exists(COMPONENTS_PATH):
            os.makedirs(COMPONENTS_PATH)

        writer_list.append(ResultsWriter(filepath=join(COMPONENTS_PATH, 'results_file.txt'), attach=False))

    CORRELATION_PLOTS = True
    Y_SCORE_PLOTS = True
    STATISTICAL_PLOTS = True
    WEIGHT_PLOTS = True
    FACTOR_PLOTS = True

    generator_test_list = [generator_test_1, generator_test_2, generator_test_3, generator_test_4]
    modality_labels_list = [m1_labels_array, m2_labels_array, m3_labels_array, m4_labels_array]

    y_scores = [predict_latent(model_dict['vae_1'], generator_test_1, (N1, latent_dim))[0],
                predict_latent(model_dict['vae_2'], generator_test_2, (N1, latent_dim))[0],
                predict_latent(model_dict['vae_3'], generator_test_3, (N1, latent_dim))[0],
                predict_latent(model_dict['vae_4'], generator_test_4, (N1, latent_dim))[0]]
    y_scores_indices = [np.where(gl == 1)[0] for gl in modality_labels_list]
    y_scores_dx_indices = [[index_0, index_1, index_2] for ys_idx in y_scores_indices]

    YSCORES_PATH = join(MODALITY_PATH, 'Y_SCORES')
    if not exists(YSCORES_PATH):
        os.makedirs(YSCORES_PATH)

    for it_ys, ys in enumerate(y_scores):
        writer = CSVWriter(filepath=join(YSCORES_PATH, modality_list[it_ys] + '.csv'),
                           header=['RID', 'event'] + [it for it in range(N_COMPONENTS)],
                           attach=False)
        rows = []
        for it_row in range(N[it_ys]):
            if modality_labels_list[it_ys][it_row] == 0:
                rows.append({**{'RID': rid_events_list_all_modalities[it_row][0],
                                'event': rid_events_list_all_modalities[it_row][1]},
                             **{it_col: '' for it_col in range(N_COMPONENTS)}})
            else:
                rows.append({**{'RID': rid_events_list_all_modalities[it_row][0],
                                'event': rid_events_list_all_modalities[it_row][1]},
                             **{it_col: ys[it_row, it_col] for it_col in range(N_COMPONENTS)}})
        writer.writerows(rows)

    if CORRELATION_PLOTS:
        print('CORRELATION_PLOTS')
        if not rid_events_list_all_modalities:
            print('########### There are no common indices from ALL modalities')
        else:
            writer_scatter = ResultsWriter(filepath=join(MODALITY_PATH, 'scatter_plot_betas.txt'), attach=False)
            writer_scatter.write('List of betas of the linear regression between modalities\n\n')

            indices_all_modalities = [it_r for it_r, (r, e) in enumerate(rid_events_list) if
                                      (r, e) in rid_events_list_all_modalities]
            y_scores_all_modalities = [y_scores[it_modality][indices_all_modalities] for it_modality in
                                       range(len(modality_list))]

            index_0 = np.where(dx_array[indices_all_modalities] == 0)[0]
            index_1 = np.where(dx_array[indices_all_modalities] == 1)[0]
            index_2 = np.where(dx_array[indices_all_modalities] == 2)[0]

            lr_model = LinearRegression()
            for it_c in range(N_COMPONENTS):
                writer_scatter.write('COMPONENT ' + str(it_c) + ':\n')

                if len(y_scores) <= 2:

                    fig, axarr = plt.subplots(1, 1)
                    axarr.plot(y_scores_all_modalities[0][index_0, it_c], y_scores_all_modalities[1][index_0, it_c],
                               'b*', label='NC')
                    axarr.plot(y_scores_all_modalities[0][index_1, it_c], y_scores_all_modalities[1][index_1, it_c],
                               'r*', label='MCI')
                    axarr.plot(y_scores_all_modalities[0][index_2, it_c], y_scores_all_modalities[1][index_2, it_c],
                               'g*', label='AD')

                    lr_model.fit(y_scores_all_modalities[0][:, it_c: it_c + 1],
                                 y_scores_all_modalities[1][:, it_c:it_c + 1])
                    axarr.plot(y_scores_all_modalities[0][:, it_c:it_c + 1],
                               lr_model.predict(y_scores_all_modalities[0][:, it_c:it_c + 1]), 'k-')
                    axarr.set_xlabel(modality_list[0])
                    axarr.set_ylabel(modality_list[1])


                else:

                    writer_scatter.write(','.join(modality_list[1:]))
                    writer_scatter.write('\n')

                    fig, axarr = plt.subplots(len(y_scores) - 1, len(y_scores) - 1)
                    for it_row in range(len(y_scores) - 1):
                        writer_scatter.write(modality_list[it_row])
                        for i in range(it_row):
                            writer_scatter.write(',     ')

                        for it_col in range(it_row + 1, len(y_scores)):

                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][index_0, it_c],
                                                           y_scores_all_modalities[it_row][index_0, it_c],
                                                           'b*', label='NC')
                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][index_1, it_c],
                                                           y_scores_all_modalities[it_row][index_1, it_c],
                                                           'r*', label='MCI')
                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][index_2, it_c],
                                                           y_scores_all_modalities[it_row][index_2, it_c],
                                                           'g*', label='AD')

                            lr_model.fit(y_scores_all_modalities[it_col][:, it_c: it_c + 1],
                                         y_scores_all_modalities[it_row][:, it_c:it_c + 1])
                            r, p = pearsonr(y_scores_all_modalities[it_col][:, it_c: it_c + 1],
                                            y_scores_all_modalities[it_row][:, it_c:it_c + 1])
                            writer_scatter.write(',' + str(np.round(np.squeeze(r), 3)))

                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][:, it_c:it_c + 1],
                                                           lr_model.predict(
                                                               y_scores_all_modalities[it_col][:, it_c:it_c + 1]), 'k-')

                            if it_col - 1 == 0:
                                for i in range(len(y_scores) - 1):
                                    axarr[i, it_col - 1].set_ylabel(modality_list[i])
                        writer_scatter.write('\n')
                        if it_row == len(y_scores) - 2:
                            for i in range(len(y_scores) - 1):
                                axarr[it_row, i].set_xlabel(modality_list[i + 1])
                plt.legend()
                plt.savefig(join(MODALITY_PATH, str(it_c), 'scatter_plot.png'))
                plt.close()
                writer_scatter.write('\n')

    if Y_SCORE_PLOTS:
        print('Y_SCORE_PLOTS')
        for it_component in range(N_COMPONENTS):
            for it_modality, modality in enumerate(modality_list):
                plt.figure()
                plt.hist(y_scores[it_modality][y_scores_indices[it_modality], it_component], bins=40)
                plt.savefig(
                    join(MODALITY_PATH, str(it_component), modality + '_' + str(it_modality) + '_histogram.png'))
                plt.close()

            # plt.figure()
            # plt.hist(y_scores[1][:, it_component], bins=40)
            # plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[1] + '_histogram.png'))
            # plt.close()
            #
            # plt.figure()
            # plt.hist(y_scores[2][:, it_component], bins=40)
            # plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[2] + '_histogram.png'))
            # plt.close()
            #
            # plt.figure()
            # plt.hist(y_scores[3][:, it_component], bins=40)
            # plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[3] + '_histogram.png'))
            # plt.close()

    if STATISTICAL_PLOTS:
        print('STATISTICAL_PLOTS')

        for it_component in range(N_COMPONENTS):
            writer_list[it_component].write('#############################\n')
            writer_list[it_component].write('RESULTS DIAGNOSTIC COMPARISON\n')
            writer_list[it_component].write('#############################\n\n')

        combination_plots = list(itertools.combinations(range(3), 2))
        for it_modality, modality in enumerate(modality_list):

            for it_component in range(N_COMPONENTS):
                writer_list[it_component].write('--- ' + modality + ' subspace ---\n')

                t = []
                p = []
                boxplot_list = [y_scores[it_modality][y_scores_dx_indices[it_modality][0], it_component],
                                y_scores[it_modality][y_scores_dx_indices[it_modality][1], it_component],
                                y_scores[it_modality][y_scores_dx_indices[it_modality][2], it_component]]

                for c in combination_plots:
                    result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                    t.append(result[0])
                    p.append(result[1])
                    writer_list[it_component].write('   - ' + str((label_diagnostic[c[0]], label_diagnostic[c[1]]))
                                                    + ': ' + str((result[0], result[1])) + '\n')

                filename = modality + '_diagnostic.png'
                plt.figure()
                plt.boxplot(boxplot_list)
                plt.xticks([1, 2, 3], label_diagnostic)
                plt.title('Significant (p<0.05) differences: ' + str(
                    [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                plt.savefig(join(MODALITY_PATH, str(it_component), filename))
                plt.close()

    if WEIGHT_PLOTS:
        print('WEIGHT_PLOTS')

        for it_component in range(N_COMPONENTS):
            writer_list[it_component].write('####################\n')
            writer_list[it_component].write('PLS Y_WEIGHT RESULTS\n')
            writer_list[it_component].write('####################\n\n')

        y_weight_generator = np.eye(N_COMPONENTS)
        y_weight_generator_labels = np.arange(0, N_COMPONENTS).reshape(-1, 1)
        dataset_y_weight = MyDataset(y_weight_generator, y_weight_generator_labels)
        generator_y_weight = torch.utils.data.DataLoader(
            dataset_y_weight,
            batch_size=N_COMPONENTS,
            shuffle=True,
            num_workers=1,
            pin_memory=torch.cuda.is_available()

        )

        y_rotations = [predict(model_dict['autoencoder_1'].decoder, generator_y_weight, M1)[0],
                       predict(model_dict['autoencoder_2'].decoder, generator_y_weight, M2)[0],
                       predict(model_dict['autoencoder_3'].decoder, generator_y_weight, M3)[0],
                       predict(model_dict['autoencoder_4'].decoder, generator_y_weight, M4)[0]]

        for it_modality, modality in enumerate(modality_list):

            for it_component in range(N_COMPONENTS):
                writer_list[it_component].write(modality_list[it_modality])
                writer_list[it_component].write('\n')

                sorted_mod = np.argsort(y_rotations[it_modality][it_component, :])[::-1]
                for m_prs in sorted_mod:
                    writer_list[it_component].write('  - ' + modality_id_list[it_modality][m_prs] + ': ' + str(
                        y_rotations[it_modality][it_component, m_prs]) + '\n')

                filename = modality + '_weights.png'

                y_min = np.min(y_rotations[it_modality][it_component, :]) * 0.9
                y_max = np.max(y_rotations[it_modality][it_component, :]) * 1.1
                y_lim = np.max([np.abs(y_min), np.abs(y_max)])

                _, ax = plt.subplots()
                ax.barh(np.arange(M[it_modality]), y_rotations[it_modality][it_component, :], align='center',
                        color='green', ecolor='black')
                ax.set_yticks(np.arange(start=0, stop=M[it_modality], step=np.max([1, int(M[it_modality] / 10)])))
                ax.set_xticks(np.concatenate((np.arange(start=-y_lim, stop=0, step=y_lim / 3),
                                              np.arange(start=0, stop=y_lim, step=y_lim / 3), [y_lim])))
                ax.invert_yaxis()
                ax.set_xlabel('Latent variable ' + str(it_component))
                ax.set_title('Relative weight of each input variable')
                ax.grid()
                ax.set_xlim([-y_lim, y_lim])
                plt.savefig(join(MODALITY_PATH, str(it_component), filename))
                plt.close()

    if FACTOR_PLOTS:
        print('FACTOR_PLOTS')

        n_rows = ROW_DICT[N_COMPONENTS]
        n_cols = int(np.ceil(N_COMPONENTS / n_rows))

        # wmh_dict = {k: v['bl'] for k, v in adni_reader.load_WMH().items() if 'bl' in list(v.keys())}
        education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
        gender_dict, gender_labels_dict = DataLoader.load_demographic(demo_names=['gender'])
        apoe4_dict, apoe4_labels_dict = DataLoader.load_demographic(demo_names=['apoe4'])
        ab_status_dict, ab_status_labels_dict = DataLoader.load_amyloid_status()
        mmse_dict, mmse_labels_dict = DataLoader.load_clinical(clinical_names=['mmse'])
        cdrsb_dict, cdrsb_labels_dict = DataLoader.load_clinical(clinical_names=['cdrsb'])

        writer_factor = ResultsWriter(filepath=join(MODALITY_PATH, 'factor_analysis.txt'), attach=False)
        writer_factor.write('###########################\n')
        writer_factor.write('FACTOR CORRELATION ANALYSIS\n')
        writer_factor.write('###########################\n')

        factor_dict_list = [age_dict, education_dict, mmse_dict, cdrsb_dict]
        factor_labels_dict_list = [age_labels_dict, education_labels_dict, mmse_labels_dict, cdrsb_labels_dict]
        factor_name_list = ['age', 'education', 'mmse', 'cdrsb']
        for factor_dict, factor_labels_dict, factor_name in zip(factor_dict_list, factor_labels_dict_list,
                                                                factor_name_list):
            writer_factor.write('\nFACTOR: ' + factor_name + '\n')

            rid_events_list_factor = [(rid, e_step) for (rid, e_step) in rid_events_list if
                                      factor_labels_dict[rid][e_step] == 1]

            for it_modality, modality in enumerate(modality_list):
                writer_factor.write('   - ' + modality + ' subspace:\n')

                rid_events_list_factor_modality = [(rid, e_step) for it_re, (rid, e_step) in
                                                   enumerate(rid_events_list_factor) if
                                                   modality_labels_list[it_modality][it_re] == 1]
                indices_rid = [it_re for it_re, (rid, e_step) in enumerate(rid_events_list) if
                               (rid, e_step) in rid_events_list_factor_modality]

                y_scores_factor = y_scores[it_modality][indices_rid]
                factor_array = np.asarray(
                    [factor_dict[rid][e_step] for (rid, e_step) in rid_events_list_factor_modality]).reshape((-1,))

                dx_array_factor = dx_array[indices_rid]
                index_0 = np.where(dx_array_factor == 0)[0]
                index_1 = np.where(dx_array_factor == 1)[0]
                index_2 = np.where(dx_array_factor == 2)[0]

                fig_y, axarr_y = plt.subplots(n_rows, n_cols)
                for it_c in range(N_COMPONENTS):
                    writer_factor.write('     Component ' + str(it_c) + ':')
                    writer_factor.write(
                        ' Y:' + str(pearsonr(y_scores_factor[:, it_c], factor_array)) + '\n')

                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_0, it_c],
                                                                              factor_array[index_0], '*b',
                                                                              label='NC')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_1, it_c],
                                                                              factor_array[index_1], '*r',
                                                                              label='MCI')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_2, it_c],
                                                                              factor_array[index_2], '*k',
                                                                              label='AD')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor_name)

                plt.figure(1)
                plt.savefig(join(MODALITY_PATH, modality + '_score_' + factor_name + '_correlation.png'))
                plt.close()

        factor_dict_list = [gender_dict, apoe4_dict, ab_status_dict]
        factor_labels_dict_list = [gender_labels_dict, apoe4_labels_dict, ab_status_labels_dict]
        factor_name_list = ['gender', 'apoe4', 'abstatus']
        for factor_dict, factor_labels_dict, factor_name in zip(factor_dict_list, factor_labels_dict_list,
                                                                factor_name_list):
            writer_factor.write('\nFACTOR: ' + factor_name + '\n')

            rid_events_list_factor = [(rid, e_step) for (rid, e_step) in rid_events_list if
                                      factor_labels_dict[rid][e_step] == 1]

            for it_modality, modality in enumerate(modality_list):
                writer_factor.write('   - ' + modality + ' subspace:\n')

                rid_events_list_factor_modality = [(rid, e_step) for it_re, (rid, e_step) in
                                                   enumerate(rid_events_list_factor) if
                                                   modality_labels_list[it_modality][it_re] == 1]
                indices_rid = [it_re for it_re, (rid, e_step) in enumerate(rid_events_list) if
                               (rid, e_step) in rid_events_list_factor_modality]

                y_scores_factor = y_scores[it_modality][indices_rid]
                factor_array = np.asarray(
                    [factor_dict[rid][e_step] for (rid, e_step) in rid_events_list_factor_modality]).reshape((-1,))

                index_list = [np.where(factor_array == unique_label)[0] for unique_label in np.unique(factor_array)]
                combination_plots = list(itertools.combinations(range(len(np.unique(factor_array))), 2))

                fig_y, axarr_y = plt.subplots(n_rows, n_cols)
                for it_c in range(N_COMPONENTS):
                    boxplot_list = [y_scores_factor[idx, it_c] for idx in index_list]

                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].boxplot(boxplot_list)
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()

                    if it_c < n_rows:
                        axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor_name)
                    elif it_c % n_rows == n_rows - 1:
                        axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')

                plt.figure(1)
                plt.savefig(join(MODALITY_PATH, modality + '_score_' + factor_name + '_correlation.png'))
                plt.close()


    Y_SCORE_PLOTS = True
    STATISTICAL_PLOTS = True
    WEIGHT_PLOTS = True
    FACTOR_PLOTS = True
    CORRELATION_PLOTS = True

    y_scores = [predict_latent(model_dict['vae_1'], generator_test_1, (N1, latent_dim))[0],
                predict_latent(model_dict['vae_2'], generator_test_2, (N1, latent_dim))[0],
                predict_latent(model_dict['vae_3'], generator_test_3, (N1, latent_dim))[0],
                predict_latent(model_dict['vae_4'], generator_test_4, (N1, latent_dim))[0]]

    if Y_SCORE_PLOTS:
        for it_component in range(N_COMPONENTS):

            plt.figure()
            plt.hist(y_scores[0][:, it_component], bins=40)
            plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[0] + '_1_histogram.png'))
            plt.close()

            plt.figure()
            plt.hist(y_scores[1][:, it_component], bins=40)
            plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[1] + '_histogram.png'))
            plt.close()

            plt.figure()
            plt.hist(y_scores[2][:, it_component], bins=40)
            plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[2] + '_histogram.png'))
            plt.close()

            plt.figure()
            plt.hist(y_scores[3][:, it_component], bins=40)
            plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[3] + '_histogram.png'))
            plt.close()

    if STATISTICAL_PLOTS:
        writer.write('#############################\n')
        writer.write('RESULTS DIAGNOSTIC COMPARISON\n')
        writer.write('#############################\n\n')

        combination_plots = list(itertools.combinations(range(3), 2))
        for it_modality, modality in enumerate(modality_list):
            writer.write('--- ' + modality + ' subspace ---\n')

            for it_component in range(N_COMPONENTS):
                COMPONENTS_PATH = join(MODALITY_PATH, str(it_component))
                if not exists(COMPONENTS_PATH):
                    os.makedirs(COMPONENTS_PATH)
                writer.write('     · Component ' + str(it_component) + '\n')
                t = []
                p = []
                boxplot_list = [y_scores[it_modality][index_0, it_component],
                                y_scores[it_modality][index_1, it_component],
                                y_scores[it_modality][index_2, it_component]]

                for c in combination_plots:
                    result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                    t.append(result[0])
                    p.append(result[1])
                    writer.write('           - ' + str((label_diagnostic[c[0]], label_diagnostic[c[1]])) + ': ' + str(
                        (result[0], result[1])) + '\n')

                filename = modality + '_diagnostic.png'
                plt.figure()
                plt.boxplot(boxplot_list)
                plt.xticks([1, 2, 3], label_diagnostic)
                plt.title('Significant (p<0.05) differences: ' + str(
                    [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                plt.savefig(join(MODALITY_PATH, str(it_component), filename))
                plt.close()

    if WEIGHT_PLOTS:
        writer.write('####################\n')
        writer.write('PLS Y_WEIGHT RESULTS\n')
        writer.write('####################\n\n')

        y_weight_generator = np.eye(N_COMPONENTS)
        y_weight_generator_labels = np.arange(0, N_COMPONENTS).reshape(-1, 1)
        dataset_y_weight = MyDataset(y_weight_generator, y_weight_generator_labels)
        generator_y_weight = torch.utils.data.DataLoader(
            dataset_y_weight,
            batch_size=N_COMPONENTS,
            shuffle=True,
            num_workers=1,
            pin_memory=torch.cuda.is_available()

        )

        y_rotations = [predict_decoder(model_dict['vae_1'], generator_y_weight, (N_COMPONENTS, M1))[0],
                       predict_decoder(model_dict['vae_2'], generator_y_weight, (N_COMPONENTS, M2))[0],
                       predict_decoder(model_dict['vae_3'], generator_y_weight, (N_COMPONENTS, M3))[0],
                       predict_decoder(model_dict['vae_4'], generator_y_weight, (N_COMPONENTS, M4))[0]]

        for it_modality, modality in enumerate(modality_list):
            for it_component in range(N_COMPONENTS):
                sorted_mod = np.argsort(np.abs(y_rotations[it_modality][it_component, :]))[::-1]
                writer.write('    · Component ' + str(it_component) + '\n')
                for m_prs in sorted_mod:
                    writer.write('         ' + modality_id_list[it_modality][m_prs] + ': ' + str(
                        y_rotations[it_modality][it_component, m_prs]) + '\n')
            writer.write('\n')

            y_min = np.min(y_rotations[it_modality]) * 0.9
            y_max = np.max(y_rotations[it_modality]) * 1.1
            y_lim = np.max([np.abs(y_min), np.abs(y_max)])

            for it_component in range(N_COMPONENTS):

                filename = modality + '_weights.png'
                x_min = np.min(y_rotations[it_modality][it_component, :])
                x_max = np.max(y_rotations[it_modality][it_component, :])
                _, ax = plt.subplots()
                ax.barh(np.arange(M[it_modality]), y_rotations[it_modality][it_component, :], align='center',
                        color='green', ecolor='black')
                ax.set_yticks(np.arange(start=0, stop=M[it_modality], step=np.max([1, int(M[it_modality] / 10)])))
                ax.set_xticks(np.concatenate((np.arange(start=-y_lim, stop=0, step=y_lim / 3),
                                              np.arange(start=0, stop=y_lim, step=y_lim / 3), [y_lim])))
                ax.invert_yaxis()
                ax.set_xlabel('Latent variable ' + str(it_component))
                ax.set_title('Relative weight of each input variable')
                ax.grid()
                ax.set_xlim([-y_lim, y_lim])
                plt.savefig(join(MODALITY_PATH, str(it_component), filename))
                plt.close()

    if FACTOR_PLOTS:
        writer.write('###########################\n')
        writer.write('FACTOR CORRELATION ANALYSIS\n')
        writer.write('###########################\n')
        n_rows = ROW_DICT[N_COMPONENTS]
        n_cols = int(np.ceil(N_COMPONENTS / n_rows))
        for it_modality, modality in enumerate(modality_list):
            writer.write('--- ' + modality + ' subspace ---\n')
            for it_factor, factor in enumerate(factor_id_list):
                fig_y, axarr_y = plt.subplots(n_rows, n_cols)
                writer.write('\nFACTOR: ' + factor + '\n')

                for it_c in range(N_COMPONENTS):

                    writer.write('    · Component ' + str(it_c + factor_array.shape[1]) + ':')
                    writer.write(
                        ' Y:' + str(pearsonr(y_scores[it_modality][:, it_c], factor_array[:, it_factor])) + '\n')

                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[it_modality][index_0, it_c],
                                                                              factor_array[index_0, it_factor], '*b',
                                                                              label='NC')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[it_modality][index_1, it_c],
                                                                              factor_array[index_1, it_factor], '*r',
                                                                              label='MCI')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[it_modality][index_2, it_c],
                                                                              factor_array[index_2, it_factor], '*k',
                                                                              label='AD')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor)

                plt.figure(1)
                plt.savefig(join(MODALITY_PATH, modality + '_score_' + factor + '_correlation.png'))
                plt.close()

    if CORRELATION_PLOTS:
        lr_model = LinearRegression()

        for it_c in range(N_COMPONENTS):

            if len(y_scores) <= 2:
                fig, axarr = plt.subplots(1, 1)
                axarr.plot(y_scores[0][index_0, it_c], y_scores[1][index_0, it_c], 'b*', label='NC')
                axarr.plot(y_scores[0][index_1, it_c], y_scores[1][index_1, it_c], 'r*', label='MCI')
                axarr.plot(y_scores[0][index_2, it_c], y_scores[1][index_2, it_c], 'g*', label='AD')

                lr_model.fit(y_scores[0][:, it_c: it_c + 1], y_scores[1][:, it_c:it_c + 1])
                axarr.plot(y_scores[0][:, it_c:it_c + 1], lr_model.predict(y_scores[0][:, it_c:it_c + 1]), 'k-')
                axarr.set_xlabel(modality_list[0])
                axarr.set_ylabel(modality_list[1])


            else:
                fig, axarr = plt.subplots(len(y_scores) - 1, len(y_scores) - 1)

                for it_row in range(len(y_scores) - 1):
                    for it_col in range(it_row+1, len(y_scores)):

                        axarr[it_row, it_col-1].plot(y_scores[it_col][index_0, it_c], y_scores[it_row][index_0, it_c],
                                                   'b*', label='NC')
                        axarr[it_row, it_col-1].plot(y_scores[it_col][index_1, it_c], y_scores[it_row][index_1, it_c],
                                                   'r*', label='MCI')
                        axarr[it_row, it_col-1].plot(y_scores[it_col][index_2, it_c], y_scores[it_row][index_2, it_c],
                                                   'g*', label='AD')

                        lr_model.fit(y_scores[it_col][:, it_c: it_c + 1], y_scores[it_row][:, it_c:it_c + 1])

                        axarr[it_row, it_col-1].plot(y_scores[it_col][:, it_c:it_c + 1],
                                                   lr_model.predict(y_scores[it_col][:, it_c:it_c + 1]), 'k-')

                        if it_col - 1 == 0:
                            for i in range(len(y_scores) - 1):
                                axarr[i, it_col-1].set_ylabel(modality_list[i])

                    if it_row == len(y_scores) - 2:
                        for i in range(len(y_scores) - 1):
                            axarr[it_row, i].set_xlabel(modality_list[i + 1])

            plt.legend()
            plt.savefig(join(MODALITY_PATH, str(it_c), 'scatter_plot.png'))
            plt.close()
