#!/imatge/acasamitjana/environments/env-latent-models/bin/python

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data import adni_reader
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT, CSVWriter
from src.dataset import MyDataset, MyDataset_disentangle
from src.models import Encoder, Decoder, Discriminator, Autoencoder, Encoder_lineal, Decoder_lineal
from src.losses import graph_smoothness_loss

import os
import numpy as np
from os.path import join, exists
import itertools


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################

N_COMPONENTS = 16#int(os.environ['SLURM_ARRAY_TASK_ID'])
EPS = 1e-15

GWAS_TYPE = None#'Whole_both_sexes'
PVALUE = 1

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/mnt/gpid08/users/adria.casamitjana', 'Multimodal_autoencoders',  'AAE', 'Multiple_events', 'Disentangle_simple', 'MissingModalities', 'nc_' + str(N_COMPONENTS), 'lineal')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['adas13', 'mmse']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):


    for batch_idx, (data, target, factor) in enumerate(generator_train):
        data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
        target_1, target_2, target_3, target_4 = target[0].to(device), target[1].to(device), target[2].to(device), target[3].to(device)
        factor = factor.to(device)

        for model_name, model in model_dict.items():
            model.zero_grad()
            model.to(device)

        #### RECONSTRUCTION LOSS
        model_dict['discriminator'].train(mode=False)
        model_dict['autoencoder_1'].train(mode=True)
        model_dict['autoencoder_2'].train(mode=True)
        model_dict['autoencoder_3'].train(mode=True)
        model_dict['autoencoder_4'].train(mode=True)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to z

        x_reconstructed_1_1 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_1_2 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_1_3 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_1_4 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        x_reconstructed_2_1 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_2 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_3 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_4 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        x_reconstructed_3_1 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_2 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_3 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_4 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        x_reconstructed_4_1 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_2 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_3 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_4 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        reconstruction_loss_1 = target_1 * ((x_reconstructed_1_1 - data_1)**2).sum(dim=-1) +\
                                target_2 * ((x_reconstructed_1_2 - data_1)**2).sum(dim=-1) +\
                                target_3 * ((x_reconstructed_1_3 - data_1)**2).sum(dim=-1) +\
                                target_4 * ((x_reconstructed_1_4 - data_1)**2).sum(dim=-1)

        reconstruction_loss_2 = target_1 * ((x_reconstructed_2_1 - data_2)**2).sum(dim=-1) +\
                                target_2 * ((x_reconstructed_2_2 - data_2)**2).sum(dim=-1) +\
                                target_3 * ((x_reconstructed_2_3 - data_2)**2).sum(dim=-1) +\
                                target_4 * ((x_reconstructed_2_4 - data_2)**2).sum(dim=-1)

        reconstruction_loss_3 = target_1 * ((x_reconstructed_3_1 - data_3) ** 2).sum(dim=-1) + \
                                target_2 * ((x_reconstructed_3_2 - data_3) ** 2).sum(dim=-1) + \
                                target_3 * ((x_reconstructed_3_3 - data_3) ** 2).sum(dim=-1) + \
                                target_4 * ((x_reconstructed_3_4 - data_3) ** 2).sum(dim=-1)

        reconstruction_loss_4 = target_1 * ((x_reconstructed_4_1 - data_4) ** 2).sum(dim=-1) + \
                                target_2 * ((x_reconstructed_4_2 - data_4) ** 2).sum(dim=-1) + \
                                target_3 * ((x_reconstructed_4_3 - data_4) ** 2).sum(dim=-1) + \
                                target_4 * ((x_reconstructed_4_4 - data_4) ** 2).sum(dim=-1)

        reconstruction_loss = torch.mean(reconstruction_loss_1 + reconstruction_loss_2 +
                                         reconstruction_loss_3 + reconstruction_loss_4)


        AAE_loss = reconstruction_loss
        AAE_loss.backward()

        optimizer_dict['autoencoder_1'].step()
        optimizer_dict['autoencoder_2'].step()
        optimizer_dict['autoencoder_3'].step()
        optimizer_dict['autoencoder_4'].step()


        #### DISCRIMINATOR LOSS
        model_dict['discriminator'].train(mode=True)
        model_dict['autoencoder_1'].train(mode=False)
        model_dict['autoencoder_2'].train(mode=False)
        model_dict['autoencoder_3'].train(mode=False)
        model_dict['autoencoder_4'].train(mode=False)

        z_real_gauss = Variable(torch.randn(data_1.size()[0], latent_space_dims) * 5.)
        z_real_gauss = z_real_gauss.to(device)
        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)

        d_real = model_dict['discriminator'](z_real_gauss)
        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2 = model_dict['discriminator'](z_sample_2)
        d_sample_3 = model_dict['discriminator'](z_sample_3)
        d_sample_4 = model_dict['discriminator'](z_sample_4)

        label_sample_1 = np.zeros((data_1.size()[0], 5))
        label_sample_1[:, 0] = 1
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = np.zeros((data_1.size()[0], 5))
        label_sample_2[:, 1] = 1
        label_sample_2 = torch.FloatTensor(label_sample_2)

        label_sample_3 = np.zeros((data_1.size()[0], 5))
        label_sample_3[:, 2] = 1
        label_sample_3 = torch.FloatTensor(label_sample_3)

        label_sample_4 = np.zeros((data_1.size()[0], 5))
        label_sample_4[:, 3] = 1
        label_sample_4 = torch.FloatTensor(label_sample_4)

        label_real = np.zeros((data_1.size()[0], 5))
        label_real[:, 4] = 1
        label_real = torch.FloatTensor(label_real)

        label_sample_1 = label_sample_1.to(device)
        label_sample_2 = label_sample_2.to(device)
        label_sample_3 = label_sample_3.to(device)
        label_sample_4 = label_sample_4.to(device)
        label_real = label_real.to(device)
        discriminator_loss = -torch.mean(torch.mean(label_real * torch.log(d_real + EPS), dim=-1) +
                                         target_1 * torch.mean(label_sample_1 * torch.log(d_sample_1 + EPS), dim=-1) +
                                         target_2 * torch.mean(label_sample_2 * torch.log(d_sample_2 + EPS), dim=-1) +
                                         target_3 * torch.mean(label_sample_3 * torch.log(d_sample_3 + EPS), dim=-1) +
                                         target_4 * torch.mean(label_sample_4 * torch.log(d_sample_4 + EPS), dim=-1))


        discriminator_loss.backward()
        optimizer_dict['discriminator'].step()

        #### GENERATOR LOSS
        model_dict['generator_1'].train(mode=True)
        model_dict['generator_2'].train(mode=True)
        model_dict['generator_3'].train(mode=True)
        model_dict['generator_4'].train(mode=True)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)


        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2 = model_dict['discriminator'](z_sample_2)
        d_sample_3 = model_dict['discriminator'](z_sample_3)
        d_sample_4 = model_dict['discriminator'](z_sample_4)

        label_sample_1 = 0.25 * np.ones((data_1.size()[0], 5))
        label_sample_1[:, 0] = 0
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = 0.25 * np.ones((data_1.size()[0], 5))
        label_sample_2[:, 1] = 0
        label_sample_2 = torch.FloatTensor(label_sample_2)

        label_sample_3 = 0.25 * np.ones((data_1.size()[0], 5))
        label_sample_3[:, 2] = 0
        label_sample_3 = torch.FloatTensor(label_sample_3)

        label_sample_4 = 0.25 * np.ones((data_1.size()[0], 5))
        label_sample_4[:, 3] = 0
        label_sample_4 = torch.FloatTensor(label_sample_4)

        label_sample_1 = label_sample_1.to(device)
        label_sample_2 = label_sample_2.to(device)
        label_sample_3 = label_sample_3.to(device)
        label_sample_4 = label_sample_4.to(device)

        generator_loss = -torch.mean(target_1 * torch.mean(label_sample_1 * torch.log(d_sample_1 + EPS), dim=-1) +
                                     target_2 * torch.mean(label_sample_2 * torch.log(d_sample_2 + EPS), dim=-1) +
                                     target_3 * torch.mean(label_sample_3 * torch.log(d_sample_3 + EPS), dim=-1) +
                                     target_4 * torch.mean(label_sample_4 * torch.log(d_sample_4 + EPS), dim=-1))

        # generator_loss = -torch.mean(target_1 * torch.mean(label_real * torch.log(d_sample_1 + EPS), dim=-1) +
        #                              target_2 * torch.mean(label_real * torch.log(d_sample_2 + EPS), dim=-1) +
        #                              target_3 * torch.mean(label_real * torch.log(d_sample_3 + EPS), dim=-1) +
        #                              target_4 * torch.mean(label_real * torch.log(d_sample_4 + EPS), dim=-1))

        generator_loss.backward()
        optimizer_dict['generator_1'].step()
        optimizer_dict['generator_2'].step()
        optimizer_dict['generator_3'].step()
        optimizer_dict['generator_4'].step()



        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item() + discriminator_loss.item() + generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Prediction Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)

    return logger

def test(model_dict, optimizer_dict, device, generator_test, epoch, latent_space_dims, logger, **kwargs):
    for batch_idx, (data, target, factor) in enumerate(generator_test):
        data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
        target_1, target_2, target_3, target_4 = target[0].to(device), target[1].to(device), target[2].to(device), target[3].to(device)
        factor = factor.to(device)

        for model_name, model in model_dict.items():
            model.to(device)
            model.train(mode=False)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to z

        x_reconstructed_1_1 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_1_2 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_1_3 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_1_4 = model_dict['autoencoder_1'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        x_reconstructed_2_1 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_2 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_3 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_4 = model_dict['autoencoder_2'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        x_reconstructed_3_1 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_2 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_3 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_4 = model_dict['autoencoder_3'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        x_reconstructed_4_1 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_2 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_3 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_4 = model_dict['autoencoder_4'].decode(
            torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        reconstruction_loss_1 = target_1 * ((x_reconstructed_1_1 - data_1)**2).sum(dim=-1) +\
                                target_2 * ((x_reconstructed_1_2 - data_1)**2).sum(dim=-1) +\
                                target_3 * ((x_reconstructed_1_3 - data_1)**2).sum(dim=-1) +\
                                target_4 * ((x_reconstructed_1_4 - data_1)**2).sum(dim=-1)

        reconstruction_loss_2 = target_1 * ((x_reconstructed_2_1 - data_2)**2).sum(dim=-1) +\
                                target_2 * ((x_reconstructed_2_2 - data_2)**2).sum(dim=-1) +\
                                target_3 * ((x_reconstructed_2_3 - data_2)**2).sum(dim=-1) +\
                                target_4 * ((x_reconstructed_2_4 - data_2)**2).sum(dim=-1)

        reconstruction_loss_3 = target_1 * ((x_reconstructed_3_1 - data_3) ** 2).sum(dim=-1) + \
                                target_2 * ((x_reconstructed_3_2 - data_3) ** 2).sum(dim=-1) + \
                                target_3 * ((x_reconstructed_3_3 - data_3) ** 2).sum(dim=-1) + \
                                target_4 * ((x_reconstructed_3_4 - data_3) ** 2).sum(dim=-1)

        reconstruction_loss_4 = target_1 * ((x_reconstructed_4_1 - data_4) ** 2).sum(dim=-1) + \
                                target_2 * ((x_reconstructed_4_2 - data_4) ** 2).sum(dim=-1) + \
                                target_3 * ((x_reconstructed_4_3 - data_4) ** 2).sum(dim=-1) + \
                                target_4 * ((x_reconstructed_4_4 - data_4) ** 2).sum(dim=-1)

        reconstruction_loss = torch.mean(reconstruction_loss_1 + reconstruction_loss_2 +
                                         reconstruction_loss_3 + reconstruction_loss_4)


        #### DISCRIMINATOR LOSS
        z_real_gauss = Variable(torch.randn(data_1.size()[0], latent_space_dims) * 5.)
        z_real_gauss = z_real_gauss.to(device)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)

        d_real = model_dict['discriminator'](z_real_gauss)
        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2 = model_dict['discriminator'](z_sample_2)
        d_sample_3 = model_dict['discriminator'](z_sample_3)
        d_sample_4 = model_dict['discriminator'](z_sample_4)

        label_sample_1 = np.zeros((data_1.size()[0], 5))
        label_sample_1[:, 0] = 1
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = np.zeros((data_1.size()[0], 5))
        label_sample_2[:, 1] = 1
        label_sample_2 = torch.FloatTensor(label_sample_2)

        label_sample_3 = np.zeros((data_1.size()[0], 5))
        label_sample_3[:, 2] = 1
        label_sample_3 = torch.FloatTensor(label_sample_3)

        label_sample_4 = np.zeros((data_1.size()[0], 5))
        label_sample_4[:, 3] = 1
        label_sample_4 = torch.FloatTensor(label_sample_4)

        label_real = np.zeros((data_1.size()[0], 5))
        label_real[:, 4] = 1
        label_real = torch.FloatTensor(label_real)

        label_sample_1 = label_sample_1.to(device)
        label_sample_2 = label_sample_2.to(device)
        label_sample_3 = label_sample_3.to(device)
        label_sample_4 = label_sample_4.to(device)
        label_real = label_real.to(device)

        discriminator_loss = -torch.mean(torch.mean(label_real * torch.log(d_real + EPS), dim=-1) +
                                         target_1 * torch.mean(label_sample_1 * torch.log(d_sample_1 + EPS), dim=-1) +
                                         target_2 * torch.mean(label_sample_2 * torch.log(d_sample_2 + EPS), dim=-1) +
                                         target_3 * torch.mean(label_sample_3 * torch.log(d_sample_3 + EPS), dim=-1) +
                                         target_4 * torch.mean(label_sample_4 * torch.log(d_sample_4 + EPS), dim=-1))

        #### GENERATOR LOSS
        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)


        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2 = model_dict['discriminator'](z_sample_2)
        d_sample_3 = model_dict['discriminator'](z_sample_3)
        d_sample_4 = model_dict['discriminator'](z_sample_4)

        label_sample_1 = 0.25*np.ones((data_1.size()[0], 5))
        label_sample_1[:, 0] = 0
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = 0.25*np.ones((data_1.size()[0], 5))
        label_sample_2[:, 1] = 0
        label_sample_2 = torch.FloatTensor(label_sample_2)

        label_sample_3 = 0.25*np.ones((data_1.size()[0], 5))
        label_sample_3[:, 2] = 0
        label_sample_3 = torch.FloatTensor(label_sample_3)

        label_sample_4 = 0.25*np.ones((data_1.size()[0], 5))
        label_sample_4[:, 3] = 0
        label_sample_4 = torch.FloatTensor(label_sample_4)

        label_sample_1 = label_sample_1.to(device)
        label_sample_2 = label_sample_2.to(device)
        label_sample_3 = label_sample_3.to(device)
        label_sample_4 = label_sample_4.to(device)

        generator_loss = -torch.mean(target_1 * torch.mean(label_sample_1 * torch.log(d_sample_1 + EPS), dim=-1) +
                                     target_2 * torch.mean(label_sample_2 * torch.log(d_sample_2 + EPS), dim=-1) +
                                     target_3 * torch.mean(label_sample_3 * torch.log(d_sample_3 + EPS), dim=-1) +
                                     target_4 * torch.mean(label_sample_4 * torch.log(d_sample_4 + EPS), dim=-1))

        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item() + discriminator_loss.item() + generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Test Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_test.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)

    return logger

def predict(model, generator_test, prediction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(num_elements,prediction_size)
    labels = torch.zeros(num_elements,1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            data = data.to(device)
            target = target.to(device)

            pred = model(data)
            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()
    return predictions.detach().numpy(), labels.detach().numpy()


################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    DataLoader = data_reader.DataLoader_Longitudinal_semisupervised()
    DataLoader_D3 = data_reader.DataLoader_Longitudinal_semisupervised(data_path=data_reader.DATA_PATH_D3)

    ## Dictionaries
    av45_dict, av45_labels_dict = DataLoader.load_modality(modality_name='av45_pet')
    vMRI_dict, vMRI_labels_dict = DataLoader.load_modality(modality_name='volumetric_mri')
    cMRI_dict, cMRI_labels_dict = DataLoader.load_modality(modality_name='cortical_thickness_mri')
    FDG_dict, FDG_labels_dict = DataLoader.load_modality(modality_name='fdg_pet')
    csf_dict, csf_labels_dict = DataLoader.load_modality(modality_name='csf')
    imaging_phenotypes_dict, imaging_phenotypes_labels_dict = DataLoader.load_modality(modality_name='imaging_phenotypes')
    education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
    age_dict, age_labels_dict = DataLoader.load_age()
    clinical_dict, clinical_labels_dict = DataLoader.load_clinical(clinical_names=clinical_names)
    dx_dict, dx_labels_dict = DataLoader.load_dx()

    rid_events_dict = {rid: [e.step for e in subject.events] for rid, subject in DataLoader.subjects_dict.items()}
    rid_events_list = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list]

    test_dict = {rid: {e_step: [age_dict[rid][e_step]] + clinical_dict[rid][e_step] + education_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}
    test_labels_dict = {rid: {e_step: age_labels_dict[rid][e_step]*clinical_labels_dict[rid][e_step]*education_labels_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}

    av45_dict_D3, av45_labels_dict_D3 = DataLoader_D3.load_modality(modality_name='av45_pet')
    vMRI_dict_D3, vMRI_labels_dict_D3 = DataLoader_D3.load_modality(modality_name='volumetric_mri')
    cMRI_dict_D3, cMRI_labels_dict_D3 = DataLoader_D3.load_modality(modality_name='cortical_thickness_mri')
    FDG_dict_D3, FDG_labels_dict_D3 = DataLoader_D3.load_modality(modality_name='fdg_pet')
    csf_dict_D3, csf_labels_dict_D3 = DataLoader_D3.load_modality(modality_name='csf')
    imaging_phenotypes_dict_D3, imaging_phenotypes_labels_dict_D3 = DataLoader_D3.load_modality(
        modality_name='imaging_phenotypes')
    education_dict_D3, education_labels_dict_D3 = DataLoader_D3.load_demographic(demo_names=['education'])
    age_dict_D3, age_labels_dict_D3 = DataLoader_D3.load_age()
    clinical_dict_D3, clinical_labels_dict_D3 = DataLoader_D3.load_clinical(clinical_names=clinical_names)
    dx_dict_D3, dx_labels_dict_D3 = DataLoader.load_dx()

    rid_events_dict_D3 = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_D3.subjects_dict.items()}
    rid_events_list_D3 = [(rid, e_step) for rid, e_step_list in rid_events_dict_D3.items() for e_step in e_step_list]

    test_dict_D3= {
    rid: {e_step: [age_dict_D3[rid][e_step]] + clinical_dict_D3[rid][e_step] + education_dict_D3[rid][e_step] for e_step in
          e_step_list} for rid, e_step_list in rid_events_dict_D3.items()}
    test_labels_dict_D3 = {
    rid: {e_step: age_labels_dict_D3[rid][e_step] * clinical_labels_dict_D3[rid][e_step] * education_labels_dict_D3[rid][e_step]
          for e_step in e_step_list} for rid, e_step_list in rid_events_dict_D3.items()}

    ## Codenames
    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    clinical_id_list = clinical_names
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    test_id_list = ['age'] + clinical_names + ['education']

    vMRI_id_list_D3 = DataLoader_D3.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list_D3 = DataLoader_D3.load_modality_codenames(modality_name='cortical_thickness_mri')
    clinical_id_list_D3 = clinical_names
    imaging_phenotypes_id_list_D3 = DataLoader_D3.load_modality_codenames(modality_name='imaging_phenotypes')
    test_id_list_D3 = ['age'] + clinical_names + ['education']

    ## Modalities used
    modality_list = ['av45','vMRI', 'cMRI', 'clinical']
    modality_dict_list = [av45_dict, vMRI_dict, cMRI_dict, clinical_dict]
    modality_dict_list_D3 = [av45_dict_D3, vMRI_dict_D3, cMRI_dict_D3, clinical_dict_D3]
    modality_labels_dict_list = [av45_labels_dict, vMRI_labels_dict, cMRI_labels_dict, clinical_labels_dict]
    modality_labels_dict_list_D3 = [av45_labels_dict_D3, vMRI_labels_dict_D3, cMRI_labels_dict_D3, clinical_labels_dict_D3]
    modality_id_list = [av45_id_list, vMRI_id_list, cMRI_id_list, clinical_id_list]

    age_array = np.asarray([age_dict[rid][e_step] for (rid, e_step) in rid_events_list])
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)
    age_labels_array = np.asarray([age_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list])
    age_array_D3 = np.asarray([age_dict_D3[rid][e_step] for (rid, e_step) in rid_events_list_D3])
    if len(age_array_D3.shape) < 2:
        age_array_D3 = age_array_D3.reshape(-1, 1)
    age_labels_array_D3 = np.asarray([age_labels_dict_D3[rid][e_step] for (rid, e_step) in rid_events_list_D3])

    m1_array = np.asarray([modality_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list])
    if len(m1_array.shape) < 2:
        m1_array = m1_array.reshape(-1, 1)
    m1_labels_array = np.asarray([modality_labels_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list])
    m1_array_D3 = np.asarray([modality_dict_list_D3[0][rid][e_step] for (rid, e_step) in rid_events_list_D3])
    if len(m1_array_D3.shape) < 2:
        m1_array_D3 = m1_array_D3.reshape(-1, 1)
    m1_labels_array_D3 = np.asarray([modality_labels_dict_list_D3[0][rid][e_step] for (rid, e_step) in rid_events_list_D3])

    m2_array = np.asarray([modality_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list])
    if len(m2_array.shape) < 2:
        m2_array = m2_array.reshape(-1, 1)
    m2_labels_array = np.asarray([modality_labels_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list])
    m2_array_D3 = np.asarray([modality_dict_list_D3[1][rid][e_step] for (rid, e_step) in rid_events_list_D3])
    if len(m2_array_D3.shape) < 2:
        m2_array_D3 = m2_array_D3.reshape(-1, 1)
    m2_labels_array_D3 = np.asarray([modality_labels_dict_list_D3[1][rid][e_step] for (rid, e_step) in rid_events_list_D3])

    m3_array = np.asarray([modality_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list])
    if len(m3_array.shape) < 2:
        m3_array = m3_array.reshape(-1, 1)
    m3_labels_array = np.asarray([modality_labels_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list])
    m3_array_D3 = np.asarray([modality_dict_list_D3[2][rid][e_step] for (rid, e_step) in rid_events_list_D3])
    if len(m3_array_D3.shape) < 2:
        m3_array_D3 = m3_array_D3.reshape(-1, 1)
    m3_labels_array_D3 = np.asarray([modality_labels_dict_list_D3[2][rid][e_step] for (rid, e_step) in rid_events_list_D3])

    m4_array = np.asarray([modality_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list])
    if len(m4_array.shape) < 2:
        m4_array = m4_array.reshape(-1, 1)
    m4_labels_array = np.asarray([modality_labels_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list])
    m4_array_D3 = np.asarray([modality_dict_list_D3[3][rid][e_step] for (rid, e_step) in rid_events_list_D3])
    if len(m4_array_D3.shape) < 2:
        m4_array_D3 = m4_array_D3.reshape(-1, 1)
    m4_labels_array_D3 = np.asarray([modality_labels_dict_list_D3[3][rid][e_step] for (rid, e_step) in rid_events_list_D3])

    dx_array = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in rid_events_list])
    if len(dx_array.shape) < 2:
        dx_array = dx_array.reshape(-1, 1)
    dx_labels_array = np.asarray([dx_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list])
    dx_array_D3 = np.asarray([dx_dict_D3[rid][e_step] for (rid, e_step) in rid_events_list_D3])
    if len(dx_array_D3.shape) < 2:
        dx_array_D3 = dx_array_D3.reshape(-1, 1)
    dx_labels_array_D3 = np.asarray([dx_labels_dict_D3[rid][e_step] for (rid, e_step) in rid_events_list_D3])

    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    ## PRE-PROCESSING
    std_scaler = StandardScaler()
    m1_array[np.where(m1_labels_array == 1)] = std_scaler.fit_transform(m1_array[np.where(m1_labels_array == 1)])
    if len(np.where(m1_labels_array_D3 == 1)[0]) > 0:
        m1_array_D3[np.where(m1_labels_array_D3 == 1)] = std_scaler.transform(m1_array_D3[np.where(m1_labels_array_D3 == 1)])
    else:
        m1_array_D3 = np.zeros((m1_array_D3.shape[0], m1_array.shape[1]))

    m2_array[np.where(m2_labels_array == 1)] = std_scaler.fit_transform(m2_array[np.where(m2_labels_array == 1)])
    if len(np.where(m2_labels_array_D3 == 1)[0]) > 0:
        m2_array_D3[np.where(m2_labels_array_D3 == 1)] = std_scaler.transform(
            m2_array_D3[np.where(m2_labels_array_D3 == 1)])
    else:
        m2_array_D3 = np.zeros((m2_array_D3.shape[0], m2_array.shape[1]))

    m3_array[np.where(m3_labels_array == 1)] = std_scaler.fit_transform(m3_array[np.where(m3_labels_array == 1)])
    if len(np.where(m3_labels_array_D3 == 1)[0]) > 0:
        m3_array_D3[np.where(m3_labels_array_D3 == 1)] = std_scaler.transform(
            m3_array_D3[np.where(m3_labels_array_D3 == 1)])
    else:
        m3_array_D3 = np.zeros((m3_array_D3.shape[0], m3_array.shape[1]))

    m4_array[np.where(m4_labels_array == 1)] = std_scaler.fit_transform(m4_array[np.where(m4_labels_array == 1)])
    if len(np.where(m4_labels_array_D3 == 1)[0]) > 0:
        m4_array_D3[np.where(m4_labels_array_D3 == 1)] = std_scaler.transform(m4_array_D3[np.where(m4_labels_array_D3 == 1)])
    else:
        m4_array_D3 = np.zeros((m4_array_D3.shape[0], m4_array.shape[1]))

    age_array[np.where(age_labels_array == 1)] = std_scaler.fit_transform(age_array[np.where(age_labels_array == 1)])
    age_array_D3[np.where(age_labels_array_D3 == 1)] = std_scaler.transform(age_array_D3[np.where(age_labels_array_D3 == 1)])

    N1, M1 = m1_array.shape
    N2, M2 = m2_array.shape
    N3, M3 = m3_array.shape
    N4, M4 = m4_array.shape
    N = [N1, N2, N3, N4]
    M = [M1, M2, M3, M4]

    print('N1: ' + str(N1) + ', M1: ' + str(M1))
    print('N2: ' + str(N1) + ', M2: ' + str(M2))
    print('N3: ' + str(N1) + ', M3: ' + str(M3))
    print('N4: ' + str(N1) + ', M4: ' + str(M4))

    rid_events_list_all_modalities = data_reader.find_list_tuple_intersection([
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if modality_labels_dict_list[0][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if modality_labels_dict_list[1][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if modality_labels_dict_list[2][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if modality_labels_dict_list[3][rid][e_step] == 1],

    ])

    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_multiview_semisupervised.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    hidden_layers_dim_1_enc = [N_COMPONENTS]
    hidden_layers_dim_2_enc = [N_COMPONENTS]
    hidden_layers_dim_3_enc = [N_COMPONENTS]
    hidden_layers_dim_4_enc = [N_COMPONENTS]
    hidden_layers_dim_1_dec = [N_COMPONENTS+1]
    hidden_layers_dim_2_dec = [N_COMPONENTS+1]
    hidden_layers_dim_3_dec = [N_COMPONENTS+1]
    hidden_layers_dim_4_dec = [N_COMPONENTS+1]
    n_latent_dec = N_COMPONENTS + 1

    hidden_layers_discriminator_dim = [10, len(modality_list) + 1]
    latent_dim = N_COMPONENTS

    Generator_model_1 = Encoder_lineal(M1, hidden_layers_dim_1_enc)
    Decoder_model_1 = Decoder_lineal(M1, hidden_layers_dim_1_dec)
    AAE_model_1 = Autoencoder(Generator_model_1, Decoder_model_1)

    Generator_model_2 = Encoder_lineal(M2, hidden_layers_dim_2_enc)
    Decoder_model_2 = Decoder_lineal(M2, hidden_layers_dim_2_dec)
    AAE_model_2 = Autoencoder(Generator_model_2, Decoder_model_2)

    Generator_model_3 = Encoder_lineal(M3, hidden_layers_dim_3_enc)
    Decoder_model_3 = Decoder_lineal(M3, hidden_layers_dim_3_dec)
    AAE_model_3 = Autoencoder(Generator_model_3, Decoder_model_3)

    Generator_model_4 = Encoder_lineal(M4, hidden_layers_dim_4_enc)
    Decoder_model_4 = Decoder_lineal(M4, hidden_layers_dim_4_dec)
    AAE_model_4 = Autoencoder(Generator_model_4, Decoder_model_4)

    Discriminator_model = Discriminator(latent_dim, hidden_layers_discriminator_dim)

    # Set learning rates
    ae_lr = learning_rate['autoencoder']
    gen_lr = learning_rate['generator']
    reg_lr = learning_rate['discriminator']

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    # encode/decode optimizers
    optim_AAE_1 = torch.optim.Adam(AAE_model_1.parameters(), lr=ae_lr)
    optim_G_1 = torch.optim.Adam(Generator_model_1.parameters(), lr=gen_lr)
    optim_AAE_2 = torch.optim.Adam(AAE_model_2.parameters(), lr=ae_lr)
    optim_G_2 = torch.optim.Adam(Generator_model_2.parameters(), lr=gen_lr)
    optim_AAE_3 = torch.optim.Adam(AAE_model_3.parameters(), lr=ae_lr)
    optim_G_3 = torch.optim.Adam(Generator_model_3.parameters(), lr=gen_lr)
    optim_AAE_4 = torch.optim.Adam(AAE_model_4.parameters(), lr=ae_lr)
    optim_G_4 = torch.optim.Adam(Generator_model_4.parameters(), lr=gen_lr)
    optim_D = torch.optim.Adam(Discriminator_model.parameters(), lr=reg_lr)

    model_dict = {'autoencoder_1': AAE_model_1, 'autoencoder_2': AAE_model_2,
                  'autoencoder_3': AAE_model_3, 'autoencoder_4': AAE_model_4,
                  'discriminator': Discriminator_model,
                  'generator_1': Generator_model_1, 'generator_2': Generator_model_2,
                  'generator_3': Generator_model_3, 'generator_4': Generator_model_4
                  }

    optimizer_dict = {'autoencoder_1': optim_AAE_1, 'autoencoder_2': optim_AAE_1,
                      'autoencoder_3': optim_AAE_3, 'autoencoder_4': optim_AAE_4,
                      'discriminator': optim_D,
                      'generator_1': optim_G_1, 'generator_2': optim_G_2,
                      'generator_3': optim_G_3, 'generator_4': optim_G_4}

    #################################
    ##########  READ DATA  ##########
    #################################

    dataset = MyDataset_disentangle([m1_array, m2_array, m3_array, m4_array],
                                    [m1_labels_array, m2_labels_array, m3_labels_array, m4_labels_array],
                                    age_array)

    generator_train = torch.utils.data.DataLoader(
        dataset,
        batch_size=N1,
        shuffle=True,
        num_workers=2,
        pin_memory=torch.cuda.is_available()

    )

    dataset_1 = MyDataset(m1_array, dx_array)
    dataset_2 = MyDataset(m2_array, dx_array)
    dataset_3 = MyDataset(m3_array, dx_array)
    dataset_4 = MyDataset(m4_array, dx_array)

    generator_test_1 = torch.utils.data.DataLoader(
        dataset_1,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_2 = torch.utils.data.DataLoader(
        dataset_2,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_3 = torch.utils.data.DataLoader(
        dataset_3,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_4 = torch.utils.data.DataLoader(
        dataset_4,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )


    dataset_1_yscores = MyDataset(np.concatenate((m1_array, m1_array_D3), axis=0), np.concatenate((dx_array, dx_array_D3), axis=0))
    dataset_2_yscores = MyDataset(np.concatenate((m2_array, m2_array_D3), axis=0), np.concatenate((dx_array, dx_array_D3), axis=0))
    dataset_3_yscores = MyDataset(np.concatenate((m3_array, m3_array_D3), axis=0), np.concatenate((dx_array, dx_array_D3), axis=0))
    dataset_4_yscores = MyDataset(np.concatenate((m4_array, m4_array_D3), axis=0), np.concatenate((dx_array, dx_array_D3), axis=0))
    release_list = ['D1_D2']*m4_array.shape[0] + ['D3']*m4_array_D3.shape[0]
    rid_events_list_yscores = rid_events_list + rid_events_list_D3
    modality_labels_list_yscores = [
        np.concatenate((m1_labels_array, m1_labels_array_D3), axis=0),np.concatenate((m2_labels_array, m2_labels_array_D3), axis=0),
        np.concatenate((m3_labels_array, m3_labels_array_D3), axis=0),np.concatenate((m4_labels_array, m4_labels_array_D3), axis=0)
    ]

    generator_test_1_yscores = torch.utils.data.DataLoader(
        dataset_1_yscores,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_2_yscores = torch.utils.data.DataLoader(
        dataset_2_yscores,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_3_yscores = torch.utils.data.DataLoader(
        dataset_3_yscores,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_4_yscores = torch.utils.data.DataLoader(
        dataset_4_yscores,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    ################################
    ##########  TRAINING  ##########
    ################################

    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    YSCORES_PATH = join(MODALITY_PATH, 'Y_SCORES')
    if not exists(YSCORES_PATH):
        os.makedirs(YSCORES_PATH)

    TRAIN_MODEL = True
    if TRAIN_MODEL:
        log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss','loss']
        logger = History()
        logger.on_train_init(log_keys)
        for epoch in range(1, n_epochs + 1):
            logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)

        torch.save(model_dict, join(MODALITY_PATH,'model_dict.pt'))
        fig, ax1 = plt.subplots()

        ax2 = ax1.twinx()
        ax1.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
        ax2.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
        ax2.plot(logger.logs['generator_loss'], 'g', label='Generator')
        ax1.plot(logger.logs['loss'], 'k', label='Total loss')

        ax1.set_xlabel('Iterations')
        ax1.set_ylabel('Reconstruction and Total loss', color='g')
        ax2.set_ylabel('Discriminator and generator loss', color='b')

        ax1.legend()
        ax2.legend()
        plt.savefig(join(MODALITY_PATH,'training.png'))
        plt.close()
    else:
        model_dict = torch.load(join(MODALITY_PATH,'model_dict.pt'))

    ###############################
    ##########  RESULTS  ##########
    ###############################
    writer_list = []
    for it_component in range(n_latent_dec):
        COMPONENTS_PATH = join(MODALITY_PATH, str(it_component))
        if not exists(COMPONENTS_PATH):
            os.makedirs(COMPONENTS_PATH)

        writer_list.append(ResultsWriter(filepath=join(COMPONENTS_PATH, 'results_file.txt'), attach=False))

    SAVE_Y_SCORES = True
    CORRELATION_PLOTS = True
    Y_SCORE_PLOTS = True
    STATISTICAL_PLOTS = True
    WEIGHT_PLOTS = True
    FACTOR_PLOTS = True

    generator_test_list = [generator_test_1,generator_test_2,generator_test_3,generator_test_4]
    modality_labels_list = [m1_labels_array, m2_labels_array, m3_labels_array, m4_labels_array]
    generator_test_list_yscores = [generator_test_1_yscores,generator_test_2_yscores,
                                   generator_test_3_yscores,generator_test_4_yscores]

    y_scores_list = [predict(model_dict['autoencoder_'+str(it_g+1)].encoder, g, latent_dim)[0] for it_g, g in enumerate(generator_test_list)]
    y_scores_list_write = [predict(model_dict['autoencoder_'+str(it_g+1)].encoder, g, latent_dim)[0] for it_g, g in enumerate(generator_test_list_yscores)]
    y_scores_indices = [np.where(gl==1)[0] for gl in modality_labels_list]
    y_scores_dx_indices = [[np.where(dx_array[ys_idx] == 0)[0],
                            np.where(dx_array[ys_idx] == 1)[0],
                            np.where(dx_array[ys_idx] == 2)[0]] for ys_idx in y_scores_indices]

    if SAVE_Y_SCORES:
        print('SAVE_Y_SCORES')
        for it_ys, ys in enumerate(y_scores_list_write):
            writer = CSVWriter(filepath=join(YSCORES_PATH, modality_list[it_ys] + '.csv'),
                               header = ['RID', 'event','release'] + [it for it in range(N_COMPONENTS)],
                               attach=False)
            rows = []
            for it_row in range(len(rid_events_list_yscores)):
                if modality_labels_list_yscores[it_ys][it_row] == 0:
                    rows.append({**{'RID': rid_events_list_yscores[it_row][0],
                                    'event': rid_events_list_yscores[it_row][1],
                                    'release': release_list[it_row]},
                                 **{it_col: '' for it_col in range(N_COMPONENTS) }})
                else:
                    rows.append({**{'RID': rid_events_list_yscores[it_row][0],
                                    'event': rid_events_list_yscores[it_row][1],
                                    'release': release_list[it_row]},
                                 **{it_col: ys[it_row, it_col] for it_col in range(N_COMPONENTS) }})
            writer.writerows(rows)

    if CORRELATION_PLOTS:
        print('CORRELATION_PLOTS')
        if not rid_events_list_all_modalities:
            print('########### There are no common indices from ALL modalities')
        else:
            writer_scatter = ResultsWriter(filepath=join(MODALITY_PATH, 'scatter_plot_betas.txt'), attach=False)
            writer_scatter.write('List of betas of the linear regression between modalities\n\n')

            indices_all_modalities = [it_r for it_r, (r,e) in enumerate(rid_events_list) if (r,e) in rid_events_list_all_modalities]
            y_scores_all_modalities = [y_scores_list[it_modality][indices_all_modalities] for it_modality in
                                       range(len(modality_list))]


            index_0 = np.where(dx_array[indices_all_modalities] == 0)[0]
            index_1 = np.where(dx_array[indices_all_modalities] == 1)[0]
            index_2 = np.where(dx_array[indices_all_modalities] == 2)[0]


            lr_model = LinearRegression()
            for it_c in range(N_COMPONENTS):
                writer_scatter.write('COMPONENT ' + str(it_c) + ':\n')

                if len(y_scores_list) <= 2:

                    fig, axarr = plt.subplots(1, 1)
                    axarr.plot(y_scores_all_modalities[0][index_0, it_c], y_scores_all_modalities[1][index_0, it_c], 'b*', label='NC')
                    axarr.plot(y_scores_all_modalities[0][index_1, it_c], y_scores_all_modalities[1][index_1, it_c], 'r*', label='MCI')
                    axarr.plot(y_scores_all_modalities[0][index_2, it_c], y_scores_all_modalities[1][index_2, it_c], 'g*', label='AD')

                    lr_model.fit(y_scores_all_modalities[0][:, it_c: it_c + 1], y_scores_all_modalities[1][:, it_c:it_c + 1])
                    axarr.plot(y_scores_all_modalities[0][:, it_c:it_c + 1], lr_model.predict(y_scores_all_modalities[0][:, it_c:it_c + 1]), 'k-')
                    axarr.set_xlabel(modality_list[0])
                    axarr.set_ylabel(modality_list[1])


                else:

                    writer_scatter.write(','.join(modality_list[1:]))
                    writer_scatter.write('\n')

                    fig, axarr = plt.subplots(len(y_scores_list) - 1, len(y_scores_list) - 1)
                    for it_row in range(len(y_scores_list) - 1):
                        writer_scatter.write(modality_list[it_row])
                        for i in range(it_row):
                            writer_scatter.write(',     ')

                        for it_col in range(it_row + 1, len(y_scores_list)):

                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][index_0, it_c], y_scores_all_modalities[it_row][index_0, it_c],
                                                           'b*', label='NC')
                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][index_1, it_c], y_scores_all_modalities[it_row][index_1, it_c],
                                                           'r*', label='MCI')
                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][index_2, it_c], y_scores_all_modalities[it_row][index_2, it_c],
                                                           'g*', label='AD')

                            lr_model.fit(y_scores_all_modalities[it_col][:, it_c: it_c + 1], y_scores_all_modalities[it_row][:, it_c:it_c + 1])
                            r, p = pearsonr(y_scores_all_modalities[it_col][:, it_c: it_c + 1], y_scores_all_modalities[it_row][:, it_c:it_c + 1])
                            writer_scatter.write(','+str(np.round(np.squeeze(r),3)))

                            axarr[it_row, it_col - 1].plot(y_scores_all_modalities[it_col][:, it_c:it_c + 1],
                                                           lr_model.predict(y_scores_all_modalities[it_col][:, it_c:it_c + 1]), 'k-')

                            if it_col - 1 == 0:
                                for i in range(len(y_scores_list) - 1):
                                    axarr[i, it_col - 1].set_ylabel(modality_list[i])
                        writer_scatter.write('\n')
                        if it_row == len(y_scores_list) - 2:
                            for i in range(len(y_scores_list) - 1):
                                axarr[it_row, i].set_xlabel(modality_list[i + 1])
                plt.legend()
                plt.savefig(join(MODALITY_PATH, str(it_c), 'scatter_plot.png'))
                plt.close()
                writer_scatter.write('\n')

    if Y_SCORE_PLOTS:
        print('Y_SCORE_PLOTS')
        for it_component in range(N_COMPONENTS):
            for it_modality, modality in enumerate(modality_list):

                plt.figure()
                plt.hist(y_scores_list[it_modality][y_scores_indices[it_modality], it_component], bins=40)
                plt.savefig(join(MODALITY_PATH, str(it_component), modality + '_' + str(it_modality) + '_histogram.png'))
                plt.close()

            # plt.figure()
            # plt.hist(y_scores_list[1][:, it_component], bins=40)
            # plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[1] + '_histogram.png'))
            # plt.close()
            #
            # plt.figure()
            # plt.hist(y_scores_list[2][:, it_component], bins=40)
            # plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[2] + '_histogram.png'))
            # plt.close()
            #
            # plt.figure()
            # plt.hist(y_scores_list[3][:, it_component], bins=40)
            # plt.savefig(join(MODALITY_PATH, str(it_component), modality_list[3] + '_histogram.png'))
            # plt.close()

    if STATISTICAL_PLOTS:
        print('STATISTICAL_PLOTS')

        for it_component in range(N_COMPONENTS):
            writer_list[it_component].write('#############################\n')
            writer_list[it_component].write('RESULTS DIAGNOSTIC COMPARISON\n')
            writer_list[it_component].write('#############################\n\n')

        combination_plots = list(itertools.combinations(range(3), 2))
        for it_modality, modality in enumerate(modality_list):

            for it_component in range(N_COMPONENTS):
                writer_list[it_component].write('--- ' + modality + ' subspace ---\n')

                t = []
                p = []
                boxplot_list = [y_scores_list[it_modality][y_scores_indices[it_modality]][y_scores_dx_indices[it_modality][0], it_component],
                                y_scores_list[it_modality][y_scores_indices[it_modality]][y_scores_dx_indices[it_modality][1], it_component],
                                y_scores_list[it_modality][y_scores_indices[it_modality]][y_scores_dx_indices[it_modality][2], it_component]]

                for c in combination_plots:
                    result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                    t.append(result[0])
                    p.append(result[1])
                    writer_list[it_component].write('   - ' + str((label_diagnostic[c[0]], label_diagnostic[c[1]]))
                                                    + ': ' + str((result[0], result[1])) + '\n')

                filename = modality + '_diagnostic.png'
                plt.figure()
                plt.boxplot(boxplot_list)
                plt.xticks([1, 2, 3], label_diagnostic)
                plt.title('Significant (p<0.05) differences: ' + str(
                    [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                plt.savefig(join(MODALITY_PATH, str(it_component), filename))
                plt.close()

    if WEIGHT_PLOTS:
        print('WEIGHT_PLOTS')
        for it_component in range(N_COMPONENTS):
            writer_list[it_component].write('####################\n')
            writer_list[it_component].write('PLS Y_WEIGHT RESULTS\n')
            writer_list[it_component].write('####################\n\n')

        y_weight_generator = np.eye(n_latent_dec)
        y_weight_generator_labels = np.arange(0, n_latent_dec).reshape(-1, 1)
        dataset_y_weight = MyDataset(y_weight_generator, y_weight_generator_labels)
        generator_y_weight = torch.utils.data.DataLoader(
            dataset_y_weight,
            batch_size=N_COMPONENTS,
            shuffle=True,
            num_workers=1,
            pin_memory=torch.cuda.is_available()

        )

        y_rotations = [predict(model_dict['autoencoder_1'].decoder, generator_y_weight, M1)[0],
                       predict(model_dict['autoencoder_2'].decoder, generator_y_weight, M2)[0],
                       predict(model_dict['autoencoder_3'].decoder, generator_y_weight, M3)[0],
                       predict(model_dict['autoencoder_4'].decoder, generator_y_weight, M4)[0]]

        for it_modality, modality in enumerate(modality_list):

            for it_component in range(n_latent_dec):
                writer_list[it_component].write(modality_list[it_modality])
                writer_list[it_component].write('\n')

                sorted_mod = np.argsort(y_rotations[it_modality][it_component, :])[::-1]
                for m_prs in sorted_mod:
                    writer_list[it_component].write('  - ' + modality_id_list[it_modality][m_prs] + ': ' + str(y_rotations[it_modality][it_component, m_prs]) + '\n')

                filename = modality + '_weights.png'

                y_min = np.min(y_rotations[it_modality][it_component, :]) * 0.9
                y_max = np.max(y_rotations[it_modality][it_component, :]) * 1.1
                y_lim = np.max([np.abs(y_min), np.abs(y_max)])


                _, ax = plt.subplots()
                ax.barh(np.arange(M[it_modality]), y_rotations[it_modality][it_component, :], align='center',
                        color='green', ecolor='black')
                ax.set_yticks(np.arange(start=0, stop=M[it_modality], step=np.max([1, int(M[it_modality] / 10)])))
                ax.set_xticks(np.concatenate((np.arange(start=-y_lim, stop=0, step=y_lim / 3),
                                              np.arange(start=0, stop=y_lim, step=y_lim / 3), [y_lim])))
                ax.invert_yaxis()
                ax.set_xlabel('Latent variable ' + str(it_component))
                ax.set_title('Relative weight of each input variable')
                ax.grid()
                ax.set_xlim([-y_lim, y_lim])
                plt.savefig(join(MODALITY_PATH, str(it_component), filename))
                plt.close()

    if FACTOR_PLOTS:
        print('FACTOR_PLOTS')

        n_rows = ROW_DICT[N_COMPONENTS]
        n_cols = int(np.ceil(N_COMPONENTS / n_rows))

        # wmh_dict = {k: v['bl'] for k, v in adni_reader.load_WMH().items() if 'bl' in list(v.keys())}
        education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
        gender_dict, gender_labels_dict = DataLoader.load_demographic(demo_names=['gender'])
        apoe4_dict, apoe4_labels_dict = DataLoader.load_demographic(demo_names=['apoe4'])
        ab_status_dict, ab_status_labels_dict = DataLoader.load_amyloid_status()
        mmse_dict, mmse_labels_dict = DataLoader.load_clinical(clinical_names=['mmse'])
        cdrsb_dict, cdrsb_labels_dict = DataLoader.load_clinical(clinical_names=['cdrsb'])

        writer_factor = ResultsWriter(filepath=join(MODALITY_PATH, 'factor_analysis.txt'), attach=False)
        writer_factor.write('###########################\n')
        writer_factor.write('FACTOR CORRELATION ANALYSIS\n')
        writer_factor.write('###########################\n')

        factor_dict_list = [age_dict, education_dict, mmse_dict, cdrsb_dict]
        factor_labels_dict_list = [age_labels_dict, education_labels_dict, mmse_labels_dict, cdrsb_labels_dict]
        factor_name_list = ['age', 'education', 'mmse', 'cdrsb']
        for factor_dict, factor_labels_dict, factor_name in zip(factor_dict_list, factor_labels_dict_list, factor_name_list):
            writer_factor.write('\nFACTOR: ' + factor_name + '\n')

            rid_events_list_factor = [(rid, e_step) for (rid, e_step) in rid_events_list if factor_labels_dict[rid][e_step] == 1]
            factor_array = np.asarray([factor_dict[rid][e_step] for (rid, e_step) in rid_events_list]).reshape(-1, )
            factor_label_array = np.asarray([factor_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list]).reshape(-1, )
            factor_indices = np.where(factor_label_array == 1)[0]

            for it_modality, modality in enumerate(modality_list):
                writer_factor.write('   - ' + modality + ' subspace:\n')

                indices_modality_factor = data_reader.find_list_intersection(
                    [y_scores_indices[it_modality], factor_indices])

                y_scores_factor = y_scores_list[it_modality][indices_modality_factor]
                dx_array_factor = dx_array[indices_modality_factor]
                index_0 = np.where(dx_array_factor == 0)[0]
                index_1 = np.where(dx_array_factor == 1)[0]
                index_2 = np.where(dx_array_factor == 2)[0]

                fig_y, axarr_y = plt.subplots(n_rows, n_cols)
                for it_c in range(N_COMPONENTS):
                    writer_factor.write('     Component ' + str(it_c) + ':')
                    writer_factor.write(
                        ' Y:' + str(pearsonr(y_scores_factor[:, it_c], factor_array[indices_modality_factor])) + '\n')

                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_0, it_c],
                                                                              factor_array[indices_modality_factor][index_0], '*b',
                                                                              label='NC')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_1, it_c],
                                                                              factor_array[indices_modality_factor][index_1], '*r',
                                                                              label='MCI')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_2, it_c],
                                                                              factor_array[indices_modality_factor][index_2], '*k',
                                                                              label='AD')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor_name)

                plt.figure(1)
                plt.savefig(join(MODALITY_PATH, modality + '_score_' + factor_name + '_correlation.png'))
                plt.close()



        factor_dict_list = [gender_dict, apoe4_dict, ab_status_dict]
        factor_labels_dict_list = [gender_labels_dict, apoe4_labels_dict, ab_status_labels_dict]
        factor_name_list = ['gender', 'apoe4', 'abstatus']
        for factor_dict, factor_labels_dict, factor_name in zip(factor_dict_list, factor_labels_dict_list,
                                                                factor_name_list):
            print(factor_name)
            writer_factor.write('\nFACTOR: ' + factor_name + '\n')

            rid_events_list_factor = [(rid, e_step) for (rid, e_step) in rid_events_list if factor_labels_dict[rid][e_step] == 1]
            factor_array = np.asarray([factor_dict[rid][e_step] for (rid, e_step) in rid_events_list])
            factor_label_array = np.asarray([factor_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list])
            factor_indices = np.where(factor_label_array == 1)[0]

            for it_modality, modality in enumerate(modality_list):
                writer_factor.write('   - ' + modality + ' subspace:\n')

                indices_modality_factor = data_reader.find_list_intersection([y_scores_indices[it_modality], factor_indices])

                y_scores_factor = y_scores_list[it_modality][indices_modality_factor]

                index_list = [np.where(factor_array[indices_modality_factor] == unique_label)[0] for unique_label in np.unique(factor_array)]
                combination_plots = list(itertools.combinations(range(len(np.unique(factor_array))), 2))

                fig_y, axarr_y = plt.subplots(n_rows, n_cols)
                for it_c in range(N_COMPONENTS):
                    boxplot_list = [y_scores_factor[idx, it_c] for idx in index_list]

                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].boxplot(boxplot_list)
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()

                    if it_c < n_rows:
                        axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor_name)
                    elif it_c % n_rows == n_rows-1:
                        axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')

                plt.figure(1)
                plt.savefig(join(MODALITY_PATH, modality + '_score_' + factor_name + '_correlation.png'))
                plt.close()

