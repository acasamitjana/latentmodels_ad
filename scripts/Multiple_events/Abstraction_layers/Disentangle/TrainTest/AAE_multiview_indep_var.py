import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data import adni_reader
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT, CSVWriter
from src.dataset import MyDataset, MyDataset_disentangle
from src.models import Encoder, Decoder, Discriminator, Autoencoder, Encoder_lineal, Decoder_lineal
from src.layers import Mean_layer, Var_layer

import os
import numpy as np
from os.path import join, exists
import itertools


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import StratifiedShuffleSplit, StratifiedKFold
from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################
N_COMPONENTS = 16
EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/mnt/gpid08/users/adria.casamitjana', 'Multimodal_autoencoders', 'AAE', 'Multiple_events',
                   'Abstraction_layers', 'Disentangle_simple', 'TrainTest','AllModalities',  'IndepVar', 'nc_' + str(N_COMPONENTS), 'lineal')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):
    for batch_idx, (data, target, factor, _) in enumerate(generator_train):
        data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
        factor = factor.to(device)

        for model_name, model in model_dict.items():
            model.zero_grad()

        #### RECONSTRUCTION LOSS
        model_dict['discriminator_1'].train(mode=False)
        model_dict['discriminator_2'].train(mode=False)
        model_dict['autoencoder_1'].train(mode=True)
        model_dict['autoencoder_2'].train(mode=True)
        model_dict['autoencoder_3'].train(mode=True)
        model_dict['autoencoder_4'].train(mode=True)

        # print('Reconstruction phase')
        # print( model_dict['autoencoder_1'].training)
        # print('===> ' + str(model_dict['autoencoder_1'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_1'].decoder.training))
        # print( model_dict['autoencoder_2'].training)
        # print('===> ' + str(model_dict['autoencoder_2'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_2'].decoder.training))
        # print( model_dict['generator_1'].training)
        # print( model_dict['generator_2'].training)
        # print( model_dict['discriminator'].training)

        z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_presample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to

        z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        z_var_1 =  Var_layer()([z_presample_1, z_mean])
        z_var_2 =  Var_layer()([z_presample_2, z_mean])
        z_var_3 =  Var_layer()([z_presample_3, z_mean])
        z_var_4 =  Var_layer()([z_presample_4, z_mean])

        z_sample_1 = torch.cat((z_mean, z_var_1), 1)
        z_sample_2 = torch.cat((z_mean, z_var_2), 1)
        z_sample_3 = torch.cat((z_mean, z_var_3), 1)
        z_sample_4 = torch.cat((z_mean, z_var_4), 1)

        x_reconstructed_1_1 = model_dict['autoencoder_1'].decode(torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
        # x_reconstructed_1_2 = model_dict['autoencoder_1'].decode(z_sample_2)  # decode to X reconstruction
        # x_reconstructed_1_3 = model_dict['autoencoder_1'].decode(z_sample_3)  # decode to X reconstruction
        # x_reconstructed_1_4 = model_dict['autoencoder_1'].decode(z_sample_4)  # decode to X reconstruction

        # x_reconstructed_2_1 = model_dict['autoencoder_2'].decode(z_sample_1)  # decode to X reconstruction
        x_reconstructed_2_2 = model_dict['autoencoder_2'].decode(torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
        # x_reconstructed_2_3 = model_dict['autoencoder_2'].decode(z_sample_3)  # decode to X reconstruction
        # x_reconstructed_2_4 = model_dict['autoencoder_2'].decode(z_sample_4)  # decode to X reconstruction

        # x_reconstructed_3_1 = model_dict['autoencoder_3'].decode(z_sample_1)  # decode to X reconstruction
        # x_reconstructed_3_2 = model_dict['autoencoder_3'].decode(z_sample_2)  # decode to X reconstruction
        x_reconstructed_3_3 = model_dict['autoencoder_3'].decode(torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
        # x_reconstructed_3_4 = model_dict['autoencoder_3'].decode(z_sample_4)  # decode to X reconstruction

        # x_reconstructed_4_1 = model_dict['autoencoder_4'].decode(z_sample_1)  # decode to X reconstruction
        # x_reconstructed_4_2 = model_dict['autoencoder_4'].decode(z_sample_2)  # decode to X reconstruction
        # x_reconstructed_4_3 = model_dict['autoencoder_4'].decode(z_sample_3)  # decode to X reconstruction
        x_reconstructed_4_4 = model_dict['autoencoder_4'].decode(torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

        reconstruction_loss_1 = ((x_reconstructed_1_1 - data_1) ** 2).sum(dim=-1)
        reconstruction_loss_2 = ((x_reconstructed_2_2 - data_2) ** 2).sum(dim=-1)
        reconstruction_loss_3 = ((x_reconstructed_3_3 - data_3) ** 2).sum(dim=-1)
        reconstruction_loss_4 = ((x_reconstructed_4_4 - data_4) ** 2).sum(dim=-1)


        reconstruction_loss = torch.mean(reconstruction_loss_1 + reconstruction_loss_2 +
                                         reconstruction_loss_3 + reconstruction_loss_4)

        reconstruction_loss.backward()
        optimizer_dict['autoencoder_1'].step()
        optimizer_dict['autoencoder_2'].step()
        optimizer_dict['autoencoder_3'].step()
        optimizer_dict['autoencoder_4'].step()


        #### DISCRIMINATOR LOSS
        model_dict['discriminator_1'].train(mode=True)
        model_dict['discriminator_2'].train(mode=True)
        model_dict['autoencoder_1'].train(mode=False)
        model_dict['autoencoder_2'].train(mode=False)
        model_dict['autoencoder_3'].train(mode=False)
        model_dict['autoencoder_4'].train(mode=False)

        z_real_gauss = 16*torch.randn(data_1.size()[0], latent_space_dims)
        z_real_gauss = z_real_gauss.to(device)

        a, b = 0.05, 0.05
        z_real_gamma = torch.distributions.Gamma(torch.tensor([a]), torch.tensor([b+1])).sample(torch.Size((data_1.size()[0], latent_space_dims,))).squeeze()
        z_real_gamma = z_real_gamma.to(device)

        z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_presample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to

        z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        z_var_1 = Var_layer()([z_presample_1, z_mean])
        z_var_2 = Var_layer()([z_presample_2, z_mean])
        z_var_3 = Var_layer()([z_presample_3, z_mean])
        z_var_4 = Var_layer()([z_presample_4, z_mean])

        d_mean_real = model_dict['discriminator_1'](z_real_gauss)
        d_mean_sample = model_dict['discriminator_1'](z_mean)
        discriminator_loss_mean = -torch.mean(torch.log(d_mean_real + EPS) + torch.log(1 - d_mean_sample + EPS))

        d_var_real = model_dict['discriminator_2'](z_real_gamma)
        d_var_sample_1 = model_dict['discriminator_2'](z_var_1)
        d_var_sample_2 = model_dict['discriminator_2'](z_var_2)
        d_var_sample_3 = model_dict['discriminator_2'](z_var_3)
        d_var_sample_4 = model_dict['discriminator_2'](z_var_4)

        label_sample_1 = np.zeros((data_1.size()[0], 5))
        label_sample_1[:, 0] = 1
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = np.zeros((data_1.size()[0], 5))
        label_sample_2[:, 1] = 1
        label_sample_2 = torch.FloatTensor(label_sample_2)

        label_sample_3 = np.zeros((data_1.size()[0], 5))
        label_sample_3[:, 2] = 1
        label_sample_3 = torch.FloatTensor(label_sample_3)

        label_sample_4 = np.zeros((data_1.size()[0], 5))
        label_sample_4[:, 3] = 1
        label_sample_4 = torch.FloatTensor(label_sample_4)

        label_real = np.zeros((data_1.size()[0], 5))
        label_real[:, 4] = 1
        label_real = torch.FloatTensor(label_real)

        label_sample_1 = label_sample_1.to(device)
        label_sample_2 = label_sample_2.to(device)
        label_sample_3 = label_sample_3.to(device)
        label_sample_4 = label_sample_4.to(device)
        label_real = label_real.to(device)

        discriminator_loss_var = -torch.mean(torch.mean(label_real * torch.log(d_var_real + EPS), dim=-1) +
                                             torch.mean(label_sample_1 * torch.log(d_var_sample_1 + EPS), dim=-1) +
                                             torch.mean(label_sample_2 * torch.log(d_var_sample_2 + EPS), dim=-1) +
                                             torch.mean(label_sample_3 * torch.log(d_var_sample_3 + EPS), dim=-1) +
                                             torch.mean(label_sample_4 * torch.log(d_var_sample_4 + EPS), dim=-1))


        discriminator_loss = discriminator_loss_mean + discriminator_loss_var

        discriminator_loss.backward()
        optimizer_dict['discriminator_1'].step()
        optimizer_dict['discriminator_2'].step()

        #### GENERATOR LOSS
        model_dict['generator_1'].train(mode=True)
        model_dict['generator_2'].train(mode=True)
        model_dict['generator_3'].train(mode=True)
        model_dict['generator_4'].train(mode=True)

        z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_presample_4 = model_dict['autoencoder_4'].encode(data_4)

        z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        d_mean_sample = model_dict['discriminator_1'](z_mean)

        generator_loss_mean = -torch.mean(torch.log(d_mean_sample + EPS))

        z_var_1 = Var_layer()([z_presample_1, z_mean])
        z_var_2 = Var_layer()([z_presample_2, z_mean])
        z_var_3 = Var_layer()([z_presample_3, z_mean])
        z_var_4 = Var_layer()([z_presample_4, z_mean])

        d_var_sample_1 = model_dict['discriminator_2'](z_var_1)
        d_var_sample_2 = model_dict['discriminator_2'](z_var_2)
        d_var_sample_3 = model_dict['discriminator_2'](z_var_3)
        d_var_sample_4 = model_dict['discriminator_2'](z_var_4)

        generator_loss_var = -torch.mean(torch.mean(label_real * torch.log(d_var_sample_1 + EPS), dim=-1) +
                                         torch.mean(label_real * torch.log(d_var_sample_2 + EPS), dim=-1) +
                                         torch.mean(label_real * torch.log(d_var_sample_3 + EPS), dim=-1) +
                                         torch.mean(label_real * torch.log(d_var_sample_4 + EPS), dim=-1))

        generator_loss = generator_loss_mean + generator_loss_var

        generator_loss.backward()
        optimizer_dict['generator_1'].step()
        optimizer_dict['generator_2'].step()
        optimizer_dict['generator_3'].step()
        optimizer_dict['generator_4'].step()

        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item() + discriminator_loss.item() + generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Prediction Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)

    return logger

def predict(model_dict, generator_test, latent_size, reconstruction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    mean_predictions = torch.zeros(latent_size)
    var_predictions = [torch.zeros(latent_size),
                                  torch.zeros(latent_size),
                                  torch.zeros(latent_size),
                                  torch.zeros(latent_size)]
    latent_predictions = [torch.zeros(latent_size),
                       torch.zeros(latent_size),
                       torch.zeros(latent_size),
                       torch.zeros(latent_size)]

    reconstruction_predictions = [torch.zeros(reconstruction_size[0]),
                                  torch.zeros(reconstruction_size[1]),
                                  torch.zeros(reconstruction_size[2]),
                                  torch.zeros(reconstruction_size[3])]
    labels = torch.zeros(num_elements,1)
    index_array = torch.zeros(num_elements,)

    with torch.no_grad():
        for i, (data, target, factor, index) in enumerate(generator_test):
            data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
            target = target.to(device)
            factor = factor.to(device)

            z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
            z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
            z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
            z_presample_4 = model_dict['autoencoder_4'].encode(data_4)
            z_presample = [z_presample_1, z_presample_2, z_presample_3, z_presample_4]

            z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
            z_var_1 = Var_layer()([z_presample_1, z_mean])
            z_var_2 = Var_layer()([z_presample_2, z_mean])
            z_var_3 = Var_layer()([z_presample_3, z_mean])
            z_var_4 = Var_layer()([z_presample_4, z_mean])

            z_sample_1 = torch.cat((z_mean, z_var_1), 1)
            z_sample_2 = torch.cat((z_mean, z_var_2), 1)
            z_sample_3 = torch.cat((z_mean, z_var_3), 1)
            z_sample_4 = torch.cat((z_mean, z_var_4), 1)

            x_reconstructed_1 = model_dict['autoencoder_1'].decode(torch.cat((z_sample_1, factor), 1))  # decode to X reconstruction
            x_reconstructed_2 = model_dict['autoencoder_2'].decode(torch.cat((z_sample_2, factor), 1))  # decode to X reconstruction
            x_reconstructed_3 = model_dict['autoencoder_3'].decode(torch.cat((z_sample_3, factor), 1))  # decode to X reconstruction
            x_reconstructed_4 = model_dict['autoencoder_4'].decode(torch.cat((z_sample_4, factor), 1))  # decode to X reconstruction

            rec_sample = [x_reconstructed_1,x_reconstructed_2,x_reconstructed_3,x_reconstructed_4]
            z_sample = [z_var_1, z_var_2, z_var_3, z_var_4]
            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements

            index_array[start:end] = index.cpu()
            mean_predictions[start:end] = z_mean.cpu()
            for it_rec in range(4):
                latent_predictions[it_rec][start:end] = z_presample[it_rec].cpu()
                var_predictions[it_rec][start:end] = z_sample[it_rec].cpu()
                reconstruction_predictions[it_rec][start:end] = rec_sample[it_rec].cpu()

            labels[start:end] = target.cpu()

    return [lp.detach().numpy() for lp in latent_predictions], \
           mean_predictions.detach().numpy(),\
           [v.detach().numpy() for v in var_predictions], \
           [r.detach().numpy() for r in reconstruction_predictions], \
           index_array.detach().numpy().astype(int)

def sample(model, generator_test, prediction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(num_elements,prediction_size)
    labels = torch.zeros(num_elements,1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            data = data.to(device)
            target = target.to(device)
            pred = model(data)
            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()
    return predictions.detach().numpy(), labels.detach().numpy()

################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    print('READING DATA', flush=True)
    DataLoader = data_reader.DataLoader_Longitudinal_semisupervised()

    ## Dictionaries
    av45_dict, av45_labels_dict = DataLoader.load_modality(modality_name='av45_pet')
    vMRI_dict, vMRI_labels_dict = DataLoader.load_modality(modality_name='volumetric_mri')
    cMRI_dict, cMRI_labels_dict = DataLoader.load_modality(modality_name='cortical_thickness_mri')
    FDG_dict, FDG_labels_dict = DataLoader.load_modality(modality_name='fdg_pet')
    csf_dict, csf_labels_dict = DataLoader.load_modality(modality_name='csf')
    imaging_phenotypes_dict, imaging_phenotypes_labels_dict = DataLoader.load_modality(modality_name='imaging_phenotypes')
    education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
    age_dict, age_labels_dict = DataLoader.load_age()
    clinical_dict, clinical_labels_dict = DataLoader.load_clinical(clinical_names=clinical_names)
    dx_dict, dx_labels_dict = DataLoader.load_dx()

    rid_events_dict = {rid: [e.step for e in subject.events] for rid, subject in DataLoader.subjects_dict.items()}
    rid_events_list = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list]

    test_dict = {rid: {e_step: [age_dict[rid][e_step]] + clinical_dict[rid][e_step] + education_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}
    test_labels_dict = {rid: {e_step: age_labels_dict[rid][e_step] * clinical_labels_dict[rid][e_step] * education_labels_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}

    ## Codenames
    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    clinical_id_list = clinical_names
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    test_id_list = ['age'] + clinical_names + ['education']

    ## Modalities used
    modality_list = ['av45', 'vMRI', 'cMRI', 'clinical']
    modality_dict_list = [av45_dict, vMRI_dict, cMRI_dict, clinical_dict]
    modality_labels_dict_list = [av45_labels_dict, vMRI_labels_dict, cMRI_labels_dict, clinical_labels_dict]
    modality_id_list = [av45_id_list, vMRI_id_list, cMRI_id_list, clinical_id_list]

    rid_events_list_all_modalities = data_reader.find_list_tuple_intersection([
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[0][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[1][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[2][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[3][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         dx_labels_dict[rid][e_step] == 1],
    ])

    rid_events_dict_all_modalities = {}
    for (rid,e) in rid_events_list_all_modalities:
        if rid not in rid_events_dict_all_modalities.keys():
            rid_events_dict_all_modalities[rid] = [e]
        else:
            rid_events_dict_all_modalities[rid].append(e)

    age_array = np.asarray([age_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)
    age_labels_array = np.asarray([age_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m1_array = np.asarray([modality_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m1_array.shape) < 2:
        m1_array = m1_array.reshape(-1, 1)
    m1_labels_array = np.asarray([modality_labels_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m2_array = np.asarray([modality_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m2_array.shape) < 2:
        m2_array = m2_array.reshape(-1, 1)
    m2_labels_array = np.asarray([modality_labels_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m3_array = np.asarray([modality_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m3_array.shape) < 2:
        m3_array = m3_array.reshape(-1, 1)
    m3_labels_array = np.asarray([modality_labels_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m4_array = np.asarray([modality_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m4_array.shape) < 2:
        m4_array = m4_array.reshape(-1, 1)
    m4_labels_array = np.asarray([modality_labels_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    dx_array = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(dx_array.shape) < 2:
        dx_array = dx_array.reshape(-1, 1)
    dx_labels_array = np.asarray([dx_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    print(np.unique(dx_array))

    ## PRE-PROCESSING
    print('PRE-PROCESSING', flush=True)

    std_scaler = StandardScaler()
    m1_array = std_scaler.fit_transform(m1_array)
    m2_array = std_scaler.fit_transform(m2_array)
    m3_array = std_scaler.fit_transform(m3_array)
    m4_array = std_scaler.fit_transform(m4_array)

    age_array = std_scaler.fit_transform(age_array)

    N1, M1 = m1_array.shape
    N2, M2 = m2_array.shape
    N3, M3 = m3_array.shape
    N4, M4 = m4_array.shape
    N = [N1, N2, N3, N4]
    M = [M1, M2, M3, M4]

    print('N1: ' + str(N1) + ', M1: ' + str(M1))
    print('N2: ' + str(N1) + ', M2: ' + str(M2))
    print('N3: ' + str(N1) + ', M3: ' + str(M3))
    print('N4: ' + str(N1) + ', M4: ' + str(M4))

    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    print('BUILD THE MODEL', flush=True)

    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_multiview.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    hidden_layers_dim_1_enc = [N_COMPONENTS]
    hidden_layers_dim_2_enc = [N_COMPONENTS]
    hidden_layers_dim_3_enc = [N_COMPONENTS]
    hidden_layers_dim_4_enc = [N_COMPONENTS]
    hidden_layers_dim_1_dec = [2 * N_COMPONENTS + 1]
    hidden_layers_dim_2_dec = [2 * N_COMPONENTS + 1]
    hidden_layers_dim_3_dec = [2 * N_COMPONENTS + 1]
    hidden_layers_dim_4_dec = [2 * N_COMPONENTS + 1]
    n_latent_dec = 2 * N_COMPONENTS + 1

    hidden_layers_discriminator_dim = [10, len(modality_list) + 1]
    latent_dim = N_COMPONENTS

    hidden_layers_discriminator_dim_1 = [10, 1]
    hidden_layers_discriminator_dim_2 = [10, len(modality_list) + 1]


    ################################
    ##########  TRAINING  ##########
    ################################
    print('TRAINING', flush=True)

    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    YSCORES_PATH = join(MODALITY_PATH, 'Y_SCORES')
    if not exists(YSCORES_PATH):
        os.makedirs(YSCORES_PATH)


    Y_SCORE_PLOTS = False
    MSE_PLOTS = False
    TRAIN_TEST_PLOTS = True

    train_error_features_dict = {}
    test_error_features_dict = {}
    train_error_subjects_dict = {}
    test_error_subjects_dict = {}

    n_splits = 10
    skf = StratifiedKFold(n_splits=n_splits)

    for it_fold in range(n_splits):
        FOLD_PATH = join(MODALITY_PATH, 'Fold_' + str(it_fold))
        for it_component in range(N_COMPONENTS):
            COMPONENTS_PATH = join(FOLD_PATH, str(it_component))
            if not exists(COMPONENTS_PATH):
                os.makedirs(COMPONENTS_PATH)

    it_fold = -1
    for train_index, test_index in skf.split(age_array,dx_array):

        it_fold += 1
        FOLD_PATH = join(MODALITY_PATH, 'Fold_' + str(it_fold))
        print('FOLD: ' + str(it_fold), flush=True)

        #################################
        ##########  READ DATA  ##########
        #################################
        N_train = len(train_index)
        N_test = len(test_index)

        rid_events_list_all_modalities_train = [rid_events_list_all_modalities[ir] for ir in train_index]
        rid_events_list_all_modalities_test = [rid_events_list_all_modalities[ir] for ir in test_index]

        modality_train_array_list = [m1_array[train_index], m2_array[train_index], m3_array[train_index],
                                     m4_array[train_index]]
        modality_test_array_list = [m1_array[test_index], m2_array[test_index], m3_array[test_index],
                                    m4_array[test_index]]
        dx_train_array, dx_test_array = dx_array[train_index], dx_array[test_index]
        age_train_array, age_test_array = age_array[train_index], age_array[test_index]

        dataset_train = MyDataset_disentangle(modality_train_array_list, dx_train_array, age_train_array, indices=True)
        generator_train = torch.utils.data.DataLoader(
            dataset_train,
            batch_size=N_train,
            shuffle=True,
            num_workers=2,
            pin_memory=torch.cuda.is_available()

        )

        dataset_test = MyDataset_disentangle(modality_test_array_list, dx_test_array, age_test_array, indices=True)
        generator_test = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=N_test,
            shuffle=True,
            num_workers=2,
            pin_memory=torch.cuda.is_available()

        )

        #Parameters
        kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
        kwargs_testing = {}
        kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        device = torch.device("cuda:0" if use_cuda else "cpu")

        TRAIN_MODEL = False
        if TRAIN_MODEL:

            Generator_model_1 = Encoder(M1, hidden_layers_dim_1_enc)
            Decoder_model_1 = Decoder(M1, hidden_layers_dim_1_dec)
            AAE_model_1 = Autoencoder(Generator_model_1, Decoder_model_1)

            Generator_model_2 = Encoder(M2, hidden_layers_dim_2_enc)
            Decoder_model_2 = Decoder(M2, hidden_layers_dim_2_dec)
            AAE_model_2 = Autoencoder(Generator_model_2, Decoder_model_2)

            Generator_model_3 = Encoder(M3, hidden_layers_dim_3_enc)
            Decoder_model_3 = Decoder(M3, hidden_layers_dim_3_dec)
            AAE_model_3 = Autoencoder(Generator_model_3, Decoder_model_3)

            Generator_model_4 = Encoder(M4, hidden_layers_dim_4_enc)
            Decoder_model_4 = Decoder(M4, hidden_layers_dim_4_dec)
            AAE_model_4 = Autoencoder(Generator_model_4, Decoder_model_4)

            Discriminator_model_1 = Discriminator(latent_dim, hidden_layers_discriminator_dim_1)
            Discriminator_model_2 = Discriminator(latent_dim, hidden_layers_discriminator_dim_2)

            # Set learning rates
            ae_lr = learning_rate['autoencoder']
            gen_lr = learning_rate['generator']
            reg_lr = learning_rate['discriminator']


            # encode/decode optimizers
            optim_AAE_1 = torch.optim.Adam(AAE_model_1.parameters(), lr=ae_lr)
            optim_G_1 = torch.optim.Adam(Generator_model_1.parameters(), lr=gen_lr)
            optim_AAE_2 = torch.optim.Adam(AAE_model_2.parameters(), lr=ae_lr)
            optim_G_2 = torch.optim.Adam(Generator_model_2.parameters(), lr=gen_lr)
            optim_AAE_3 = torch.optim.Adam(AAE_model_3.parameters(), lr=ae_lr)
            optim_G_3 = torch.optim.Adam(Generator_model_3.parameters(), lr=gen_lr)
            optim_AAE_4 = torch.optim.Adam(AAE_model_4.parameters(), lr=ae_lr)
            optim_G_4 = torch.optim.Adam(Generator_model_4.parameters(), lr=gen_lr)
            optim_D_1 = torch.optim.Adam(Discriminator_model_1.parameters(), lr=reg_lr)
            optim_D_2 = torch.optim.Adam(Discriminator_model_2.parameters(), lr=reg_lr)

            model_dict = {'autoencoder_1': AAE_model_1, 'autoencoder_2': AAE_model_2,
                          'autoencoder_3': AAE_model_3, 'autoencoder_4': AAE_model_4,
                          'discriminator_1': Discriminator_model_1, 'discriminator_2': Discriminator_model_2,
                          'generator_1': Generator_model_1, 'generator_2': Generator_model_2,
                          'generator_3': Generator_model_3, 'generator_4': Generator_model_4
                          }

            for model_name, model in model_dict.items():
                model.to(device)

            optimizer_dict = {'autoencoder_1': optim_AAE_1, 'autoencoder_2': optim_AAE_1,
                              'autoencoder_3': optim_AAE_3, 'autoencoder_4': optim_AAE_4,
                              'discriminator_1': optim_D_1, 'discriminator_2': optim_D_2,
                              'generator_1': optim_G_1, 'generator_2': optim_G_2,
                              'generator_3': optim_G_3, 'generator_4': optim_G_4}

            log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
            logger = History()
            logger.on_train_init(log_keys)
            for epoch in range(1, n_epochs + 1):
                logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)

            torch.save(model_dict, join(FOLD_PATH,'model_dict.pt'))
            fig, ax1 = plt.subplots()

            ax2 = ax1.twinx()
            ax1.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
            ax2.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
            ax2.plot(logger.logs['generator_loss'], 'g', label='Generator')
            ax1.plot(logger.logs['loss'], 'k', label='Total loss')

            ax1.set_xlabel('Iterations')
            ax1.set_ylabel('Reconstruction and Total loss', color='g')
            ax2.set_ylabel('Discriminator and generator loss', color='b')

            ax1.legend()
            ax2.legend()
            plt.savefig(join(FOLD_PATH,'training.png'))
            plt.close()
        else:
            if use_cuda:
                model_dict = torch.load(join(FOLD_PATH, 'model_dict.pt'))
            else:
                model_dict = torch.load(join(FOLD_PATH, 'model_dict.pt'), map_location='cpu')

        ###############################
        ##########  RESULTS  ##########
        ###############################
        train_error_features_dict[it_fold] = {}
        test_error_features_dict[it_fold] = {}
        train_error_subjects_dict[it_fold] = {}
        test_error_subjects_dict[it_fold] = {}

        reconstruction_size_train = [(N_train, m) for m in M]
        reconstruction_size_test = [(N_test, m) for m in M]

        y_scores_train, y_scores_mean_train, y_scores_var_train, x_rec_train, indices_reordered_train = predict(model_dict, generator_train, (N_train, latent_dim), reconstruction_size_train)
        y_scores_test, y_scores_mean_test, y_scores_var_test, x_rec_test, indices_reordered_test = predict(model_dict, generator_test, (N_test, latent_dim), reconstruction_size_test)

        rid_events_list_all_modalities_train = [rid_events_list_all_modalities_train[ir] for ir in indices_reordered_train]
        rid_events_list_all_modalities_test = [rid_events_list_all_modalities_test[ir] for ir in indices_reordered_test]

        modality_train_array_list = [mll[indices_reordered_train] for mll in modality_train_array_list]
        modality_test_array_list = [mll[indices_reordered_test] for mll in modality_test_array_list]

        dx_train_array = dx_train_array[indices_reordered_train]
        dx_test_array = dx_test_array[indices_reordered_test]
        age_train_array = age_train_array[indices_reordered_train]
        age_test_array = age_test_array[indices_reordered_test]

        y_scores_dx_train_indices = [[np.where(dx_train_array == 0)[0],
                                      np.where(dx_train_array == 1)[0],
                                      np.where(dx_train_array == 2)[0]]]

        y_scores_dx_test_indices = [[np.where(dx_test_array == 0)[0],
                                     np.where(dx_test_array == 1)[0],
                                     np.where(dx_test_array == 2)[0]]]

        for it_modality in range(len(modality_list)):
            mse_per_feature = np.mean((modality_train_array_list[it_modality] - x_rec_train[it_modality]) ** 2, axis=0)
            train_error_features_dict[it_fold][it_modality] = mse_per_feature

            mse_per_feature = np.mean((modality_test_array_list[it_modality] - x_rec_test[it_modality]) ** 2, axis=0)
            test_error_features_dict[it_fold][it_modality] = mse_per_feature

            mse_per_subject = np.mean((modality_train_array_list[it_modality] - x_rec_train[it_modality]) ** 2, axis=1)
            train_error_subjects_dict[it_fold][it_modality] = mse_per_subject

            mse_per_subject = np.mean((modality_test_array_list[it_modality] - x_rec_test[it_modality]) ** 2, axis=1)
            test_error_subjects_dict[it_fold][it_modality] = mse_per_subject


        if Y_SCORE_PLOTS:
            print('Y_SCORE_PLOTS')
            for it_component in range(N_COMPONENTS):
                for it_modality, modality in enumerate(modality_list):
                    plt.figure()
                    plt.hist(y_scores_var_train[it_modality][:, it_component], bins=40)
                    plt.savefig(join(FOLD_PATH, str(it_component), modality + '_var_histogram_train.png'))
                    plt.close()

                    plt.figure()
                    plt.hist(y_scores_var_test[it_modality][:, it_component], bins=40)
                    plt.savefig(join(FOLD_PATH, str(it_component), modality + '_var_histogram_test.png'))
                    plt.close()

                for it_modality, modality in enumerate(modality_list):
                    plt.figure()
                    plt.hist(y_scores_train[it_modality][:, it_component], bins=40)
                    plt.savefig(join(FOLD_PATH, str(it_component), modality + '_histogram_train.png'))
                    plt.close()

                    plt.figure()
                    plt.hist(y_scores_test[it_modality][:, it_component], bins=40)
                    plt.savefig(join(FOLD_PATH, str(it_component), modality + '_histogram_test.png'))
                    plt.close()

                plt.figure()
                plt.hist(y_scores_mean_train[:, it_component], bins=40)
                plt.savefig(join(FOLD_PATH, str(it_component), 'mean_histogram_train.png'))
                plt.close()

                plt.figure()
                plt.hist(y_scores_mean_test[:, it_component], bins=40)
                plt.savefig(join(FOLD_PATH, str(it_component), 'mean_histogram_test.png'))
                plt.close()


        if MSE_PLOTS:
            print('MSE_PLOTS')

            #### TRAIN
            writer_mse = ResultsWriter(filepath=join(FOLD_PATH, 'mse_results_train.txt'), attach=False)
            writer_mse.write('###########\n')
            writer_mse.write('MSE RESULTS\n')
            writer_mse.write('###########\n\n')
            for it_modality, modality in enumerate(modality_list):

                # if modality in ['test', 'csf', 'clinical', 'imaging_phenotypes']:
                mse_per_feature = np.mean((modality_train_array_list[it_modality] - x_rec_train[it_modality]) ** 2, axis=0)
                mse_per_subject = np.mean((modality_train_array_list[it_modality] - x_rec_train[it_modality]) ** 2, axis=1)

                writer_mse.write(modality_list[it_modality])
                writer_mse.write('\n')

                sorted_mse = np.argsort(mse_per_feature)[::-1]
                for it_mse in sorted_mse:
                    writer_mse.write(
                        '  - ' + modality_id_list[it_modality][it_mse] + ': ' + str(mse_per_feature[it_mse]) + '\n')

                index_0 = np.where(dx_train_array == 0)[0]
                index_1 = np.where(dx_train_array == 1)[0]
                index_2 = np.where(dx_train_array == 2)[0]
                index_dx_list = [index_0, index_1, index_2]

                boxplot_list = [mse_per_subject[idx] for idx in index_dx_list]
                combination_plots = [(0, 1), (0, 2), (1, 2)]
                p, t = [], []
                writer_mse.write('\n')
                writer_mse.write('  ### STATISTICAL RESULTS:')
                writer_mse.write('\n')
                for c in combination_plots:
                    result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                    t.append(result[0])
                    p.append(result[1])
                    writer_mse.write('      - ' + label_diagnostic[c[0]] + '-' + label_diagnostic[c[1]] + ': t=' + str(
                        result[0]) + ' p=' + str(result[1]))
                    writer_mse.write('\n')
                writer_mse.write('\n')

                plt.figure()
                plt.boxplot(boxplot_list)
                plt.grid()
                plt.ylabel('MSE')
                plt.xlabel('Clinical Diagnosis')
                plt.title('Significant (p<0.05) differences: ' + str(
                    [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                plt.savefig(join(FOLD_PATH, modality + '_mse_diagnostic_error_train.png'))
                plt.close()

                writer_mse.write('      - AGE:' + str(pearsonr(age_train_array.reshape(-1,), mse_per_subject)))
                writer_mse.write('\n')
                writer_mse.write('\n')
                writer_mse.write('\n')

                plt.figure()
                plt.plot(age_train_array[index_0], mse_per_subject[index_0], '*b', label='NC')
                plt.plot(age_train_array[index_1], mse_per_subject[index_1], '*r', label='MCI')
                plt.plot(age_train_array[index_2], mse_per_subject[index_2], '*k', label='AD')
                plt.grid()
                plt.xlabel('Age')
                plt.ylabel('MSE')
                plt.savefig(join(FOLD_PATH, modality + '_mse_age_error_train.png'))
                plt.close()

                plt.figure()
                plt.plot(modality_train_array_list[it_modality][index_0], x_rec_train[it_modality][index_0], '*b', label='NC')
                plt.plot(modality_train_array_list[it_modality][index_1], x_rec_train[it_modality][index_1], '*r', label='MCI')
                plt.plot(modality_train_array_list[it_modality][index_2], x_rec_train[it_modality][index_2], '*k', label='AD')
                plt.grid()
                plt.xlabel('True')
                plt.ylabel('Predicted')
                plt.savefig(join(FOLD_PATH, modality + '_true_predicted_train.png'))
                plt.close()

            #### TEST
            writer_mse = ResultsWriter(filepath=join(FOLD_PATH, 'mse_results_test.txt'), attach=False)
            writer_mse.write('###########\n')
            writer_mse.write('MSE RESULTS\n')
            writer_mse.write('###########\n\n')
            for it_modality, modality in enumerate(modality_list):

                # if modality in ['test', 'csf', 'clinical', 'imaging_phenotypes']:
                mse_per_feature = np.mean((modality_test_array_list[it_modality] - x_rec_test[it_modality]) ** 2,
                                          axis=0)
                mse_per_subject = np.mean((modality_test_array_list[it_modality] - x_rec_test[it_modality]) ** 2,
                                          axis=1)

                writer_mse.write(modality_list[it_modality])
                writer_mse.write('\n')

                sorted_mse = np.argsort(mse_per_feature)[::-1]
                for it_mse in sorted_mse:
                    writer_mse.write(
                        '  - ' + modality_id_list[it_modality][it_mse] + ': ' + str(mse_per_feature[it_mse]) + '\n')

                index_0 = np.where(dx_test_array == 0)[0]
                index_1 = np.where(dx_test_array == 1)[0]
                index_2 = np.where(dx_test_array == 2)[0]
                index_dx_list = [index_0, index_1, index_2]

                boxplot_list = [mse_per_subject[idx] for idx in index_dx_list]
                combination_plots = [(0, 1), (0, 2), (1, 2)]
                p, t = [], []
                writer_mse.write('\n')
                writer_mse.write('  ### STATISTICAL RESULTS:')
                writer_mse.write('\n')
                for c in combination_plots:
                    result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                    t.append(result[0])
                    p.append(result[1])
                    writer_mse.write('      - ' + label_diagnostic[c[0]] + '-' + label_diagnostic[c[1]] + ': t=' + str(
                        result[0]) + ' p=' + str(result[1]))
                    writer_mse.write('\n')
                writer_mse.write('\n')

                plt.figure()
                plt.boxplot(boxplot_list)
                plt.grid()
                plt.ylabel('MSE')
                plt.xlabel('Clinical Diagnosis')
                plt.title('Significant (p<0.05) differences: ' + str(
                    [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                plt.savefig(join(FOLD_PATH, modality + '_mse_diagnostic_error_test.png'))
                plt.close()

                writer_mse.write('      - AGE:' + str(pearsonr(age_test_array.reshape(-1,), mse_per_subject)))
                writer_mse.write('\n')
                writer_mse.write('\n')
                writer_mse.write('\n')

                plt.figure()
                plt.plot(age_test_array[index_0], mse_per_subject[index_0], '*b', label='NC')
                plt.plot(age_test_array[index_1], mse_per_subject[index_1], '*r', label='MCI')
                plt.plot(age_test_array[index_2], mse_per_subject[index_2], '*k', label='AD')
                plt.grid()
                plt.xlabel('Age')
                plt.ylabel('MSE')
                plt.legend()
                plt.savefig(join(FOLD_PATH, modality + '_mse_age_error_test.png'))
                plt.close()

                plt.figure()
                plt.plot(modality_test_array_list[it_modality][index_0], x_rec_test[it_modality][index_0], '*b',
                         label='NC')
                plt.plot(modality_test_array_list[it_modality][index_1], x_rec_test[it_modality][index_1], '*r',
                         label='MCI')
                plt.plot(modality_test_array_list[it_modality][index_2], x_rec_test[it_modality][index_2], '*k',
                         label='AD')
                plt.grid()
                plt.xlabel('True')
                plt.ylabel('Predicted')
                plt.legend()
                plt.savefig(join(FOLD_PATH, modality + '_true_predicted_test.png'))
                plt.close()


    if TRAIN_TEST_PLOTS:
        print('TRAIN_TEST_PLOTS', flush=True)

        for it_modality in range(len(modality_list)):
            writer_mse = ResultsWriter(filepath=join(MODALITY_PATH, modality_list[it_modality]
                                                     + '_mse_results.txt'), attach=False)

            boxplot_list = [[train_error_features_dict[it_fold][it_modality][it_M] for it_fold in range(n_splits)] for it_M in range(M[it_modality])]

            writer_mse.write('TRAIN RESULTS:')
            writer_mse.write('\n')

            for it_b, b in enumerate(boxplot_list):
                writer_mse.write(modality_id_list[it_modality][it_b] + ': ')
                writer_mse.write('MEAN: ' + str(round(np.mean(b), 3)) + 'VAR: ' + str(round(np.var(b), 3)))
                writer_mse.write('\n')

            plt.figure()
            plt.boxplot(boxplot_list)
            plt.grid()
            plt.ylim((0, 5))
            plt.ylabel('MSE')
            plt.xlabel('Features')
            plt.title('Modality ' + str(it_modality))
            plt.savefig(join(MODALITY_PATH, modality_list[it_modality] + '_mse_per_feature_train.png'))
            plt.close()

            boxplot_list = [[test_error_features_dict[it_fold][it_modality][it_M] for it_fold in range(n_splits)] for it_M in range(M[it_modality])]

            writer_mse.write('TEST RESULTS:')
            writer_mse.write('\n')

            for it_b, b in enumerate(boxplot_list):
                writer_mse.write(modality_id_list[it_modality][it_b] + ': ')
                writer_mse.write('MEAN: ' + str(round(np.mean(b), 3)) + 'VAR: ' + str(round(np.var(b), 3)))
                writer_mse.write('\n')

            plt.figure()
            plt.boxplot(boxplot_list)
            plt.grid()
            plt.ylim((0, 5))
            plt.ylabel('MSE')
            plt.xlabel('Features')
            plt.title('Modality ' + str(it_modality))
            plt.savefig(join(MODALITY_PATH, modality_list[it_modality]  + '_mse_per_feature_test.png'))
            plt.close()

        writer_global_mse = ResultsWriter(filepath=join(MODALITY_PATH, 'all_modalities_mse_results.txt'), attach=False)

        boxplot_list = [[np.mean([train_error_features_dict[it_fold][it_modality][it_M] for it_M in range(M[it_modality])]) for it_fold in range(n_splits)] for it_modality in range(len(modality_list))]

        writer_global_mse.write('TRAIN RESULTS:')
        writer_global_mse.write('\n')

        for it_b, b in enumerate(boxplot_list):
            writer_global_mse.write(modality_list[it_b] + ': ')
            writer_global_mse.write('MEAN: ' + str(round(np.mean(b), 3)) + 'VAR: ' + str(round(np.var(b), 3)))
            writer_global_mse.write('\n')

        plt.figure()
        plt.boxplot(boxplot_list)
        plt.grid()
        plt.ylim((0, 5))
        plt.ylabel('MSE')
        plt.xlabel('Features')
        plt.title('ALL Modalities')
        plt.savefig(join(MODALITY_PATH, 'all_modalities_mse_per_feature_train.png'))
        plt.close()

        boxplot_list = [[np.mean([test_error_features_dict[it_fold][it_modality][it_M] for it_M in range(M[it_modality])]) for it_fold in range(n_splits)]  for it_modality in range(len(modality_list))]

        writer_global_mse.write('TEST RESULTS:')
        writer_global_mse.write('\n')

        for it_b, b in enumerate(boxplot_list):
            writer_global_mse.write(modality_list[it_b] + ': ')
            writer_global_mse.write('MEAN: ' + str(round(np.mean(b), 3)) + 'VAR: ' + str(round(np.var(b), 3)))
            writer_global_mse.write('\n')

        plt.figure()
        plt.boxplot(boxplot_list)
        plt.grid()
        plt.ylim((0, 5))
        plt.ylabel('MSE')
        plt.xlabel('Features')
        plt.title('ALL Modalities')
        plt.savefig(join(MODALITY_PATH, 'all_modalities_mse_per_feature_test.png'))
        plt.close()

