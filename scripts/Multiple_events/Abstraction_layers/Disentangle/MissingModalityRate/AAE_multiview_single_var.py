import torch
import copy

from Data import adni_reader
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT, CSVWriter
from src.dataset import MyDataset, MyDataset_disentangle
from src.models import Encoder, Decoder, Discriminator, Autoencoder, Encoder_lineal, Decoder_lineal
from src.layers import Mean_layer, Var_layer

import os
import numpy as np
from os.path import join, exists
import itertools


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import StratifiedShuffleSplit, StratifiedKFold
from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################
N_COMPONENTS = 16
EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/mnt/gpid08/users/adria.casamitjana', 'Multimodal_autoencoders', 'AAE', 'Multiple_events',
                   'Abstraction_layers', 'Disentangle_simple', 'MissModRate', 'AllModalities',  'SingleVar', 'nc_' + str(N_COMPONENTS), 'lineal')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):
    for batch_idx, (data, target, factor, index) in enumerate(generator_train):
        data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
        target_1, target_2, target_3, target_4 = target[0].to(device), target[1].to(device), target[2].to(device), target[3].to(device)
        factor = factor.to(device)

        for model_name, model in model_dict.items():
            model.zero_grad()

        #### RECONSTRUCTION LOSS
        model_dict['discriminator_1'].train(mode=False)
        model_dict['discriminator_2'].train(mode=False)
        model_dict['autoencoder_1'].train(mode=True)
        model_dict['autoencoder_2'].train(mode=True)
        model_dict['autoencoder_3'].train(mode=True)
        model_dict['autoencoder_4'].train(mode=True)

        # print('Reconstruction phase')
        # print( model_dict['autoencoder_1'].training)
        # print('===> ' + str(model_dict['autoencoder_1'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_1'].decoder.training))
        # print( model_dict['autoencoder_2'].training)
        # print('===> ' + str(model_dict['autoencoder_2'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_2'].decoder.training))
        # print( model_dict['generator_1'].training)
        # print( model_dict['generator_2'].training)
        # print( model_dict['discriminator'].training)

        z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_presample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to

        z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        z_var = Var_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        z_sample = torch.cat((z_mean, z_var), 1)

        x_reconstructed_1_1 = model_dict['autoencoder_1'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction
        x_reconstructed_2_2 = model_dict['autoencoder_2'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction
        x_reconstructed_3_3 = model_dict['autoencoder_3'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction
        x_reconstructed_4_4 = model_dict['autoencoder_4'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction

        reconstruction_loss_1 = target_1 * ((x_reconstructed_1_1 - data_1) ** 2).sum(dim=-1)
        reconstruction_loss_2 = target_2 * ((x_reconstructed_2_2 - data_2) ** 2).sum(dim=-1)
        reconstruction_loss_3 = target_3 * ((x_reconstructed_3_3 - data_3) ** 2).sum(dim=-1)
        reconstruction_loss_4 = target_4 * ((x_reconstructed_4_4 - data_4) ** 2).sum(dim=-1)

        reconstruction_loss = torch.mean(reconstruction_loss_1 + reconstruction_loss_2 +
                                         reconstruction_loss_3 + reconstruction_loss_4)

        reconstruction_loss.backward()
        optimizer_dict['autoencoder_1'].step()
        optimizer_dict['autoencoder_2'].step()
        optimizer_dict['autoencoder_3'].step()
        optimizer_dict['autoencoder_4'].step()


        #### DISCRIMINATOR LOSS
        model_dict['discriminator_1'].train(mode=True)
        model_dict['discriminator_2'].train(mode=True)
        model_dict['autoencoder_1'].train(mode=False)
        model_dict['autoencoder_2'].train(mode=False)
        model_dict['autoencoder_3'].train(mode=False)
        model_dict['autoencoder_4'].train(mode=False)

        z_real_gauss = 16*torch.randn(data_1.size()[0], latent_space_dims)
        z_real_gauss = z_real_gauss.to(device)

        a, b = 0.05, 0.05
        z_real_gamma = torch.distributions.Gamma(torch.tensor([a]), torch.tensor([b+1])).sample(torch.Size((data_1.size()[0], latent_space_dims,))).squeeze()
        z_real_gamma = z_real_gamma.to(device)

        z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_presample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to

        z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        z_var=Var_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])

        d_mean_real = model_dict['discriminator_1'](z_real_gauss)
        d_mean_sample = model_dict['discriminator_1'](z_mean)
        discriminator_loss_mean = -torch.mean(torch.log(d_mean_real + EPS) + torch.log(1 - d_mean_sample + EPS))

        d_var_real = model_dict['discriminator_2'](z_real_gamma)
        d_var_sample = model_dict['discriminator_2'](z_var)

        discriminator_loss_var = -torch.mean(torch.log(d_var_real + EPS) + torch.log(1 - d_var_sample + EPS))

        discriminator_loss = discriminator_loss_mean + discriminator_loss_var

        discriminator_loss.backward()
        optimizer_dict['discriminator_1'].step()
        optimizer_dict['discriminator_2'].step()

        #### GENERATOR LOSS
        model_dict['generator_1'].train(mode=True)
        model_dict['generator_2'].train(mode=True)
        model_dict['generator_3'].train(mode=True)
        model_dict['generator_4'].train(mode=True)

        z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_presample_4 = model_dict['autoencoder_4'].encode(data_4)

        z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        d_mean_sample = model_dict['discriminator_1'](z_mean)
        generator_loss_mean = -torch.mean(torch.log(d_mean_sample + EPS))

        z_var = Var_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
        d_var_sample = model_dict['discriminator_2'](z_var)
        generator_loss_var = -torch.mean(torch.log(d_var_sample + EPS))

        generator_loss = generator_loss_mean + generator_loss_var

        generator_loss.backward()
        optimizer_dict['generator_1'].step()
        optimizer_dict['generator_2'].step()
        optimizer_dict['generator_3'].step()
        optimizer_dict['generator_4'].step()

        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item() + discriminator_loss.item() + generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Prediction Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)

    return logger

def predict(model_dict, generator_test, latent_size,reconstruction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    mean_predictions = torch.zeros(latent_size)
    var_predictions = torch.zeros(latent_size)
    latent_predictions = [torch.zeros(latent_size),
                          torch.zeros(latent_size),
                          torch.zeros(latent_size),
                          torch.zeros(latent_size)]
    reconstruction_predictions = [torch.zeros(reconstruction_size[0]),
                                  torch.zeros(reconstruction_size[1]),
                                  torch.zeros(reconstruction_size[2]),
                                  torch.zeros(reconstruction_size[3])]
    labels = [torch.zeros(num_elements,1),torch.zeros(num_elements,1),
              torch.zeros(num_elements,1),torch.zeros(num_elements,1)]
    index_array = torch.zeros(num_elements,)

    with torch.no_grad():
        for i, (data, target, factor, index) in enumerate(generator_test):
            data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
            factor = factor.to(device)

            z_presample_1 = model_dict['autoencoder_1'].encode(data_1)
            z_presample_2 = model_dict['autoencoder_2'].encode(data_2)
            z_presample_3 = model_dict['autoencoder_3'].encode(data_3)
            z_presample_4 = model_dict['autoencoder_4'].encode(data_4)
            z_presample = [z_presample_1, z_presample_2, z_presample_3, z_presample_4]

            z_mean = Mean_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])
            z_var = Var_layer()([z_presample_1, z_presample_2, z_presample_3, z_presample_4])

            z_sample = torch.cat((z_mean, z_var), 1)

            x_reconstructed_1 = model_dict['autoencoder_1'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction
            x_reconstructed_2 = model_dict['autoencoder_2'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction
            x_reconstructed_3 = model_dict['autoencoder_3'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction
            x_reconstructed_4 = model_dict['autoencoder_4'].decode(torch.cat((z_sample, factor), 1))  # decode to X reconstruction

            rec_sample = [x_reconstructed_1,x_reconstructed_2,x_reconstructed_3,x_reconstructed_4]

            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements

            index_array[start:end] = index.cpu()
            mean_predictions[start:end] = z_mean.cpu()
            var_predictions[start:end] = z_var.cpu()

            for it_rec in range(4):
                latent_predictions[it_rec][start:end] = z_presample[it_rec].cpu()
                reconstruction_predictions[it_rec][start:end] = rec_sample[it_rec].cpu()
                labels[start:end][it_rec] = target[it_rec]


        return [v.detach().numpy() for v in latent_predictions], \
               mean_predictions.detach().numpy(), \
               var_predictions.detach().numpy(), \
               [r.detach().numpy() for r in reconstruction_predictions], \
               index_array.detach().numpy().astype(int)

def sample(model, generator_test, prediction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(num_elements,prediction_size)
    labels = torch.zeros(num_elements,1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            data = data.to(device)
            target = target.to(device)
            pred = model(data)
            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()
    return predictions.detach().numpy(), labels.detach().numpy()

################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    print('READING DATA', flush=True)
    DataLoader = data_reader.DataLoader_Longitudinal_semisupervised()

    ## Dictionaries
    av45_dict, av45_labels_dict = DataLoader.load_modality(modality_name='av45_pet')
    vMRI_dict, vMRI_labels_dict = DataLoader.load_modality(modality_name='volumetric_mri')
    cMRI_dict, cMRI_labels_dict = DataLoader.load_modality(modality_name='cortical_thickness_mri')
    FDG_dict, FDG_labels_dict = DataLoader.load_modality(modality_name='fdg_pet')
    csf_dict, csf_labels_dict = DataLoader.load_modality(modality_name='csf')
    imaging_phenotypes_dict, imaging_phenotypes_labels_dict = DataLoader.load_modality(modality_name='imaging_phenotypes')
    education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
    age_dict, age_labels_dict = DataLoader.load_age()
    clinical_dict, clinical_labels_dict = DataLoader.load_clinical(clinical_names=clinical_names)
    dx_dict, dx_labels_dict = DataLoader.load_dx()

    rid_events_dict = {rid: [e.step for e in subject.events] for rid, subject in DataLoader.subjects_dict.items()}
    rid_events_list = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list]

    test_dict = {rid: {e_step: [age_dict[rid][e_step]] + clinical_dict[rid][e_step] + education_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}
    test_labels_dict = {rid: {e_step: age_labels_dict[rid][e_step] * clinical_labels_dict[rid][e_step] * education_labels_dict[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}

    ## Codenames
    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    clinical_id_list = clinical_names
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    test_id_list = ['age'] + clinical_names + ['education']

    ## Modalities used
    modality_list = ['av45', 'vMRI', 'cMRI', 'clinical']
    modality_dict_list = [av45_dict, vMRI_dict, cMRI_dict, clinical_dict]
    modality_labels_dict_list = [av45_labels_dict, vMRI_labels_dict, cMRI_labels_dict, clinical_labels_dict]
    modality_id_list = [av45_id_list, vMRI_id_list, cMRI_id_list, clinical_id_list]

    rid_events_list_all_modalities = data_reader.find_list_tuple_intersection([
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[0][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[1][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[2][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         modality_labels_dict_list[3][rid][e_step] == 1],
        [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if
         dx_labels_dict[rid][e_step] == 1],
    ])

    rid_events_dict_all_modalities = {}
    for (rid,e) in rid_events_list_all_modalities:
        if rid not in rid_events_dict_all_modalities.keys():
            rid_events_dict_all_modalities[rid] = [e]
        else:
            rid_events_dict_all_modalities[rid].append(e)

    age_array = np.asarray([age_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)
    age_labels_array = np.asarray([age_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m1_array = np.asarray([modality_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m1_array.shape) < 2:
        m1_array = m1_array.reshape(-1, 1)
    m1_labels_array = np.asarray([modality_labels_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m2_array = np.asarray([modality_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m2_array.shape) < 2:
        m2_array = m2_array.reshape(-1, 1)
    m2_labels_array = np.asarray([modality_labels_dict_list[1][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m3_array = np.asarray([modality_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m3_array.shape) < 2:
        m3_array = m3_array.reshape(-1, 1)
    m3_labels_array = np.asarray([modality_labels_dict_list[2][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    m4_array = np.asarray([modality_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(m4_array.shape) < 2:
        m4_array = m4_array.reshape(-1, 1)
    m4_labels_array = np.asarray([modality_labels_dict_list[3][rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    dx_array = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])
    if len(dx_array.shape) < 2:
        dx_array = dx_array.reshape(-1, 1)
    dx_labels_array = np.asarray([dx_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list_all_modalities])

    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    print(np.unique(dx_array))

    ## PRE-PROCESSING
    print('PRE-PROCESSING', flush=True)

    std_scaler = StandardScaler()
    m1_array = std_scaler.fit_transform(m1_array)
    m2_array = std_scaler.fit_transform(m2_array)
    m3_array = std_scaler.fit_transform(m3_array)
    m4_array = std_scaler.fit_transform(m4_array)

    age_array = std_scaler.fit_transform(age_array)

    N1, M1 = m1_array.shape
    N2, M2 = m2_array.shape
    N3, M3 = m3_array.shape
    N4, M4 = m4_array.shape
    N = [N1, N2, N3, N4]
    M = [M1, M2, M3, M4]

    print('N1: ' + str(N1) + ', M1: ' + str(M1))
    print('N2: ' + str(N1) + ', M2: ' + str(M2))
    print('N3: ' + str(N1) + ', M3: ' + str(M3))
    print('N4: ' + str(N1) + ', M4: ' + str(M4))

    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    print('BUILD THE MODEL', flush=True)

    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_multiview_missing_modality.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    hidden_layers_dim_1_enc = [N_COMPONENTS]
    hidden_layers_dim_2_enc = [N_COMPONENTS]
    hidden_layers_dim_3_enc = [N_COMPONENTS]
    hidden_layers_dim_4_enc = [N_COMPONENTS]
    hidden_layers_dim_1_dec = [2 * N_COMPONENTS + 1]
    hidden_layers_dim_2_dec = [2 * N_COMPONENTS + 1]
    hidden_layers_dim_3_dec = [2 * N_COMPONENTS + 1]
    hidden_layers_dim_4_dec = [2 * N_COMPONENTS + 1]
    n_latent_dec = 2 * N_COMPONENTS + 1

    hidden_layers_discriminator_dim = [10, len(modality_list) + 1]
    latent_dim = N_COMPONENTS

    hidden_layers_discriminator_dim_1 = [10, 1]
    hidden_layers_discriminator_dim_2 = [10, len(modality_list) + 1]

    #Parameters
    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")


    ################################
    ##########  TRAINING  ##########
    ################################
    color_code = ['b', 'r', 'g', 'm', 'c', 'k']

    print('TRAINING', flush=True)

    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    YSCORES_PATH = join(MODALITY_PATH, 'Y_SCORES')
    if not exists(YSCORES_PATH):
        os.makedirs(YSCORES_PATH)

    TRAIN_MODEL = False
    Y_SCORE_PLOTS = True
    MSE_PLOTS = True
    GLOBAL_PLOTS = True

    error_features_dict = {}
    error_subjects_dict = {}

    missing_mod_rate_list = [0, 0.1, 0.2, 0.5, 0.75, 1.0]

    for modality in modality_list:
        SUBMOD_PATH = join(MODALITY_PATH, modality)
        for mmr in missing_mod_rate_list:
            MMR_PATH = join(SUBMOD_PATH, 'MMR_' + str(mmr))
            for it_component in range(N_COMPONENTS):
                COMPONENTS_PATH = join(MMR_PATH, str(it_component))
                if not exists(COMPONENTS_PATH):
                    os.makedirs(COMPONENTS_PATH)

    for it_modality_1, modality_1 in enumerate(modality_list):
        print('MODALITY: ' + modality_1, flush=True)
        SUBMOD_PATH = join(MODALITY_PATH, modality_1)

        error_features_dict[modality_1] = {}
        error_subjects_dict[modality_1] = {}

        for it_mmr, mmr in enumerate(missing_mod_rate_list):
            print('MMR_rate: ' + str(mmr), flush=True)
            MMR_PATH = join(SUBMOD_PATH, 'MMR_' + str(mmr))

            error_features_dict[modality_1][mmr] = {}
            error_subjects_dict[modality_1][mmr] = {}

            #################################
            ##########  READ DATA  ##########
            #################################
            modality_array_list = [copy.deepcopy(m1_array), copy.deepcopy(m2_array),
                                   copy.deepcopy(m3_array), copy.deepcopy(m4_array)]
            modality_labels_list = [copy.deepcopy(m1_labels_array), copy.deepcopy(m2_labels_array),
                                    copy.deepcopy(m3_labels_array), copy.deepcopy(m4_labels_array)]

            indices_drop = np.random.choice(N1, int(N1 * mmr), replace=False)
            modality_labels_list[it_modality_1][indices_drop] = 0
            modality_array_list[it_modality_1][indices_drop] = np.zeros((len(indices_drop), M[it_modality_1]))

            dataset_train = MyDataset_disentangle(modality_array_list, modality_labels_list, age_array, indices=True)
            generator_train = torch.utils.data.DataLoader(
                dataset_train,
                batch_size=N1,
                shuffle=True,
                num_workers=2,
                pin_memory=torch.cuda.is_available()

            )
            dataset_test = MyDataset_disentangle(modality_array_list, dx_array, age_array, indices=True)
            generator_test = torch.utils.data.DataLoader(
                dataset_test,
                batch_size=N1,
                shuffle=True,
                num_workers=2,
                pin_memory=torch.cuda.is_available()

            )


            if TRAIN_MODEL:
                Generator_model_1 = Encoder(M1, hidden_layers_dim_1_enc)
                Decoder_model_1 = Decoder(M1, hidden_layers_dim_1_dec)
                AAE_model_1 = Autoencoder(Generator_model_1, Decoder_model_1)

                Generator_model_2 = Encoder(M2, hidden_layers_dim_2_enc)
                Decoder_model_2 = Decoder(M2, hidden_layers_dim_2_dec)
                AAE_model_2 = Autoencoder(Generator_model_2, Decoder_model_2)

                Generator_model_3 = Encoder(M3, hidden_layers_dim_3_enc)
                Decoder_model_3 = Decoder(M3, hidden_layers_dim_3_dec)
                AAE_model_3 = Autoencoder(Generator_model_3, Decoder_model_3)

                Generator_model_4 = Encoder(M4, hidden_layers_dim_4_enc)
                Decoder_model_4 = Decoder(M4, hidden_layers_dim_4_dec)
                AAE_model_4 = Autoencoder(Generator_model_4, Decoder_model_4)

                Discriminator_model_1 = Discriminator(latent_dim, hidden_layers_discriminator_dim_1)
                Discriminator_model_2 = Discriminator(latent_dim, hidden_layers_discriminator_dim_2)

                # Set learning rates
                ae_lr = learning_rate['autoencoder']
                gen_lr = learning_rate['generator']
                reg_lr = learning_rate['discriminator']

                # encode/decode optimizers
                optim_AAE_1 = torch.optim.Adam(AAE_model_1.parameters(), lr=ae_lr)
                optim_G_1 = torch.optim.Adam(Generator_model_1.parameters(), lr=gen_lr)
                optim_AAE_2 = torch.optim.Adam(AAE_model_2.parameters(), lr=ae_lr)
                optim_G_2 = torch.optim.Adam(Generator_model_2.parameters(), lr=gen_lr)
                optim_AAE_3 = torch.optim.Adam(AAE_model_3.parameters(), lr=ae_lr)
                optim_G_3 = torch.optim.Adam(Generator_model_3.parameters(), lr=gen_lr)
                optim_AAE_4 = torch.optim.Adam(AAE_model_4.parameters(), lr=ae_lr)
                optim_G_4 = torch.optim.Adam(Generator_model_4.parameters(), lr=gen_lr)
                optim_D_1 = torch.optim.Adam(Discriminator_model_1.parameters(), lr=reg_lr)
                optim_D_2 = torch.optim.Adam(Discriminator_model_2.parameters(), lr=reg_lr)

                model_dict = {'autoencoder_1': AAE_model_1, 'autoencoder_2': AAE_model_2,
                              'autoencoder_3': AAE_model_3, 'autoencoder_4': AAE_model_4,
                              'discriminator_1': Discriminator_model_1, 'discriminator_2': Discriminator_model_2,
                              'generator_1': Generator_model_1, 'generator_2': Generator_model_2,
                              'generator_3': Generator_model_3, 'generator_4': Generator_model_4
                              }

                for model_name, model in model_dict.items():
                    model.to(device)

                optimizer_dict = {'autoencoder_1': optim_AAE_1, 'autoencoder_2': optim_AAE_1,
                                  'autoencoder_3': optim_AAE_3, 'autoencoder_4': optim_AAE_4,
                                  'discriminator_1': optim_D_1, 'discriminator_2': optim_D_2,
                                  'generator_1': optim_G_1, 'generator_2': optim_G_2,
                                  'generator_3': optim_G_3, 'generator_4': optim_G_4}



                log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
                logger = History()
                logger.on_train_init(log_keys)
                for epoch in range(1, n_epochs + 1):
                    logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)

                torch.save(model_dict, join(MMR_PATH,'model_dict.pt'))
                fig, ax1 = plt.subplots()

                ax2 = ax1.twinx()
                ax1.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
                ax2.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
                ax2.plot(logger.logs['generator_loss'], 'g', label='Generator')
                ax1.plot(logger.logs['loss'], 'k', label='Total loss')

                ax1.set_xlabel('Iterations')
                ax1.set_ylabel('Reconstruction and Total loss', color='g')
                ax2.set_ylabel('Discriminator and generator loss', color='b')

                ax1.legend()
                ax2.legend()
                plt.savefig(join(MMR_PATH,'training.png'))
                plt.close()

            else:
                if use_cuda:
                    model_dict = torch.load(join(MMR_PATH, 'model_dict.pt'))
                else:
                    model_dict = torch.load(join(MMR_PATH, 'model_dict.pt'), map_location='cpu')


            ###############################
            ##########  RESULTS  ##########
            ###############################
            reconstruction_size = [(N1, m) for m in M]

            y_scores, y_scores_mean, y_scores_var, x_rec, indices_reordered = predict(model_dict, generator_test, (N1, latent_dim), reconstruction_size)

            modality_array_list = [m1_array[indices_reordered], m2_array[indices_reordered],
                                   m3_array[indices_reordered], m4_array[indices_reordered]]
            modality_labels_list = [copy.deepcopy(m1_labels_array), copy.deepcopy(m2_labels_array),
                                    copy.deepcopy(m3_labels_array), copy.deepcopy(m4_labels_array)]
            modality_labels_list[it_modality_1][indices_drop] = 0
            modality_labels_list = [mll[indices_reordered] for mll in modality_labels_list]

            dx_array_tmp = dx_array[indices_reordered]
            age_array_tmp = age_array[indices_reordered]

            y_scores_dx_indices = [[np.where(dx_array_tmp == 0)[0],
                                    np.where(dx_array_tmp == 1)[0],
                                    np.where(dx_array_tmp == 2)[0]]]

            for it_modality_2, modality_2 in enumerate(modality_list):
                mse_per_feature = np.mean((modality_array_list[it_modality_2] - x_rec[it_modality_2]) ** 2, axis=0)
                error_features_dict[modality_1][mmr][modality_2] = mse_per_feature

                mse_per_subject = np.mean((modality_array_list[it_modality_2] - x_rec[it_modality_2]) ** 2, axis=1)
                error_subjects_dict[modality_1][mmr][modality_2] = mse_per_subject

            error_features_dict[modality_1][mmr][modality_1 + '_missing'] = np.mean((modality_array_list[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]] - x_rec[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]]) ** 2, axis=0)

            if Y_SCORE_PLOTS:
                print('Y_SCORE_PLOTS')
                for it_component in range(N_COMPONENTS):
                    for it_modality_2, modality_2 in enumerate(modality_list):
                        plt.figure()
                        plt.hist(y_scores[it_modality_2][:, it_component], bins=40)
                        plt.savefig(join(MMR_PATH, str(it_component), modality_2 + '_histogram.png'))
                        plt.close()

                    plt.figure()
                    plt.hist(y_scores[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0], it_component], bins=10)
                    plt.savefig(join(MMR_PATH, str(it_component), modality_1 + '_missing_histogram.png'))
                    plt.close()

                    plt.figure()
                    plt.hist(y_scores_var[:, it_component], bins=40)
                    plt.savefig(join(MMR_PATH, str(it_component), 'var_histogram.png'))
                    plt.close()

                    plt.figure()
                    plt.hist(y_scores_var[np.where(modality_labels_list[it_modality_1] == 0)[0], it_component], bins=10)
                    plt.savefig(join(MMR_PATH, str(it_component), 'var_missing_histogram.png'))
                    plt.close()

                    plt.figure()
                    plt.hist(y_scores_mean[:, it_component], bins=40)
                    plt.savefig(join(MMR_PATH, str(it_component), 'mean_histogram.png'))
                    plt.close()

                    plt.figure()
                    plt.hist(y_scores_mean[np.where(modality_labels_list[it_modality_1] == 0)[0], it_component], bins=10)
                    plt.savefig(join(MMR_PATH, str(it_component), 'mean_missing_histogram.png'))
                    plt.close()


            if MSE_PLOTS:
                print('MSE_PLOTS')

                #### TRAIN
                writer_mse = ResultsWriter(filepath=join(MMR_PATH, 'mse_results.txt'), attach=False)
                writer_mse.write('###########\n')
                writer_mse.write('MSE RESULTS\n')
                writer_mse.write('###########\n\n')
                for it_modality_2, modality_2 in enumerate(modality_list):

                    mse_per_feature = np.mean((modality_array_list[it_modality_2] - x_rec[it_modality_2]) ** 2, axis=0)
                    mse_per_subject = np.mean((modality_array_list[it_modality_2] - x_rec[it_modality_2]) ** 2, axis=1)

                    writer_mse.write(modality_list[it_modality_2])
                    writer_mse.write('\n')

                    sorted_mse = np.argsort(mse_per_feature)[::-1]
                    for it_mse in sorted_mse:
                        writer_mse.write(
                            '  - ' + modality_id_list[it_modality_2][it_mse] + ': ' + str(mse_per_feature[it_mse]) + '\n')

                    index_0 = np.where(dx_array_tmp == 0)[0]
                    index_1 = np.where(dx_array_tmp == 1)[0]
                    index_2 = np.where(dx_array_tmp == 2)[0]
                    index_dx_list = [index_0, index_1, index_2]

                    boxplot_list = [mse_per_subject[idx] for idx in index_dx_list]
                    combination_plots = [(0, 1), (0, 2), (1, 2)]
                    p, t = [], []
                    writer_mse.write('\n')
                    writer_mse.write('  ### STATISTICAL RESULTS:')
                    writer_mse.write('\n')
                    for c in combination_plots:
                        result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                        t.append(result[0])
                        p.append(result[1])
                        writer_mse.write('      - ' + label_diagnostic[c[0]] + '-' + label_diagnostic[c[1]] + ': t=' + str(
                            result[0]) + ' p=' + str(result[1]))
                        writer_mse.write('\n')
                    writer_mse.write('\n')

                    plt.figure()
                    plt.boxplot(boxplot_list)
                    plt.grid()
                    plt.ylabel('MSE')
                    plt.xlabel('Clinical Diagnosis')
                    plt.title('Significant (p<0.05) differences: ' + str(
                        [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                    plt.savefig(join(MMR_PATH, modality_2 + '_mse_diagnostic_error.png'))
                    plt.close()

                    writer_mse.write('      - AGE:' + str(pearsonr(age_array_tmp.reshape(-1,), mse_per_subject)))
                    writer_mse.write('\n')
                    writer_mse.write('\n')
                    writer_mse.write('\n')

                    plt.figure()
                    plt.plot(age_array_tmp[index_0], mse_per_subject[index_0], '*b', label='NC')
                    plt.plot(age_array_tmp[index_1], mse_per_subject[index_1], '*r', label='MCI')
                    plt.plot(age_array_tmp[index_2], mse_per_subject[index_2], '*k', label='AD')
                    plt.grid()
                    plt.xlabel('Age')
                    plt.ylabel('MSE')
                    plt.legend()
                    plt.savefig(join(MMR_PATH, modality_2 + '_mse_age_error.png'))
                    plt.close()

                    plt.figure()
                    plt.plot(modality_array_list[it_modality_2][index_0], x_rec[it_modality_2][index_0], '*b', label='NC')
                    plt.plot(modality_array_list[it_modality_2][index_1], x_rec[it_modality_2][index_1], '*r', label='MCI')
                    plt.plot(modality_array_list[it_modality_2][index_2], x_rec[it_modality_2][index_2], '*k', label='AD')
                    plt.grid()
                    plt.xlabel('True')
                    plt.ylabel('Predicted')
                    plt.legend()
                    plt.savefig(join(MMR_PATH, modality_2 + '_true_predicted.png'))
                    plt.close()

                if mmr == 0:
                    continue
                else:
                    mse_per_feature = np.mean((modality_array_list[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]] - x_rec[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]]) ** 2, axis=0)
                    mse_per_subject = np.mean((modality_array_list[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]] - x_rec[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]]) ** 2, axis=1)

                    writer_mse.write(modality_1 + '_missing')
                    writer_mse.write('\n')

                    sorted_mse = np.argsort(mse_per_feature)[::-1]
                    for it_mse in sorted_mse:
                        writer_mse.write(
                            '  - ' + modality_id_list[it_modality_1][it_mse] + ': ' + str(mse_per_feature[it_mse]) + '\n')

                    age_array_tmp_missing = age_array_tmp[np.where(modality_labels_list[it_modality_1] == 0)[0]]
                    dx_array_tmp_missing = dx_array_tmp[np.where(modality_labels_list[it_modality_1] == 0)[0]]

                    index_0 = np.where(dx_array_tmp_missing == 0)[0]
                    index_1 = np.where(dx_array_tmp_missing == 1)[0]
                    index_2 = np.where(dx_array_tmp_missing == 2)[0]
                    index_dx_list = [index_0, index_1, index_2]

                    boxplot_list = [mse_per_subject[idx] for idx in index_dx_list]
                    combination_plots = [(0, 1), (0, 2), (1, 2)]
                    p, t = [], []
                    writer_mse.write('\n')
                    writer_mse.write('  ### STATISTICAL RESULTS:')
                    writer_mse.write('\n')
                    for c in combination_plots:
                        result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                        t.append(result[0])
                        p.append(result[1])
                        writer_mse.write('      - ' + label_diagnostic[c[0]] + '-' + label_diagnostic[c[1]] + ': t=' + str(
                            result[0]) + ' p=' + str(result[1]))
                        writer_mse.write('\n')
                    writer_mse.write('\n')

                    plt.figure()
                    plt.boxplot(boxplot_list)
                    plt.grid()
                    plt.ylabel('MSE')
                    plt.xlabel('Clinical Diagnosis')
                    plt.title('Significant (p<0.05) differences: ' + str(
                        [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                    plt.savefig(join(MMR_PATH, modality_1 + '_missing_mse_diagnostic_error.png'))
                    plt.close()

                    writer_mse.write('      - AGE:' + str(pearsonr(age_array_tmp_missing.reshape(-1, ), mse_per_subject)))
                    writer_mse.write('\n')
                    writer_mse.write('\n')
                    writer_mse.write('\n')

                    plt.figure()
                    plt.plot(age_array_tmp_missing[index_0], mse_per_subject[index_0], '*b', label='NC')
                    plt.plot(age_array_tmp_missing[index_1], mse_per_subject[index_1], '*r', label='MCI')
                    plt.plot(age_array_tmp_missing[index_2], mse_per_subject[index_2], '*k', label='AD')
                    plt.grid()
                    plt.xlabel('Age')
                    plt.ylabel('MSE')
                    plt.legend()
                    plt.savefig(join(MMR_PATH, modality_1 + '_missing_mse_age_error.png'))
                    plt.close()

                    plt.figure()
                    plt.plot(modality_array_list[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]][index_0], x_rec[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]][index_0], '*b', label='NC')
                    plt.plot(modality_array_list[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]][index_1], x_rec[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]][index_1], '*r', label='MCI')
                    plt.plot(modality_array_list[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]][index_2], x_rec[it_modality_1][np.where(modality_labels_list[it_modality_1] == 0)[0]][index_2], '*k', label='AD')
                    plt.grid()
                    plt.xlabel('True')
                    plt.ylabel('Predicted')
                    plt.legend()
                    plt.savefig(join(MMR_PATH, modality_1 + '_missing_true_predicted.png'))
                    plt.close()

            del modality_array_list
            del modality_labels_list

    if GLOBAL_PLOTS:
        print('GLOBAL_PLOTS', flush=True)

        for it_modality_1, modality_1 in enumerate(modality_list):
            print('MODALITY: ' + modality_1, flush=True)
            SUBMOD_PATH = join(MODALITY_PATH, modality_1)
            plt.figure()
            for it_modality_2, modality_2 in enumerate(modality_list):
                plt.plot(missing_mod_rate_list, [np.mean(error_features_dict[modality_1][mmr][modality_2]) for mmr in missing_mod_rate_list], color_code[it_modality_2], label = modality_2)

            plt.plot(missing_mod_rate_list, [np.mean(error_features_dict[modality_1][mmr][modality_1 + '_missing']) for mmr in missing_mod_rate_list], color_code[4], label=modality_1 + '_missing')

            plt.grid()
            plt.ylabel('MSE')
            plt.xlabel('Missing modality Rate')
            plt.title('Missing_modality: ' + modality_1)
            plt.legend()
            plt.savefig(join(MODALITY_PATH, 'missing_' + modality_1 +'_mse_per_feature.png'))
            plt.close()

