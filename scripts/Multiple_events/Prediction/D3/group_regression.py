import csv
import warnings
from os.path import join, exists
import os
from src.utils.io_utils import ResultsWriter
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *
import numpy as np
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import explained_variance_score, mean_squared_error, r2_score, mean_absolute_error
from sklearn.exceptions import UndefinedMetricWarning
from sklearn.preprocessing import StandardScaler
warnings.simplefilter(action='ignore', category=UndefinedMetricWarning)


def get_float(value):
    try:
        if value == 'nan':
            return None

        return float(value)

    except:

        return None

BASE_PATH = join('/mnt/gpid08/users/adria.casamitjana/Multimodal_autoencoders/AAE/Multiple_events/',
                 'Disentangle_adversarial','MissingModalities','nc_16','lineal','av45_vMRI_cMRI_clinical')

INPUT_PATH = join(BASE_PATH, 'Y_SCORES')
OUTPUT_PATH = join(BASE_PATH, 'Group_Regression')
if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

writer = ResultsWriter(filepath=join(OUTPUT_PATH, 'results_file.txt'), attach=False)

std_scaler = StandardScaler()

## Read Y-scores

##### Read y-scores #####
DataLoader_train = data_reader.DataLoader_Longitudinal_semisupervised()
DataLoader_test = data_reader.DataLoader_Longitudinal_semisupervised(data_path=data_reader.DATA_PATH_D3)


rid_events_dict_train = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_train.subjects_dict.items()}
rid_events_dict_test = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_test.subjects_dict.items()}
rid_events_list_train = [(rid, e_step) for rid, e_step_list in rid_events_dict_train.items() for e_step in e_step_list]
rid_events_list_test = [(rid, e_step) for rid, e_step_list in rid_events_dict_test.items() for e_step in e_step_list]

modality_list = ['av45', 'vMRI', 'cMRI', 'clinical']
modality_dict_train_list = []
modality_dict_test_list = []
for modality in modality_list:
    modality_dict_train = {}
    modality_dict_test = {}
    with open(join(INPUT_PATH,modality + '.csv'), 'r') as csvfile:
        csvreader = csv.DictReader(csvfile)
        header = csvreader.fieldnames
        M = len(header) - 3
        for row in csvreader:
            if row['release'] == 'D1_D2':
                if row['RID'] in list(rid_events_dict_train.keys()):
                    if row['RID'] in modality_dict_train.keys():
                        modality_dict_train[row['RID']][int(row['event'])] = [get_float(row[h]) for h in header[3:]]
                    else:
                        modality_dict_train[row['RID']] = {int(row['event']): [get_float(row[h]) for h in header[3:]]}
                elif row['RID'] in list(rid_events_dict_test.keys()):
                    if row['RID'] in modality_dict_test.keys():
                        modality_dict_test[row['RID']][int(row['event'])] = [get_float(row[h]) for h in header[3:]]
                    else:
                        modality_dict_test[row['RID']] = {int(row['event']): [get_float(row[h]) for h in header[3:]]}
            else:
                pass
    modality_dict_train_list.append(modality_dict_train)
    modality_dict_test_list.append(modality_dict_test)


##### Compute mean and variance layers #####
print(M)
mean_dict_train = {}
var_dict_train = {}
mean_array_train = np.zeros((len(rid_events_list_train), M))
var_array_train = np.zeros((len(rid_events_list_train), M))
valid_rid_events_list_train = []
for it_rid, (rid,e_step) in enumerate(rid_events_list_train):
    if rid not in mean_dict_train.keys():
        mean_dict_train[rid] = {}
    if rid not in var_dict_train.keys():
        var_dict_train[rid] = {}

    n_sbj = [np.sum([1 for mdl in modality_dict_train_list if mdl[rid][e_step][m] is not None]) for m in range(M)]
    if 0 in n_sbj:
        mean_dict_train[rid][e_step] = [0 for m in range(M)]
        var_dict_train[rid][e_step] = [0 for m in range(M)]

    else:
        valid_rid_events_list_train.append((rid,e_step))
        mean_dict_train[rid][e_step] = [np.sum([mdl[rid][e_step][m] for mdl in modality_dict_train_list if mdl[rid][e_step][m] is not None])/n_sbj[m] for m in range(M)]
        var_dict_train[rid][e_step] = [np.sum([(mdl[rid][e_step][m] - mean_dict_train[rid][e_step][m]) ** 2 for mdl in modality_dict_train_list if mdl[rid][e_step][m] is not None]) / n_sbj[m] for m in range(M)]

mean_dict_test = {}
var_dict_test = {}
mean_array_test = np.zeros((len(rid_events_list_test), M))
var_array_test = np.zeros((len(rid_events_list_test), M))
valid_rid_events_list_test = []
for it_rid, (rid,e_step) in enumerate(rid_events_list_test):
    if rid not in mean_dict_test.keys():
        mean_dict_test[rid] = {}
    if rid not in var_dict_test.keys():
        var_dict_test[rid] = {}

    n_sbj = [np.sum([1 for mdl in modality_dict_test_list if mdl[rid][e_step][m] is not None]) for m in range(M)]
    if 0 in n_sbj:
        mean_dict_test[rid][e_step] = [0 for m in range(M)]
        var_dict_test[rid][e_step] = [0 for m in range(M)]

    else:
        valid_rid_events_list_test.append((rid,e_step))
        mean_dict_test[rid][e_step] = [np.sum([mdl[rid][e_step][m] for mdl in modality_dict_test_list if mdl[rid][e_step][m] is not None])/n_sbj[m] for m in range(M)]
        var_dict_test[rid][e_step] = [np.sum([(mdl[rid][e_step][m] - mean_dict_test[rid][e_step][m]) ** 2 for mdl in modality_dict_test_list if mdl[rid][e_step][m] is not None]) / n_sbj[m] for m in range(M)]

icv_dict_train, icv_labels_dict_train = DataLoader_train.load_imaging_phenotypes(imaging_phenotye=['icv'])
valid_rid_events_list_train = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_train if icv_labels_dict_train[rid][e_step] == 1]

icv_dict_test, icv_labels_dict_test = DataLoader_test.load_imaging_phenotypes(imaging_phenotye=['icv'])
valid_rid_events_list_test = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_test if icv_labels_dict_test[rid][e_step] == 1]

imaging_phenotypes_id_list = ['ventricles']#, 'hippocampus', 'whole_brain', 'entorhinal','fusiform', 'middle_temporal']
for imaging_phenotype in imaging_phenotypes_id_list:
    imaging_phenotypes_dict_train, imaging_phenotypes_labels_dict_train = DataLoader_train.load_imaging_phenotypes(imaging_phenotye=[imaging_phenotype])
    valid_rid_events_list_train = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_train if imaging_phenotypes_labels_dict_train[rid][e_step] == 1]
    Y_array_train = np.asarray([[imaging_phenotypes_dict_train[rid][e_step][0]/icv_dict_train[rid][e_step][0]]  for (rid, e_step) in valid_rid_events_list_train])

    imaging_phenotypes_dict_test, imaging_phenotypes_labels_dict_test = DataLoader_test.load_imaging_phenotypes(imaging_phenotye=[imaging_phenotype])
    valid_rid_events_list_test = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_test if imaging_phenotypes_labels_dict_test[rid][e_step] == 1]
    Y_array_test = np.asarray([[imaging_phenotypes_dict_test[rid][e_step][0]/icv_dict_test[rid][e_step][0]] for (rid, e_step) in valid_rid_events_list_test])

    Y_array_train = std_scaler.fit_transform(Y_array_train)
    Y_array_test = std_scaler.transform(Y_array_test)

    ## Compute mean and variance layers
    mean_array_train = np.asarray([mean_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])
    var_array_train = np.asarray([var_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])
    mean_array_test = np.asarray([mean_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])
    var_array_test = np.asarray([var_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])


    print(mean_array_train.shape)
    print('N train: ' + str(mean_array_train.shape[0]))
    print('M train: ' + str(mean_array_train.shape[1]))
    print('N test: ' + str(mean_array_test.shape[0]))
    print('M test: ' + str(mean_array_test.shape[1]))

    ## Logistic regression
    X_array_train = mean_array_train
    X_array_test = mean_array_test

    train_results = {'mse': [], 'mae': [], 'r2': []}
    test_results = {'mse': [], 'mae': [], 'r2': []}

    lr_model = LinearRegression(fit_intercept=True, normalize=False)

    lr_model.fit(X_array_train, Y_array_train)
    y_train_predicted = lr_model.predict(X_array_train)
    Y_array_train = std_scaler.inverse_transform(Y_array_train)
    y_train_predicted = std_scaler.inverse_transform(y_train_predicted)
    y_test_predicted = lr_model.predict(X_array_test)
    Y_array_test = std_scaler.inverse_transform(Y_array_test)
    y_test_predicted = std_scaler.inverse_transform(y_test_predicted)


    train_results['mse'].append(mean_squared_error(Y_array_train, y_train_predicted))
    train_results['mae'].append(mean_absolute_error(Y_array_train, y_train_predicted))
    train_results['r2'].append(r2_score(Y_array_train, y_train_predicted))

    test_results['mse'].append(mean_squared_error(Y_array_test, y_test_predicted))
    test_results['mae'].append(mean_absolute_error(Y_array_test, y_test_predicted))
    test_results['r2'].append(r2_score(Y_array_test, y_test_predicted))

    #Results
    print('TRAINING RESULTS')
    print('Intercept: ' + str(lr_model.intercept_) + ' coef: ' + str(lr_model.coef_))
    print('==> Global')
    print('     - MSE: ' + str(np.mean(train_results['mse'])))
    print('     - MAE: ' + str(np.mean(train_results['mae'])))
    print('     - R2: ' + str(np.mean(train_results['r2'])))


    print('')
    print('TESTING RESULTS')
    print('==> Global')
    print('     - MSE: ' + str(np.mean(test_results['mse'])))
    print('     - MAE: ' + str(np.mean(test_results['mae'])))
    print('     - R2: ' + str(np.mean(test_results['r2'])))

    writer.write('##############################')
    writer.write('#####' + imaging_phenotype + ' REGRESSION #####')
    writer.write('##############################')


    writer.write('TRAINING RESULTS')
    writer.write('\n')
    writer.write('==> Global')
    writer.write('\n')
    writer.write('     - MSE: ' + str(np.mean(train_results['mse'])))
    writer.write('\n')
    writer.write('     - MAE: ' + str(np.mean(train_results['mae'])))
    writer.write('\n')
    writer.write('     - R2: ' + str(np.mean(train_results['r2'])))
    writer.write('\n\n')

    writer.write('TESTING RESULTS')
    writer.write('\n')
    writer.write('==> Global')
    writer.write('\n')
    writer.write('     - MSE: ' + str(np.mean(test_results['mse'])))
    writer.write('\n')
    writer.write('     - MAE: ' + str(np.mean(test_results['mae'])))
    writer.write('\n')
    writer.write('     - R2: ' + str(np.mean(test_results['r2'])))
    writer.write('\n\n')




