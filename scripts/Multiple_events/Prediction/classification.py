import csv
import warnings
from os.path import join, exists
import os
from src.utils.io_utils import ResultsWriter
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, roc_auc_score, \
    precision_recall_curve, roc_curve, confusion_matrix
from sklearn.exceptions import UndefinedMetricWarning

warnings.simplefilter(action='ignore', category=UndefinedMetricWarning)


def get_float(value):
    try:
        if value == 'nan':
            return None

        return float(value)

    except:

        return None

BASE_PATH = join('/mnt/gpid08/users/adria.casamitjana/Multimodal_autoencoders/AAE/Multiple_events/',
                 'Disentangle_adversarial','MissingModalities','nc_16','lineal','av45_vMRI_cMRI_clinical')

INPUT_PATH = join(BASE_PATH, 'Y_SCORES')
OUTPUT_PATH = join(BASE_PATH, 'Classification')
if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

writer = ResultsWriter(filepath=join(OUTPUT_PATH, 'results_file.txt'), attach=False)

## Read Y-scores

##### Read y-scores #####
DataLoader_train = data_reader.DataLoader_Longitudinal_semisupervised()
DataLoader_train.filter_subjects_release(release='D1')
DataLoader_test = data_reader.DataLoader_Longitudinal_semisupervised()
DataLoader_test.filter_subjects_release(release='D2')

release_dict_train = {rid: subject.events[0].release for rid, subject in DataLoader_train.subjects_dict.items()}
release_dict_test = {rid: subject.events[0].release for rid, subject in DataLoader_test.subjects_dict.items()}


rid_events_dict_train = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_train.subjects_dict.items()}
rid_events_dict_test = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_test.subjects_dict.items()}
rid_events_list_train = [(rid, e_step) for rid, e_step_list in rid_events_dict_train.items() for e_step in e_step_list]
rid_events_list_test = [(rid, e_step) for rid, e_step_list in rid_events_dict_test.items() for e_step in e_step_list]

modality_list = ['av45', 'vMRI', 'cMRI', 'clinical']
modality_dict_train_list = []
modality_dict_test_list = []
for modality in modality_list:
    modality_dict_train = {}
    modality_dict_test = {}
    with open(join(INPUT_PATH,modality + '.csv'), 'r') as csvfile:
        csvreader = csv.DictReader(csvfile)
        header = csvreader.fieldnames
        M = len(header) - 3
        for row in csvreader:
            if row['release'] == 'D1_D2':
                if row['RID'] in list(rid_events_dict_train.keys()):
                    if row['RID'] in modality_dict_train.keys():
                        modality_dict_train[row['RID']][int(row['event'])] = [get_float(row[h]) for h in header[3:]]
                    else:
                        modality_dict_train[row['RID']] = {int(row['event']): [get_float(row[h]) for h in header[3:]]}
                elif row['RID'] in list(rid_events_dict_test.keys()):
                    if row['RID'] in modality_dict_test.keys():
                        modality_dict_test[row['RID']][int(row['event'])] = [get_float(row[h]) for h in header[3:]]
                    else:
                        modality_dict_test[row['RID']] = {int(row['event']): [get_float(row[h]) for h in header[3:]]}
            else:
                pass
    modality_dict_train_list.append(modality_dict_train)
    modality_dict_test_list.append(modality_dict_test)

##### Compute mean and variance layers #####
print(M)
mean_dict_train = {}
var_dict_train = {}

mean_array_train = np.zeros((len(rid_events_list_train), M))
var_array_train = np.zeros((len(rid_events_list_train), M))
valid_rid_events_list_train = []
for it_rid, (rid,e_step) in enumerate(rid_events_list_train):
    if rid not in mean_dict_train.keys():
        mean_dict_train[rid] = {}
    if rid not in var_dict_train.keys():
        var_dict_train[rid] = {}

    n_sbj = [np.sum([1 for mdl in modality_dict_train_list if mdl[rid][e_step][m] is not None]) for m in range(M)]
    if 0 in n_sbj:
        mean_dict_train[rid][e_step] = [0 for m in range(M)]
        var_dict_train[rid][e_step] = [0 for m in range(M)]

    else:
        valid_rid_events_list_train.append((rid,e_step))
        mean_dict_train[rid][e_step] = [np.sum([mdl[rid][e_step][m] for mdl in modality_dict_train_list if mdl[rid][e_step][m] is not None])/n_sbj[m] for m in range(M)]
        var_dict_train[rid][e_step] = [np.sum([(mdl[rid][e_step][m] - mean_dict_train[rid][e_step][m]) ** 2 for mdl in modality_dict_train_list if mdl[rid][e_step][m] is not None]) / n_sbj[m] for m in range(M)]

mean_dict_test = {}
var_dict_test = {}
mean_array_test = np.zeros((len(rid_events_list_test), M))
var_array_test = np.zeros((len(rid_events_list_test), M))
valid_rid_events_list_test = []
for it_rid, (rid,e_step) in enumerate(rid_events_list_test):
    if rid not in mean_dict_test.keys():
        mean_dict_test[rid] = {}
    if rid not in var_dict_test.keys():
        var_dict_test[rid] = {}

    n_sbj = [np.sum([1 for mdl in modality_dict_test_list if mdl[rid][e_step][m] is not None]) for m in range(M)]
    if 0 in n_sbj:
        mean_dict_test[rid][e_step] = [0 for m in range(M)]
        var_dict_test[rid][e_step] = [0 for m in range(M)]

    else:
        valid_rid_events_list_test.append((rid,e_step))
        mean_dict_test[rid][e_step] = [np.sum([mdl[rid][e_step][m] for mdl in modality_dict_test_list if mdl[rid][e_step][m] is not None])/n_sbj[m] for m in range(M)]
        var_dict_test[rid][e_step] = [np.sum([(mdl[rid][e_step][m] - mean_dict_test[rid][e_step][m]) ** 2 for mdl in modality_dict_test_list if mdl[rid][e_step][m] is not None]) / n_sbj[m] for m in range(M)]


dx_list = [{0:1,1:0,2:0}, {0:0,1:1,2:0}, {0:0,1:0,2:1}]
for DX_DICT in dx_list:
    DIAGNOSIS_1_AB_0 = 1
    if DIAGNOSIS_1_AB_0 == 1:
        dx_dict, dx_labels_dict = DataLoader_train.load_dx(dx_transformation_dict = DX_DICT)
        valid_rid_events_list_train = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_train if dx_labels_dict[rid][e_step] == 1]
        Y_array_train = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])

        dx_dict, dx_labels_dict = DataLoader_test.load_dx(dx_transformation_dict = DX_DICT)
        valid_rid_events_list_test = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_test if dx_labels_dict[rid][e_step] == 1]
        Y_array_test = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])

    else:
        dx_dict, _ = DataLoader_train.load_dx()
        abstatus_dict, abstatus_labels_dict = DataLoader_train.load_amyloid_status()
        # ab_dict, ab_labels_dict = DataLoader_train.load_csf(csf_names=['ab'])
        valid_rid_events_list_train = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_train if abstatus_labels_dict[rid][e_step] == 1 and dx_dict[rid][e_step] == 0]
        Y_array_train = np.asarray([abstatus_dict[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])

        dx_dict, _ = DataLoader_test.load_dx()
        abstatus_dict, abstatus_labels_dict = DataLoader_test.load_amyloid_status()
        # ab_dict, ab_labels_dict = DataLoader_test.load_csf(csf_names=['ab'])
        valid_rid_events_list_test = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_test if abstatus_labels_dict[rid][e_step] == 1 and dx_dict[rid][e_step] == 0]
        Y_array_test = np.asarray([abstatus_dict[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])
        print(Y_array_train)

    ## Compute mean and variance layers
    mean_array_train = np.asarray([mean_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])
    var_array_train = np.asarray([var_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])
    mean_array_test = np.asarray([mean_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])
    var_array_test = np.asarray([var_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])



    print(mean_array_train.shape)
    print('N train: ' + str(mean_array_train.shape[0]))
    print('M train: ' + str(mean_array_train.shape[1]))
    print('N test: ' + str(mean_array_test.shape[0]))
    print('M test: ' + str(mean_array_test.shape[1]))

    ## Logistic regression
    X_array_train = mean_array_train
    X_array_test = mean_array_test

    if len(np.unique(Y_array_train)) == 2:
        train_results = {'accuracy': [], 'recall': [], 'precision': [], 'specificity': [],  'f1': [], 'auc': []}
        test_results = {'accuracy': [], 'recall': [], 'precision': [], 'specificity': [],  'f1': [], 'auc': []}

    else:
        train_results = {'confusion_matrix': np.zeros((len(np.unique(Y_array_train)),len(np.unique(Y_array_train))))}
        test_results = {'confusion_matrix': np.zeros((len(np.unique(Y_array_test)),len(np.unique(Y_array_test))))}

    lr_model = LogisticRegression(solver='liblinear', multi_class='auto')

    lr_model.fit(X_array_train, Y_array_train)
    y_train_predicted = lr_model.predict(X_array_train)
    y_test_predicted = lr_model.predict(X_array_test)

    if len(np.unique(Y_array_train)) == 2:

        train_results['accuracy'].append(accuracy_score(Y_array_train, y_train_predicted))
        train_results['recall'].append(recall_score(Y_array_train, y_train_predicted, pos_label=np.max(np.unique(Y_array_train))))
        train_results['precision'].append(precision_score(Y_array_train, y_train_predicted, pos_label=np.max(np.unique(Y_array_train))))
        train_results['specificity'].append(recall_score(Y_array_train, y_train_predicted, pos_label=np.min(np.unique(Y_array_train))))
        train_results['f1'].append(f1_score(Y_array_train, y_train_predicted, pos_label=np.max(np.unique(Y_array_train))))
        train_results['auc'].append(roc_auc_score(Y_array_train, y_train_predicted))

        test_results['accuracy'].append(accuracy_score(Y_array_test, y_test_predicted))
        test_results['recall'].append(recall_score(Y_array_test, y_test_predicted, pos_label=np.max(np.unique(Y_array_test))))
        test_results['precision'].append(precision_score(Y_array_test, y_test_predicted, pos_label=np.max(np.unique(Y_array_test))))
        test_results['specificity'].append(recall_score(Y_array_test, y_test_predicted, pos_label=np.min(np.unique(Y_array_test))))
        test_results['f1'].append(f1_score(Y_array_test, y_test_predicted, pos_label=np.max(np.unique(Y_array_test))))
        test_results['auc'].append(roc_auc_score(Y_array_test, y_test_predicted))

    else:
        train_results['confusion_matrix'] += confusion_matrix(Y_array_train, y_train_predicted, labels=np.sort(np.unique(Y_array_train)))
        test_results['confusion_matrix'] += confusion_matrix(Y_array_test, y_test_predicted, labels=np.sort(np.unique(Y_array_test)))


    #Results

    if len(np.unique(Y_array_train)) == 2:
        print('TRAINING RESULTS')
        print('Intercept: ' + str(lr_model.intercept_) + ' coef: ' + str(lr_model.coef_))
        print('==> Global')
        print('     - Accuracy: ' + str(np.mean(train_results['accuracy'])))
        print('     - Recall: ' + str(np.mean(train_results['recall'])))
        print('     - Precision: ' + str(np.mean(train_results['precision'])))
        print('     - F1: ' + str(np.mean(train_results['f1'])))
        print('     - Specificity: ' + str(np.mean(train_results['specificity'])))
        print('     - AUC: ' + str(np.mean(train_results['auc'])))

        print('')
        print('TESTING RESULTS')
        print('==> Global')
        print('     - Accuracy: ' + str(np.mean(test_results['accuracy'])))
        print('     - Recall: ' + str(np.mean(test_results['recall'])))
        print('     - Precision: ' + str(np.mean(test_results['precision'])))
        print('     - F1: ' + str(np.mean(test_results['f1'])))
        print('     - Specificity: ' + str(np.mean(test_results['specificity'])))
        print('     - AUC: ' + str(np.mean(test_results['auc'])))

        if DIAGNOSIS_1_AB_0 == 1:
            writer.write('DIAGNOSIS CLASSIFICATION: ')
            writer.write(str(DX_DICT))
            writer.write('\n')
        else:
            writer.write('AMYLOID POSITIVITY CLASSIFICATION: ')


        writer.write('TRAINING RESULTS')
        writer.write('\n')
        writer.write('==> Global')
        writer.write('\n')
        writer.write('     - Accuracy: ' + str(np.mean(train_results['accuracy'])))
        writer.write('\n')
        writer.write('     - Recall: ' + str(np.mean(train_results['recall'])))
        writer.write('\n')
        writer.write('     - Precision: ' + str(np.mean(train_results['precision'])))
        writer.write('\n')
        writer.write('     - F1: ' + str(np.mean(train_results['f1'])))
        writer.write('\n')
        writer.write('     - Specificity: ' + str(np.mean(train_results['specificity'])))
        writer.write('\n')
        writer.write('     - AUC: ' + str(np.mean(train_results['auc'])))

        writer.write('\n\n')
        writer.write('TESTING RESULTS')
        writer.write('\n')
        writer.write('==> Global')
        writer.write('\n')
        writer.write('     - Accuracy: ' + str(np.mean(test_results['accuracy'])))
        writer.write('\n')
        writer.write('     - Recall: ' + str(np.mean(test_results['recall'])))
        writer.write('\n')
        writer.write('     - Precision: ' + str(np.mean(test_results['precision'])))
        writer.write('\n')
        writer.write('     - F1: ' + str(np.mean(test_results['f1'])))
        writer.write('\n')
        writer.write('     - Specificity: ' + str(np.mean(test_results['specificity'])))
        writer.write('\n')
        writer.write('     - AUC: ' + str(np.mean(test_results['auc'])))
        writer.write('\n\n')


    else:
        print('TRAINING RESULTS')
        print(' - Confusion matrix: ')
        print(train_results['confusion_matrix'])

        print()
        print('TESTING RESULTS')
        print(' - Confusion matrix: ')
        print(test_results['confusion_matrix'])

        if DIAGNOSIS_1_AB_0 == 1:
            writer.write('DIAGNOSIS CLASSIFICATION: ')
            writer.write(str(DX_DICT))
            writer.write('\n')
        else:
            writer.write('AMYLOID POSITIVITY CLASSIFICATION: ')

        writer.write('TRAINING RESULTS')
        writer.write('\n')
        writer.write(' - Confusion matrix: ')
        writer.write('\n')
        writer.write(train_results['confusion_matrix'])
        writer.write('\n')

        writer.write('\n')
        writer.write('TESTING RESULTS')
        writer.write('\n')
        writer.write(' - Confusion matrix: ')
        writer.write('\n')
        writer.write(test_results['confusion_matrix'])
        writer.write('\n\n')


