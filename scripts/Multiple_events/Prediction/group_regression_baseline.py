import csv
import warnings
from os.path import join, exists
import os
from src.utils.io_utils import ResultsWriter
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *
import numpy as np
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import explained_variance_score, mean_squared_error, r2_score, mean_absolute_error
from sklearn.exceptions import UndefinedMetricWarning
from sklearn.preprocessing import StandardScaler
warnings.simplefilter(action='ignore', category=UndefinedMetricWarning)


def get_float(value):
    try:
        if value == 'nan':
            return None

        return float(value)

    except:

        return None

BASE_PATH = join('/mnt/gpid08/users/adria.casamitjana/Multimodal_autoencoders/AAE/Multiple_events/',
                 'Disentangle_adversarial','MissingModalities','nc_16','lineal','av45_vMRI_cMRI_clinical')

INPUT_PATH = join(BASE_PATH, 'Y_SCORES')
OUTPUT_PATH = join(BASE_PATH, 'Group_Regression')
if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

writer = ResultsWriter(filepath=join(OUTPUT_PATH, 'results_file.txt'), attach=False)
std_scaler = StandardScaler()
clinical_names = ['adas13', 'mmse']
######################################
##########   READ DATA  ##############
######################################
DataLoader_train = data_reader.DataLoader_Longitudinal_semisupervised()
DataLoader_test = data_reader.DataLoader_Longitudinal_semisupervised(data_path=data_reader.DATA_PATH_D3)

## Dictionaries
av45_dict_train, av45_labels_dict_train = DataLoader_train.load_modality(modality_name='av45_pet')
vMRI_dict_train, vMRI_labels_dict_train = DataLoader_train.load_modality(modality_name='volumetric_mri')
cMRI_dict_train, cMRI_labels_dict_train = DataLoader_train.load_modality(modality_name='cortical_thickness_mri')
FDG_dict_train, FDG_labels_dict_train = DataLoader_train.load_modality(modality_name='fdg_pet')
csf_dict_train, csf_labels_dict_train = DataLoader_train.load_modality(modality_name='csf')
imaging_phenotypes_dict_train, imaging_phenotypes_labels_dict_train = DataLoader_train.load_modality(modality_name='imaging_phenotypes')
education_dict_train, education_labels_dict_train = DataLoader_train.load_demographic(demo_names=['education'])
age_dict_train, age_labels_dict_train = DataLoader_train.load_age()
clinical_dict_train, clinical_labels_dict_train = DataLoader_train.load_clinical(clinical_names=clinical_names)
dx_dict_train, dx_labels_dict_train = DataLoader_train.load_dx()

rid_events_dict_train = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_train.subjects_dict.items()}
rid_events_list_train = [(rid, e_step) for rid, e_step_list in rid_events_dict_train.items() for e_step in e_step_list]

test_dict = {rid: {e_step: [age_dict_train[rid][e_step]] + clinical_dict_train[rid][e_step] + education_dict_train[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict_train.items()}
test_labels_dict = {rid: {e_step: age_labels_dict_train[rid][e_step]*clinical_labels_dict_train[rid][e_step]*education_labels_dict_train[rid][e_step] for e_step in e_step_list} for rid, e_step_list in rid_events_dict_train.items()}

av45_dict_test, av45_labels_dict_test = DataLoader_test.load_modality(modality_name='av45_pet')
vMRI_dict_test, vMRI_labels_dict_test = DataLoader_test.load_modality(modality_name='volumetric_mri')
cMRI_dict_test, cMRI_labels_dict_test = DataLoader_test.load_modality(modality_name='cortical_thickness_mri')
FDG_dict_test, FDG_labels_dict_test = DataLoader_test.load_modality(modality_name='fdg_pet')
csf_dict_test, csf_labels_dict_test = DataLoader_test.load_modality(modality_name='csf')
imaging_phenotypes_dict_test, imaging_phenotypes_labels_dict_test = DataLoader_test.load_modality(modality_name='imaging_phenotypes')
education_dict_test, education_labels_dict_test = DataLoader_test.load_demographic(demo_names=['education'])
age_dict_test, age_labels_dict_test = DataLoader_test.load_age()
clinical_dict_test, clinical_labels_dict_test = DataLoader_test.load_clinical(clinical_names=clinical_names)
dx_dict_test, dx_labels_dict_test = DataLoader_test.load_dx()

rid_events_dict_test = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_test.subjects_dict.items()}
rid_events_list_test = [(rid, e_step) for rid, e_step_list in rid_events_dict_test.items() for e_step in e_step_list]

test_dict_test = {
    rid: {e_step: [age_dict_test[rid][e_step]] + clinical_dict_test[rid][e_step] + education_dict_test[rid][e_step] for
          e_step in
          e_step_list} for rid, e_step_list in rid_events_dict_test.items()}
test_labels_dict_test = {
    rid: {
    e_step: age_labels_dict_test[rid][e_step] * clinical_labels_dict_test[rid][e_step] * education_labels_dict_test[rid][
        e_step]
    for e_step in e_step_list} for rid, e_step_list in rid_events_dict_test.items()}

## Codenames
av45_id_list = DataLoader_train.load_modality_codenames(modality_name='av45_pet')
vMRI_id_list = DataLoader_train.load_modality_codenames(modality_name='volumetric_mri')
cMRI_id_list = DataLoader_train.load_modality_codenames(modality_name='cortical_thickness_mri')
FDG_id_list = DataLoader_train.load_modality_codenames(modality_name='fdg_pet')
csf_id_list = DataLoader_train.load_modality_codenames(modality_name='csf')
clinical_id_list = clinical_names
imaging_phenotypes_id_list = DataLoader_train.load_modality_codenames(modality_name='imaging_phenotypes')
test_id_list = ['age'] + clinical_names + ['education']

vMRI_id_list_test = DataLoader_test.load_modality_codenames(modality_name='volumetric_mri')
cMRI_id_list_test = DataLoader_test.load_modality_codenames(modality_name='cortical_thickness_mri')
clinical_id_list_test = clinical_names
imaging_phenotypes_id_list_test = DataLoader_test.load_modality_codenames(modality_name='imaging_phenotypes')
test_id_list_test = ['age'] + clinical_names + ['education']

## Modalities used
modality_list = ['av45','vMRI', 'cMRI', 'clinical']
modality_dict_list_train = [av45_dict_train, vMRI_dict_train, cMRI_dict_train, clinical_dict_train]
modality_dict_list_test = [av45_dict_test, vMRI_dict_test, cMRI_dict_test, clinical_dict_test]
modality_labels_dict_list_train = [av45_labels_dict_train, vMRI_labels_dict_train, cMRI_labels_dict_train, clinical_labels_dict_train]
modality_labels_dict_list_test = [av45_labels_dict_test, vMRI_labels_dict_test, cMRI_labels_dict_test, clinical_labels_dict_test]
modality_id_list = [av45_id_list, vMRI_id_list, cMRI_id_list, clinical_id_list]

age_array_train = np.asarray([age_dict_train[rid][e_step] for (rid, e_step) in rid_events_list_train])
if len(age_array_train.shape) < 2:
    age_array_train = age_array_train.reshape(-1, 1)
age_labels_array_train = np.asarray([age_labels_dict_train[rid][e_step] for (rid, e_step) in rid_events_list_train])
age_array_test = np.asarray([age_dict_test[rid][e_step] for (rid, e_step) in rid_events_list_test])
if len(age_array_test.shape) < 2:
    age_array_test = age_array_test.reshape(-1, 1)
age_labels_array_test = np.asarray([age_labels_dict_test[rid][e_step] for (rid, e_step) in rid_events_list_test])

m1_array_train = np.asarray([modality_dict_list_train[0][rid][e_step] for (rid, e_step) in rid_events_list_train])
if len(m1_array_train.shape) < 2:
    m1_array_train = m1_array_train.reshape(-1, 1)
m1_labels_array_train = np.asarray([modality_labels_dict_list_train[0][rid][e_step] for (rid, e_step) in rid_events_list_train])
m1_array_test = np.asarray([modality_dict_list_test[0][rid][e_step] for (rid, e_step) in rid_events_list_test])
if len(m1_array_test.shape) < 2:
    m1_array_test = m1_array_test.reshape(-1, 1)
m1_labels_array_test = np.asarray(
    [modality_labels_dict_list_test[0][rid][e_step] for (rid, e_step) in rid_events_list_test])

m2_array_train = np.asarray([modality_dict_list_train[1][rid][e_step] for (rid, e_step) in rid_events_list_train])
if len(m2_array_train.shape) < 2:
    m2_array_train = m2_array_train.reshape(-1, 1)
m2_labels_array_train = np.asarray([modality_labels_dict_list_train[1][rid][e_step] for (rid, e_step) in rid_events_list_train])
m2_array_test = np.asarray([modality_dict_list_test[1][rid][e_step] for (rid, e_step) in rid_events_list_test])
if len(m2_array_test.shape) < 2:
    m2_array_test = m2_array_test.reshape(-1, 1)
m2_labels_array_test = np.asarray(
    [modality_labels_dict_list_test[1][rid][e_step] for (rid, e_step) in rid_events_list_test])

m3_array_train = np.asarray([modality_dict_list_train[2][rid][e_step] for (rid, e_step) in rid_events_list_train])
if len(m3_array_train.shape) < 2:
    m3_array_train = m3_array_train.reshape(-1, 1)
m3_labels_array_train = np.asarray([modality_labels_dict_list_train[2][rid][e_step] for (rid, e_step) in rid_events_list_train])
m3_array_test = np.asarray([modality_dict_list_test[2][rid][e_step] for (rid, e_step) in rid_events_list_test])
if len(m3_array_test.shape) < 2:
    m3_array_test = m3_array_test.reshape(-1, 1)
m3_labels_array_test = np.asarray(
    [modality_labels_dict_list_test[2][rid][e_step] for (rid, e_step) in rid_events_list_test])

m4_array_train = np.asarray([modality_dict_list_train[3][rid][e_step] for (rid, e_step) in rid_events_list_train])
if len(m4_array_train.shape) < 2:
    m4_array_train = m4_array_train.reshape(-1, 1)
m4_labels_array_train = np.asarray([modality_labels_dict_list_train[3][rid][e_step] for (rid, e_step) in rid_events_list_train])
m4_array_test = np.asarray([modality_dict_list_test[3][rid][e_step] for (rid, e_step) in rid_events_list_test])
if len(m4_array_test.shape) < 2:
    m4_array_test = m4_array_test.reshape(-1, 1)
m4_labels_array_test = np.asarray(
    [modality_labels_dict_list_test[3][rid][e_step] for (rid, e_step) in rid_events_list_test])

dx_array_train = np.asarray([dx_dict_train[rid][e_step] for (rid, e_step) in rid_events_list_train])
if len(dx_array_train.shape) < 2:
    dx_array_train = dx_array_train.reshape(-1, 1)
dx_labels_array_train = np.asarray([dx_labels_dict_train[rid][e_step] for (rid, e_step) in rid_events_list_train])
dx_array_test = np.asarray([dx_dict_test[rid][e_step] for (rid, e_step) in rid_events_list_test])
if len(dx_array_test.shape) < 2:
    dx_array_test = dx_array_test.reshape(-1, 1)
dx_labels_array_test = np.asarray([dx_labels_dict_test[rid][e_step] for (rid, e_step) in rid_events_list_test])

imaging_phenotypes_id_list = ['ventricles', 'hippocampus', 'whole_brain', 'entorhinal','fusiform', 'middle_temporal', 'icv' ]
for imaging_phenotype in imaging_phenotypes_id_list:
    imaging_phenotypes_dict_train, imaging_phenotypes_labels_dict_train = DataLoader_train.load_imaging_phenotypes(imaging_phenotye=[imaging_phenotype])
    valid_rid_events_list_train = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_train if imaging_phenotypes_labels_dict_train[rid][e_step] == 1]
    Y_array_train = np.asarray([imaging_phenotypes_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])

    imaging_phenotypes_dict_test, imaging_phenotypes_labels_dict_test = DataLoader_test.load_imaging_phenotypes(imaging_phenotye=[imaging_phenotype])
    valid_rid_events_list_test = [(rid, e_step) for (rid, e_step) in valid_rid_events_list_test if imaging_phenotypes_labels_dict_test[rid][e_step] == 1]
    Y_array_test = np.asarray([imaging_phenotypes_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])

    Y_array_train = std_scaler.fit_transform(Y_array_train)
    Y_array_test = std_scaler.transform(Y_array_test)

    ## Compute mean and variance layers
    mean_array_train = np.asarray([mean_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])
    var_array_train = np.asarray([var_dict_train[rid][e_step] for (rid, e_step) in valid_rid_events_list_train])
    mean_array_test = np.asarray([mean_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])
    var_array_test = np.asarray([var_dict_test[rid][e_step] for (rid, e_step) in valid_rid_events_list_test])


    print(mean_array_train.shape)
    print('N train: ' + str(mean_array_train.shape[0]))
    print('M train: ' + str(mean_array_train.shape[1]))
    print('N test: ' + str(mean_array_test.shape[0]))
    print('M test: ' + str(mean_array_test.shape[1]))

    ## Logistic regression
    X_array_train = mean_array_train
    X_array_test = mean_array_test

    train_results = {'mse': [], 'mae': [], 'r2': []}
    test_results = {'mse': [], 'mae': [], 'r2': []}

    lr_model = LinearRegression(fit_intercept=True, normalize=False)

    lr_model.fit(X_array_train, Y_array_train)
    y_train_predicted = lr_model.predict(X_array_train)
    y_test_predicted = lr_model.predict(X_array_test)


    train_results['mse'].append(mean_squared_error(Y_array_train, y_train_predicted))
    train_results['mae'].append(mean_absolute_error(Y_array_train, y_train_predicted))
    train_results['r2'].append(r2_score(Y_array_train, y_train_predicted))

    test_results['mse'].append(mean_squared_error(Y_array_test, y_test_predicted))
    test_results['mae'].append(mean_absolute_error(Y_array_test, y_test_predicted))
    test_results['r2'].append(r2_score(Y_array_test, y_test_predicted))

    #Results
    print('TRAINING RESULTS')
    print('Intercept: ' + str(lr_model.intercept_) + ' coef: ' + str(lr_model.coef_))
    print('==> Global')
    print('     - MSE: ' + str(np.mean(train_results['mse'])))
    print('     - MAE: ' + str(np.mean(train_results['mae'])))
    print('     - R2: ' + str(np.mean(train_results['r2'])))


    print('')
    print('TESTING RESULTS')
    print('==> Global')
    print('     - MSE: ' + str(np.mean(test_results['mse'])))
    print('     - MAE: ' + str(np.mean(test_results['mae'])))
    print('     - R2: ' + str(np.mean(test_results['r2'])))

    writer.write('##############################')
    writer.write('#####' + imaging_phenotype + ' REGRESSION #####')
    writer.write('##############################')


    writer.write('TRAINING RESULTS')
    writer.write('\n')
    writer.write('==> Global')
    writer.write('\n')
    writer.write('     - MSE: ' + str(np.mean(train_results['mse'])))
    writer.write('\n')
    writer.write('     - MAE: ' + str(np.mean(train_results['mae'])))
    writer.write('\n')
    writer.write('     - R2: ' + str(np.mean(train_results['r2'])))
    writer.write('\n\n')

    writer.write('TESTING RESULTS')
    writer.write('\n')
    writer.write('==> Global')
    writer.write('\n')
    writer.write('     - MSE: ' + str(np.mean(test_results['mse'])))
    writer.write('\n')
    writer.write('     - MAE: ' + str(np.mean(test_results['mae'])))
    writer.write('\n')
    writer.write('     - R2: ' + str(np.mean(test_results['r2'])))
    writer.write('\n\n')




