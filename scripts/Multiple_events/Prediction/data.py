import csv
import warnings
from os.path import join, exists
import os
from src.utils.io_utils import ResultsWriter
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, roc_auc_score, \
    precision_recall_curve, roc_curve, confusion_matrix
from sklearn.exceptions import UndefinedMetricWarning

warnings.simplefilter(action='ignore', category=UndefinedMetricWarning)


def get_float(value):
    try:
        if value == 'nan':
            return None

        return float(value)

    except:

        return None

BASE_PATH = join('/mnt/gpid08/users/adria.casamitjana/Multimodal_autoencoders/AAE/Multiple_events/',
                 'Disentangle_adversarial','MissingModalities','nc_16','lineal','av45_vMRI_cMRI_clinical')

INPUT_PATH = join(BASE_PATH, 'Y_SCORES')
OUTPUT_PATH = join(BASE_PATH, 'Classification')
if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

writer = ResultsWriter(filepath=join(OUTPUT_PATH, 'results_file.txt'), attach=False)

## Read Y-scores

##### Read y-scores #####
DataLoader_train = data_reader.DataLoader_Longitudinal_semisupervised()
DataLoader_test = data_reader.DataLoader_Longitudinal_semisupervised(data_path=data_reader.DATA_PATH_D3)

subjects_dict_train = DataLoader_train.subjects_dict
subjects_dict_test = DataLoader_test.subjects_dict

release_dict_train = {rid: [e.release for e in subject.events] for rid, subject in DataLoader_train.subjects_dict.items()}

rid_events_dict_train= {rid: [e.step for e in subject.events] for rid, subject in DataLoader_train.subjects_dict.items()}
rid_events_dict_test = {rid: [e.step for e in subject.events] for rid, subject in DataLoader_test.subjects_dict.items()}

rid_dt_dict_train= {rid: [e.viscode for e in subject.events] for rid, subject in DataLoader_train.subjects_dict.items()}
rid_dt_dict_test = {rid: [e.viscode for e in subject.events] for rid, subject in DataLoader_test.subjects_dict.items()}


overlapping_subjects = 0
overlapping_dict = {}
for rid, event_list in rid_dt_dict_test.items():
    if rid in list(rid_events_dict_train.keys()):
        overlapping_subjects += 1
        overlapping_dict[rid] = {'Train': rid_dt_dict_train[rid], 'Test': rid_dt_dict_test[rid]}

for rid, train_test_dict in overlapping_dict.items():
    print(rid + ': ' + 'Train: ' + str(train_test_dict['Train']) + ', ' + 'Test: ' + str(train_test_dict['Test']) )

print('Total: ' + str(overlapping_subjects))
eleven = 0
ten = 0
ten_eleven = 0
one = 0
zero = 0
others = 0
overlapped_eleven = 0
overlapped_ten = 0
overlapped_ten_eleven = 0
overlapped_one = 0
overlapped_zero = 0
overlapped_others = 0
for rid, release in release_dict_train.items():
    if np.unique(release) == 11:
        eleven += 1
        if rid in list(overlapping_dict.keys()):
            overlapped_eleven += 1
    elif np.unique(release) == 10:
        ten += 1
        if rid in list(overlapping_dict.keys()):
            overlapped_ten += 1
    elif 10 in release and 11 in release:
        ten_eleven +=1
        if rid in list(overlapping_dict.keys()):
            overlapped_ten_eleven += 1
    elif np.unique(release) == 0:
        zero += 1
        if rid in list(overlapping_dict.keys()):
            overlapped_zero += 1
    elif np.unique(release) == 1:
        print(rid + ': ' + str(release))
        one += 1
        if rid in list(overlapping_dict.keys()):
            overlapped_one += 1
    else:
        others += 1
        if rid in list(overlapping_dict.keys()):
            overlapped_others += 1

        # print(rid + ': ' + str(release))

print('TEN: ' + str(ten))
print('  --> Overlapped: ' + str(overlapped_ten))
print('ELEVEN: ' + str(eleven))
print('  --> Overlapped: ' + str(overlapped_eleven))
print('TEN_ELEVEN: ' + str(ten_eleven))
print('  --> Overlapped: ' + str(overlapped_ten_eleven))
print('ZER0: ' + str(zero))
print('  --> Overlapped: ' + str(overlapped_zero))
print('ONE: ' + str(one))
print('  --> Overlapped: ' + str(overlapped_one))
print('OTHERS: ' + str(others))
print('  --> Overlapped: ' + str(overlapped_others))

