import csv
import warnings
from os.path import join
from src.utils.io_utils import ResultsWriter
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold, StratifiedShuffleSplit
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score, roc_auc_score, \
    precision_recall_curve, roc_curve, confusion_matrix
from sklearn.exceptions import UndefinedMetricWarning

warnings.simplefilter(action='ignore', category=UndefinedMetricWarning)


def get_float(value):
    try:
        return float(value)
    except:
        return None

INPUT_PATH = '/work/acasamitjana/Multimodal_autoencoders/Multiple_events/AAE_multiview_semisupervised/nc_16/lineal/av45_vMRI_cMRI_clinical/Y_SCORES'
writer = ResultsWriter(filepath=join(INPUT_PATH, 'classification.txt'), attach=False)


##### Read y-scores #####
DataLoader = data_reader.DataLoader_Longitudinal_semisupervised()

rid_events_dict = {rid: [e.step for e in subject.events] for rid, subject in DataLoader.subjects_dict.items()}
rid_events_list = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list]

modality_list = ['av45', 'vMRI', 'cMRI','clinical']
modality_dict_list = []
for modality in modality_list:
    modality_dict = {}
    with open(join(INPUT_PATH,modality + '.csv'), 'r') as csvfile:
        csvreader = csv.DictReader(csvfile)
        header = csvreader.fieldnames
        M = len(header) - 1
        for row in csvreader:
            if row['RID'] in modality_dict.keys():
                modality_dict[row['RID']][row['event']] = [get_float(row[h]) for h in header[1:]]
            else:
                modality_dict[row['RID']] = {row['event']: [get_float(row[h]) for h in header[1:]]}

    modality_dict_list.append(modality_dict)


##### Compute mean and variance layers #####
rid_event_exclude = []
mean_dict = {}
var_dict = {}

mean_array = np.zeros((len(rid_events_list), M))
var_array = np.zeros((len(rid_events_list), M))
for it_rid, (rid,e_step) in enumerate(rid_events_list):
    if rid not in mean_dict.keys():
        mean_dict[rid] = {}
    if rid not in var_dict.keys():
        mean_dict[rid] = {}

    n_sbj = [np.sum([1 for mdl in modality_dict_list if mdl[rid][e_step][m] is not None]) for m in range(M)]
    if 0 in n_sbj:
        mean_dict[rid][e_step] = [0 for m in range(M)]
        var_dict[rid][e_step] = [0 for m in range(M)]

    else:
        mean_dict[rid][e_step] = [np.sum([mdl[rid][e_step][m] for mdl in modality_dict_list if mdl[rid][e_step][m] is not None])/n_sbj[m] for m in range(M)]
        var_dict[rid][e_step] = [np.sum([(mdl[rid][e_step][m] - mean_dict[rid][e_step][m]) ** 2 for mdl in modality_dict_list if mdl[rid][e_step][m] is not None]) / n_sbj[m] for m in range(M)]

