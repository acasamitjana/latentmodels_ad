import networkx as nx

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import normalize_sparse, sparse_mx_to_torch_sparse_tensor, encode_onehot, sparse_to_tuple
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT
from src.dataset import MyDataset, MyDataset_structure
from src.models import Encoder, Decoder, Discriminator, Autoencoder, Encoder_lineal, Decoder_lineal
from src.losses import graph_smoothness_loss
import os
import numpy as np
from os.path import join, exists
import itertools

import scipy.sparse as sp
from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################

N_COMPONENTS = 5
EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'Multiple_events','AAE_singleview', 'nc_' + str(N_COMPONENTS))

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, graph_matrix, logger, **kwargs):

    for model_name, model in model_dict.items():
        model.train()

    for batch_idx, (data,target, index_batch) in enumerate(generator_train):

        index_batch = index_batch.tolist()
        sim_matrix = graph_matrix[index_batch][:,index_batch]
        sim_matrix = torch.Tensor(sim_matrix)
        data, target, sim_matrix = data.to(device), target.to(device), sim_matrix.to(device)  # sento to either gpu or cpu
        for model_name, model in model_dict.items():
            model.zero_grad()

        x_sample, z_sample = model_dict['autoencoder'](data)  # encode to z
        rec_loss = F.mse_loss(x_sample, data)
        graph_loss = graph_smoothness_loss(z_sample, sim_matrix=sim_matrix)

        AAE_loss = rec_loss + graph_loss
        AAE_loss.backward()
        optimizer_dict['autoencoder'].step()

        # Discriminator
        ## true prior is random normal (randn)
        ## this is constraining the Z-projection to be normal!
        model_dict['autoencoder'].eval()
        z_real_gauss = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
        D_real_gauss = model_dict['discriminator'](z_real_gauss)

        z_fake_gauss = model_dict['generator'](data)
        D_fake_gauss = model_dict['discriminator'](z_fake_gauss)

        D_loss = -torch.mean(torch.log(D_real_gauss + EPS) + torch.log(1 - D_fake_gauss + EPS))

        D_loss.backward()
        optimizer_dict['discriminator'].step()

        # Generator
        model_dict['generator'].train()
        z_fake_gauss =  model_dict['generator'](data)
        D_fake_gauss =  model_dict['discriminator'](z_fake_gauss)

        G_loss = -torch.mean(torch.log(D_fake_gauss + EPS))

        G_loss.backward()
        optimizer_dict['generator'].step()

        if batch_idx % kwargs['log_interval'] == 0: #Logging
            log_dict = {'reconstruction_loss': rec_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'graph_loss': graph_loss.item(),
                        'loss': AAE_loss.item()+D_loss.item()+G_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data)) + '/' + str(len(generator_train.dataset)) + ') ' + ','.join([k +': ' + str(v) for k,v in log_dict.items()])
            print(to_print)
    return logger

def test(model_dict, device, generator_test, epoch, latent_space_dims, logger, save_file_flag=True):
    for model_name, model in model_dict.items():
        model.eval()

    with torch.no_grad():
        for batch_idx, (data, _, _) in enumerate(generator_test):
            data = data.to(device)

            # Autoencoder
            recon_batch, latent_batch = model_dict['autoencoder'](data)
            AAE_loss = F.mse_loss(recon_batch, data)

            # Discriminator
            latent_gauss_batch = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
            D_real = model_dict['discriminator'](latent_batch)
            D_gauss = model_dict['discriminator'](latent_gauss_batch)
            D_loss = -torch.mean(torch.log(D_gauss + EPS) + torch.log(1 - D_real + EPS))

            # Generator
            G_loss = -torch.mean(torch.log(D_real + EPS))

            log_dict = {'reconstruction_loss': AAE_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': AAE_loss.item() + D_loss.item() + G_loss.item()}

            logger.on_step_fi(log_dict)

    to_print = '====> Test epoch: ' + str(epoch) +' '+ ','.join([k + ': ' + str(np.mean(v)) for k, v in log_dict.items()])
    print(to_print)
    return logger

def predict(model, generator_test, prediction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(prediction_size)
    labels = torch.zeros(num_elements,1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            pred = model(data)

            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()

    return predictions.detach().numpy(), labels.detach().numpy()

################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    DataLoader = data_reader.DataLoader_Longitudinal_semisupervised()

    ## Dictionaries
    av45_dict, av45_labels_dict = DataLoader.load_modality(modality_name='av45_pet')
    vMRI_dict, vMRI_labels_dict = DataLoader.load_modality(modality_name='volumetric_mri')
    cMRI_dict, cMRI_labels_dict = DataLoader.load_modality(modality_name='cortical_thickness_mri')
    FDG_dict, FDG_labels_dict = DataLoader.load_modality(modality_name='fdg_pet')
    csf_dict, csf_labels_dict = DataLoader.load_modality(modality_name='csf')
    imaging_phenotypes_dict, imaging_phenotypes_labels_dict = DataLoader.load_modality(modality_name='imaging_phenotypes')
    education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
    age_dict, age_labels_dict = DataLoader.load_age()
    clinical_dict, clinical_labels_dict = DataLoader.load_clinical(clinical_names=clinical_names)
    dx_dict, dx_labels_dict = DataLoader.load_dx()

    rid_events_dict = {rid: [e.step for e in subject.events] for rid, subject in DataLoader.subjects_dict.items()}
    rid_events_list = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list]

    similarity_matrix = np.zeros((len(rid_events_list), len(rid_events_list)))
    n_rows = 0
    for k, v in rid_events_dict.items():
        if len(v) == 1:
            ones_matrix = 1
        else:
            ones_matrix = 1/(len(v)-1)*np.ones((len(v), len(v)))
        similarity_matrix[n_rows:n_rows+len(v),n_rows:n_rows+len(v)] = ones_matrix
        similarity_matrix[n_rows:n_rows + len(v), n_rows:n_rows + len(v)] -= np.eye(len(v))
        n_rows += len(v)

    similarity_matrix = similarity_matrix/len(list(rid_events_dict.keys()))

    fig, ax = plt.subplots()
    im = ax.imshow(similarity_matrix[:100,:100])
    fig.colorbar(im, orientation='vertical')
    plt.savefig(join(OUTPUT_PATH, 'similarity_matrix.png'))
    plt.close()

    G = nx.from_numpy_matrix(similarity_matrix)

    # Adjacency matrix
    adj = nx.adj_matrix(G)
    adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)  # symmetric
    adj = normalize_sparse(adj + sp.eye(adj.shape[0]))  # add noise
    n_nodes = adj.shape[0]
    adjacency_matrix = sparse_mx_to_torch_sparse_tensor(adj)



    fig, ax = plt.subplots()
    im = ax.imshow(adjacency_matrix.to_dense().numpy()[:100,:100])
    fig.colorbar(im, orientation='vertical')
    plt.savefig(join(OUTPUT_PATH, 'adjacency_matrix.png'))
    plt.close()

    test_dict = {
    rid: {e_step: [age_dict[rid][e_step]] + clinical_dict[rid][e_step] + education_dict[rid][e_step] for e_step in
          e_step_list} for rid, e_step_list in rid_events_dict.items()}
    test_labels_dict = {
    rid: {e_step: age_labels_dict[rid][e_step] * clinical_labels_dict[rid][e_step] * education_labels_dict[rid][e_step]
          for e_step in e_step_list} for rid, e_step_list in rid_events_dict.items()}

    ## Codenames
    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    clinical_id_list = clinical_names
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    test_id_list = ['age'] + clinical_names + ['education']

    ## Modalities used
    modality_list = ['vMRI']
    modality_dict_list = [vMRI_dict]
    modality_labels_dict_list = [vMRI_labels_dict]
    modality_id_list = [vMRI_id_list]

    age_array = np.asarray([age_dict[rid][e_step] for (rid, e_step) in rid_events_list])
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)
    age_labels_array = np.asarray([age_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list])

    modality_array = np.asarray([modality_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list])
    if len(modality_array.shape) < 2:
        m1_array = modality_array.reshape(-1, 1)
    modality_labels_array = np.asarray([modality_labels_dict_list[0][rid][e_step] for (rid, e_step) in rid_events_list])

    dx_array = np.asarray([dx_dict[rid][e_step] for (rid, e_step) in rid_events_list])
    if len(dx_array.shape) < 2:
        dx_array = dx_array.reshape(-1, 1)
    dx_labels_array = np.asarray([dx_labels_dict[rid][e_step] for (rid, e_step) in rid_events_list])

    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    ## PRE-PROCESSING

    std_scaler = StandardScaler()
    modality_array[np.where(modality_labels_array == 1)] = std_scaler.fit_transform(modality_array[np.where(modality_labels_array == 1)])

    age_array[np.where(age_labels_array == 1)] = std_scaler.fit_transform(age_array[np.where(age_labels_array == 1)])

    N1, M1 = modality_array.shape
    N = [N1]
    M = [M1]

    print('N1: ' + str(N1) + ', M1: ' + str(M1))

    rid_events_list_all_modalities = [(rid, e_step) for rid, e_step_list in rid_events_dict.items() for e_step in e_step_list if modality_labels_dict_list[0][rid][e_step] == 1]


    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_singleview.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    input_dim = M[0]
    hidden_layers_dim = [N_COMPONENTS]
    hidden_layers_discriminator_dim = [10, 3]
    latent_dim = hidden_layers_dim[-1]

    EPS = 1e-15

    Generator_model = Encoder_lineal(input_dim, hidden_layers_dim)
    Decoder_model = Decoder_lineal(input_dim, hidden_layers_dim)
    AAE_model = Autoencoder(Generator_model, Decoder_model)
    Discriminator_model = Discriminator(latent_dim, hidden_layers_discriminator_dim)

    # Set learning rates
    ae_lr = learning_rate['autoencoder']
    gen_lr = learning_rate['generator']
    reg_lr = learning_rate['discriminator']

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    # encode/decode optimizers
    optim_AAE = torch.optim.Adam(AAE_model.parameters(), lr=ae_lr)
    optim_G = torch.optim.Adam(Generator_model.parameters(), lr=gen_lr)
    optim_D = torch.optim.Adam(Discriminator_model.parameters(), lr=reg_lr)

    model_dict = {'autoencoder': AAE_model, 'discriminator': Discriminator_model, 'generator': Generator_model}
    optimizer_dict = {'autoencoder': optim_AAE, 'discriminator': optim_D, 'generator': optim_G}

    #################################
    ##########  READ DATA  ##########
    #################################

    dataset_train = MyDataset_structure(modality_array, age_array, similarity_matrix)
    generator_train = torch.utils.data.DataLoader(
        dataset_train,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2,
        pin_memory=torch.cuda.is_available()

    )

    dataset_test = MyDataset(modality_array, age_array)
    generator_test = torch.utils.data.DataLoader(
        dataset_test,
        batch_size=modality_array.shape[0],
        shuffle=True,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    ################################
    ##########  TRAINING  ##########
    ################################
    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    writer = ResultsWriter(filepath=join(MODALITY_PATH, 'results_file.txt'))

    log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'graph_loss','loss']
    logger_train = History()
    logger_train.on_train_init(log_keys)

    logger_test = History()
    logger_test.on_train_init(log_keys)

    for epoch in range(1, n_epochs + 1):
        logger_train = train(model_dict, optimizer_dict, device, generator_train, epoch, latent_dim, similarity_matrix, logger_train, **kwargs_training)
        # logger_test = test(model_dict, device, generator_test, epoch, latent_dim, logger_test, save_file_flag=True)

    ###############################
    ##########  RESULTS  ##########
    ###############################
    fig, ax1 = plt.subplots()

    ax2 = ax1.twinx()
    ax1.plot(logger_train.logs['reconstruction_loss'], 'b', label='Reconstruction')
    ax2.plot(logger_train.logs['discriminator_loss'], 'r', label='Discriminator')
    ax2.plot(logger_train.logs['generator_loss'], 'g', label='Generator')
    plt.plot(logger_train.logs['graph_loss'], 'c', label='Graph')
    ax1.plot(logger_train.logs['loss'], 'k', label='Total loss')

    plt.legend()
    plt.savefig(join(MODALITY_PATH, 'training.png'))
    plt.close()

    y_scores, labels_scores = predict(model_dict['autoencoder'].encoder, generator_test, (N[0],latent_dim))
    for it_component in range(N_COMPONENTS):
        plt.figure()
        plt.hist(y_scores[:,it_component], bins= 40)
        plt.savefig(join(MODALITY_PATH, 'histogram_nc_' + str(it_component) + '.png'))
        plt.close()

    STATISTICAL_PLOTS = True
    if STATISTICAL_PLOTS:
        writer.write('#############################\n')
        writer.write('RESULTS DIAGNOSTIC COMPARISON\n')
        writer.write('#############################\n\n')
        writer.write('--- Modality subspace ---\n')

        combination_plots = list(itertools.combinations(range(3), 2))
        for it_component in range(N_COMPONENTS):
            writer.write('     · Component ' + str(it_component) + '\n')
            t = []
            p = []
            boxplot_list = [y_scores[index_0, it_component], y_scores[index_1, it_component],
                            y_scores[index_2, it_component]]
            for c in combination_plots:
                result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                t.append(result[0])
                p.append(result[1])
                writer.write('           - ' + str((label_diagnostic[c[0]], label_diagnostic[c[1]])) + ': ' + str(
                    (result[0], result[1])) + '\n')

            filename = 'modality_diagnostic' + str(it_component) + '.png'
            plt.figure()
            plt.boxplot(boxplot_list)
            plt.xticks([1, 2, 3], label_diagnostic)
            plt.title('Significant (p<0.05) differences: ' + str(
                [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
            plt.savefig(join(MODALITY_PATH, filename))
            plt.close()

    WEIGHT_PLOTS = True
    if WEIGHT_PLOTS:
        writer.write('####################\n')
        writer.write('PLS Y_WEIGHT RESULTS\n')
        writer.write('####################\n\n')

        y_weight_generator = np.eye(N_COMPONENTS)
        y_weight_generator_labels = np.arange(0,N_COMPONENTS).reshape(-1,1)
        dataset_y_weight = MyDataset(y_weight_generator, y_weight_generator_labels)
        generator_y_weight = torch.utils.data.DataLoader(
            dataset_y_weight,
            batch_size=N_COMPONENTS,
            shuffle=True,
            num_workers=1,
            pin_memory=torch.cuda.is_available()

        )

        y_rotations, y_rotations_label = predict(model_dict['autoencoder'].decoder, generator_y_weight, (N_COMPONENTS,M[0]))
        for it_component in range(N_COMPONENTS):
            sorted_mod = np.argsort(np.abs(y_rotations[it_component,:]))[::-1]
            writer.write('    · Component ' + str(it_component) + '\n')
            for m_prs in sorted_mod:
                writer.write('         ' + modality_id_list[0][m_prs] + ': ' + str(y_rotations[it_component,m_prs]) + '\n')
        writer.write('\n')

        y_min = np.min(y_rotations) * 0.9
        y_max = np.max(y_rotations) * 1.1
        y_lim = np.max([np.abs(y_min), np.abs(y_max)])

        for it_component in range(N_COMPONENTS):
            filename = 'y_weights_' + str(it_component) + '.png'
            x_min = np.min(y_rotations[it_component,:])
            x_max = np.max(y_rotations[it_component,:])
            _, ax = plt.subplots()
            ax.barh(np.arange(M[0]), y_rotations[it_component, :], align='center', color='green', ecolor='black')
            ax.set_yticks(np.arange(start=0, stop=M[0], step=np.max([1, int(M[0] / 10)])))
            ax.set_xticks(np.concatenate((np.arange(start=-y_lim, stop=0, step=y_lim / 3),
                                          np.arange(start=0, stop=y_lim, step=y_lim / 3), [y_lim])))
            ax.invert_yaxis()
            ax.set_xlabel('Latent variable ' + str(it_component))
            ax.set_title('Relative weight of each input variable')
            ax.grid()
            ax.set_xlim([-y_lim, y_lim])
            plt.savefig(join(MODALITY_PATH, filename))
            plt.close()

    FACTOR_PLOTS = True
    if FACTOR_PLOTS:
        n_rows = ROW_DICT[N_COMPONENTS]
        n_cols = int(np.ceil(N_COMPONENTS / n_rows))

        # wmh_dict = {k: v['bl'] for k, v in adni_reader.load_WMH().items() if 'bl' in list(v.keys())}
        education_dict, education_labels_dict = DataLoader.load_demographic(demo_names=['education'])
        gender_dict, gender_labels_dict = DataLoader.load_demographic(demo_names=['gender'])
        apoe4_dict, apoe4_labels_dict = DataLoader.load_demographic(demo_names=['apoe4'])
        ab_status_dict, ab_status_labels_dict = DataLoader.load_amyloid_status()
        mmse_dict, mmse_labels_dict = DataLoader.load_clinical(clinical_names=['mmse'])
        cdrsb_dict, cdrsb_labels_dict = DataLoader.load_clinical(clinical_names=['cdrsb'])

        writer_factor = ResultsWriter(filepath=join(MODALITY_PATH, 'factor_analysis.txt'), attach=False)
        writer_factor.write('###########################\n')
        writer_factor.write('FACTOR CORRELATION ANALYSIS\n')
        writer_factor.write('###########################\n')

        factor_dict_list = [age_dict, education_dict, mmse_dict, cdrsb_dict]
        factor_labels_dict_list = [age_labels_dict, education_labels_dict, mmse_labels_dict, cdrsb_labels_dict]
        factor_name_list = ['age', 'education', 'mmse', 'cdrsb']
        for factor_dict, factor_labels_dict, factor_name in zip(factor_dict_list, factor_labels_dict_list, factor_name_list):

            print(factor_name)
            writer.write('###########################\n')
            writer.write('FACTOR CORRELATION ANALYSIS\n')
            writer.write('###########################\n')

            fig_y, axarr_y = plt.subplots(n_rows, n_cols)
            writer.write('\nFACTOR: ' + factor_name + '\n')

            rid_events_list_factor = [(rid, e_step) for (rid, e_step) in rid_events_list if factor_labels_dict[rid][e_step]==1]
            rid_events_list_factor_modality = [(rid, e_step) for it_re, (rid, e_step) in enumerate(rid_events_list_factor) if modality_labels_array[it_re] == 1]

            factor_array = np.asarray([factor_dict[rid][e_step] for (rid, e_step) in rid_events_list_factor_modality]).reshape((-1,))

            indices_rid = [it_re for it_re, (rid, e_step) in enumerate(rid_events_list) if (rid, e_step) in rid_events_list_factor_modality]
            y_scores_factor = y_scores[indices_rid]
            dx_array_factor = dx_array[indices_rid]
            index_0 = np.where(dx_array_factor == 0)[0]
            index_1 = np.where(dx_array_factor == 1)[0]
            index_2 = np.where(dx_array_factor == 2)[0]

            for it_c in range(N_COMPONENTS):
                writer.write('    · Component ' + str(it_c) + ':')
                writer.write(' Y:' + str(pearsonr(y_scores_factor[:, it_c], factor_array)) + '\n')


                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_0, it_c],
                                                                          factor_array[index_0], '*b',
                                                                          label='NC')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_1, it_c],
                                                                          factor_array[index_1], '*r',
                                                                          label='MCI')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores_factor[index_2, it_c],
                                                                          factor_array[index_2], '*k',
                                                                          label='AD')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor_name)

            plt.figure(1)
            plt.savefig(join(MODALITY_PATH, 'y_score_' + factor_name + '_correlation.png'))
            plt.close()