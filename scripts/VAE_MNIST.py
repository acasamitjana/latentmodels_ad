import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix
import numpy as np

from Data.TADPOLE.tadpole_reader import *
from Data.TADPOLE import data_reader
from src.utils.io_utils import ResultsWriter, ROW_DICT, ConfigFile
from src.models import VAE

from matplotlib import pyplot as plt
import os
from os.path import exists, join
import itertools

plt.switch_backend('Agg')

N_COMPONENTS = 16

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'VAE_MNIST')
RESULTS_PATH = join(OUTPUT_PATH, 'results')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

if not exists(RESULTS_PATH):
    os.makedirs(RESULTS_PATH)


def loss_function(recon_x, x, mu, logvar):
    BCE = F.binary_cross_entropy(recon_x, x.view(-1, 784), reduction='sum')

    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

    return BCE + KLD


def train(model, generator_train, epoch, *args, **kwargs):
    model.train()
    train_loss = 0
    for batch_idx, (data, _) in enumerate(generator_train):
        data = data.to(device)
        optimizer.zero_grad()
        recon_batch, mu, logvar = model(data)
        loss = loss_function(recon_batch, data, mu, logvar)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()
        if batch_idx % kwargs['log_interval'] == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(generator_train.dataset),
                100. * batch_idx / len(generator_train),
                loss.item() / len(data)))

    print('====> Epoch: {} Average loss: {:.4f}'.format(
          epoch, train_loss / len(generator_train.dataset)))


def test(model, generator_test, epoch, save_file_flag=True):
    model.eval()
    batch_size = generator_test.batch_size

    test_loss = 0
    with torch.no_grad():
        for i, (data, _) in enumerate(generator_test):
            data = data.to(device)
            recon_batch, mu, logvar = model(data)
            test_loss += loss_function(recon_batch, data, mu, logvar).item()
            if i == 0 and save_file_flag:
                n = min(data.size(0), 8)
                comparison = torch.cat([data[:n], recon_batch.view(batch_size, 1, 28, 28)[:n]])
                save_image(comparison.cpu(),join(RESULTS_PATH, 'reconstruction_' + str(epoch) + '.png'), nrow=n)

    test_loss /= len(generator_test.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))

if __name__ == '__main__':


    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_multiview_MNIST.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    torch.manual_seed(42)
    if use_cuda:
        torch.cuda.manual_seed(42)

    input_dim = 784
    hidden_layer_dims = [400, 20]
    model = VAE(input_dim=input_dim, hidden_layer_dims=hidden_layer_dims).to(device)
    optimizer = optim.Adam(model.parameters(), lr=1e-3)



    #################################
    ##########  READ DATA  ##########
    #################################

    generator_train = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, transform=transforms.Compose([
            transforms.ToTensor(),
            # transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=batch_size, shuffle=False, **kwargs_generator
    )

    generator_test = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.ToTensor(),
            # transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=1, shuffle=False, **kwargs_generator
    )


    ################################
    ##########  TRAINING  ##########
    ################################
    for epoch in range(1, n_epochs + 1):
        train(model, generator_train, epoch, **kwargs_training)
        test(model, generator_train, epoch)
        with torch.no_grad():
            sample = torch.randn(64, 20).to(device)
            sample = model.decode(sample).cpu()
            save_image(sample.view(64, 1, 28, 28),join(RESULTS_PATH, 'sample_' + str(epoch) + '.png'))

    ###############################
    ##########  RESULTS  ##########
    ###############################