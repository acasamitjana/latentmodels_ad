import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT
from src.dataset import MyDataset
from src.models import Encoder, Decoder, Discriminator

import os
import numpy as np
from os.path import join, exists
import itertools

from sklearn.manifold import TSNE
from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################

EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'MNIST', 'AAE_multiview_MNIST')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
def train(model_dict, optimizer_dict, device, generator_train_1, generator_train_2, epoch, latent_space_dims, logger, **kwargs):

    for view_name, view_dict in model_dict.items():
        if isinstance(view_dict, dict):
            for model_name, model in view_dict.items():
                model.train(mode=True)
        else:
            view_dict.train(mode=True)

    for batch_idx, ((data_1,target_2), (data_2,target_2)) in enumerate(zip(generator_train_1, generator_train_2)):

        data_1 = data_1.reshape(-1,784)
        data_2 = data_2.reshape(-1,784)

        data_1, data_2 = data_1.to(device), data_2.to(device)  # sento to either gpu or cpu
        for view_name, view_dict in model_dict.items():
            if isinstance(view_dict, dict):
                for model_name, model in view_dict.items():
                    model.zero_grad()
            else:
                view_dict.zero_grad()

        #### RECONSTRUCTION LOSS
        z_sample_1 = model_dict['multiview_1']['encoder'](data_1)
        z_sample_2 = model_dict['multiview_2']['encoder'](data_2)  #encode to z

        x_reconstructed_1_1 = model_dict['multiview_1']['decoder'](z_sample_1)  #decode to X reconstruction
        x_reconstructed_1_2 = model_dict['multiview_1']['decoder'](z_sample_2)  #decode to X reconstruction
        x_reconstructed_2_1 = model_dict['multiview_2']['decoder'](z_sample_1)  #decode to X reconstruction
        x_reconstructed_2_2 = model_dict['multiview_2']['decoder'](z_sample_2)  #decode to X reconstruction

        reconstruction_loss_1 = F.mse_loss(x_reconstructed_1_1, data_1) + F.mse_loss(x_reconstructed_1_2, data_1)
        reconstruction_loss_2 = F.mse_loss(x_reconstructed_2_1, data_2) + F.mse_loss(x_reconstructed_2_2, data_2)

        reconstruction_loss_1 *= model_dict['multiview_1']['encoder'].hidden_dims[-1]
        reconstruction_loss_2 *= model_dict['multiview_2']['encoder'].hidden_dims[-1]
        reconstruction_loss = reconstruction_loss_1 + reconstruction_loss_2

        reconstruction_loss.backward()
        optimizer_dict['decoder'].step()
        optimizer_dict['encoder'].step()

        #### DISCRIMINATOR LOSS
        model_dict['multiview_1']['encoder'].train(mode=False)
        model_dict['multiview_2']['encoder'].train(mode=False)

        z_real_gauss = Variable(torch.randn(data_1.size()[0], latent_space_dims) * 5.)
        z_sample_1 = model_dict['multiview_1']['encoder'](data_1)
        z_sample_2 = model_dict['multiview_2']['encoder'](data_2)

        d_real = model_dict['discriminator'](z_real_gauss)
        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2= model_dict['discriminator'](z_sample_2)

        label_real = np.zeros((data_1.size()[0],3))
        label_real[:,0] = 1
        label_real = torch.FloatTensor(label_real)

        label_sample_1 = np.zeros((data_1.size()[0],3))
        label_sample_1[:,1] = 1
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = np.zeros((data_1.size()[0],3))
        label_sample_2[:,2] = 1
        label_sample_2 = torch.FloatTensor(label_sample_2)

        discriminator_loss = -torch.mean(label_real*torch.log(d_real + EPS) + label_sample_1*torch.log(d_sample_1 + EPS) + label_sample_2*torch.log(d_sample_2 + EPS))

        discriminator_loss.backward()
        optimizer_dict['discriminator'].step()

        #### GENERATOR LOSS
        model_dict['multiview_1']['encoder'].train()
        model_dict['multiview_2']['encoder'].train()

        z_sample_1 =  model_dict['multiview_1']['encoder'](data_1)
        z_sample_2 =  model_dict['multiview_1']['encoder'](data_2)

        d_sample_1 =  model_dict['discriminator'](z_sample_1)
        d_sample_2 =  model_dict['discriminator'](z_sample_2)

        generator_loss = -torch.mean(label_real*torch.log(d_sample_1 + EPS) + label_real*torch.log(d_sample_2 + EPS))

        generator_loss.backward()
        optimizer_dict['generator'].step()

        if batch_idx % kwargs['log_interval'] == 0: #Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item()+discriminator_loss.item()+generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(len(generator_train_1.dataset)) + ') ' + ','.join([k +': ' + str(round(v,3)) for k,v in log_dict.items()])
            print(to_print)

    return logger

def predict(model, generator_test, prediction_size):

    model.eval() #Initialize self.training = False

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(num_elements,prediction_size)
    labels = torch.zeros(num_elements)
    for i, (data,target) in enumerate(generator_test):
        data = data.reshape(-1,784)
        pred = model(data)

        start = i * batch_size
        end = start + batch_size
        if i == num_batches - 1:
            end = num_elements
        predictions[start:end] = pred.cpu()
        labels[start:end] = target.cpu()


    return predictions.detach().numpy(), labels.detach().numpy()

def generate(model, generator_test, latent_dim):
    model.eval()  # Initialize self.training = False
    z_gauss = Variable(torch.randn(1, latent_dim) * 5.)
    pred = model(z_gauss)

    return pred.detach().numpy()


################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_multiview_MNIST.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    torch.manual_seed(42)
    if use_cuda:
        torch.cuda.manual_seed(42)
    ######################################
    ##########   READ DATA  ##############
    ######################################
    torch.manual_seed(42)
    if use_cuda:
        torch.cuda.manual_seed(42)
    generator_train_1 = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True, transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=batch_size, shuffle=False, **kwargs_generator
    )

    generator_train_2 = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, transform=transforms.Compose([
            transforms.RandomRotation((180,180)),
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=batch_size, shuffle=False, **kwargs_generator
    )

    generator_test_1 = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=1, shuffle=False, **kwargs_generator
    )

    generator_test_2 = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.RandomRotation((180,180)),
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=1, shuffle=False, **kwargs_generator
    )

    generator_test_latent_1 = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=batch_size, shuffle=False, **kwargs_generator
    )

    generator_test_latent_2 = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, transform=transforms.Compose([
            transforms.RandomRotation((180, 180)),
            transforms.ToTensor(),
            transforms.Normalize((0.1307,), (0.3081,))
        ])),
        batch_size=batch_size, shuffle=False, **kwargs_generator
    )

    torch.manual_seed(42)
    for batch_idx, ((data_1,target_1), (data_2,target_2)) in enumerate(zip(generator_train_1, generator_train_2)):
        print(target_1)
        print(target_2)
        exit()


    M = 784
    N_COMPONENTS = 10

    OUTPUT_PATH = join(OUTPUT_PATH, 'nc_' + str(N_COMPONENTS))
    MODALITY_PATH = join(OUTPUT_PATH, 'Rotation')

    if not exists(OUTPUT_PATH):
        os.makedirs(OUTPUT_PATH)

    ########################################
    ##########   EXPERIMENTS  ##############
    ########################################
    hidden_layer_list = [1000, 500, 100, N_COMPONENTS]
    discriminator_layer_list = [10, 5, 1]

    encoder_model_1 = Encoder(input_dim=M, hidden_layer_dims=hidden_layer_list).to(device)
    decoder_model_1 = Decoder(input_dim=M, hidden_layer_dims=hidden_layer_list, activation='sigmoid').to(device)

    encoder_model_2 = Encoder(input_dim=M, hidden_layer_dims=hidden_layer_list).to(device)
    decoder_model_2 = Decoder(input_dim=M, hidden_layer_dims=hidden_layer_list, activation='sigmoid').to(device)

    discriminator_model = Discriminator(input_dim=N_COMPONENTS, hidden_layer=discriminator_layer_list).to(device)

    # Set learning rates
    autoencoder_lr = learning_rate['autoencoder']
    discriminator_lr = learning_rate['discriminator']
    generator_lr = learning_rate['generator']

    # encode/decode optimizers
    decoder_optimizer_1 = torch.optim.Adam(nn.ParameterList(decoder_model_1.parameters()), lr=autoencoder_lr)
    encoder_optimizer_1 = torch.optim.Adam(nn.ParameterList(encoder_model_1.parameters()), lr=autoencoder_lr)
    decoder_optimizer_2 = torch.optim.Adam(nn.ParameterList(decoder_model_2.parameters()), lr=autoencoder_lr)
    encoder_optimizer_2 = torch.optim.Adam(nn.ParameterList(encoder_model_2.parameters()), lr=autoencoder_lr)

    # regularizing optimizers
    generator_optimizer = torch.optim.Adam(nn.ParameterList(encoder_model_1.parameters()), lr=generator_lr)
    discriminator_optimizer = torch.optim.Adam(nn.ParameterList(discriminator_model.parameters()), lr=discriminator_lr)

    model_dict = {'multiview_1': {'encoder': encoder_model_1, 'decoder': decoder_model_1},
                  'multiview_2': {'encoder': encoder_model_2, 'decoder': decoder_model_2},
                  'discriminator': discriminator_model}

    optimizer_dict = {'encoder': encoder_optimizer_1, 'decoder': decoder_optimizer_1, 'discriminator': discriminator_optimizer, 'generator': generator_optimizer}


    # Train: epochs

    log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
    logger = History()
    logger.on_train_init(log_keys)
    for epoch in range(1, n_epochs + 1):
        logger = train(model_dict, optimizer_dict, device, generator_train_1, generator_train_2, epoch, N_COMPONENTS, logger, **kwargs_training)

    writer = ResultsWriter(filepath=join(MODALITY_PATH, 'results_file.txt'))

    plt.figure()
    plt.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
    plt.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
    plt.plot(logger.logs['generator_loss'], 'k', label='Generator')
    plt.legend()
    plt.savefig(join(OUTPUT_PATH,'training.png'))
    plt.close()


    ### Multi-view 1
    data,target = next(iter(generator_test_1))
    encoded_1 = model_dict['multiview_1']['encoder'](data.reshape(1,784))
    encoded_1 = torch.FloatTensor(encoded_1)
    decoded_1 = model_dict['multiview_1']['decoder'](encoded_1).detach().numpy().reshape(28,28)

    fig, axarr = plt.subplots(1, 2)
    axarr[0].imshow(data.detach().numpy().reshape(28,28))
    axarr[1].imshow(decoded_1)
    plt.savefig(join(OUTPUT_PATH, 'VIEW1.png'))
    plt.close()

    ### Multi-view 1
    data, target = next(iter(generator_test_2))
    encoded_2 = model_dict['multiview_2']['encoder'](data.reshape(1,784))
    encoded_2 = torch.FloatTensor(encoded_2)
    decoded_2 = model_dict['multiview_2']['decoder'](encoded_2).detach().numpy().reshape(28,28)

    fig, axarr = plt.subplots(1, 2)
    axarr[0].imshow(data.detach().numpy().reshape(28,28))
    axarr[1].imshow(decoded_2)
    plt.savefig(join(OUTPUT_PATH, 'VIEW2.png'))
    plt.close()


    #Latent space
    data, target = predict(model_dict['multiview_1']['encoder'], generator_test_latent_1, N_COMPONENTS)
    tsne_model = TSNE(n_components=2)
    projections = tsne_model.fit_transform(data)

    color_code = ['b','r','c','m','y','g','k','orange', 'pink', 'gray']
    plt.figure()
    for i in range(10):
        plt.plot(projections[np.where(target==i)], color=color_code[i], label = str(i), linestyle='None', marker = '*')

    plt.legend()
    plt.savefig(join(OUTPUT_PATH, 'latent_space.png'))
    plt.close()





