import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import datasets, transforms
from torchvision.utils import save_image

from torch.autograd import Variable

from src.utils.preprocessing import to_np, to_var
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.models import Autoencoder, Discriminator, Encoder, Decoder

from matplotlib import pyplot as plt
from os.path import join, exists
import os

plt.switch_backend('Agg')

OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'AAE_MNIST')
RESULTS_PATH = join(OUTPUT_PATH, 'results' + '_slow_dx')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

if not exists(RESULTS_PATH):
    os.makedirs(RESULTS_PATH)

# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):

    for model_name, model in model_dict.items():
        model.train()
    for batch_idx, (data,target) in enumerate(generator_train):
        data = data.reshape(-1,784)
        data, target = data.to(device), target.to(device)  # sento to either gpu or cpu
        for model_name, model in model_dict.items():
            model.zero_grad()

        x_sample, z_sample = model_dict['autoencoder']([data, target])  # encode to z
        AAE_loss = F.binary_cross_entropy(x_sample + EPS, data + EPS)

        AAE_loss.backward()
        optimizer_dict['autoencoder'].step()

        # Discriminator
        ## true prior is random normal (randn)
        ## this is constraining the Z-projection to be normal!
        model_dict['autoencoder'].eval()
        z_real_gauss = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
        D_real_gauss = model_dict['discriminator'](z_real_gauss)

        z_fake_gauss = model_dict['generator'](data)
        D_fake_gauss = model_dict['discriminator'](z_fake_gauss)

        D_loss = -torch.mean(torch.log(D_real_gauss + EPS) + torch.log(1 - D_fake_gauss + EPS))

        D_loss.backward()
        optimizer_dict['discriminator'].step()

        # Generator
        model_dict['generator'].train()
        z_fake_gauss =  model_dict['generator'](data)
        D_fake_gauss =  model_dict['discriminator'](z_fake_gauss)

        G_loss = -torch.mean(torch.log(D_fake_gauss + EPS))

        G_loss.backward()
        optimizer_dict['generator'].step()

        if batch_idx % kwargs['log_interval'] == 0: #Logging
            log_dict = {'reconstruction_loss': AAE_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': AAE_loss.item()+D_loss.item()+G_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data)) + '/' + str(len(generator_train.dataset)) + ') ' + ','.join([k +': ' + str(v) for k,v in log_dict.items()])
            print(to_print)
    return logger

def test(model_dict, device, generator_train, epoch, latent_space_dims, logger, save_file_flag=True):
    for model_name, model in model_dict.items():
        model.eval()

    batch_size = generator_test.batch_size

    test_loss = 0
    with torch.no_grad():
        for batch_idx, (data, target) in enumerate(generator_test):
            data = data.reshape(-1, 784)
            data, target = data.to(device), target.to(device)  # sento to either gpu or cpu

            # Autoencoder
            recon_batch, latent_batch = model_dict['autoencoder']([data,target])
            AAE_loss = F.binary_cross_entropy(recon_batch + EPS, data + EPS)

            # Discriminator
            latent_gauss_batch = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
            D_real = model_dict['discriminator'](latent_batch)
            D_gauss = model_dict['discriminator'](latent_gauss_batch)
            D_loss = -torch.mean(torch.log(D_gauss + EPS) + torch.log(1 - D_real + EPS))

            # Generator
            G_loss = -torch.mean(torch.log(D_real + EPS))

            log_dict = {'reconstruction_loss': AAE_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': AAE_loss.item() + D_loss.item() + G_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join([k + ': ' + str(v) for k, v in log_dict.items()])
            print(to_print)

            if batch_idx == 0 and save_file_flag:
                n = min(data.size(0), 8)
                comparison = torch.cat([data.reshape(-1, 1, 28,28)[:n], recon_batch.view(batch_size, 1, 28, 28)[:n]])
                save_image(comparison.cpu(),join(RESULTS_PATH, 'reconstruction_' + str(epoch) + '.png'), nrow=n)

    test_loss /= len(generator_test.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))
    return logger



if __name__ == '__main__':


    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/tutorial_AAE.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda =  use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    input_dim = 784
    hidden_layers = [1000, 500, 10]
    hidden_layers_decoder = [1000, 500, 11]
    latent_dim = hidden_layers[-1]
    EPS = 1e-15

    Generator_model = Encoder(input_dim, hidden_layers)
    Decoder_model = Decoder(input_dim, hidden_layers_decoder)
    AAE_model = Autoencoder(Generator_model, Decoder_model)
    Discriminator_model = Discriminator(latent_dim, [200,50,1])

    # Set learning rates
    ae_lr = learning_rate['autoencoder']
    gen_lr = learning_rate['generator']
    reg_lr = learning_rate['discriminator']

    kwargs_training={'log_interval' : config_file.log_interval} #Nmber of steps
    kwargs_testing={}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    # encode/decode optimizers
    optim_AAE = torch.optim.Adam(AAE_model.parameters(), lr=ae_lr)
    optim_G = torch.optim.Adam(Generator_model.parameters(), lr=gen_lr)
    optim_D = torch.optim.Adam(Discriminator_model.parameters(), lr=reg_lr)

    model_dict = {'autoencoder': AAE_model , 'discriminator': Discriminator_model, 'generator': Generator_model}
    optimizer_dict = {'autoencoder': optim_AAE, 'discriminator': optim_D, 'generator': optim_G}

    #################################
    ##########  READ DATA  ##########
    #################################

    generator_train = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=True, download=True, transform=transforms.Compose([
                               transforms.ToTensor(),
                               #transforms.Normalize((0.1307,), (0.3081,))
                           ])),
        batch_size=batch_size, shuffle=True, **kwargs_generator
    )

    generator_test = torch.utils.data.DataLoader(
        datasets.MNIST('../data', train=False, download=True, transform=transforms.Compose([
                               transforms.ToTensor(),
                              # transforms.Normalize((0.1307,), (0.3081,))
                           ])),
        batch_size=batch_size, shuffle=True, **kwargs_generator
    )

    ################################
    ##########  TRAINING  ##########
    ################################
    log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
    logger_train = History()
    logger_train.on_train_init(log_keys)

    logger_test = History()
    logger_test.on_train_init(log_keys)

    for epoch in range(1, n_epochs + 1):
        logger_train = train(model_dict, optimizer_dict, device, generator_train, epoch, latent_dim, logger_train, **kwargs_training)
        logger_test = test(model_dict, device, generator_test, epoch, latent_dim, logger_test, save_file_flag=True)

        with torch.no_grad():
            sample = torch.randn(64, latent_dim).to(device)
            target = torch.randint(low=0, high=10, size=(64,))
            sample = model_dict['autoencoder'].decode([sample, ]).cpu()
            save_image(sample.view(64, 1, 28, 28), join(RESULTS_PATH, 'sample_' + str(epoch) + '.png'))

        ###############################
        ##########  RESULTS  ##########
        ###############################
        plt.figure()
        plt.plot(logger_train.logs['reconstruction_loss'], 'b', label='Reconstruction')
        plt.plot(logger_train.logs['discriminator_loss'], 'r', label='Discriminator')
        plt.plot(logger_train.logs['generator_loss'], 'k', label='Generator')
        plt.legend()
        plt.savefig(join(RESULTS_PATH, 'training.png'))
        plt.close()

        plt.figure()
        plt.plot(logger_test.logs['reconstruction_loss'], 'b', label='Reconstruction')
        plt.plot(logger_test.logs['discriminator_loss'], 'r', label='Discriminator')
        plt.plot(logger_test.logs['generator_loss'], 'k', label='Generator')
        plt.legend()
        plt.savefig(join(RESULTS_PATH, 'testing.png'))
        plt.close()
