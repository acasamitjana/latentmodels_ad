import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms

from torch.autograd import Variable

from src.utils.preprocessing import to_np, to_var
from src.utils.io_utils import ConfigFile
from src.callbacks import History

from matplotlib import pyplot as plt
from os.path import join, exists
import os

plt.switch_backend('Agg')

# Encoder
class Q_net(nn.Module):
    def __init__(self, X_dim, N, z_dim):
        super(Q_net, self).__init__()
        self.lin1 = nn.Linear(X_dim, N)
        self.lin2 = nn.Linear(N, N)
        self.lin3gauss = nn.Linear(N, z_dim)

    def forward(self, x):
        x = self.lin1(x)#F.dropout(self.lin1(x), p=0.25, training=self.training)
        x = F.relu(x)
        x = self.lin2(x)#F.dropout(self.lin2(x), p=0.25, training=self.training)
        x = F.relu(x)
        xgauss = self.lin3gauss(x)
        return xgauss


# Decoder
class P_net(nn.Module):
    def __init__(self, X_dim, N, z_dim):
        super(P_net, self).__init__()
        self.lin1 = nn.Linear(z_dim, N)
        self.lin2 = nn.Linear(N, N)
        self.lin3 = nn.Linear(N, X_dim)

    def forward(self, x):
        x = self.lin1(x)#F.dropout(self.lin1(x), p=0.25, training=self.training)
        x = F.relu(x)
        x = self.lin2(x)#F.dropout(self.lin2(x), p=0.25, training=self.training)
        x = self.lin3(x)
        return torch.sigmoid(x)


# Discriminator
class D_net_gauss(nn.Module):
    def __init__(self, N, z_dim):
        super(D_net_gauss, self).__init__()
        self.lin1 = nn.Linear(z_dim, N)
        self.lin2 = nn.Linear(N, N)
        self.lin3 = nn.Linear(N, 1)

    def forward(self, x):
        x = self.lin1(x)#F.dropout(self.lin1(x), p=0.2, training=self.training)
        x = F.relu(x)
        x = self.lin2(x)#F.dropout(self.lin2(x), p=0.2, training=self.training)
        x = F.relu(x)
        return F.sigmoid(self.lin3(x))


# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, **kwargs):

    log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
    logger = History()
    logger.on_train_init(log_keys)
    for model_name, model in model_dict.items():
        model.train()
    for batch_idx, (data,target) in enumerate(generator_train):
        data = data.reshape(-1,784)
        data, target = data.to(device), target.to(device)  # sento to either gpu or cpu
        for model_name, model in model_dict.items():
            model.zero_grad()

        z_sample = model_dict['encoder'](data)  # encode to z
        X_sample = model_dict['decoder'](z_sample)  # decode to X reconstruction
        recon_loss = F.binary_cross_entropy(X_sample + EPS, data + EPS)

        recon_loss.backward()
        optimizer_dict['decoder'].step()
        optimizer_dict['encoder'].step()

        # Discriminator
        ## true prior is random normal (randn)
        ## this is constraining the Z-projection to be normal!
        model_dict['encoder'].eval()
        z_real_gauss = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
        D_real_gauss = model_dict['discriminator'](z_real_gauss)

        z_fake_gauss = model_dict['encoder'](data)
        D_fake_gauss = model_dict['discriminator'](z_fake_gauss)

        D_loss = -torch.mean(torch.log(D_real_gauss + EPS) + torch.log(1 - D_fake_gauss + EPS))

        D_loss.backward()
        optimizer_dict['discriminator'].step()

        # Generator
        model_dict['encoder'].train()
        z_fake_gauss =  model_dict['encoder'](data)
        D_fake_gauss =  model_dict['discriminator'](z_fake_gauss)

        G_loss = -torch.mean(torch.log(D_fake_gauss + EPS))

        G_loss.backward()
        optimizer_dict['generator'].step()

        if batch_idx % kwargs['log_interval'] == 0: #Logging
            log_dict = {'reconstruction_loss': recon_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': recon_loss.item()+D_loss.item()+G_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data)) + '/' + str(len(generator_train.dataset)) + ') ' + ','.join([k +': ' + str(v) for k,v in log_dict.items()])
            print(to_print)




###########################################################################
############################### Main ######################################
###########################################################################
config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/tutorial_AAE.yaml')

use_GPU = config_file.use_GPU
batch_size = config_file.batch_size
use_cuda =  use_GPU and torch.cuda.is_available()
learning_rate = config_file.learning_rate
momentum = config_file.momentum
n_epochs = config_file.n_epochs


EPS = 1e-15
z_red_dims = 120
Q = Q_net(784, 1000, z_red_dims)
P = P_net(784, 1000, z_red_dims)
D_gauss = D_net_gauss(500, z_red_dims)

# Set learning rates
gen_lr = learning_rate['generator']
reg_lr = learning_rate['discriminator']

kwargs_training={'log_interval' : config_file.log_interval} #Nmber of steps
kwargs_testing={}
kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
device = torch.device("cuda:0" if use_cuda else "cpu")

# encode/decode optimizers
optim_P = torch.optim.Adam(P.parameters(), lr=gen_lr)
optim_Q_enc = torch.optim.Adam(Q.parameters(), lr=gen_lr)

# regularizing optimizers
optim_Q_gen = torch.optim.Adam(Q.parameters(), lr=reg_lr)
optim_D = torch.optim.Adam(D_gauss.parameters(), lr=reg_lr)

model_dict = {'encoder': Q, 'decoder': P, 'discriminator': D_gauss}
optimizer_dict = {'encoder': optim_Q_enc, 'decoder': optim_P, 'discriminator': optim_D, 'generator': optim_Q_gen}


# MNIST Dataset
# Load data + build generators
generator_train = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=True, download=True, transform=transforms.Compose([
                           transforms.ToTensor(),
                           #transforms.Normalize((0.1307,), (0.3081,))
                       ])),
    batch_size=batch_size, shuffle=True, **kwargs_generator
)

generator_test = torch.utils.data.DataLoader(
    datasets.MNIST('../data', train=False, download=True, transform=transforms.Compose([
                           transforms.ToTensor(),
                          # transforms.Normalize((0.1307,), (0.3081,))
                       ])),
    batch_size=1, shuffle=True, **kwargs_generator
)

data_1, target = next(iter(generator_test))
data_2, target = next(iter(generator_test))
data_3, target = next(iter(generator_test))
data_4, target = next(iter(generator_test))


# Train: epochs
log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
logger = History()
logger.on_train_init(log_keys)
for epoch in range(1, n_epochs + 1):
    logger = train(model_dict, optimizer_dict, device, generator_train, epoch, z_red_dims, **kwargs_training)


OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'AAE_MNIST')
if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

plt.figure()
plt.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
plt.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
plt.plot(logger.logs['generator_loss'], 'k', label='Generator')
plt.legend()
plt.savefig(join(OUTPUT_PATH, 'training.png'))
plt.close()

encoded = model_dict['encoder'](data_1.reshape(1,784))
encoded = torch.FloatTensor(encoded)
decoded = model_dict['decoder'](encoded).detach().numpy().reshape(28,28)

fig, axarr = plt.subplots(1, 2)
axarr[0].imshow(data_1.detach().numpy().reshape(28,28))
axarr[1].imshow(decoded)
plt.savefig(join(OUTPUT_PATH, 'example1_epoch' + str(epoch) + '.png'))
plt.close()

encoded = model_dict['encoder'](data_2.reshape(1, 784))
encoded = torch.FloatTensor(encoded)
decoded = model_dict['decoder'](encoded).detach().numpy().reshape(28, 28)

fig, axarr = plt.subplots(1, 2)
axarr[0].imshow(data_2.detach().numpy().reshape(28, 28))
axarr[1].imshow(decoded)
plt.savefig(join(OUTPUT_PATH,  'example2_epoch' + str(epoch) + '.png'))
plt.close()

encoded = model_dict['encoder'](data_3.reshape(1, 784))
encoded = torch.FloatTensor(encoded)
decoded = model_dict['decoder'](encoded).detach().numpy().reshape(28, 28)

fig, axarr = plt.subplots(1, 2)
axarr[0].imshow(data_3.detach().numpy().reshape(28, 28))
axarr[1].imshow(decoded)
plt.savefig(join(OUTPUT_PATH,  'example3_epoch' + str(epoch) + '.png'))
plt.close()

encoded = model_dict['encoder'](data_4.reshape(1, 784))
encoded = torch.FloatTensor(encoded)
decoded = model_dict['decoder'](encoded).detach().numpy().reshape(28, 28)

fig, axarr = plt.subplots(1, 2)
axarr[0].imshow(data_4.detach().numpy().reshape(28, 28))
axarr[1].imshow(decoded)
plt.savefig(join(OUTPUT_PATH,  'example4_epoch' + str(epoch) + '.png'))
plt.close()