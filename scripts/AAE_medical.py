import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT
from src.dataset import MyDataset
from src.models import Encoder, Decoder, Discriminator

import os
import numpy as np
from os.path import join, exists
import itertools


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################

N_COMPONENTS = 16
EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'AAE_disentangle', 'nc_' + str(N_COMPONENTS))

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):

    for model_name, model in model_dict.items():
        model.train()
    for batch_idx, (data,target) in enumerate(generator_train):
        data = data.reshape(-1,784)
        data, target = data.to(device), target.to(device)  # sento to either gpu or cpu
        for model_name, model in model_dict.items():
            model.zero_grad()

        x_sample, z_sample = model_dict['autoencoder'](data)  # encode to z
        AAE_loss = F.binary_cross_entropy(x_sample + EPS, data + EPS)

        AAE_loss.backward()
        optimizer_dict['autoencoder'].step()

        # Discriminator
        ## true prior is random normal (randn)
        ## this is constraining the Z-projection to be normal!
        model_dict['autoencoder'].eval()
        z_real_gauss = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
        D_real_gauss = model_dict['discriminator'](z_real_gauss)

        z_fake_gauss = model_dict['generator'](data)
        D_fake_gauss = model_dict['discriminator'](z_fake_gauss)

        D_loss = -torch.mean(torch.log(D_real_gauss + EPS) + torch.log(1 - D_fake_gauss + EPS))

        D_loss.backward()
        optimizer_dict['discriminator'].step()

        # Generator
        model_dict['generator'].train()
        z_fake_gauss =  model_dict['generator'](data)
        D_fake_gauss =  model_dict['discriminator'](z_fake_gauss)

        G_loss = -torch.mean(torch.log(D_fake_gauss + EPS))

        G_loss.backward()
        optimizer_dict['generator'].step()

        if batch_idx % kwargs['log_interval'] == 0: #Logging
            log_dict = {'reconstruction_loss': AAE_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': AAE_loss.item()+D_loss.item()+G_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data)) + '/' + str(len(generator_train.dataset)) + ') ' + ','.join([k +': ' + str(v) for k,v in log_dict.items()])
            print(to_print)
    return logger

def test(model_dict, device, generator_train, epoch, latent_space_dims, logger, save_file_flag=True):
    for model_name, model in model_dict.items():
        model.eval()

    batch_size = generator_test.batch_size

    test_loss = 0
    with torch.no_grad():
        for batch_idx, (data, _) in enumerate(generator_test):
            data = data.reshape(-1, 784)
            data = data.to(device)

            # Autoencoder
            recon_batch, latent_batch = model_dict['autoencoder'](data)
            AAE_loss = F.binary_cross_entropy(recon_batch + EPS, data + EPS)

            # Discriminator
            latent_gauss_batch = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
            D_real = model_dict['discriminator'](latent_batch)
            D_gauss = model_dict['discriminator'](latent_gauss_batch)
            D_loss = -torch.mean(torch.log(D_gauss + EPS) + torch.log(1 - D_real + EPS))

            # Generator
            G_loss = -torch.mean(torch.log(D_real + EPS))

            log_dict = {'reconstruction_loss': AAE_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': AAE_loss.item() + D_loss.item() + G_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join([k + ': ' + str(v) for k, v in log_dict.items()])
            print(to_print)

            if batch_idx == 0 and save_file_flag:
                n = min(data.size(0), 8)
                print(recon_batch.size())
                print(data.size())
                comparison = torch.cat([data.reshape(-1, 1, 28,28)[:n], recon_batch.view(batch_size, 1, 28, 28)[:n]])
                save_image(comparison.cpu(),join(RESULTS_PATH, 'reconstruction_' + str(epoch) + '.png'), nrow=n)

    test_loss /= len(generator_test.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))
    return logger



################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    DataLoader = data_reader.DataLoader()
    av45_dict = DataLoader.load_modality_baseline(modality_name='av45_pet')
    vMRI_dict = DataLoader.load_modality_baseline(modality_name='volumetric_mri')
    cMRI_dict = DataLoader.load_modality_baseline(modality_name='cortical_thickness_mri')
    FDG_dict = DataLoader.load_modality_baseline(modality_name='fdg_pet')
    csf_dict = DataLoader.load_modality_baseline(modality_name='csf')
    imaging_phenotypes_dict = DataLoader.load_modality_baseline(modality_name='imaging_phenotypes')

    demographic_dict = DataLoader.load_demographic_baseline(demo_names=demo_names)
    age_dict = DataLoader.load_age()
    clinical_dict = DataLoader.load_clinical_baseline(clinical_names=clinical_names)
    dx_dict = DataLoader.load_dx_baseline()

    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    clinical_id_list = ['age'] + clinical_names + ['education']

    modality_list = ['volumetric_mri']
    modality_dict_list = [vMRI_dict]
    modality_id_list = [vMRI_id_list]

    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    writer_legend.write('############################\n')
    writer_legend.write('########## LEGEND ##########\n')
    writer_legend.write('############################\n\n')
    for it_l in range(len(modality_id_list)):
        writer_legend.write('----------  ' + modality_list[it_l] + ' --------\n')
        for it_mod_id, mod_id in enumerate(modality_id_list[it_l]):
            writer_legend.write(str(it_mod_id) + '. ' + mod_id + '\n')
        writer_legend.write('-----------------------------\n')

    rid_list = data_reader.find_list_intersection([list(mdict.keys()) for mdict in modality_dict_list] +
                                                  [list(demographic_dict.keys()),
                                                   list(age_dict.keys()),
                                                   list(csf_dict.keys()),
                                                   list(dx_dict.keys()),
                                                   ])

    csf_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, csf_dict.items())).values()))
    if len(csf_array.shape) < 2:
        csf_array = csf_array.reshape(-1, 1)

    demographic_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, demographic_dict.items())).values()))
    if len(demographic_array.shape) < 2:
        demographic_array = demographic_array.reshape(-1, 1)

    age_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, age_dict.items())).values()))
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)

    modality_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[0].items())).values()))
    if len(modality_array.shape) < 2:
        modality_array = modality_array.reshape(-1, 1)


    factor_array = np.concatenate((age_array, demographic_array), axis=1)
    factor_id_list = ['age'] + demo_names

    dx_array =  np.asarray(list(dict(filter(lambda x: x[0] in rid_list, dx_dict.items())).values()))
    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    ## PRE-PROCESSING
    std_scaler = StandardScaler()
    modality_array = std_scaler.fit_transform(modality_array)
    # m2_array = std_scaler.fit_transform(m2_array)
    # m3_array = std_scaler.fit_transform(m3_array)
    # m4_array = std_scaler.fit_transform(m4_array)

    csf_array = std_scaler.fit_transform(csf_array)
    demographic_array = std_scaler.fit_transform(demographic_array)
    age_array = std_scaler.fit_transform(age_array)

    N, M = modality_array.shape
    # N2, M2 = m2_array.shape
    # N3, M3 = m3_array.shape
    # N4, M4 = m4_array.shape

    print('N: ' + str(N))
    print('M: ' + str(M))

    ########################################
    ##########   EXPERIMENTS  ##############
    ########################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_medical.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda =  use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    kwargs_training={'log_interval' : config_file.log_interval} #Nmber of steps
    kwargs_testing={}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")


    hidden_layer_list = [N_COMPONENTS]
    discriminator_layer_list = [1]

    encoder_model = Encoder(input_dim=M1, hidden_layer=hidden_layer_list).to(device)
    decoder_model = Decoder(input_dim=M1, hidden_layer=hidden_layer_list).to(device)
    discriminator_model = Discriminator(input_dim=N_COMPONENTS, hidden_layer=hidden_layer_list).to(device)

    # Set learning rates
    autoencoder_lr = learning_rate['autoencoder']
    discriminator_lr = learning_rate['discriminator']
    generator_lr = learning_rate['generator']

    # encode/decode optimizers
    decoder_optimizer = torch.optim.Adam(nn.ParameterList(decoder_model.parameters()), lr=autoencoder_lr)
    encoder_optimizer = torch.optim.Adam(nn.ParameterList(encoder_model.parameters()), lr=autoencoder_lr)

    # regularizing optimizers
    generator_optimizer = torch.optim.Adam(nn.ParameterList(encoder_model.parameters()), lr=generator_lr)
    discriminator_optimizer = torch.optim.Adam(nn.ParameterList(discriminator_model.parameters()), lr=discriminator_lr)

    model_dict = {'encoder': encoder_model, 'decoder': decoder_model, 'discriminator': discriminator_model}
    optimizer_dict = {'encoder': encoder_optimizer, 'decoder': decoder_optimizer, 'discriminator': discriminator_optimizer, 'generator': generator_optimizer}


    # Load data + build generators

    dataset = MyDataset(m1_array, age_array)
    generator_train = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=2,
        pin_memory=torch.cuda.is_available()

    )

    generator_test = torch.utils.data.DataLoader(
        dataset,
        batch_size=m1_array.shape[0],
        shuffle=True,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    # Train: epochs

    log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
    logger = History()
    logger.on_train_init(log_keys)
    for epoch in range(1, n_epochs + 1):
        logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)

    writer = ResultsWriter(filepath=join(MODALITY_PATH, 'results_file.txt'))

    plt.figure()
    plt.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
    plt.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
    plt.plot(logger.logs['generator_loss'], 'k', label='Generator')
    plt.legend()
    plt.savefig(join(MODALITY_PATH,'training.png'))
    plt.close()


    y_scores = predict(model_dict['encoder'], generator_test, (N1,hidden_layer_list[-1]))
    for it_component in range(N_COMPONENTS):
        plt.figure()
        plt.hist(y_scores[:,it_component], bins= 40)
        plt.savefig(join(MODALITY_PATH, 'histogram_nc_' + str(it_component) + '.png'))
        plt.close()

    STATISTICAL_PLOTS = True
    if STATISTICAL_PLOTS:
        writer.write('#############################\n')
        writer.write('RESULTS DIAGNOSTIC COMPARISON\n')
        writer.write('#############################\n\n')
        writer.write('--- Modality subspace ---\n')

        combination_plots = list(itertools.combinations(range(3), 2))
        for it_component in range(N_COMPONENTS):
            writer.write('     · Component ' + str(it_component) + '\n')
            t = []
            p = []
            boxplot_list = [y_scores[index_0, it_component], y_scores[index_1, it_component],
                            y_scores[index_2, it_component]]
            for c in combination_plots:
                result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                t.append(result[0])
                p.append(result[1])
                writer.write('           - ' + str((label_diagnostic[c[0]], label_diagnostic[c[1]])) + ': ' + str(
                    (result[0], result[1])) + '\n')

            filename = 'modality_diagnostic' + str(it_component) + '.png'
            plt.figure()
            plt.boxplot(boxplot_list)
            plt.xticks([1, 2, 3], label_diagnostic)
            plt.title('Significant (p<0.05) differences: ' + str(
                [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
            plt.savefig(join(MODALITY_PATH, filename))
            plt.close()

    WEIGHT_PLOTS = True
    if WEIGHT_PLOTS:
        writer.write('####################\n')
        writer.write('PLS Y_WEIGHT RESULTS\n')
        writer.write('####################\n\n')

        y_weight_generator = np.eye(N_COMPONENTS)
        dataset_y_weight = MyDataset(y_weight_generator, y_weight_generator)
        generator_y_weight = torch.utils.data.DataLoader(
            dataset_y_weight,
            batch_size=N_COMPONENTS,
            shuffle=True,
            num_workers=1,
            pin_memory=torch.cuda.is_available()

        )

        y_rotations = predict(model_dict['decoder'], generator_y_weight, (N_COMPONENTS,M1))
        for it_component in range(N_COMPONENTS):
            sorted_mod = np.argsort(np.abs(y_rotations[it_component,:]))[::-1]
            writer.write('    · Component ' + str(it_component) + '\n')
            for m_prs in sorted_mod:
                writer.write('         ' + modality_id_list[0][m_prs] + ': ' + str(y_rotations[it_component,m_prs]) + '\n')
        writer.write('\n')

        y_min = np.min(y_rotations) * 0.9
        y_max = np.max(y_rotations) * 1.1
        y_lim = np.max([np.abs(y_min), np.abs(y_max)])

        for it_component in range(N_COMPONENTS):
            filename = 'y_weights_' + str(it_component) + '.png'
            x_min = np.min(y_rotations[it_component,:])
            x_max = np.max(y_rotations[it_component,:])
            _, ax = plt.subplots()
            ax.barh(np.arange(M1), y_rotations[it_component, :], align='center', color='green', ecolor='black')
            ax.set_yticks(np.arange(start=0, stop=M1, step=np.max([1, int(M1 / 10)])))
            ax.set_xticks(np.concatenate((np.arange(start=-y_lim, stop=0, step=y_lim / 3),
                                          np.arange(start=0, stop=y_lim, step=y_lim / 3), [y_lim])))
            ax.invert_yaxis()
            ax.set_xlabel('Latent variable ' + str(it_component))
            ax.set_title('Relative weight of each input variable')
            ax.grid()
            ax.set_xlim([-y_lim, y_lim])
            plt.savefig(join(MODALITY_PATH, filename))
            plt.close()

    FACTOR_PLOTS = True
    if FACTOR_PLOTS:
        writer.write('###########################\n')
        writer.write('FACTOR CORRELATION ANALYSIS\n')
        writer.write('###########################\n')
        n_rows = ROW_DICT[N_COMPONENTS]
        n_cols = int(np.ceil(N_COMPONENTS / n_rows))
        for it_factor, factor in enumerate(factor_id_list):
            fig_y, axarr_y = plt.subplots(n_rows, n_cols)
            writer.write('\nFACTOR: ' + factor + '\n')

            for it_c in range(N_COMPONENTS):
                writer.write('    · Component ' + str(it_c + factor_array.shape[1]) + ':')
                writer.write(' Y:' + str(pearsonr(y_scores[:, it_c], factor_array[:, it_factor])) + '\n')


                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[index_0, it_c],
                                                                          factor_array[index_0, it_factor], '*b',
                                                                          label='NC')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[index_1, it_c],
                                                                          factor_array[index_1, it_factor], '*r',
                                                                          label='MCI')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[index_2, it_c],
                                                                          factor_array[index_2, it_factor], '*k',
                                                                          label='AD')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')
                axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor)

            plt.figure(1)
            plt.savefig(join(MODALITY_PATH, 'y_score_' + factor + '_correlation.png'))
            plt.close()