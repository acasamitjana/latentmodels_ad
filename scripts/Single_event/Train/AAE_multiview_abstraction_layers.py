import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data import adni_reader
from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT
from src.dataset import MyDataset
from src.models import Encoder, Decoder, Discriminator, Autoencoder, Encoder_lineal, Decoder_lineal
from src.layers import Mean_layer, Var_layer

import os
import numpy as np
from os.path import join, exists
import itertools


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################

N_COMPONENTS = 16
EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'


BASELINE_OR_SINGLE = 1
if BASELINE_OR_SINGLE == 0:
    OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'Baseline','AAE_multiview_abstraction_layers', 'nc_' + str(N_COMPONENTS), 'lineal')
else:
    OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'Single', 'AAE_multiview_abstraction_layers', 'nc_' + str(N_COMPONENTS), 'lineal')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):
    for batch_idx, (data, target) in enumerate(generator_train):
        data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)

        for model_name, model in model_dict.items():
            model.zero_grad()

        #### RECONSTRUCTION LOSS
        model_dict['discriminator_1'].train(mode=False)
        model_dict['discriminator_2'].train(mode=False)
        model_dict['autoencoder_1'].train(mode=True)
        model_dict['autoencoder_2'].train(mode=True)
        model_dict['autoencoder_3'].train(mode=True)
        model_dict['autoencoder_4'].train(mode=True)

        # print('Reconstruction phase')
        # print( model_dict['autoencoder_1'].training)
        # print('===> ' + str(model_dict['autoencoder_1'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_1'].decoder.training))
        # print( model_dict['autoencoder_2'].training)
        # print('===> ' + str(model_dict['autoencoder_2'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_2'].decoder.training))
        # print( model_dict['generator_1'].training)
        # print( model_dict['generator_2'].training)
        # print( model_dict['discriminator'].training)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)  # encode to

        z_mean = Mean_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])
        z_var =  Var_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])

        z_sample = torch.cat((z_mean, z_var), 1)

        x_reconstructed_1 = model_dict['autoencoder_1'].decode(z_sample)  # decode to X reconstruction
        x_reconstructed_2 = model_dict['autoencoder_2'].decode(z_sample)  # decode to X reconstruction
        x_reconstructed_3 = model_dict['autoencoder_3'].decode(z_sample)  # decode to X reconstruction
        x_reconstructed_4 = model_dict['autoencoder_4'].decode(z_sample)  # decode to X reconstruction

        reconstruction_loss_1 = ((x_reconstructed_1 - data_1) ** 2).sum(dim=-1)
        reconstruction_loss_2 = ((x_reconstructed_2 - data_2) ** 2).sum(dim=-1)
        reconstruction_loss_3 = ((x_reconstructed_3 - data_3) ** 2).sum(dim=-1)
        reconstruction_loss_4 = ((x_reconstructed_4 - data_4) ** 2).sum(dim=-1)

        reconstruction_loss = torch.mean(reconstruction_loss_1 + reconstruction_loss_2 +
                                         reconstruction_loss_3 + reconstruction_loss_4)

        reconstruction_loss.backward()
        optimizer_dict['autoencoder_1'].step()
        optimizer_dict['autoencoder_2'].step()
        optimizer_dict['autoencoder_3'].step()
        optimizer_dict['autoencoder_4'].step()


        #### DISCRIMINATOR LOSS
        model_dict['discriminator_1'].train(mode=True)
        model_dict['discriminator_2'].train(mode=True)
        model_dict['autoencoder_1'].train(mode=False)
        model_dict['autoencoder_2'].train(mode=False)
        model_dict['autoencoder_3'].train(mode=False)
        model_dict['autoencoder_4'].train(mode=False)

        z_real_gauss = 16*torch.randn(data_1.size()[0], latent_space_dims)
        z_real_gamma = torch.distributions.Gamma(torch.tensor([0.5]), torch.tensor([0.5])).sample(torch.Size((data_1.size()[0], latent_space_dims,))).squeeze()

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)

        z_mean = Mean_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])
        z_var = Var_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])

        d_mean_real = model_dict['discriminator_1'](z_real_gauss)
        d_mean_sample = model_dict['discriminator_1'](z_mean)

        d_var_real = model_dict['discriminator_2'](z_real_gamma)
        d_var_sample = model_dict['discriminator_2'](z_var)


        discriminator_loss_mean = -torch.mean(torch.log(d_mean_real + EPS) + torch.log(1 - d_mean_sample + EPS))
        discriminator_loss_var = -torch.mean(torch.log(d_var_real + EPS) + torch.log(1 - d_var_sample + EPS))

        discriminator_loss = discriminator_loss_mean + discriminator_loss_var

        discriminator_loss.backward()
        optimizer_dict['discriminator_1'].step()
        optimizer_dict['discriminator_2'].step()

        #### GENERATOR LOSS
        model_dict['generator_1'].train(mode=True)
        model_dict['generator_2'].train(mode=True)
        model_dict['generator_3'].train(mode=True)
        model_dict['generator_4'].train(mode=True)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
        z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
        z_sample_4 = model_dict['autoencoder_4'].encode(data_4)

        z_mean = Mean_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])
        z_var = Var_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])

        d_mean_sample = model_dict['discriminator_1'](z_mean)
        d_var_sample = model_dict['discriminator_2'](z_var)

        generator_loss_mean = -torch.mean(torch.log(d_mean_sample + EPS))
        generator_loss_var = -torch.mean(torch.log(d_var_sample + EPS))

        generator_loss = generator_loss_mean + generator_loss_var

        generator_loss.backward()
        optimizer_dict['generator_1'].step()
        optimizer_dict['generator_2'].step()
        optimizer_dict['generator_3'].step()
        optimizer_dict['generator_4'].step()

        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item() + discriminator_loss.item() + generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)

    return logger

def predict(model_dict, generator_test, latent_size,reconstruction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    latent_predictions = torch.zeros(latent_size)
    reconstruction_predictions = [torch.zeros(reconstruction_size[0]),
                                  torch.zeros(reconstruction_size[1]),
                                  torch.zeros(reconstruction_size[2]),
                                  torch.zeros(reconstruction_size[3])]
    labels = torch.zeros(num_elements,1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            data_1, data_2, data_3, data_4 = data[0].to(device), data[1].to(device), data[2].to(device), data[3].to(device)
            z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
            z_sample_2 = model_dict['autoencoder_2'].encode(data_2)
            z_sample_3 = model_dict['autoencoder_3'].encode(data_3)
            z_sample_4 = model_dict['autoencoder_4'].encode(data_4)

            z_mean = Mean_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])
            z_var = Var_layer()([z_sample_1, z_sample_2, z_sample_3, z_sample_4])

            z_sample = torch.cat((z_mean, z_var), 1)

            x_reconstructed_1 = model_dict['autoencoder_1'].decode(z_sample)  # decode to X reconstruction
            x_reconstructed_2 = model_dict['autoencoder_2'].decode(z_sample)  # decode to X reconstruction
            x_reconstructed_3 = model_dict['autoencoder_3'].decode(z_sample)  # decode to X reconstruction
            x_reconstructed_4 = model_dict['autoencoder_4'].decode(z_sample)  # decode to X reconstruction

            rec_sample = [x_reconstructed_1,x_reconstructed_2,x_reconstructed_3,x_reconstructed_4]
            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            latent_predictions[start:end] = z_sample.cpu()
            for it_rec in range(4):
                reconstruction_predictions[it_rec][start:end] = rec_sample[it_rec].cpu()

            labels[start:end] = target.cpu()

    return latent_predictions.detach().numpy(), [r.detach().numpy() for r in reconstruction_predictions], labels.detach().numpy()


################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    DataLoader = data_reader.DataLoader()
    if BASELINE_OR_SINGLE == 0:
        DataLoader.filter_baseline_subject_dict()
    else:
        DataLoader.filter_single_label_subject_dict()

    av45_dict = DataLoader.load_modality_single(modality_name='av45_pet')
    vMRI_dict = DataLoader.load_modality_single(modality_name='volumetric_mri')
    cMRI_dict = DataLoader.load_modality_single(modality_name='cortical_thickness_mri')
    FDG_dict = DataLoader.load_modality_single(modality_name='fdg_pet')
    csf_dict = DataLoader.load_modality_single(modality_name='csf')
    imaging_phenotypes_dict = DataLoader.load_modality_single(modality_name='imaging_phenotypes')

    education_dict = DataLoader.load_demographic_single(demo_names=['education'])
    age_dict = DataLoader.load_age_single()
    mmse_dict = DataLoader.load_clinical_single(clinical_names=['mmse'])
    cdrsb_dict = DataLoader.load_clinical_single(clinical_names=['cdrsb'])
    faq_dict = DataLoader.load_clinical_single(clinical_names=['faq'])
    adas11_dict = DataLoader.load_clinical_single(clinical_names=['adas11'])

    dx_dict = DataLoader.load_dx_single()
    rid_list_test = data_reader.find_list_intersection([list(age_dict.keys()),
                                                        list(mmse_dict.keys()),
                                                        list(cdrsb_dict.keys()),
                                                        list(education_dict.keys()),
                                                        ])
    test_dict = {rid: [age_dict[rid],mmse_dict[rid],cdrsb_dict[rid],education_dict[rid]] for rid in rid_list_test}

    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    clinical_id_list = clinical_names
    test_id_list = ['age','mmse', 'cdrsb','education']

    modality_list = ['csf','vMRI', 'cMRI', 'test']
    modality_dict_list = [csf_dict, vMRI_dict, cMRI_dict, test_dict]
    modality_id_list = [csf_id_list, vMRI_id_list, cMRI_id_list, test_id_list]

    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    writer_list = []
    for it_component in range(N_COMPONENTS):
        COMPONENTS_PATH = join(MODALITY_PATH, str(it_component))
        if not exists(COMPONENTS_PATH):
            os.makedirs(COMPONENTS_PATH)

        writer_list.append(ResultsWriter(filepath=join(COMPONENTS_PATH, 'results_file.txt'), attach=False))


    rid_list = data_reader.find_list_intersection([list(mdict.keys()) for mdict in modality_dict_list] +
                                                  [list(age_dict.keys()),
                                                   list(education_dict.keys()),
                                                   list(dx_dict.keys()),
                                                   ])

    education_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, education_dict.items())).values()))
    if len(education_array.shape) < 2:
        education_array = education_array.reshape(-1, 1)

    age_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, age_dict.items())).values()))
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)

    m1_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[0].items())).values()))
    if len(m1_array.shape) < 2:
        m1_array = m1_array.reshape(-1, 1)

    m2_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[1].items())).values()))
    if len(m2_array.shape) < 2:
        m2_array = m2_array.reshape(-1, 1)

    m3_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[2].items())).values()))
    if len(m3_array.shape) < 2:
        m3_array = m3_array.reshape(-1, 1)

    m4_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[3].items())).values()))
    if len(m4_array.shape) < 2:
        m4_array = m4_array.reshape(-1, 1)


    dx_array =  np.asarray(list(dict(filter(lambda x: x[0] in rid_list, dx_dict.items())).values())).reshape(-1,1)
    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    ## PRE-PROCESSING
    std_scaler = StandardScaler()
    m1_array = std_scaler.fit_transform(m1_array)
    m2_array = std_scaler.fit_transform(m2_array)
    m3_array = std_scaler.fit_transform(m3_array)
    m4_array = std_scaler.fit_transform(m4_array)
    age_array = std_scaler.fit_transform(age_array)

    N1, M1 = m1_array.shape
    N2, M2 = m2_array.shape
    N3, M3 = m3_array.shape
    N4, M4 = m4_array.shape
    M = [M1, M2, M3, M4]

    print('N1: ' + str(N1))
    print('M1: ' + str(M1))
    print('M2: ' + str(M2))
    print('M3: ' + str(M3))
    print('M4: ' + str(M4))


    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_multiview.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    hidden_layers_dim_encoder = [N_COMPONENTS]
    hidden_layers_dim_decoder= [2*N_COMPONENTS]


    hidden_layers_discriminator_dim = [10, 1]
    latent_dim = N_COMPONENTS

    Generator_model_1 = Encoder_lineal(M1, hidden_layers_dim_encoder)
    Decoder_model_1 = Decoder_lineal(M1, hidden_layers_dim_decoder)
    AAE_model_1 = Autoencoder(Generator_model_1, Decoder_model_1)

    Generator_model_2 = Encoder_lineal(M2, hidden_layers_dim_encoder)
    Decoder_model_2 = Decoder_lineal(M2, hidden_layers_dim_decoder)
    AAE_model_2 = Autoencoder(Generator_model_2, Decoder_model_2)

    Generator_model_3 = Encoder_lineal(M3, hidden_layers_dim_encoder)
    Decoder_model_3 = Decoder_lineal(M3, hidden_layers_dim_decoder)
    AAE_model_3 = Autoencoder(Generator_model_3, Decoder_model_3)

    Generator_model_4 = Encoder_lineal(M4, hidden_layers_dim_encoder)
    Decoder_model_4 = Decoder_lineal(M4, hidden_layers_dim_decoder)
    AAE_model_4 = Autoencoder(Generator_model_4, Decoder_model_4)

    Discriminator_model_1 = Discriminator(latent_dim, hidden_layers_discriminator_dim)
    Discriminator_model_2 = Discriminator(latent_dim, hidden_layers_discriminator_dim)

    # Set learning rates
    ae_lr = learning_rate['autoencoder']
    gen_lr = learning_rate['generator']
    reg_lr = learning_rate['discriminator']

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    # encode/decode optimizers
    optim_AAE_1 = torch.optim.Adam(AAE_model_1.parameters(), lr=ae_lr)
    optim_G_1 = torch.optim.Adam(Generator_model_1.parameters(), lr=gen_lr)
    optim_AAE_2 = torch.optim.Adam(AAE_model_2.parameters(), lr=ae_lr)
    optim_G_2 = torch.optim.Adam(Generator_model_2.parameters(), lr=gen_lr)
    optim_AAE_3 = torch.optim.Adam(AAE_model_3.parameters(), lr=ae_lr)
    optim_G_3 = torch.optim.Adam(Generator_model_3.parameters(), lr=gen_lr)
    optim_AAE_4 = torch.optim.Adam(AAE_model_4.parameters(), lr=ae_lr)
    optim_G_4 = torch.optim.Adam(Generator_model_4.parameters(), lr=gen_lr)
    optim_D_1 = torch.optim.Adam(Discriminator_model_1.parameters(), lr=reg_lr)
    optim_D_2 = torch.optim.Adam(Discriminator_model_2.parameters(), lr=reg_lr)

    model_dict = {'autoencoder_1': AAE_model_1, 'autoencoder_2': AAE_model_2,
                  'autoencoder_3': AAE_model_3, 'autoencoder_4': AAE_model_4,
                  'discriminator_1': Discriminator_model_1, 'discriminator_2': Discriminator_model_2,
                  'generator_1': Generator_model_1, 'generator_2': Generator_model_2,
                  'generator_3': Generator_model_3, 'generator_4': Generator_model_4
                  }

    optimizer_dict = {'autoencoder_1': optim_AAE_1, 'autoencoder_2': optim_AAE_1,
                      'autoencoder_3': optim_AAE_3, 'autoencoder_4': optim_AAE_4,
                      'discriminator_1': optim_D_1, 'discriminator_2': optim_D_1,
                      'generator_1': optim_G_1, 'generator_2': optim_G_2,
                      'generator_3': optim_G_3, 'generator_4': optim_G_4}

    #################################
    ##########  READ DATA  ##########
    #################################

    sss = StratifiedShuffleSplit(n_splits=1)
    it_split = 0
    for train_index, test_index in sss.split(dx_array,dx_array):

        N_train = len(train_index)
        N_test = len(test_index)

        m1_train = m1_array[train_index]
        m2_train = m2_array[train_index]
        m3_train = m3_array[train_index]
        m4_train = m4_array[train_index]

        m1_test = m1_array[test_index]
        m2_test = m2_array[test_index]
        m3_test = m3_array[test_index]
        m4_test = m4_array[test_index]

        dx_train = dx_array[train_index]
        dx_test = dx_array[test_index]

        modality_train = [m1_train, m2_train, m3_train, m4_train]
        dataset_train = MyDataset(modality_train, dx_train)
        generator_train = torch.utils.data.DataLoader(
            dataset_train,
            batch_size=N1,
            shuffle=True,
            num_workers=2,
            pin_memory=torch.cuda.is_available()

        )
        modality_test = [m1_test, m2_test, m3_test, m4_test]
        dataset_test = MyDataset(modality_test, dx_test)
        generator_test = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=N1,
            shuffle=False,
            num_workers=1,
            pin_memory=torch.cuda.is_available()
        )



        ################################
        ##########  TRAINING  ##########
        ################################

        log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
        logger = History()
        logger.on_train_init(log_keys)
        for epoch in range(1, n_epochs + 1):
            logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)

        fig, ax1 = plt.subplots()

        ax2 = ax1.twinx()
        ax1.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
        ax2.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
        ax2.plot(logger.logs['generator_loss'], 'g', label='Generator')
        ax1.plot(logger.logs['loss'], 'k', label='Total loss')

        ax1.set_xlabel('Iterations')
        ax1.set_ylabel('Reconstruction and Total loss', color='g')
        ax2.set_ylabel('Discriminator and generator loss', color='b')

        ax1.legend()
        ax2.legend()
        plt.savefig(join(MODALITY_PATH,'training.png'))
        plt.close()


        ###############################
        ##########  RESULTS  ##########
        ###############################
        CLASSIFICATION_PLOTS = True
        REGRESSION_PLOTS = True

        reconstruction_size_train = [(N_train,m) for m in M]
        reconstruction_size_test = [(N_test,m) for m in M]

        y_scores_train, x_rec_train, _ = predict(model_dict, generator_train, (N_train, 2*latent_dim), reconstruction_size_train)
        y_scores_test, x_rec_test, _ = predict(model_dict, generator_test, (N_test, 2*latent_dim), reconstruction_size_test)


        if CLASSIFICATION_PLOTS:
            train_results = {}
            test_results = {}

            lr_model = LogisticRegression()
            lr_model.fit(y_scores_train, dx_train)
            dx_predicted_train = lr_model.predict(y_scores_train)
            dx_predicted_test = lr_model.predict(y_scores_test)

            train_results['confusion_matrix'] = confusion_matrix(dx_train, dx_predicted_train, labels=np.sort(np.unique(dx_train)))
            test_results['confusion_matrix'] = confusion_matrix(dx_test, dx_predicted_test, labels=np.sort(np.unique(dx_test)))

            print('TRAINING RESULTS')
            print(' - Confusion matrix: ')
            print(train_results['confusion_matrix'])

            print()
            print('TESTING RESULTS')
            print(' - Confusion matrix: ')
            print(test_results['confusion_matrix'])


        if REGRESSION_PLOTS:
            train_results = {'mse':{}}
            test_results = {'mse':{}}

            print(len(x_rec_train))
            print(len(x_rec_test))

            it_mod = 0
            for y_true, y_pred in zip(modality_train, x_rec_train):
                print(y_true.shape)
                print(y_pred.shape)

                if modality_list[it_mod] in ['test', 'csf']:
                    train_results['mse'][it_mod] = np.sum((y_true - y_pred) ** 2, axis=0) / N_train
                else:
                    train_results['mse'][it_mod] = np.sum((y_true-y_pred)**2) / N_train
                it_mod += 1

            it_mod = 0
            for y_true, y_pred in zip(modality_test, x_rec_test):
                print(y_true.shape)
                print(y_pred.shape)
                if modality_list[it_mod] in ['test', 'csf']:
                    test_results['mse'][it_mod] = np.sum((y_true - y_pred) ** 2, axis=0) / N_test
                else:
                    test_results['mse'][it_mod] = np.sum((y_true-y_pred)**2) / N_test
                it_mod += 1

            print('TRAINING RESULTS')
            print(' - MSE: ')
            print(train_results['mse'])
            print()

            print('TESTING RESULTS')
            print(' - MSE: ')
            print(test_results['mse'])



        it_split+=1



