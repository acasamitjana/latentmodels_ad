import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.datasets as dsets
from torchvision import datasets, transforms
from torch.autograd import Variable

from Data.TADPOLE import data_reader
from Data.TADPOLE.tadpole_reader import *

from src.utils.preprocessing import to_np, to_var, shuffle
from src.utils.io_utils import ConfigFile
from src.callbacks import History
from src.utils.io_utils import ResultsWriter, ROW_DICT
from src.dataset import MyDataset
from src.models import Encoder, Decoder, Discriminator, Autoencoder, Encoder_lineal, Decoder_lineal

import os
import numpy as np
from os.path import join, exists
import itertools


from scipy.stats import pearsonr, ttest_ind
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from matplotlib import pyplot as plt

plt.switch_backend('Agg')

#########################################
########### GLOBAL PARAMETERS ###########
#########################################

N_COMPONENTS = 16
EPS = 1e-15

TARGET_DATA_PATH = '/projects/neuro/ADNI/TADPOLE/tadpole_pickle.p'
OUTPUT_PATH = join('/work/acasamitjana/Multimodal_autoencoders', 'AAE_twoview', 'nc_' + str(N_COMPONENTS), 'lineal')

if not exists(OUTPUT_PATH):
    os.makedirs(OUTPUT_PATH)

clinical_names = ['cdrsb', 'mmse', 'adas11', 'faq']#['cdrsb','mmse', 'adas11', 'adas13', 'ravlt_forgetting', 'ravlt_immediate', 'ravlt_learning', 'ravlt_per_forgetting', 'faq', 'moca']
demo_names = ['gender', 'education', 'apoe4']
label_diagnostic = ['NC', 'MCI', 'AD']

writer_legend = ResultsWriter(filepath = join(OUTPUT_PATH,'legend.txt'), attach=False)


#######################################
########### TRAINING SCHEME ###########
#######################################
# Training scheme
def train(model_dict, optimizer_dict, device, generator_train, epoch, latent_space_dims, logger, **kwargs):

    for batch_idx, (data_1,data_2) in enumerate(generator_train):
        data_1, data_2 = data_1.to(device), data_2.to(device)  # sento to either gpu or cpu

        for model_name, model in model_dict.items():
            model.zero_grad()

        #### RECONSTRUCTION LOSS
        model_dict['discriminator'].train(mode=False)
        model_dict['autoencoder_1'].train(mode=True)
        model_dict['autoencoder_2'].train(mode=True)

        # print('Reconstruction phase')
        # print( model_dict['autoencoder_1'].training)
        # print('===> ' + str(model_dict['autoencoder_1'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_1'].decoder.training))
        # print( model_dict['autoencoder_2'].training)
        # print('===> ' + str(model_dict['autoencoder_2'].encoder.training))
        # print('===> ' + str(model_dict['autoencoder_2'].decoder.training))
        # print( model_dict['generator_1'].training)
        # print( model_dict['generator_2'].training)
        # print( model_dict['discriminator'].training)

        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)  # encode to z

        x_reconstructed_1_1 = model_dict['autoencoder_1'].decode(z_sample_1)  # decode to X reconstruction
        x_reconstructed_1_2 = model_dict['autoencoder_1'].decode(z_sample_2)  # decode to X reconstruction
        x_reconstructed_2_1 = model_dict['autoencoder_2'].decode(z_sample_1)  # decode to X reconstruction
        x_reconstructed_2_2 = model_dict['autoencoder_2'].decode(z_sample_2)  # decode to X reconstruction

        reconstruction_loss_1 = ((x_reconstructed_1_1 - data_1)**2).mean(dim=-1) +\
                                ((x_reconstructed_1_2 - data_1)**2).mean(dim=-1)

        reconstruction_loss_2 = ((x_reconstructed_2_1 - data_2)**2).mean(dim=-1) +\
                                ((x_reconstructed_2_2 - data_2)**2).mean(dim=-1)

        # reconstruction_loss_1 *= model_dict['autoencoder_1'].encoder.hidden_dims[-1]
        # reconstruction_loss_2 *= model_dict['autoencoder_2'].encoder.hidden_dims[-1]
        reconstruction_loss = torch.mean(reconstruction_loss_1 + reconstruction_loss_2)

        reconstruction_loss.backward()
        optimizer_dict['autoencoder_1'].step()
        optimizer_dict['autoencoder_2'].step()

        #### DISCRIMINATOR LOSS
        model_dict['discriminator'].train(mode=True)
        model_dict['autoencoder_1'].train(mode=False)
        model_dict['autoencoder_2'].train(mode=False)


        z_real_gauss = Variable(torch.randn(data_1.size()[0], latent_space_dims) * 5.)
        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)

        d_real = model_dict['discriminator'](z_real_gauss)
        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2 = model_dict['discriminator'](z_sample_2)

        label_sample_1 = np.zeros((data_1.size()[0], 3))
        label_sample_1[:, 0] = 1
        label_sample_1 = torch.FloatTensor(label_sample_1)

        label_sample_2 = np.zeros((data_1.size()[0], 3))
        label_sample_2[:, 1] = 1
        label_sample_2 = torch.FloatTensor(label_sample_2)

        label_real = np.zeros((data_1.size()[0], 3))
        label_real[:, 2] = 1
        label_real = torch.FloatTensor(label_real)

        discriminator_loss = -torch.mean(torch.mean(label_real * torch.log(d_real + EPS), dim=-1) +
                                         torch.mean(label_sample_1 * torch.log(d_sample_1 + EPS), dim=-1) +
                                         torch.mean(label_sample_2 * torch.log(d_sample_2 + EPS), dim=-1))


        discriminator_loss.backward()
        optimizer_dict['discriminator'].step()

        #### GENERATOR LOSS
        model_dict['generator_1'].train(mode=True)
        model_dict['generator_2'].train(mode=True)


        z_sample_1 = model_dict['autoencoder_1'].encode(data_1)
        z_sample_2 = model_dict['autoencoder_2'].encode(data_2)

        d_sample_1 = model_dict['discriminator'](z_sample_1)
        d_sample_2 = model_dict['discriminator'](z_sample_2)

        generator_loss = -torch.mean(torch.mean(label_real * torch.log(d_sample_1 + EPS), dim=-1) + torch.mean(label_real * torch.log(d_sample_2 + EPS), dim=-1))

        generator_loss.backward()
        optimizer_dict['generator_1'].step()
        optimizer_dict['generator_2'].step()


        if batch_idx % kwargs['log_interval'] == 0:  # Logging
            log_dict = {'reconstruction_loss': reconstruction_loss.item(),
                        'discriminator_loss': discriminator_loss.item(),
                        'generator_loss': generator_loss.item(),
                        'loss': reconstruction_loss.item() + discriminator_loss.item() + generator_loss.item()}

            logger.on_step_fi(log_dict)

            to_print = 'Train Epoch: ' + str(epoch) + '(' + str(batch_idx * len(data_1)) + '/' + str(
                len(generator_train.dataset)) + ') ' + ','.join(
                [k + ': ' + str(round(v, 3)) for k, v in log_dict.items()])
            print(to_print)

    return logger

def test(model_dict, device, generator_test, epoch, latent_space_dims, logger):
    for model_name, model in model_dict.items():
        model.eval()

    with torch.no_grad():
        for batch_idx, (data, _) in enumerate(generator_test):
            data = data.to(device)

            # Autoencoder
            recon_batch, latent_batch = model_dict['autoencoder'](data)
            AAE_loss = F.binary_cross_entropy(recon_batch + EPS, data + EPS)

            # Discriminator
            latent_gauss_batch = Variable(torch.randn(data.size()[0], latent_space_dims) * 5.)
            D_real = model_dict['discriminator'](latent_batch)
            D_gauss = model_dict['discriminator'](latent_gauss_batch)
            D_loss = -torch.mean(torch.log(D_gauss + EPS) + torch.log(1 - D_real + EPS))

            # Generator
            G_loss = -torch.mean(torch.log(D_real + EPS))

            log_dict = {'reconstruction_loss': AAE_loss.item(),
                        'discriminator_loss': D_loss.item(),
                        'generator_loss': G_loss.item(),
                        'loss': AAE_loss.item() + D_loss.item() + G_loss.item()}

            logger.on_step_fi(log_dict)

            # if batch_idx == 0 and save_file_flag:
            #     n = min(data.size(0), 8)
            #     comparison = torch.cat([data.reshape(-1, 1, 28,28)[:n], recon_batch.view(batch_size, 1, 28, 28)[:n]])
            #     save_image(comparison.cpu(),join(MODALITY_PATH, 'reconstruction_' + str(epoch) + '.png'), nrow=n)

    to_print = 'Train Epoch: ' + str(epoch) + ','.join([k + ': ' + str(np.mean(v)) for k, v in log_dict.items()])
    print(to_print)
    return logger

def predict(model, generator_test, prediction_size):

    num_elements = len(generator_test.dataset)
    num_batches = len(generator_test)
    batch_size = generator_test.batch_size
    predictions = torch.zeros(prediction_size)
    labels = torch.zeros(num_elements,1)

    with torch.no_grad():
        for i, (data, target) in enumerate(generator_test):
            pred = model(data)
            print(pred.size())
            start = i * batch_size
            end = start + batch_size
            if i == num_batches - 1:
                end = num_elements
            predictions[start:end] = pred.cpu()
            labels[start:end] = target.cpu()

    return predictions.detach().numpy(), labels.detach().numpy()


################################
###########   MAIN   ###########
################################
if __name__ == '__main__':

    ######################################
    ##########   READ DATA  ##############
    ######################################
    DataLoader = data_reader.DataLoader()
    av45_dict = DataLoader.load_modality_baseline(modality_name='av45_pet')
    vMRI_dict = DataLoader.load_modality_baseline(modality_name='volumetric_mri')
    cMRI_dict = DataLoader.load_modality_baseline(modality_name='cortical_thickness_mri')
    FDG_dict = DataLoader.load_modality_baseline(modality_name='fdg_pet')
    csf_dict = DataLoader.load_modality_baseline(modality_name='csf')
    imaging_phenotypes_dict = DataLoader.load_modality_baseline(modality_name='imaging_phenotypes')

    demographic_dict = DataLoader.load_demographic_baseline(demo_names=demo_names)
    age_dict = DataLoader.load_age()
    clinical_dict = DataLoader.load_clinical_baseline(clinical_names=clinical_names)
    dx_dict = DataLoader.load_dx_baseline()

    av45_id_list = DataLoader.load_modality_codenames(modality_name='av45_pet')
    vMRI_id_list = DataLoader.load_modality_codenames(modality_name='volumetric_mri')
    cMRI_id_list = DataLoader.load_modality_codenames(modality_name='cortical_thickness_mri')
    FDG_id_list = DataLoader.load_modality_codenames(modality_name='fdg_pet')
    csf_id_list = DataLoader.load_modality_codenames(modality_name='csf')
    imaging_phenotypes_id_list = DataLoader.load_modality_codenames(modality_name='imaging_phenotypes')
    clinical_id_list = ['age'] + clinical_names + ['education']

    modality_list = ['volumetric_mri', 'csf']
    modality_dict_list = [vMRI_dict, csf_dict]
    modality_id_list = [vMRI_id_list, csf_id_list]

    MODALITY_PATH = join(OUTPUT_PATH, '_'.join(modality_list))
    if not exists(MODALITY_PATH):
        os.makedirs(MODALITY_PATH)

    writer_legend.write('############################\n')
    writer_legend.write('########## LEGEND ##########\n')
    writer_legend.write('############################\n\n')
    for it_l in range(len(modality_id_list)):
        writer_legend.write('----------  ' + modality_list[it_l] + ' --------\n')
        for it_mod_id, mod_id in enumerate(modality_id_list[it_l]):
            writer_legend.write(str(it_mod_id) + '. ' + mod_id + '\n')
        writer_legend.write('-----------------------------\n')

    rid_list = data_reader.find_list_intersection([list(mdict.keys()) for mdict in modality_dict_list] +
                                                  [list(demographic_dict.keys()),
                                                   list(age_dict.keys()),
                                                   list(csf_dict.keys()),
                                                   list(dx_dict.keys()),
                                                   ])

    csf_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, csf_dict.items())).values()))
    if len(csf_array.shape) < 2:
        csf_array = csf_array.reshape(-1, 1)

    demographic_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, demographic_dict.items())).values()))
    if len(demographic_array.shape) < 2:
        demographic_array = demographic_array.reshape(-1, 1)

    age_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, age_dict.items())).values()))
    if len(age_array.shape) < 2:
        age_array = age_array.reshape(-1, 1)

    m1_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[0].items())).values()))
    if len(m1_array.shape) < 2:
        m1_array = m1_array.reshape(-1, 1)

    m2_array = np.asarray(list(dict(filter(lambda x: x[0] in rid_list, modality_dict_list[1].items())).values()))
    if len(m2_array.shape) < 2:
        m2_array = m2_array.reshape(-1, 1)

    factor_array = np.concatenate((age_array, demographic_array), axis=1)
    factor_id_list = ['age'] + demo_names

    dx_array =  np.asarray(list(dict(filter(lambda x: x[0] in rid_list, dx_dict.items())).values())).reshape(-1,1)
    index_0 = np.where(dx_array == 0)[0]
    index_1 = np.where(dx_array == 1)[0]
    index_2 = np.where(dx_array == 2)[0]

    ## PRE-PROCESSING
    std_scaler = StandardScaler()
    m1_array = std_scaler.fit_transform(m1_array)
    m2_array = std_scaler.fit_transform(m2_array)

    csf_array = std_scaler.fit_transform(csf_array)
    demographic_array = std_scaler.fit_transform(demographic_array)
    age_array = std_scaler.fit_transform(age_array)

    N1, M1 = m1_array.shape
    N2, M2 = m2_array.shape
    M = [M1, M2]

    print('N1: ' + str(N1))
    print('M1: ' + str(M1))
    print('M2: ' + str(M2))


    #######################################
    ##########  BUILD THE MODEL  ##########
    #######################################
    config_file = ConfigFile('/imatge/acasamitjana/Repositories/Latent_models/config_files/AAE_twoview.yaml')

    use_GPU = config_file.use_GPU
    batch_size = config_file.batch_size
    use_cuda = use_GPU and torch.cuda.is_available()
    learning_rate = config_file.learning_rate
    momentum = config_file.momentum
    n_epochs = config_file.n_epochs

    hidden_layers_dim_1 = [N_COMPONENTS]
    hidden_layers_dim_2 = [N_COMPONENTS]

    hidden_layers_discriminator_dim = [10, len(modality_list) + 1]
    latent_dim = N_COMPONENTS

    Generator_model_1 = Encoder_lineal(M1, hidden_layers_dim_1)
    Decoder_model_1 = Decoder_lineal(M1, hidden_layers_dim_1)
    AAE_model_1 = Autoencoder(Generator_model_1, Decoder_model_1)

    Generator_model_2 = Encoder_lineal(M2, hidden_layers_dim_2)
    Decoder_model_2 = Decoder_lineal(M2, hidden_layers_dim_2)
    AAE_model_2 = Autoencoder(Generator_model_2, Decoder_model_2)

    Discriminator_model = Discriminator(latent_dim, hidden_layers_discriminator_dim)

    # Set learning rates
    ae_lr = learning_rate['autoencoder']
    gen_lr = learning_rate['generator']
    reg_lr = learning_rate['discriminator']

    kwargs_training = {'log_interval': config_file.log_interval}  # Nmber of steps
    kwargs_testing = {}
    kwargs_generator = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda:0" if use_cuda else "cpu")

    # encode/decode optimizers
    optim_AAE_1 = torch.optim.Adam(AAE_model_1.parameters(), lr=ae_lr)
    optim_G_1 = torch.optim.Adam(Generator_model_1.parameters(), lr=gen_lr)
    optim_AAE_2 = torch.optim.Adam(AAE_model_2.parameters(), lr=ae_lr)
    optim_G_2 = torch.optim.Adam(Generator_model_2.parameters(), lr=gen_lr)
    optim_D = torch.optim.Adam(Discriminator_model.parameters(), lr=reg_lr)

    model_dict = {'autoencoder_1': AAE_model_1, 'autoencoder_2': AAE_model_2, 'discriminator': Discriminator_model,
                  'generator_1': Generator_model_1, 'generator_2': Generator_model_2}
    optimizer_dict = {'autoencoder_1': optim_AAE_1, 'autoencoder_2': optim_AAE_1,'discriminator': optim_D,
                      'generator_1': optim_G_1, 'generator_2': optim_G_2}

    #################################
    ##########  READ DATA  ##########
    #################################

    dataset = MyDataset(m1_array, m2_array)
    generator_train = torch.utils.data.DataLoader(
        dataset,
        batch_size=N1,
        shuffle=True,
        num_workers=2,
        pin_memory=torch.cuda.is_available()

    )

    dataset_1 = MyDataset(m1_array, dx_array)
    dataset_2 = MyDataset(m2_array, dx_array)

    generator_test_1 = torch.utils.data.DataLoader(
        dataset_1,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    generator_test_2 = torch.utils.data.DataLoader(
        dataset_2,
        batch_size=N1,
        shuffle=False,
        num_workers=1,
        pin_memory=torch.cuda.is_available()

    )

    ################################
    ##########  TRAINING  ##########
    ################################

    log_keys = ['reconstruction_loss', 'discriminator_loss', 'generator_loss', 'loss']
    logger = History()
    logger.on_train_init(log_keys)
    for epoch in range(1, n_epochs + 1):
        logger = train(model_dict, optimizer_dict, device, generator_train, epoch, N_COMPONENTS, logger, **kwargs_training)

    writer = ResultsWriter(filepath=join(MODALITY_PATH, 'results_file.txt'), attach=False)

    plt.figure()
    plt.plot(logger.logs['reconstruction_loss'], 'b', label='Reconstruction')
    plt.plot(logger.logs['discriminator_loss'], 'r', label='Discriminator')
    plt.plot(logger.logs['generator_loss'], 'g', label='Generator')
    plt.plot(logger.logs['loss'], 'k', label='Total loss')

    plt.legend()
    plt.savefig(join(MODALITY_PATH,'training.png'))
    plt.close()

    ###############################
    ##########  RESULTS  ##########
    ###############################
    STATISTICAL_PLOTS = True
    WEIGHT_PLOTS = True
    FACTOR_PLOTS = True
    CORRELATION_PLOTS = True

    y_scores = [predict(model_dict['autoencoder_1'].encoder, generator_test_1, (N1,latent_dim))[0],
                predict(model_dict['autoencoder_2'].encoder, generator_test_2, (N1,latent_dim))[0]]

    for it_component in range(N_COMPONENTS):
        plt.figure()
        plt.hist(y_scores[0][:,it_component], bins= 40)
        plt.savefig(join(MODALITY_PATH, modality_list[0] + '_1_histogram_nc_' + str(it_component) + '.png'))
        plt.close()

        plt.figure()
        plt.hist(y_scores[1][:, it_component], bins=40)
        plt.savefig(join(MODALITY_PATH, modality_list[1] + '_histogram_nc_' + str(it_component) + '.png'))
        plt.close()


    if STATISTICAL_PLOTS:
        writer.write('#############################\n')
        writer.write('RESULTS DIAGNOSTIC COMPARISON\n')
        writer.write('#############################\n\n')

        combination_plots = list(itertools.combinations(range(3), 2))
        for it_modality, modality in enumerate(modality_list):
            writer.write('--- ' + modality + ' subspace ---\n')

            for it_component in range(N_COMPONENTS):
                writer.write('     · Component ' + str(it_component) + '\n')
                t = []
                p = []
                boxplot_list = [y_scores[it_modality][index_0, it_component],
                                y_scores[it_modality][index_1, it_component],
                                y_scores[it_modality][index_2, it_component]]

                for c in combination_plots:
                    result = ttest_ind(boxplot_list[c[0]], boxplot_list[c[1]])
                    t.append(result[0])
                    p.append(result[1])
                    writer.write('           - ' + str((label_diagnostic[c[0]], label_diagnostic[c[1]])) + ': ' + str(
                        (result[0], result[1])) + '\n')

                filename = modality + '_diagnostic' + str(it_component) + '.png'
                plt.figure()
                plt.boxplot(boxplot_list)
                plt.xticks([1, 2, 3], label_diagnostic)
                plt.title('Significant (p<0.05) differences: ' + str(
                    [c for it_c, c in enumerate(combination_plots) if p[it_c] < 0.05]))
                plt.savefig(join(MODALITY_PATH, filename))
                plt.close()


    if WEIGHT_PLOTS:
        writer.write('####################\n')
        writer.write('PLS Y_WEIGHT RESULTS\n')
        writer.write('####################\n\n')

        y_weight_generator = np.eye(N_COMPONENTS)
        y_weight_generator_labels = np.arange(0,N_COMPONENTS).reshape(-1,1)
        dataset_y_weight = MyDataset(y_weight_generator, y_weight_generator_labels)
        generator_y_weight = torch.utils.data.DataLoader(
            dataset_y_weight,
            batch_size=N_COMPONENTS,
            shuffle=True,
            num_workers=1,
            pin_memory=torch.cuda.is_available()

        )

        y_rotations = [predict(model_dict['autoencoder_1'].decoder, generator_y_weight, (N_COMPONENTS, M1))[0],
                       predict(model_dict['autoencoder_2'].decoder, generator_y_weight, (N_COMPONENTS, M2))[0]]

        for it_modality, modality in enumerate(modality_list):
            for it_component in range(N_COMPONENTS):
                sorted_mod = np.argsort(np.abs(y_rotations[it_modality][it_component,:]))[::-1]
                writer.write('    · Component ' + str(it_component) + '\n')
                for m_prs in sorted_mod:
                    writer.write('         ' + modality_id_list[it_modality][m_prs] + ': ' + str(y_rotations[it_modality][it_component,m_prs]) + '\n')
            writer.write('\n')

            y_min = np.min(y_rotations[it_modality]) * 0.9
            y_max = np.max(y_rotations[it_modality]) * 1.1
            y_lim = np.max([np.abs(y_min), np.abs(y_max)])

            for it_component in range(N_COMPONENTS):
                filename = modality + '_weights_' + str(it_component) + '.png'
                x_min = np.min(y_rotations[it_modality][it_component,:])
                x_max = np.max(y_rotations[it_modality][it_component,:])
                _, ax = plt.subplots()
                ax.barh(np.arange(M[it_modality]), y_rotations[it_modality][it_component, :], align='center', color='green', ecolor='black')
                ax.set_yticks(np.arange(start=0, stop=M[it_modality], step=np.max([1, int(M[it_modality] / 10)])))
                ax.set_xticks(np.concatenate((np.arange(start=-y_lim, stop=0, step=y_lim / 3),
                                              np.arange(start=0, stop=y_lim, step=y_lim / 3), [y_lim])))
                ax.invert_yaxis()
                ax.set_xlabel('Latent variable ' + str(it_component))
                ax.set_title('Relative weight of each input variable')
                ax.grid()
                ax.set_xlim([-y_lim, y_lim])
                plt.savefig(join(MODALITY_PATH, filename))
                plt.close()

    if FACTOR_PLOTS:
        writer.write('###########################\n')
        writer.write('FACTOR CORRELATION ANALYSIS\n')
        writer.write('###########################\n')
        n_rows = ROW_DICT[N_COMPONENTS]
        n_cols = int(np.ceil(N_COMPONENTS / n_rows))
        for it_modality, modality in enumerate(modality_list):
            writer.write('--- ' + modality + ' subspace ---\n')
            for it_factor, factor in enumerate(factor_id_list):
                fig_y, axarr_y = plt.subplots(n_rows, n_cols)
                writer.write('\nFACTOR: ' + factor + '\n')

                for it_c in range(N_COMPONENTS):
                    writer.write('    · Component ' + str(it_c + factor_array.shape[1]) + ':')
                    writer.write(' Y:' + str(pearsonr(y_scores[it_modality][:, it_c], factor_array[:, it_factor])) + '\n')

                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[it_modality][index_0, it_c],
                                                                              factor_array[index_0, it_factor], '*b',
                                                                              label='NC')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[it_modality][index_1, it_c],
                                                                              factor_array[index_1, it_factor], '*r',
                                                                              label='MCI')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].plot(y_scores[it_modality][index_2, it_c],
                                                                              factor_array[index_2, it_factor], '*k',
                                                                              label='AD')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].grid()
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_xlabel('Y scores')
                    axarr_y[it_c % n_rows, int(np.floor(it_c / n_rows))].set_ylabel(factor)

                plt.figure(1)
                plt.savefig(join(MODALITY_PATH, modality + '_score_' + factor + '_correlation.png'))
                plt.close()


    if CORRELATION_PLOTS:
        lr_model = LinearRegression()

        for it_c in range(N_COMPONENTS):

            if len(y_scores) <= 2:
                fig, axarr = plt.subplots(1, 1)
                axarr.plot(y_scores[0][index_0, it_c], y_scores[1][index_0, it_c], 'b*', label='NC')
                axarr.plot(y_scores[0][index_1, it_c], y_scores[1][index_1, it_c], 'r*', label='MCI')
                axarr.plot(y_scores[0][index_2, it_c], y_scores[1][index_2, it_c], 'g*', label='AD')

                lr_model.fit(y_scores[0][:, it_c: it_c + 1], y_scores[1][:, it_c:it_c + 1])
                axarr.plot(y_scores[0][:, it_c:it_c + 1], lr_model.predict(y_scores[0][:, it_c:it_c + 1]), 'k-')
                axarr.set_xlabel(modality_list[0])
                axarr.set_ylabel(modality_list[1])


            else:
                fig, axarr = plt.subplots(len(y_scores)-1, len(y_scores)-1)

                for it_row in range(len(y_scores)-1):
                    for it_col in range(it_row+1,len(y_scores)):

                        axarr[it_row,it_col-1].plot(y_scores[it_col][index_0,it_c], y_scores[it_row][index_0,it_c], 'b*', label='NC')
                        axarr[it_row,it_col-1].plot(y_scores[it_col][index_1,it_c], y_scores[it_row][index_1,it_c], 'r*', label='MCI')
                        axarr[it_row,it_col-1].plot(y_scores[it_col][index_2,it_c], y_scores[it_row][index_2,it_c], 'g*', label='AD')

                        lr_model.fit(y_scores[it_col][:,it_c: it_c+1], y_scores[it_row][:, it_c:it_c+1])
                        axarr[it_row,it_col-1].plot(y_scores[it_col][:,it_c:it_c+1], lr_model.predict(y_scores[it_col][:,it_c:it_c+1]), 'k-')

                        if it_col - 1 == 0:
                            for i in range(len(y_scores) - 1):
                                axarr[i,it_col-1].set_ylabel(modality_list[i])

                    if it_row == len(y_scores)-2:
                        for i in range(len(y_scores) - 1):
                            axarr[it_row,i].set_ylabel(modality_list[i+1])

            plt.legend()
            plt.savefig(join(MODALITY_PATH,'scatter_plot' + str(it_c) + '.png'))
            plt.close()